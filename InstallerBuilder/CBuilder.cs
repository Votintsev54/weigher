﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Xml;
// ReSharper disable AssignNullToNotNullAttribute
// ReSharper disable PossibleNullReferenceException

namespace InstallerBuilder
{
    public class CBuilder
    {
        //public string solutionPath = "";
        public string InstallerDirPath = @"c:\MyProjects\Weigher\Installer";
        public string InstallerProjectName = @"Installer.csproj";

        public string CustomInstallerDirPath;

        //public string ProjectPath = @"c:\Projects\Camera\SyncViewer\SyncViewer.csproj";
        public string ProjectPath = @"c:\MyProjects\Weigher\Weigher\Weigher.csproj";
        public string ConfigurationName = "Debug";
        public string ProjectOutputPath;

        public CBuilder()
        {
        }

        public CBuilder(string projectPath,string configurationName)
        {
            ProjectPath = projectPath;
            ConfigurationName = configurationName;
        }

        public void FindOutputPath()
        {
            //XmlDocument docProj = new XmlDocument();
            //docProj.Load(ProjectPath);
            //docProj.PreserveWhitespace = true;
            //string path = "a:PropertyGroup[contains(@Condition,\"" + ConfigurationName + "\")]";
            ////string path = "a:PropertyGroup[@Condition]";
            //XmlNamespaceManager xnm = new XmlNamespaceManager(docProj.NameTable);
            //xnm.AddNamespace("a",docProj.DocumentElement.NamespaceURI);
            //XmlNodeList pgList = docProj.DocumentElement.SelectNodes(path, xnm);
            var outputPath = "bin\\" + ConfigurationName + "\\";
            var projectPath = Path.GetDirectoryName(ProjectPath);
            ProjectOutputPath = Path.Combine(projectPath, outputPath);
        }

        public string[] GetFiles()
        {
            List<string> ret = new List<string>();
            foreach (var filePath in Directory.EnumerateFiles(ProjectOutputPath,"*",SearchOption.AllDirectories))
            {
                var fileName = Path.GetFileName(filePath);
                // ReSharper disable once PossibleNullReferenceException
                if (fileName.Contains(".vshost.")) continue;
                if (fileName.EndsWith(".manifest")) continue;
                if (fileName.EndsWith(".config")) continue;
                if (fileName.EndsWith(".log")) continue;
                ret.Add(filePath);
            }
            return ret.ToArray();
        }

        void CopyDir(string fromDir, string toDir)
        {
            Directory.CreateDirectory(toDir);
            foreach (string s1 in Directory.GetFiles(fromDir))
            {
                string s2 = toDir + "\\" + Path.GetFileName(s1);
                File.Copy(s1, s2);
            }
            foreach (string s in Directory.GetDirectories(fromDir))
            {
                if (Path.GetFileName(s) == "bin") continue;
                if (Path.GetFileName(s) == "obj") continue;
                CopyDir(s, toDir + "\\" + Path.GetFileName(s));
            }
        }

        public static void Compress(string uncompressedFilePath,string compresseFilePath)
        {
            FileInfo file = new FileInfo(uncompressedFilePath);
            using (FileStream originalFileStream = file.OpenRead())
            {
                using (FileStream compressedFileStream = File.Create(compresseFilePath))
                {
                    using (DeflateStream compressionStream = new DeflateStream(compressedFileStream,CompressionMode.Compress))
                    {
                        originalFileStream.CopyTo(compressionStream);
                    }
                }
            }
        }


        public void CreateInstallerProject()
        {
            // ProjectOutputPath
            FindOutputPath();

            //CustomInstallerPath = Path.Combine(ProjectOutputPath, "InstallerProject");
            //CopyDir(InstallerDirPath, CustomInstallerPath);
            CustomInstallerDirPath = InstallerDirPath; // модифицируем на месте

            XmlDocument docProj = new XmlDocument();
            string customInstallerProjectPath = Path.Combine(CustomInstallerDirPath, InstallerProjectName);
            docProj.Load(customInstallerProjectPath);
            docProj.PreserveWhitespace = true;
            string uri = docProj.DocumentElement.NamespaceURI;
            XmlElement elGroup = docProj.CreateElement("ItemGroup",uri);
            CleanFiles(docProj.DocumentElement);

            string outFileDirPath = Path.Combine(CustomInstallerDirPath, "Files");
            CleanDirectory(outFileDirPath);

            var files = GetFiles();
            foreach (var filePath in files)
            {
                string fileDir = Path.GetDirectoryName(filePath)+"\\";
                if (fileDir != ProjectOutputPath)
                {
                    string subDirectory = fileDir.Replace(ProjectOutputPath, "");
                    outFileDirPath = Path.Combine(CustomInstallerDirPath, subDirectory);
                    Directory.CreateDirectory(outFileDirPath);
                }
                string compressionFilePath = Path.Combine(outFileDirPath, Path.GetFileName(filePath)) + ".cmp";
                Compress(filePath, compressionFilePath);
                XmlElement elEmbeddedResource = docProj.CreateElement("EmbeddedResource", uri);
                //XmlElement elLink = docProj.CreateElement("Link", uri);
                //elLink.InnerText = Path.GetFileName(compressionFilePath);
                //elEmbeddedResource.AppendChild(elLink);
                elEmbeddedResource.SetAttribute("Include", compressionFilePath);
                elGroup.AppendChild(elEmbeddedResource);
                
            }
            docProj.DocumentElement.AppendChild(elGroup);
            //docProj.Save(customInstallerProjectPath);
            XmlWriterSettings xws = new XmlWriterSettings();
            xws.Indent = true;
            xws.IndentChars = " ";
            xws.NewLineChars = "\r\n";
            xws.NewLineHandling = NewLineHandling.Replace;
            XmlWriter xw = XmlWriter.Create(customInstallerProjectPath, xws);
            docProj.Save(xw);
            xw.Close();

            BuildProject(ConfigurationName, customInstallerProjectPath);

        }

        private void CleanFiles(XmlElement docElement)
        {
            List<XmlElement> listrItemGroup = new List<XmlElement>();
            foreach (XmlNode igNode in docElement.ChildNodes)
            {
                if (!(igNode is XmlElement))
                    continue;
                XmlElement ig = (XmlElement)igNode;
                if (ig.Name != "ItemGroup")
                    continue;
                List<XmlElement> listEbedded = new List<XmlElement>();
                foreach (XmlElement embedded in ig.ChildNodes)
                {
                    if (embedded.Name != "EmbeddedResource")
                        continue;
                    string path = embedded.GetAttribute("Include");
                    if (path != null && (path.StartsWith(@"Files\") || path.Contains(@"\Files\")))
                        listEbedded.Add(embedded);
                }
                foreach (var embedded in listEbedded)
                    ig.RemoveChild(embedded);
                if (!ig.HasChildNodes && !ig.HasAttributes)
                    listrItemGroup.Add(ig);
            }
            foreach (var ig in listrItemGroup)
                docElement.RemoveChild(ig);
        }

        private void CleanDirectory(string outFileDirPath)
        {
            if (!Directory.Exists(outFileDirPath))
                Directory.CreateDirectory(outFileDirPath);
            foreach (string file in Directory.EnumerateFiles(outFileDirPath))
            {
                File.Delete(file);
            }
        }

        public void BuildProject()
        {
            BuildProject( ConfigurationName,  ProjectPath);
        }

        public void BuildProject(string configurationName, string projectPath)
        {
            // var MSBUILD = @"c:\Program Files (x86)\MSBuild\12.0\Bin\MSBuild.exe";
            var MSBUILD = @"c:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe" ;
            if (File.Exists(MSBUILD))
            {
                var process = new Process
                {
                    StartInfo =
                {
                    FileName = MSBUILD,
                    Arguments = @"/p:Configuration=" + configurationName + " /m \"" + projectPath + "\"",
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    //WorkingDirectory = CustomInstallerDirPath
                }
                };

                process.Start();
                process.WaitForExit();
            }

        }
    }
}