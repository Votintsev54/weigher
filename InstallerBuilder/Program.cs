﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace InstallerBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 2)
            {
                var b = new CBuilder(args[0],args[1]);
                b.CreateInstallerProject();
            }
            else
            {
                var b = new CBuilder();
                b.BuildProject();
                b.CreateInstallerProject();
            }
        }
    }
}
