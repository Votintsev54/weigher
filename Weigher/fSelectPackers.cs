﻿using System;
using System.Windows.Forms;
using DbCommon;
using WeigherData;

namespace Weigher
{
    public partial class fSelectPackers: Form
    {
        public fSelectPackers(CDatabaseWeigher db)
        {
            if (db == null)
                MessageBox.Show("db==null");
            this.db = db;
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            fSettings f = new fSettings();
            f.ShowDialog();
        }

        readonly CDatabaseWeigher db ;
        SortList<Packer> plist;

        private void fOrders_Load(object sender, EventArgs e)
        {
                plist = db.Packer.GetAll();
                dataGridView1.DataSource = plist;
        }

        public Packer packer = null;

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                packer = (Packer)dataGridView1.SelectedRows[0].DataBoundItem;
                Close();
            }

        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dataGridView1.SelectedRows.Count == 1)
                {
                    packer = (Packer)dataGridView1.SelectedRows[0].DataBoundItem;
                    Close();
                }
            }
        }

  

    }
}
