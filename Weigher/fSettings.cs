﻿using System;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Linq;
using System.Collections.Generic;
using WeigherDatabase;
using QRCoder;

namespace Weigher
{
    public partial class fSettings : Form
    {
        public fSettings()
        {
            InitializeComponent();
        }

        RadioButton[] checkCount;
        Dictionary<QrSettings.QrStates, RadioButton> dictQrStates;

        private void fSettings_Load(object sender, EventArgs e)
        {
            var ws = WeigherSettings.Current;
            PrinterSettings.StringCollection sc = PrinterSettings.InstalledPrinters;
            for (int i = 0; i < sc.Count; i++)
            {
                cbSysPrinter.Items.Add(sc[i]);
                cbLabelPrinter.Items.Add(sc[i]);
                cbLabelPrinter2.Items.Add(sc[i]);
            }
            tbPath.Text = ws.OtherSettings.ExlPath;
            cbIncludeDir.Checked = ws.OtherSettings.ExlPathWsf;
            cbSysPrinter.Text = ws.PrintersSettings.PrinterA4;
            cbLabelPrinter.Text = ws.PrintersSettings.PrinterLabels;
            cbLabelPrinter2.Text = ws.PrintersSettings.PrinterLabels2;
            tbMinNom.Text = ws.OtherSettings.ExlMinNom.ToString();
            tbPackListPrintCount.Text = ws.PrintersSettings.PackListPrintCount.ToString();
            tbPackCheckPrintCount.Text = ws.PrintersSettings.PalletLabelPrintCount.ToString();

            checkCount = new RadioButton[] { radioCheck0, radioCheck1, radioCheck2 };
            checkCount[ws.PrintersSettings.CheckPrintCount].Checked = true;
            cbRollCountButtons.Checked = ws.OtherSettings.ShowRollCountButtons;

            //cbQRLevel.Text = cbQRLevel.Items.Cast<string>().First(item => item.StartsWith(ws.QrSettings.ErrorLevel));
            cbQRLevel.Text = cbQRLevel.Items.Cast<string>().First(item => item.StartsWith(ws.QrSettings.ECCLevel.ToString()));
            numQRBlockSize.Value = ws.QrSettings.BlockSize;
            tbDelimeter.Text = ws.QrSettings.Delimeter;
            tbTerminator.Text = ws.QrSettings.Terminator;
            dictQrStates = new Dictionary<QrSettings.QrStates, RadioButton>();
            dictQrStates[QrSettings.QrStates.NotPrint] = radioQrNotPrint;
            dictQrStates[QrSettings.QrStates.PrintWithDelimeter] = radioQrWithDelimeter;
            dictQrStates[QrSettings.QrStates.PrintJson] = radioQrJson;
            dictQrStates[ws.QrSettings.QrState].Checked = true;
            cbReadOnly.Checked = ws.OtherSettings.ReadOnlyMode;
            tbPassword.Text = ws.OtherSettings.SettingPwd;

            if (!fAskPwd.AskPwd()) Close();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            var ws = WeigherSettings.Current;
            ws.OtherSettings.ExlPath = tbPath.Text;
            ws.OtherSettings.ExlPathWsf = cbIncludeDir.Checked;
            ws.PrintersSettings.PrinterA4 = cbSysPrinter.Text;
            ws.PrintersSettings.PrinterLabels = cbLabelPrinter.Text;
            ws.PrintersSettings.PrinterLabels2 = cbLabelPrinter2.Text;
            if (int.TryParse(tbMinNom.Text, out int ivalue))
                ws.OtherSettings.ExlMinNom = ivalue;
            ws.PrintersSettings.CheckPrintCount = checkCount.TakeWhile(rb => !rb.Checked).Count();

            if (int.TryParse(tbPackListPrintCount.Text, out int ipc))
                ws.PrintersSettings.PackListPrintCount = ipc;
            if (int.TryParse(tbPackCheckPrintCount.Text, out int ic))
                ws.PrintersSettings.PalletLabelPrintCount = ic;

            ws.OtherSettings.ShowRollCountButtons = cbRollCountButtons.Checked;

            ws.QrSettings.ECCLevel = (QRCodeGenerator.ECCLevel)Enum.Parse(typeof(QRCodeGenerator.ECCLevel), cbQRLevel.Text.Substring(0, 1));
            ws.QrSettings.BlockSize = (int)numQRBlockSize.Value;
            ws.QrSettings.Delimeter = tbDelimeter.Text;
            ws.QrSettings.Terminator = tbTerminator.Text;
            ws.QrSettings.QrState = dictQrStates.First(kp => kp.Value.Checked).Key;
            ws.OtherSettings.ReadOnlyMode = cbReadOnly.Checked;
            ws.OtherSettings.SettingPwd = tbPassword.Text;

            ws.Save();

        }

        private void btnFindDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.Description = "Выберите папку с заказами...";
            fd.SelectedPath = tbPath.Text;
            fd.ShowNewFolderButton = false;
            if (fd.ShowDialog() == DialogResult.OK)
            {
                tbPath.Text = fd.SelectedPath;
            }
        }


    }
}
