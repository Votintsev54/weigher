﻿using WeigherData;

namespace Weigher
{
    partial class fOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fOrders));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.принтерыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.соединениеСБДToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.упаковщикиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выбратьУпаковщикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.соответствияСтолбцовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.печатныеОтчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справочникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.связьСВесамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.numberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateStartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateFinishDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioStudy2 = new System.Windows.Forms.RadioButton();
            this.radioStudy1 = new System.Windows.Forms.RadioButton();
            this.radioStudy0 = new System.Windows.Forms.RadioButton();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton1});
            this.toolStrip1.Location = new System.Drawing.Point(880, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(61, 39);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "Настройки";
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.принтерыToolStripMenuItem,
            this.соединениеСБДToolStripMenuItem,
            this.упаковщикиToolStripMenuItem,
            this.выбратьУпаковщикаToolStripMenuItem,
            this.соответствияСтолбцовToolStripMenuItem,
            this.печатныеОтчетыToolStripMenuItem,
            this.справочникиToolStripMenuItem,
            this.связьСВесамиToolStripMenuItem});
            this.toolStripSplitButton1.Image = global::Weigher.Properties.Resources.advancedsettings_2825;
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(48, 36);
            this.toolStripSplitButton1.ToolTipText = "Настройки";
            // 
            // принтерыToolStripMenuItem
            // 
            this.принтерыToolStripMenuItem.Name = "принтерыToolStripMenuItem";
            this.принтерыToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.принтерыToolStripMenuItem.Text = "Принтеры";
            this.принтерыToolStripMenuItem.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // соединениеСБДToolStripMenuItem
            // 
            this.соединениеСБДToolStripMenuItem.Name = "соединениеСБДToolStripMenuItem";
            this.соединениеСБДToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.соединениеСБДToolStripMenuItem.Text = "Соединение с БД";
            this.соединениеСБДToolStripMenuItem.Click += new System.EventHandler(this.соединениеСБДToolStripMenuItem_Click);
            // 
            // упаковщикиToolStripMenuItem
            // 
            this.упаковщикиToolStripMenuItem.Name = "упаковщикиToolStripMenuItem";
            this.упаковщикиToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.упаковщикиToolStripMenuItem.Text = "Упаковщики";
            this.упаковщикиToolStripMenuItem.Click += new System.EventHandler(this.упаковщикиToolStripMenuItem_Click);
            // 
            // выбратьУпаковщикаToolStripMenuItem
            // 
            this.выбратьУпаковщикаToolStripMenuItem.Name = "выбратьУпаковщикаToolStripMenuItem";
            this.выбратьУпаковщикаToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.выбратьУпаковщикаToolStripMenuItem.Text = "Выбрать упаковщика";
            this.выбратьУпаковщикаToolStripMenuItem.Click += new System.EventHandler(this.выбратьУпаковщикаToolStripMenuItem_Click);
            // 
            // соответствияСтолбцовToolStripMenuItem
            // 
            this.соответствияСтолбцовToolStripMenuItem.Name = "соответствияСтолбцовToolStripMenuItem";
            this.соответствияСтолбцовToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.соответствияСтолбцовToolStripMenuItem.Text = "Соответствия столбцов";
            this.соответствияСтолбцовToolStripMenuItem.Click += new System.EventHandler(this.соответствияСтолбцовToolStripMenuItem_Click);
            // 
            // печатныеОтчетыToolStripMenuItem
            // 
            this.печатныеОтчетыToolStripMenuItem.Name = "печатныеОтчетыToolStripMenuItem";
            this.печатныеОтчетыToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.печатныеОтчетыToolStripMenuItem.Text = "Печатные отчеты";
            this.печатныеОтчетыToolStripMenuItem.Click += new System.EventHandler(this.ПечатныеОтчетыToolStripMenuItem_Click);
            // 
            // справочникиToolStripMenuItem
            // 
            this.справочникиToolStripMenuItem.Name = "справочникиToolStripMenuItem";
            this.справочникиToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.справочникиToolStripMenuItem.Text = "Справочники";
            this.справочникиToolStripMenuItem.Click += new System.EventHandler(this.справочникиToolStripMenuItem_Click);
            // 
            // связьСВесамиToolStripMenuItem
            // 
            this.связьСВесамиToolStripMenuItem.Name = "связьСВесамиToolStripMenuItem";
            this.связьСВесамиToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.связьСВесамиToolStripMenuItem.Text = "Связь с весами";
            this.связьСВесамиToolStripMenuItem.Click += new System.EventHandler(this.связьСВесамиToolStripMenuItem_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numberDataGridViewTextBoxColumn,
            this.CustNumber,
            this.nameDataGridViewTextBoxColumn,
            this.dateStartDataGridViewTextBoxColumn,
            this.dateFinishDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.orderBindingSource;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Location = new System.Drawing.Point(0, 133);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.Size = new System.Drawing.Size(941, 461);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDoubleClick);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // numberDataGridViewTextBoxColumn
            // 
            this.numberDataGridViewTextBoxColumn.DataPropertyName = "Number";
            this.numberDataGridViewTextBoxColumn.HeaderText = "№ заказа";
            this.numberDataGridViewTextBoxColumn.Name = "numberDataGridViewTextBoxColumn";
            this.numberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // CustNumber
            // 
            this.CustNumber.DataPropertyName = "CustNumber";
            this.CustNumber.HeaderText = "номер заказчика";
            this.CustNumber.Name = "CustNumber";
            this.CustNumber.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Наименование";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateStartDataGridViewTextBoxColumn
            // 
            this.dateStartDataGridViewTextBoxColumn.DataPropertyName = "DateStart";
            this.dateStartDataGridViewTextBoxColumn.HeaderText = "Начало обработки";
            this.dateStartDataGridViewTextBoxColumn.Name = "dateStartDataGridViewTextBoxColumn";
            this.dateStartDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateFinishDataGridViewTextBoxColumn
            // 
            this.dateFinishDataGridViewTextBoxColumn.DataPropertyName = "DateFinish";
            this.dateFinishDataGridViewTextBoxColumn.HeaderText = "Окончание обработки";
            this.dateFinishDataGridViewTextBoxColumn.Name = "dateFinishDataGridViewTextBoxColumn";
            this.dateFinishDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // orderBindingSource
            // 
            this.orderBindingSource.DataSource = typeof(WeigherData.Orders);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.tbNumber);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(4, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(873, 124);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Отбор";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioStudy2);
            this.groupBox2.Controls.Add(this.radioStudy1);
            this.groupBox2.Controls.Add(this.radioStudy0);
            this.groupBox2.Location = new System.Drawing.Point(552, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 97);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Стадия";
            // 
            // radioStudy2
            // 
            this.radioStudy2.AutoSize = true;
            this.radioStudy2.Location = new System.Drawing.Point(118, 60);
            this.radioStudy2.Name = "radioStudy2";
            this.radioStudy2.Size = new System.Drawing.Size(97, 22);
            this.radioStudy2.TabIndex = 2;
            this.radioStudy2.Text = "Закрытые";
            this.radioStudy2.UseVisualStyleBackColor = true;
            this.radioStudy2.CheckedChanged += new System.EventHandler(this.radioStudy0_CheckedChanged);
            // 
            // radioStudy1
            // 
            this.radioStudy1.AutoSize = true;
            this.radioStudy1.Location = new System.Drawing.Point(118, 39);
            this.radioStudy1.Name = "radioStudy1";
            this.radioStudy1.Size = new System.Drawing.Size(115, 22);
            this.radioStudy1.TabIndex = 1;
            this.radioStudy1.Text = "В обработке";
            this.radioStudy1.UseVisualStyleBackColor = true;
            this.radioStudy1.CheckedChanged += new System.EventHandler(this.radioStudy0_CheckedChanged);
            // 
            // radioStudy0
            // 
            this.radioStudy0.AutoSize = true;
            this.radioStudy0.Checked = true;
            this.radioStudy0.Location = new System.Drawing.Point(118, 19);
            this.radioStudy0.Name = "radioStudy0";
            this.radioStudy0.Size = new System.Drawing.Size(73, 22);
            this.radioStudy0.TabIndex = 0;
            this.radioStudy0.TabStop = true;
            this.radioStudy0.Text = "Новые";
            this.radioStudy0.UseVisualStyleBackColor = true;
            this.radioStudy0.CheckedChanged += new System.EventHandler(this.radioStudy0_CheckedChanged);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(158, 86);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(369, 24);
            this.tbName.TabIndex = 3;
            this.tbName.TextChanged += new System.EventHandler(this.tbNumber_TextChanged);
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(158, 56);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(96, 24);
            this.tbNumber.TabIndex = 1;
            this.tbNumber.TextChanged += new System.EventHandler(this.tbNumber_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Наименование";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Checked = false;
            this.dateTimePicker2.Location = new System.Drawing.Point(341, 18);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowCheckBox = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(186, 24);
            this.dateTimePicker2.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(308, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "по";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(116, 18);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowCheckBox = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(186, 24);
            this.dateTimePicker1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Номер";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 18);
            this.label1.TabIndex = 5;
            this.label1.Text = "за период с";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 595);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(941, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(76, 17);
            this.toolStripStatusLabel1.Text = "Упаковщик :";
            // 
            // fOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 617);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(957, 655);
            this.Name = "fOrders";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ерматель. Участок упаковки.";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fOrders_FormClosed);
            this.Load += new System.EventHandler(this.fOrders_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource orderBindingSource;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem принтерыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem соединениеСБДToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem упаковщикиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выбратьУпаковщикаToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioStudy2;
        private System.Windows.Forms.RadioButton radioStudy1;
        private System.Windows.Forms.RadioButton radioStudy0;
        private System.Windows.Forms.ToolStripMenuItem соответствияСтолбцовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem печатныеОтчетыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справочникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem связьСВесамиToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateStartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateFinishDataGridViewTextBoxColumn;
    }
}