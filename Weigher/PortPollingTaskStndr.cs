﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using DbCommon.Helpers;

namespace Weigher
{
    public class PortPollingTaskStndr { }
    public partial class fOrderDetails
    {
        /// <summary>
        /// Опрос ком порта
        /// </summary>
        private void PortPollingTaskStndr()
        {
            var portReady = ComPortCreateAndOpen();
            Invoke(new Action(() => toolStripButton2.Enabled = portReady));
            if (!portReady) return;
            byte[] buf = new byte[5];
            try
            {
                _mPort.Write(new byte[] { 0x48 }, 0, 1);
                _mPort.BaseStream.Flush();
                Thread.Sleep(50); 
                int count = _mPort.Read(buf, 0, 5);
                if (count == 2)
                {
                    var info = BitConverter.ToUInt16(buf, 0);
                    LogInfo($"Значение дискретности весов : {info}");
                }
            }
            catch (Exception ex)
            {
                Invoke(new Action(() => toolStripButton2.Enabled = false));
                Log.Error(ex, "PortPollingTaskStndr");
                return;
            }
            while (_pollingFlag && portReady)
            {
                try
                {
                    ushort wStatus = 0;
                    // Запрос статуса
                    _mPort.Write(new byte[] { 0x44 }, 0, 1);
                    _mPort.BaseStream.Flush();
                    Thread.Sleep(50);
                    int c = _mPort.Read(buf, 0, 5);
                    LogInfo(string.Format("PortPollingTaskStndr, прочитано {0} байт", c));
                    if (c == 2)
                    {
                        wStatus = (ushort)(BitConverter.ToUInt16(buf, 0) & 0x80); // D7 – индикатор процесса взвешивания: 0 – не завершен, 1– завершен; 
                        if (wStatus > 0)
                        {
                            _mPort.Write(new byte[] { 0x45 }, 0, 1);
                            _mPort.BaseStream.Flush();
                            Thread.Sleep(50); // было 20 и работало
                            c = _mPort.Read(buf, 0, 5);
                            if (c == 2)
                            {
                                ushort valueW = BitConverter.ToUInt16(buf, 0);
                                decimal w = valueW & 0x7FFF;
                                if ((valueW & 0x8000) > 0)
                                    w = -w;
                                if (Math.Abs(w - _wprev) > 2)
                                {
                                    _wprev = w;
                                    var displayw = (_wprev / 100m).ToString(CultureInfo.InvariantCulture);
                                    Invoke(new Action(() =>
                                    {
                                        toolStripStatusLabel2.Text = @"Текущий вес: " + displayw;
                                        labelWeith.Text = displayw;
                                        labelWeith.Refresh();
                                    }));
                                }
                            }
                        }
                    }
                    _wPrevStatus = wStatus;
                    Thread.Sleep(50);
                }
                catch (TimeoutException)
                {
                    Log.Error("TimeoutException при опросе порта");
                    if (!RestartSerialPort())
                        break;
                }
                catch (ThreadInterruptedException)
                {
                    break;
                }
                catch (IOException ex)
                {
                    Log.Error(ex, "при опросе порта");
                    if (!RestartSerialPort())
                    {
                        BeginInvoke(new Action(() =>
                        {
                            toolStripButton2.Enabled = false;
                        }));
                        break;
                    }
                }
            }
        }

    }
}
