﻿using System.IO.Ports;

namespace Weigher
{
    public class SettingsPort
    {
        public SettingsPort(){}
        public string Port { get; set; } ="COM2";
        public int BaudRate { get; set; } = 4800;
        public StopBits StopBits { get; set; } = StopBits.One;
        public Parity Parity { get; set; } =Parity.Even;
    }

    public class SettingsPrinter
    {
        public SettingsPrinter(){}
        public string PrinterA4 { get; set; } = "";
        public string PrinterLabels { get; set; } = "";
        public string PrinterLabels2 { get; set; } = "";
        public int CheckPrintCount { get; set; } = 0;

        /// <summary>
        /// Количество упаковочных листов по умолчанию
        /// </summary>
        public int PackListPrintCount { get; set; } = 3;
        public int PalletLabelPrintCount { get; set; } = 0;
    }

    public class SettingsProp
    {
        public SettingsProp() { }
        public string ExlPath { get; set; } = "C:\\Projects\\Weigher\\TestData";
        public bool ExlPathWsf { get; set; } = false;
        public string ExlPageName { get; set; } = "Бирка";
        public int BarDigit { get; set; } = 9;
        public bool RandomBtn { get; set; } = true;
        public int ExlMinNom { get; set; } = 100;
        public string SettingPwd { get; set; } = "n";
        public string BarPrefix { get; set; } = "213";
        public string BarPrefixGroup { get; set; } = "214";
        public bool ShowRollCountButtons { get; set; } = true;
        public string ManagerPageName { get; set; } = "МЕНЕДЖЕР";
        public bool ReadOnlyMode { get; set; } = false;
    }
}