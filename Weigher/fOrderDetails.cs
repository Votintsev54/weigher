﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DbCommon;
using DbCommon.Helpers;
using WeigherData;
using Weigher.Properties;
using FastReport;
using FastReport_Report = FastReport.Report;
using DbReport = WeigherData.Report;
using WpfWeigher.Views;
using WpfWeigher.ViewModels;

namespace Weigher
{
    public partial class fOrderDetails : Form
    {
        public fOrderDetails()
        {
            InitializeComponent();
        }

        private readonly CDatabaseWeigher _db;
        private readonly Orders _order;
        private Interface _currentInterface;

        public fOrderDetails(CDatabaseWeigher db, Orders order)
        {
            _db = db;
            _order = order;
            InitializeComponent();

            if (!WeigherSettings.Current.OtherSettings.RandomBtn)
                Controls.Remove(btnRandom);

            btnStudy.Checked = order.DateFinish == null;
            SetStudy(order.DateFinish == null);
        }

        private void fOrderDetails_Load(object sender, EventArgs e)
        {
            Log.Info("Открыт заказ id:{0},Number:{1},Name:{2}", _order.Id, _order.Number, _order.Name);
            Text += _order.Number + @" " + _order.Name;
            _order.OrderDetails.Sort("Id");
            dgOrderDetails.DataSource = _order.OrderDetails;
            dgPackages.DataSource = _order.Packages;

            ///// Текущий интерфейс
            foreach (var intf in _db.Interface.GetAll())
                cbInterface.Items.Add(intf.Name);

            _currentInterface = _order.Interface ?? _order.OrderDetails.FirstOrDefault()?.Customer?.GroupCustomer?.Interface ?? _db.Interface.GetById(1);
            cbInterface.SelectedItem = _currentInterface.Name;

            if (_order.OrderDetails.Count == 0)
                foreach (Control c in Controls)
                    c.Enabled = false;

            if (_order.OrderDetails.Count > 0)
                StartComPortPolling();

            string[] s1 = { "1", "2", "3", "4", "5", "6" };
            foreach (string s in s1)
                cbGroupRoll.Items.Add(s);
            cbGroupRoll.Text = @"1";

            string[] s2 = { "0", "1", "2" };
            foreach (string s in s2)
                cbCheckCopies.Items.Add(s);
            cbCheckCopies.Text = WeigherSettings.Current.PrintersSettings.CheckPrintCount.ToString();

            bool bFixLenAreaCheck = _order.OrderDetails.Any(od => od.FixLen != null || od.FixArea != null);
            chkChangeDP.Checked = bFixLenAreaCheck;

            WeigherSettings ws = WeigherSettings.ReadFromFile(true);
            if (!ws.OtherSettings.ShowRollCountButtons)
            {

                radioRolls1.Visible = false;
                radioRolls2.Visible = false;
                radioRolls3.Visible = false;
                radioRolls4.Visible = false;
                radioRolls5.Visible = false;
                Column3.Visible = false;
                numRolsInBag.Visible = false;
            }
            else
            {
                numRolsInBag.Value = 1;
                var _rolsInBagButtons = new RadioButton[] { radioRolls1, radioRolls2, radioRolls3, radioRolls4, radioRolls5 };
                numRolsInBag.GotFocus += (a, b) =>
                {
                    foreach (var rb in _rolsInBagButtons)
                        rb.Checked = false;
                };
                foreach (var rb in _rolsInBagButtons)
                {
                    rb.CheckedChanged += (s, b) =>
                    {
                        if (rb.Checked)
                            numRolsInBag.Value = int.Parse(rb.Text);
                    };
                }
            }

            numSkleek.Value = 0;
            var _skleekButtons = new RadioButton[] { radioSkleek0, radioSkleek1, radioSkleek2, radioSkleek3 };
            numSkleek.GotFocus += (a, b) =>
            {
                foreach (var rb in _skleekButtons)
                    rb.Checked = false;
            };
            foreach (var rb in _skleekButtons)
            {
                rb.CheckedChanged += (s, b) =>
                {
                    if (rb.Checked)
                        numSkleek.Value = int.Parse(rb.Text);
                };
            }
        }

        private void StartComPortPolling()
        {
            switch ((WeigherSettings.Current.ScaleSettings.Protokol)
)
            {
                case WpfWeigher.Models.ScaleProtokols.Protokol2:
                    if (string.IsNullOrEmpty(WeigherSettings.Current.ScaleSettings.SerialSettings.ComPort))
                    {
                        toolStripButton2.Enabled = false;
                        toolStripButton2.ToolTipText = "Подключение к весам не настроено";
                        return;
                    }
                    //var searchList = new ScaleSearchList();
                    //var scales = searchList.GetScales();

                    _pollingFlag = true;
                    if (_pollingFlag)
                    {
                        _pollingThread = new Thread(PortPollingTask);
                        _pollingThread.Start();
                    }

                    break;
                case WpfWeigher.Models.ScaleProtokols.Stndr:
                    if (string.IsNullOrEmpty(WeigherSettings.Current.ScaleSettings.SerialSettings.ComPort))
                    {
                        toolStripButton2.Enabled = false;
                        toolStripButton2.ToolTipText = "Подключение к весам не настроено";
                        return;
                    }
                    //var searchList = new ScaleSearchList();
                    //var scales = searchList.GetScales();

                    _pollingFlag = true;
                    if (_pollingFlag)
                    {
                        _pollingThread = new Thread(PortPollingTaskStndr);
                        _pollingThread.Start();
                    }

                    break;
                case WpfWeigher.Models.ScaleProtokols.Protokol100:
                    _pollingFlag = true;
                    PortPolling100(true);
                    break;
                case WpfWeigher.Models.ScaleProtokols.Protokol1c:
                    _pollingFlag = true;
                    PortPolling100(false);
                    break;
                default:
                    break;
            }

        }

        private int GetCheckCopies()
        {
            return Convert.ToInt32(cbCheckCopies.Text);
        }

        private SerialPort _mPort;

        private Thread _pollingThread;
        private bool _pollingFlag;

        private bool ComPortCreateAndOpen()
        {
            if (_mPort == null)
            {
                var portSettings = WeigherSettings.Current.ScaleSettings.SerialSettings;
                _mPort = new SerialPort
                {
                    PortName = portSettings.ComPort,
                    BaudRate = portSettings.BaudRate,
                    Parity = portSettings.Parity,
                    DataBits = 8,
                    StopBits = portSettings.StopBits,
                    Handshake = Handshake.None,
                    ReadTimeout = 100,
                    WriteTimeout = 100
                };
            }
            try
            {
                _mPort.Open();
                _mPort.DiscardInBuffer();
                return true;
            }
            catch (IOException ex)
            {
                Log.Error(ex, "m_port.Open()");
                _mPort = null;
                return false;
            }

        }

        private void ComPortCloseAndDispose()
        {
            if (_mPort != null)
            {
                try
                {
                    _mPort.Dispose();
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "m_port.Dispose()");
                }
                _mPort = null;
            }
        }

        private void LogInfo(string info)
        {
            Log.Info(info);
        }

        private int _wPrevStatus;
        private const int WZeroStatus = 0x04C0;   //  100 11000000
        private const int WFlowStatus = 0x0400;
        private const int WReadyStatus = 0x0480;  //  100 10000000
        private const int WReadyStatusTara = 1184;//10001 10000100
        private const int WZeroStatusTara = 1248;
        private const int WFlowStatusTara = 1056;
        //1184 1056 1248 1056 1184
        private decimal _wprev;


        private bool RestartSerialPort()
        {
            ComPortCloseAndDispose();
            Thread.Sleep(1000);
            if (!ComPortCreateAndOpen())
            {
                Log.Error("Нет связи с весами, нужно проверить подключение и перезапустить программу! ");
                MessageBox.Show(@"Нет связи с весами, нужно проверить соединение и перезапустить программу!",
                    @"Ошибка...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void AddRoll(decimal modWeigth)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                var rolls = (int)numRolsInBag.Value;
                if (modWeigth <= 0) return;
                decimal weigth = modWeigth / 100m;

                Log.Info("AddRoll: заказ id:{0},Number:{1},вес:{2}", _order.Id, _order.Number, weigth);

                if (dgOrderDetails.SelectedRows.Count == 1)
                {
                    var curDetail = (OrderDetail)dgOrderDetails.SelectedRows[0].DataBoundItem;
                    decimal vesVtulok = curDetail.VtulkaWeight * rolls;
                    weigth = weigth - vesVtulok;
                    if (weigth <= 0) return;

                    Package curPackage = null;
                    if (_order.Packages.Count == 0)
                    {
                        curPackage = _db.Package.NewInstance();
                        curPackage.PackageNumber = MaxPackageNumber(_order);
                        curPackage.Barcode = _db.GetBarCode(WeigherSettings.Current.OtherSettings.BarPrefix, WeigherSettings.Current.OtherSettings.BarDigit);

                        _order.Packages.Add(curPackage);
                        dgPackages.Rows[0].Selected = true;
                        _order.Save2();
                    }
                    if (dgPackages.SelectedRows.Count != 1)
                        dgPackages.Rows[dgPackages.Rows.Count - 1].Selected = true;
                    curPackage = (Package)dgPackages.SelectedRows[0].DataBoundItem;

                    var newRoll = _db.Roll.NewInstance();
                    newRoll.RollBarcode = _db.GetBarCode(WeigherSettings.Current.OtherSettings.BarPrefix, WeigherSettings.Current.OtherSettings.BarDigit);
                    newRoll.RollWeight = weigth;
                    newRoll.VtulkaWeight = vesVtulok;
                    newRoll.Skleek = (int)numSkleek.Value;
                    newRoll.BruttoWeight = weigth + vesVtulok;
                    newRoll.RollCount = rolls;
                    if (curDetail.FrameWeight > 0)
                        newRoll.FrameCount = (int)(weigth / curDetail.FrameWeight);
                    try
                    {
                        newRoll.RollNumber = MaxRollNumber(_order);
                    }
                    catch (InvalidOperationException)
                    {
                        newRoll.RollNumber = 1;
                    }

                    newRoll.PackageId = curPackage.Id;
                    curDetail.Rolls.Add(newRoll);
                    _order.Save2();

                    // увеличиваем количество заглушек
                    curPackage.StubCount += 2;
                    curPackage.Save2();
                    dgPackages.InvalidateRow(dgPackages.SelectedRows[0].Index);

                    ((SortList<Roll>)dgRolls.DataSource).Add(newRoll);
                    dgRolls.Rows[dgRolls.Rows.Count - 1].Selected = true;
                    int firstDispRow = dgRolls.Rows.Count - 9;
                    if (firstDispRow < 0)
                        firstDispRow = 0;

                    dgRolls.FirstDisplayedCell = dgRolls.Rows[firstDispRow].Cells[0];

                    PrintSingleCheck();

                    int group = Convert.ToInt32(cbGroupRoll.Text);
                    if (group > 1 && group == dgRolls.SelectedRows.Count)
                    {
                        PrintGroupCheck();
                        ClearSelectedRolls();
                    }
                    if (group == 1)
                        ClearSelectedRolls();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "AddRoll");
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void PrintGroupCheck()
        {
            int copies = GetCheckCopies();
            if (copies == 0) return;
            if (dgRolls.SelectedRows.Count <= 1) return;
            try
            {
                //MessageBox.Show("Печать группы"); return;

                FastReport_Report r = new FastReport_Report();
                var _groupCustomerId = _order.OrderDetails.FirstOrDefault()?.Customer.GroupCustomerId;
                DbReport rbd = GetReport("Этикетки", _groupCustomerId, _currentInterface.Id);
                if (rbd == null)
                {
                    MessageBox.Show($"Не найден печатный отчет Этикетки для интерфейса {_currentInterface.Id} заказчик {_groupCustomerId}");
                    return;
                }
                r.LoadFromString(rbd.Value);

                r.RegisterData(new Orders[] { _order }, "Orders");
                Roll GRoll = _db.Roll.NewInstance();
                foreach (DataGridViewRow row in dgRolls.SelectedRows)
                {
                    Roll roll = (Roll)row.DataBoundItem;
                    GRoll.RollWeight += roll.RollWeight;
                    GRoll.RollNumber = 0;
                    GRoll.RollBarcode = roll.RollBarcode;
                    GRoll.OrderDetailId = roll.OrderDetailId;
                    GRoll.PackageId = roll.PackageId;
                }
                GRoll.RollBarcode = WeigherSettings.Current.OtherSettings.BarPrefixGroup + GRoll.RollBarcode.Substring(3);
                r.RegisterData(new[] { GRoll }, "Roll");
                r.SetParameterValue("RollCount", dgRolls.SelectedRows.Count);
                r.PrintSettings.Printer = GetLabelsPrinter();
                r.PrintSettings.Copies = copies;

                r.PrintSettings.ShowDialog = false;
                r.Print();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Ошибка при печати");
                MessageBox.Show("Ошибка при печати, проверьте настройки принтера!", "Внимание...", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void ClearSelectedRolls()
        {
            foreach (DataGridViewRow row in dgRolls.SelectedRows)
                row.Selected = false;
        }


        private int MaxRollNumber(Orders orders)
        {
            int ret = (from det in orders.OrderDetails from r in det.Rolls select r.RollNumber).Concat(new[] { 0 }).Max();
            return ret + 1;
        }

        private int MaxPackageNumber(Orders orders)
        {
            return _order.Packages.Select(r => r.PackageNumber).Concat(new[] { 0 }).Max() + 1;
        }

        private readonly Random _rand = new Random(10000);

        private void button4_Click(object sender, EventArgs e)
        {
            AddRoll((short)_rand.Next(short.MaxValue));
        }

        private void btnNextPackage_Click(object sender, EventArgs e)
        {
            if (_order.OrderDetails.Count == 0) return;
            Package curPackage = _db.Package.NewInstance();
            curPackage.Barcode = _db.GetBarCode(WeigherSettings.Current.OtherSettings.BarPrefix, WeigherSettings.Current.OtherSettings.BarDigit);
            curPackage.PackageNumber = MaxPackageNumber(_order);
            _order.Packages.Add(curPackage);
            _order.Save2();
            dgPackages.Rows[dgPackages.Rows.Count - 1].Selected = true;
            radioRolls1.Checked = true;
        }

        private void dgPackages_SelectionChanged(object sender, EventArgs e)
        {
            if (dgPackages.SelectedRows.Count == 1 && dgOrderDetails.SelectedRows.Count == 1)
            {
                OrderDetail detail = (OrderDetail)dgOrderDetails.SelectedRows[0].DataBoundItem;
                Package curPackage = (Package)(dgPackages.SelectedRows[0].DataBoundItem);
                int packageId = curPackage.Id;
                dgRolls.DataSource = detail.Rolls.Where(r0 => r0.PackageId == packageId);
            }
            else
            {
                dgRolls.DataSource = null;
            }
        }

        private void dgOrderDetails_SelectionChanged(object sender, EventArgs e)
        {
            dgPackages_SelectionChanged(sender, e); // то же самое делаем
            radioRolls1.Checked = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgOrderDetails.SelectedRows.Count == 1)
            {
                int i = dgOrderDetails.SelectedRows[0].Index + 1;
                if (i < dgOrderDetails.RowCount)
                    dgOrderDetails.Rows[i].Selected = true;
            }
        }

        private void dgRolls_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (dgv.CurrentRow == null) return;
            Roll r = (Roll)dgv.CurrentRow.DataBoundItem;
            r.RollWeight = r.BruttoWeight - r.VtulkaWeight;
            _order.Save2();
            dgv.Invalidate();
        }

        private void dgRolls_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            Roll r = (Roll)e.Row.DataBoundItem;
            OrderDetail detail = r.OrdersDetail;
            detail.Rolls.DeleteObject(r);
            detail.Save2();
            // e.Cancel = true; здесь не надо
        }

        private void dgPackages_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (MessageBox.Show("Будут удалены все результаты взвешивания палеты!\r\nУдалить?"
                , "Удаление палеты", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Package package = (Package)e.Row.DataBoundItem;
                int PackageId = package.Id;
                foreach (OrderDetail detail in _order.OrderDetails)
                {
                    var rollsForRemove = detail.Rolls.Where(r0 => r0.PackageId == PackageId).ToList();
                    foreach (Roll roll in rollsForRemove)
                        detail.Rolls.DeleteObject(roll);
                }
                _order.Packages.DeleteObject(package);
                _order.Save2();
            }
            e.Cancel = true;
        }

        private void btnStudy_Click(object sender, EventArgs e)
        {
            if (btnStudy.Checked)
                _order.DateFinish = null;
            else
                _order.DateFinish = DateTime.Now;
            _order.Save2();

            SetStudy(btnStudy.Checked);

        }

        private void SetStudy(bool p)
        {
            if (p)
            {
                dgOrderDetails.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2;
                btnStudy.Image = Resources.kgpg;
                dgRolls.AllowUserToDeleteRows = true;
                dgRolls.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2;
                dgPackages.AllowUserToDeleteRows = true;
                btnNextPackage.Enabled = true;
                toolStripButton2.Enabled = true;

            }
            else
            {
                dgOrderDetails.EditMode = DataGridViewEditMode.EditProgrammatically;
                btnStudy.Image = Resources.kgpg_5307;
                dgRolls.AllowUserToDeleteRows = false;
                dgRolls.EditMode = DataGridViewEditMode.EditProgrammatically;
                dgPackages.AllowUserToDeleteRows = false;
                btnNextPackage.Enabled = false;
                toolStripButton2.Enabled = false;
                if ((DateTime.Now - _order.DateFinish.Value).TotalHours > 24 * 4)
                    btnStudy.Enabled = false;
            }
        }

        private void Grid_Enter(object sender, EventArgs e)
        {
            DataGridView c = (DataGridView)sender;
            c.BackgroundColor = Color.White;
        }

        private void Grid_Leave(object sender, EventArgs e)
        {
            DataGridView c = (DataGridView)sender;
            c.BackgroundColor = Color.FromKnownColor(KnownColor.AppWorkspace);
            if (c == dgRolls)
                ClearSelectedRolls();
        }

        private void PrintSingleCheck()
        {
            if (GetCheckCopies() == 0)
                return;
            if (dgRolls.Rows.Count == 0) return;
            try
            {
                //MessageBox.Show("Печать!"); return;

                FastReport_Report r = new FastReport_Report();
                var _groupCustomerId = _order.OrderDetails.FirstOrDefault()?.Customer.GroupCustomerId;
                DbReport rbd = GetReport("Этикетки", _groupCustomerId, _currentInterface.Id);
                if (rbd == null)
                {
                    MessageBox.Show($"Не найден печатный отчет Этикетки для интерфейса {_currentInterface.Id} заказчик {_groupCustomerId}");
                    return;
                }

                r.LoadFromString(rbd.Value);

                r.RegisterData(new[] { _order }, "Orders");
                DataGridViewRow row = dgRolls.Rows[dgRolls.Rows.Count - 1];
                r.RegisterData(new[] { (Roll)row.DataBoundItem }, "Roll");
                r.SetParameterValue("RollCount", 1);
                r.PrintSettings.Printer = GetLabelsPrinter();
                r.PrintSettings.Copies = GetCheckCopies();

                r.PrintSettings.ShowDialog = false;
                r.Print();

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Ошибка при печати");
                MessageBox.Show("Ошибка при печати, проверьте настройки принтера!", "Ошибка...", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private AskPrintDialogViewModel askPrintDialogViewModel = new AskPrintDialogViewModel();

        private void tsbPrintCheck_Click(object sender, EventArgs e)
        {
            if (dgRolls.RowCount == 0)
                return;

            if (askPrintDialogViewModel.AskForPrint())
            {
                AskPrintDialog d = new AskPrintDialog(askPrintDialogViewModel);
                if (!(d.ShowDialog() ?? false))
                    return;
                askPrintDialogViewModel = (AskPrintDialogViewModel)d.DataContext;
            }

            FastReport_Report r = new FastReport_Report();
            EnvironmentSettings environmentSettings = new EnvironmentSettings();
            PreviewSettings previewSettings = new PreviewSettings();
            var _groupCustomerId = _order.OrderDetails.FirstOrDefault()?.Customer.GroupCustomerId;
            DbReport rbd = GetReport("Этикетки", _groupCustomerId, _currentInterface.Id);
            if (rbd == null)
            {
                MessageBox.Show($"Не найден печатный отчет Этикетки для интерфейса {_currentInterface.Id} заказчик {_groupCustomerId}");
                return;
            }
            r.LoadFromString(rbd.Value);

            r.RegisterData(new[] { _order }, "Orders");
            r.RegisterData((SortList<Roll>)dgRolls.DataSource, "Roll");
            r.SetParameterValue("RollCount", 1);
            r.PrintSettings.Printer = GetLabelsPrinter();
            r.PrintSettings.Copies = askPrintDialogViewModel.CheckCopies;

            previewSettings.Buttons = (((PreviewButtons.Print | PreviewButtons.Zoom)
                                        | PreviewButtons.PageSetup)
                                       | PreviewButtons.Navigator)
                                      | PreviewButtons.Close;
            //настраиваем какие кнопки будут отображаться в диалоге простора отчета

            environmentSettings.PreviewSettings = previewSettings;

            //Roll roll;
            //roll.OrdersDetail.Orders.Packer.Nom

            if (askPrintDialogViewModel.CheckPreview)
                r.Show();
            else
            {
                r.PrintSettings.ShowDialog = false;
                r.Print();
            }

        }

        private DbReport GetReport(string name, Guid? groupCustomerId, int interfaceId)
        {
            var repList = _db.Report.GetObjectListByExpr(r0 => r0.Name == name);
            if (repList.Count() <= 1) return repList.FirstOrDefault();
            var ret = repList.Where(rep => rep.InterfaceId.GetValueOrDefault(1) == interfaceId);
            if (!ret.Any())
                return repList.FirstOrDefault(rep => rep.InterfaceId.GetValueOrDefault(1) == 1);
            if (ret.Count() == 1) return ret.First();
            ret = ret.Where(rep => rep.GroupCustomerId == groupCustomerId);
            return ret.FirstOrDefault();
        }

        static AskPrintDialogViewModel listAskPrintDialogViewModel = new AskPrintDialogViewModel() { CheckCopies = WeigherSettings.Current.PrintersSettings.PackListPrintCount };

        private void PrintPackList(string reportName, bool printBrutto = false, int copies = 0)
        {
            if (dgPackages.SelectedRows.Count != 1) return;

            Package package = (Package)dgPackages.SelectedRows[0].DataBoundItem;
            int packageId = package.Id;
            var orderDetails =
                _order.OrderDetails.Where(detail => detail.Rolls.Any(roll => roll.PackageId == packageId)).ToList();

            var rolls =
                (from detail in orderDetails from roll in detail.Rolls where roll.PackageId == packageId select roll)
                    .ToList();

            if (rolls.Count == 0)
            {
                MessageBox.Show("Упаковочный лист пустой!", "Внимание...", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }
            if (listAskPrintDialogViewModel.AskForPrint())
            {
                AskPrintDialog d = new AskPrintDialog(listAskPrintDialogViewModel);
                if (d.ShowDialog() ?? false)
                    listAskPrintDialogViewModel = (AskPrintDialogViewModel)d.DataContext;
            }

            FastReport_Report r = new FastReport_Report();
            EnvironmentSettings environmentSettings = new EnvironmentSettings();
            PreviewSettings previewSettings = new PreviewSettings();

            var _groupCustomerId = _order.OrderDetails.FirstOrDefault()?.Customer.GroupCustomerId;
            DbReport rbd = GetReport(reportName, _groupCustomerId, _currentInterface.Id);
            if (rbd == null)
            {
                MessageBox.Show($"Не найден печатный отчет {reportName} для интерфейса {_currentInterface.Id} заказчик {_groupCustomerId}");
                return;
            }

            r.LoadFromString(rbd.Value);

            r.RegisterData(new[] { _order }, "Orders");
            r.RegisterData(new[] { package }, "Package");
            r.RegisterData(orderDetails, "OrderDetail");
            r.RegisterData(rolls, "Roll");

            //r.SetParameterValue("cs_Novosibirsk", "Новосибирск");
            //r.SetParameterValue("cs_Year", "г.");
            r.SetParameterValue("ReportDate", ((Package)dgPackages.SelectedRows[0].DataBoundItem).Date);
            r.SetParameterValue("BruttoFlag", printBrutto);
            r.PrintSettings.Printer = GetA4Printer();
            r.PrintSettings.Copies = copies > 0 ? copies : listAskPrintDialogViewModel.CheckCopies;

            //настраиваем какие кнопки будут отображаться в диалоге простора отчета
            previewSettings.Buttons = PreviewButtons.Print | PreviewButtons.Zoom |
                                      PreviewButtons.PageSetup | PreviewButtons.Navigator |
                                      PreviewButtons.Close;

            environmentSettings.PreviewSettings = previewSettings;

            if (listAskPrintDialogViewModel.CheckPreview)
                r.Show();
            else
            {
                r.PrintSettings.ShowDialog = false;
                r.Print();
            }

            if (WeigherSettings.Current.PrintersSettings.PalletLabelPrintCount > 0)
            {
                DbReport palletLabel = GetReport("Ярлык к палетте", _groupCustomerId, _currentInterface.Id);
                r.LoadFromString(palletLabel.Value);
                r.PrintSettings.Printer = GetA4Printer();
                if (listAskPrintDialogViewModel.CheckPreview)
                    r.Show();
                else
                {
                    r.PrintSettings.Copies = WeigherSettings.Current.PrintersSettings.PalletLabelPrintCount;
                    r.PrintSettings.ShowDialog = false;
                    r.Print();
                }
            }
        }

        private string GetA4Printer()
        {
            var ps = WeigherSettings.Current.PrintersSettings;
            if (int.TryParse(_currentInterface.PrinterA4, out var num))
            {
                switch (num)
                {
                    case 1: return ps.PrinterA4;
                    case 2: return ps.PrinterLabels;
                    case 3: return ps.PrinterLabels2;
                }
            }
            return ps.PrinterA4;
        }
        private string GetLabelsPrinter()
        {
            var ps = WeigherSettings.Current.PrintersSettings;
            if (int.TryParse(_currentInterface.PrinterLabel, out var num))
            {
                switch (num)
                {
                    case 1: return ps.PrinterA4;
                    case 2: return ps.PrinterLabels;
                    case 3: return ps.PrinterLabels2;
                }
            }
            return ps.PrinterLabels;
        }

        //private void fOrderDetails_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.F5)
        //    {
        //        button1_Click(null, null);
        //    }
        //    if (e.KeyCode == Keys.Space)
        //    {
        //        MessageBox.Show("Space");
        //    }
        //}

        // ReSharper disable once InconsistentNaming
        private const int WM_KEYDOWN = 0x0100;

        protected override bool ProcessKeyPreview(ref Message m)
        {
            if (m.Msg == WM_KEYDOWN && _pollingFlag)
            {
                if (m.WParam == (IntPtr)Keys.Space)
                {
                    m.Result = (IntPtr)0;
                    AddRoll(_wprev);
                    return true;
                }
            }
            return base.ProcessKeyPreview(ref m);
        }

        private void fOrderDetails_FormClosed(object sender, FormClosedEventArgs e)
        {
            _order.Save2();
            if (_pollingFlag)
            {
                _pollingFlag = false;
                if (_pollingThread != null)
                {
                    if (_pollingThread.ThreadState == ThreadState.WaitSleepJoin)
                        _pollingThread.Interrupt();
                    _pollingThread.Join();
                }
                ComPortCloseAndDispose();
            }
            scaleForm?.Close();
            Log.Info("Завершен заказ id:{0},Number:{1},Name:{2}", _order.Id, _order.Number, _order.Name);
        }

        private void dgOrderDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            _order.Save2();
        }

        static readonly NumberFormatInfo _nfiDot = new NumberFormatInfo() { CurrencyDecimalSeparator = "." };

        private void dgOrderDetails_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception is FormatException)
            {
                string s = ((DataGridView)sender).CurrentCell.EditedFormattedValue as string;
                if (s != null && ((DataGridView)sender).CurrentCell.Value is decimal)
                {
                    decimal value = 0;
                    if (decimal.TryParse(s.Replace(",", "."), NumberStyles.Number, _nfiDot, out value))
                    {
                        ((DataGridView)sender).CurrentCell.Value = value;
                        e.Cancel = false;
                        return;
                    }
                }
            }
            MessageBox.Show("Требуется ввести число!", "Внимание...", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }


        private void tsbPrintGroupCheck_Click(object sender, EventArgs e)
        {
            PrintGroupCheck();
            ClearSelectedRolls();
        }

        private void cbGroupRoll_TextChanged(object sender, EventArgs e)
        {
            ClearSelectedRolls();
        }


        private void tsbPrint_Click(object sender, EventArgs e)
        {
            PrintPackList("Упаковочный лист", cbPrintBrutto.Checked);
            //if (WeigherSettings.Current.PrintersSettings.PalletLabelPrintCount > 0)
            //{
            //    PrintPackList("Ярлык к палетте", cbPrintBrutto.Checked, WeigherSettings.Current.PrintersSettings.PalletLabelPrintCount);
            //}
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            PrintPackList("Спецификация");
        }

        private void chkChangeDP_CheckedChanged(object sender, EventArgs e)
        {
            FixLen.Visible = chkChangeDP.Checked;
            FixArea.Visible = chkChangeDP.Checked;

        }



        private void dgPackages_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            _order.Save2();
        }


        private void CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (_wprev <= 0) return;
            decimal weigth = _wprev / 100m;

            //decimal weigth = 0.31m;
            var gv = sender as DataGridView;
            var col = e.ColumnIndex;
            var row = e.RowIndex;
            // ReSharper disable once PossibleNullReferenceException
            if (col >= 0 && e.ColumnIndex < gv.ColumnCount)
            {
                if (row >= 0 && row < gv.RowCount)
                {
                    var dpn = gv.Columns[e.ColumnIndex].DataPropertyName;
                    if (dpn.Contains("Weight"))
                    {
                        gv.Rows[row].Cells[col].Value = weigth;
                        ((BObject)gv.Rows[row].DataBoundItem).Save2();
                    }
                }
            }
        }

        private void btnVtulka_Click(object sender, EventArgs e)
        {
            if (_wprev <= 0) return;
            decimal weigth = _wprev / 100m;

            Log.Info("завес втулки: заказ id:{0},Number:{1},вес:{2}", _order.Id, _order.Number, weigth);

            if (dgOrderDetails.SelectedRows.Count == 1)
            {
                OrderDetail curDetail = (OrderDetail)dgOrderDetails.SelectedRows[0].DataBoundItem;
                curDetail.VtulkaWeight = weigth;
                curDetail.Save2();
            }
        }

        private void cmAddBrakOrderDetails_Click(object sender, EventArgs e)
        {
            if (dgOrderDetails.SelectedRows.Count != 1) return;
            OrderDetail od = (OrderDetail)dgOrderDetails.SelectedRows[0].DataBoundItem;
            OrderDetail odBrak = (OrderDetail)od.Clone();
            odBrak.ParentId = od.Id;
            odBrak.ProductName += "(брак)";
            odBrak.Barcode = _db.GetBarCode(WeigherSettings.Current.OtherSettings.BarPrefix, WeigherSettings.Current.OtherSettings.BarDigit);
            _order.OrderDetails.Add(odBrak);
        }

        private void dgOrderDetails_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dg = (DataGridView)sender;
            if (e.RowIndex > -1 && e.RowIndex < dg.RowCount)
            {
                DataGridViewRow dgRow = dg.Rows[e.RowIndex];
                if (dgRow.Selected)
                    dgRow.DefaultCellStyle.SelectionBackColor = ((OrderDetail)dgRow.DataBoundItem).ParentId > 0
                    ? Color.BlueViolet : Color.Blue;
                else
                    dgRow.DefaultCellStyle.BackColor = ((OrderDetail)dgRow.DataBoundItem).ParentId > 0
                        ? Color.LightBlue : Color.White;

            }
        }

        private void contextMenuOrderDetails_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

            if (dgOrderDetails.SelectedRows.Count != 1) return;
            OrderDetail od = (OrderDetail)dgOrderDetails.SelectedRows[0].DataBoundItem;
            if (od.ParentId > 0)
                e.Cancel = true;
            else
            {
                int odId = od.Id;
                if (_order.OrderDetails.Exists(ods => ods.ParentId == odId))
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgOrderDetails_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                dgOrderDetails.Rows[e.RowIndex].Selected = true;
            }
            catch
            {
                // ignored
            }
        }

        private void cbInterface_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbInterface_SelectedIndexChanged(object sender, EventArgs e)
        {
            var name = cbInterface.Text;
            var intf = _db.Interface.GetAll().FirstOrDefault(i => i.Name == name);
            if (intf != null)
            {
                _currentInterface = intf;
                //if (_currentInterface.UseBags)
                {
                    panel2.Visible = _currentInterface.UseBags;
                }
                //if (_currentInterface.UseSkleiki)
                {
                    panel1.Visible = _currentInterface.UseSkleiki;
                    Skleek.Visible = _currentInterface.UseSkleiki;
                }
                //if (_currentInterface.UseGroups)
                {
                    toolStripLabel1.Visible = _currentInterface.UseGroups;
                    cbGroupRoll.Visible = _currentInterface.UseGroups;
                }
                //if (_currentInterface.UseBrutto)
                {
                    cbPrintBrutto.Visible = _currentInterface.UseBrutto;
                    Mcarton.Visible = !_currentInterface.UseBrutto;
                    Mkrishki.Visible = !_currentInterface.UseBrutto;
                    Mstretch.Visible = !_currentInterface.UseBrutto;
                }
            }
        }
    }
}