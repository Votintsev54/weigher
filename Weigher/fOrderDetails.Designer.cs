﻿using WeigherData;

namespace Weigher
{
    partial class fOrderDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fOrderDetails));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripLabel();
            this.btnStudy = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cbGroupRoll = new System.Windows.Forms.ToolStripComboBox();
            this.tsbPrintGroupCheck = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.cbInterface = new System.Windows.Forms.ToolStripComboBox();
            this.dgOrderDetails = new System.Windows.Forms.DataGridView();
            this.specificationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productCharacteristicDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rollWidthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Shelf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VtulkaWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FixLen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FixArea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.contextMenuOrderDetails = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmAddBrakOrderDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.dgPackages = new System.Windows.Forms.DataGridView();
            this.packageNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TareWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mcarton = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mkrishki = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mstretch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgRolls = new System.Windows.Forms.DataGridView();
            this.rollNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rollBarcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Skleek = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rollWeightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rollBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnRandom = new System.Windows.Forms.Button();
            this.labelWeith = new System.Windows.Forms.Label();
            this.cbCheckCopies = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.chkChangeDP = new System.Windows.Forms.CheckBox();
            this.btnNextPackage = new System.Windows.Forms.Button();
            this.btnNextProduct = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cbPrintBrutto = new System.Windows.Forms.CheckBox();
            this.btnVtulka = new System.Windows.Forms.Button();
            this.radioRolls1 = new System.Windows.Forms.RadioButton();
            this.radioRolls2 = new System.Windows.Forms.RadioButton();
            this.radioRolls3 = new System.Windows.Forms.RadioButton();
            this.radioRolls4 = new System.Windows.Forms.RadioButton();
            this.radioRolls5 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.numRolsInBag = new System.Windows.Forms.NumericUpDown();
            this.numSkleek = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.radioSkleek3 = new System.Windows.Forms.RadioButton();
            this.radioSkleek2 = new System.Windows.Forms.RadioButton();
            this.radioSkleek1 = new System.Windows.Forms.RadioButton();
            this.radioSkleek0 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgOrderDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailBindingSource)).BeginInit();
            this.contextMenuOrderDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPackages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRolls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rollBindingSource)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRolsInBag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSkleek)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(64, 64);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2,
            this.btnStudy,
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.cbGroupRoll,
            this.tsbPrintGroupCheck,
            this.toolStripLabel2,
            this.cbInterface});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(1324, 71);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Enabled = false;
            this.toolStripButton2.Image = global::Weigher.Properties.Resources.balance;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(64, 68);
            // 
            // btnStudy
            // 
            this.btnStudy.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnStudy.CheckOnClick = true;
            this.btnStudy.Image = global::Weigher.Properties.Resources.kgpg;
            this.btnStudy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStudy.Name = "btnStudy";
            this.btnStudy.Size = new System.Drawing.Size(113, 68);
            this.btnStudy.Text = "Стадия";
            this.btnStudy.Click += new System.EventHandler(this.btnStudy_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::Weigher.Properties.Resources.Edit;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(68, 68);
            this.toolStripButton1.Text = "Печать спецификации";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 71);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(79, 68);
            this.toolStripLabel1.Text = "Гр-ть роллы:";
            // 
            // cbGroupRoll
            // 
            this.cbGroupRoll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGroupRoll.Name = "cbGroupRoll";
            this.cbGroupRoll.Size = new System.Drawing.Size(121, 71);
            this.cbGroupRoll.TextChanged += new System.EventHandler(this.cbGroupRoll_TextChanged);
            // 
            // tsbPrintGroupCheck
            // 
            this.tsbPrintGroupCheck.Image = global::Weigher.Properties.Resources.fileprint_9821;
            this.tsbPrintGroupCheck.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPrintGroupCheck.Name = "tsbPrintGroupCheck";
            this.tsbPrintGroupCheck.Size = new System.Drawing.Size(167, 68);
            this.tsbPrintGroupCheck.Text = "Этикетка группы";
            this.tsbPrintGroupCheck.Click += new System.EventHandler(this.tsbPrintGroupCheck_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(72, 68);
            this.toolStripLabel2.Text = "Интерфейс:";
            // 
            // cbInterface
            // 
            this.cbInterface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInterface.Name = "cbInterface";
            this.cbInterface.Size = new System.Drawing.Size(121, 71);
            this.cbInterface.SelectedIndexChanged += new System.EventHandler(this.cbInterface_SelectedIndexChanged);
            // 
            // dgOrderDetails
            // 
            this.dgOrderDetails.AllowUserToAddRows = false;
            this.dgOrderDetails.AllowUserToDeleteRows = false;
            this.dgOrderDetails.AllowUserToResizeRows = false;
            this.dgOrderDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgOrderDetails.AutoGenerateColumns = false;
            this.dgOrderDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgOrderDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.specificationDataGridViewTextBoxColumn,
            this.materialDataGridViewTextBoxColumn,
            this.clientDataGridViewTextBoxColumn,
            this.productNameDataGridViewTextBoxColumn,
            this.productCharacteristicDataGridViewTextBoxColumn,
            this.rollWidthDataGridViewTextBoxColumn,
            this.Shelf,
            this.VtulkaWeight,
            this.FixLen,
            this.FixArea,
            this.Column2});
            this.dgOrderDetails.DataSource = this.orderDetailBindingSource;
            this.dgOrderDetails.Location = new System.Drawing.Point(0, 74);
            this.dgOrderDetails.MultiSelect = false;
            this.dgOrderDetails.Name = "dgOrderDetails";
            this.dgOrderDetails.RowHeadersVisible = false;
            this.dgOrderDetails.RowTemplate.ContextMenuStrip = this.contextMenuOrderDetails;
            this.dgOrderDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgOrderDetails.ShowEditingIcon = false;
            this.dgOrderDetails.Size = new System.Drawing.Size(1222, 354);
            this.dgOrderDetails.TabIndex = 1;
            this.dgOrderDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgOrderDetails_CellEndEdit);
            this.dgOrderDetails.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.CellMouseDoubleClick);
            this.dgOrderDetails.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgOrderDetails_CellMouseDown);
            this.dgOrderDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgOrderDetails_DataError);
            this.dgOrderDetails.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgOrderDetails_RowPrePaint);
            this.dgOrderDetails.SelectionChanged += new System.EventHandler(this.dgOrderDetails_SelectionChanged);
            this.dgOrderDetails.Enter += new System.EventHandler(this.Grid_Enter);
            this.dgOrderDetails.Leave += new System.EventHandler(this.Grid_Leave);
            // 
            // specificationDataGridViewTextBoxColumn
            // 
            this.specificationDataGridViewTextBoxColumn.DataPropertyName = "Specification";
            this.specificationDataGridViewTextBoxColumn.HeaderText = "ТУ";
            this.specificationDataGridViewTextBoxColumn.Name = "specificationDataGridViewTextBoxColumn";
            this.specificationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // materialDataGridViewTextBoxColumn
            // 
            this.materialDataGridViewTextBoxColumn.DataPropertyName = "Material";
            this.materialDataGridViewTextBoxColumn.HeaderText = "Материал";
            this.materialDataGridViewTextBoxColumn.Name = "materialDataGridViewTextBoxColumn";
            this.materialDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // clientDataGridViewTextBoxColumn
            // 
            this.clientDataGridViewTextBoxColumn.DataPropertyName = "Client";
            this.clientDataGridViewTextBoxColumn.HeaderText = "Заказчик";
            this.clientDataGridViewTextBoxColumn.Name = "clientDataGridViewTextBoxColumn";
            this.clientDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // productNameDataGridViewTextBoxColumn
            // 
            this.productNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.productNameDataGridViewTextBoxColumn.DataPropertyName = "ProductName";
            this.productNameDataGridViewTextBoxColumn.HeaderText = "Наименование продукции";
            this.productNameDataGridViewTextBoxColumn.Name = "productNameDataGridViewTextBoxColumn";
            this.productNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // productCharacteristicDataGridViewTextBoxColumn
            // 
            this.productCharacteristicDataGridViewTextBoxColumn.DataPropertyName = "ProductCharacteristic";
            this.productCharacteristicDataGridViewTextBoxColumn.HeaderText = "Характеристика продукции";
            this.productCharacteristicDataGridViewTextBoxColumn.Name = "productCharacteristicDataGridViewTextBoxColumn";
            this.productCharacteristicDataGridViewTextBoxColumn.ReadOnly = true;
            this.productCharacteristicDataGridViewTextBoxColumn.Width = 150;
            // 
            // rollWidthDataGridViewTextBoxColumn
            // 
            this.rollWidthDataGridViewTextBoxColumn.DataPropertyName = "RollWidth";
            this.rollWidthDataGridViewTextBoxColumn.HeaderText = "Ширина ролла";
            this.rollWidthDataGridViewTextBoxColumn.Name = "rollWidthDataGridViewTextBoxColumn";
            this.rollWidthDataGridViewTextBoxColumn.ReadOnly = true;
            this.rollWidthDataGridViewTextBoxColumn.ToolTipText = "Ширина ролла";
            // 
            // Shelf
            // 
            this.Shelf.DataPropertyName = "Shelf";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Shelf.DefaultCellStyle = dataGridViewCellStyle1;
            this.Shelf.HeaderText = "Срок хранения";
            this.Shelf.Name = "Shelf";
            this.Shelf.ToolTipText = "Срок хранения";
            // 
            // VtulkaWeight
            // 
            this.VtulkaWeight.DataPropertyName = "VtulkaWeight";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.VtulkaWeight.DefaultCellStyle = dataGridViewCellStyle2;
            this.VtulkaWeight.HeaderText = "Вес втулки";
            this.VtulkaWeight.Name = "VtulkaWeight";
            this.VtulkaWeight.ToolTipText = "Двойной клик - взять с весов";
            // 
            // FixLen
            // 
            this.FixLen.DataPropertyName = "FixLen";
            this.FixLen.HeaderText = "Длина";
            this.FixLen.Name = "FixLen";
            this.FixLen.Visible = false;
            // 
            // FixArea
            // 
            this.FixArea.DataPropertyName = "FixArea";
            this.FixArea.HeaderText = "Площадь";
            this.FixArea.Name = "FixArea";
            this.FixArea.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Density";
            this.Column2.HeaderText = "Грамматура";
            this.Column2.Name = "Column2";
            // 
            // orderDetailBindingSource
            // 
            this.orderDetailBindingSource.DataSource = typeof(WeigherData.OrderDetail);
            // 
            // contextMenuOrderDetails
            // 
            this.contextMenuOrderDetails.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmAddBrakOrderDetails});
            this.contextMenuOrderDetails.Name = "contextMenuOrderDetails";
            this.contextMenuOrderDetails.Size = new System.Drawing.Size(224, 26);
            this.contextMenuOrderDetails.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuOrderDetails_Opening);
            // 
            // cmAddBrakOrderDetails
            // 
            this.cmAddBrakOrderDetails.Name = "cmAddBrakOrderDetails";
            this.cmAddBrakOrderDetails.Size = new System.Drawing.Size(223, 22);
            this.cmAddBrakOrderDetails.Text = "Добавить запись для брака";
            this.cmAddBrakOrderDetails.Click += new System.EventHandler(this.cmAddBrakOrderDetails_Click);
            // 
            // dgPackages
            // 
            this.dgPackages.AllowUserToAddRows = false;
            this.dgPackages.AllowUserToResizeRows = false;
            this.dgPackages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgPackages.AutoGenerateColumns = false;
            this.dgPackages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPackages.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.packageNumberDataGridViewTextBoxColumn,
            this.barcodeDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.TareWeight,
            this.Mcarton,
            this.Mkrishki,
            this.Mstretch});
            this.dgPackages.DataSource = this.packageBindingSource;
            this.dgPackages.Location = new System.Drawing.Point(673, 557);
            this.dgPackages.MultiSelect = false;
            this.dgPackages.Name = "dgPackages";
            this.dgPackages.RowHeadersVisible = false;
            this.dgPackages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPackages.Size = new System.Drawing.Size(549, 228);
            this.dgPackages.TabIndex = 3;
            this.dgPackages.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPackages_CellEndEdit);
            this.dgPackages.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.CellMouseDoubleClick);
            this.dgPackages.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgOrderDetails_DataError);
            this.dgPackages.SelectionChanged += new System.EventHandler(this.dgPackages_SelectionChanged);
            this.dgPackages.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgPackages_UserDeletingRow);
            this.dgPackages.Enter += new System.EventHandler(this.Grid_Enter);
            this.dgPackages.Leave += new System.EventHandler(this.Grid_Leave);
            // 
            // packageNumberDataGridViewTextBoxColumn
            // 
            this.packageNumberDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.packageNumberDataGridViewTextBoxColumn.DataPropertyName = "PackageNumber";
            this.packageNumberDataGridViewTextBoxColumn.HeaderText = "Номер палеты";
            this.packageNumberDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.packageNumberDataGridViewTextBoxColumn.Name = "packageNumberDataGridViewTextBoxColumn";
            // 
            // barcodeDataGridViewTextBoxColumn
            // 
            this.barcodeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.barcodeDataGridViewTextBoxColumn.DataPropertyName = "Barcode";
            this.barcodeDataGridViewTextBoxColumn.HeaderText = "Штрихкод";
            this.barcodeDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.barcodeDataGridViewTextBoxColumn.Name = "barcodeDataGridViewTextBoxColumn";
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Дата";
            this.dateDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            // 
            // TareWeight
            // 
            this.TareWeight.DataPropertyName = "TareWeight";
            this.TareWeight.HeaderText = "Масса тары";
            this.TareWeight.Name = "TareWeight";
            this.TareWeight.ToolTipText = "Масса тары";
            // 
            // Mcarton
            // 
            this.Mcarton.DataPropertyName = "Mcarton";
            this.Mcarton.HeaderText = "Масса картона";
            this.Mcarton.Name = "Mcarton";
            this.Mcarton.ToolTipText = "Масса картона";
            // 
            // Mkrishki
            // 
            this.Mkrishki.DataPropertyName = "Mkrishki";
            this.Mkrishki.HeaderText = "Масса крышки";
            this.Mkrishki.Name = "Mkrishki";
            this.Mkrishki.ToolTipText = "Масса крышки";
            // 
            // Mstretch
            // 
            this.Mstretch.DataPropertyName = "Mstretch";
            this.Mstretch.HeaderText = "Масса стрейча";
            this.Mstretch.Name = "Mstretch";
            this.Mstretch.ToolTipText = "Масса стрейча";
            // 
            // packageBindingSource
            // 
            this.packageBindingSource.DataSource = typeof(WeigherData.Package);
            // 
            // dgRolls
            // 
            this.dgRolls.AllowUserToAddRows = false;
            this.dgRolls.AllowUserToResizeRows = false;
            this.dgRolls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dgRolls.AutoGenerateColumns = false;
            this.dgRolls.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRolls.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rollNumberDataGridViewTextBoxColumn,
            this.rollBarcodeDataGridViewTextBoxColumn,
            this.Column3,
            this.Skleek,
            this.dataGridViewTextBoxColumn1,
            this.rollWeightDataGridViewTextBoxColumn});
            this.dgRolls.DataSource = this.rollBindingSource;
            this.dgRolls.Location = new System.Drawing.Point(0, 557);
            this.dgRolls.Name = "dgRolls";
            this.dgRolls.RowHeadersVisible = false;
            this.dgRolls.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRolls.Size = new System.Drawing.Size(533, 228);
            this.dgRolls.TabIndex = 2;
            this.dgRolls.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRolls_CellEndEdit);
            this.dgRolls.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgOrderDetails_DataError);
            this.dgRolls.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgRolls_UserDeletingRow);
            this.dgRolls.Enter += new System.EventHandler(this.Grid_Enter);
            this.dgRolls.Leave += new System.EventHandler(this.Grid_Leave);
            // 
            // rollNumberDataGridViewTextBoxColumn
            // 
            this.rollNumberDataGridViewTextBoxColumn.DataPropertyName = "RollNumber";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.rollNumberDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.rollNumberDataGridViewTextBoxColumn.HeaderText = "Номер ролла";
            this.rollNumberDataGridViewTextBoxColumn.Name = "rollNumberDataGridViewTextBoxColumn";
            this.rollNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.rollNumberDataGridViewTextBoxColumn.Width = 90;
            // 
            // rollBarcodeDataGridViewTextBoxColumn
            // 
            this.rollBarcodeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rollBarcodeDataGridViewTextBoxColumn.DataPropertyName = "RollBarcode";
            this.rollBarcodeDataGridViewTextBoxColumn.HeaderText = "Штрихкод";
            this.rollBarcodeDataGridViewTextBoxColumn.Name = "rollBarcodeDataGridViewTextBoxColumn";
            this.rollBarcodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "RollCount";
            this.Column3.HeaderText = "Ролов";
            this.Column3.Name = "Column3";
            this.Column3.ToolTipText = "Взвешено вместе ролов";
            this.Column3.Width = 60;
            // 
            // Skleek
            // 
            this.Skleek.DataPropertyName = "Skleek";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Skleek.DefaultCellStyle = dataGridViewCellStyle4;
            this.Skleek.HeaderText = "Склеек";
            this.Skleek.Name = "Skleek";
            this.Skleek.ToolTipText = "Количество склеек в роле";
            this.Skleek.Width = 90;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "VtulkaWeight";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn1.HeaderText = "Вес втулки";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ToolTipText = "Вес втулки";
            // 
            // rollWeightDataGridViewTextBoxColumn
            // 
            this.rollWeightDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rollWeightDataGridViewTextBoxColumn.DataPropertyName = "RollWeight";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.rollWeightDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.rollWeightDataGridViewTextBoxColumn.HeaderText = "Вес";
            this.rollWeightDataGridViewTextBoxColumn.Name = "rollWeightDataGridViewTextBoxColumn";
            this.rollWeightDataGridViewTextBoxColumn.ReadOnly = true;
            this.rollWeightDataGridViewTextBoxColumn.ToolTipText = "Двойной клик - взять с весов";
            // 
            // rollBindingSource
            // 
            this.rollBindingSource.DataSource = typeof(WeigherData.Roll);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 789);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1324, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // btnRandom
            // 
            this.btnRandom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRandom.Location = new System.Drawing.Point(1233, 740);
            this.btnRandom.Name = "btnRandom";
            this.btnRandom.Size = new System.Drawing.Size(79, 45);
            this.btnRandom.TabIndex = 7;
            this.btnRandom.Text = "Случ. вес";
            this.btnRandom.UseVisualStyleBackColor = true;
            this.btnRandom.Click += new System.EventHandler(this.button4_Click);
            // 
            // labelWeith
            // 
            this.labelWeith.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelWeith.AutoSize = true;
            this.labelWeith.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelWeith.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelWeith.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelWeith.Location = new System.Drawing.Point(548, 671);
            this.labelWeith.Name = "labelWeith";
            this.labelWeith.Size = new System.Drawing.Size(119, 44);
            this.labelWeith.TabIndex = 8;
            this.labelWeith.Text = "00.00";
            // 
            // cbCheckCopies
            // 
            this.cbCheckCopies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCheckCopies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCheckCopies.FormattingEnabled = true;
            this.cbCheckCopies.Location = new System.Drawing.Point(1232, 270);
            this.cbCheckCopies.Name = "cbCheckCopies";
            this.cbCheckCopies.Size = new System.Drawing.Size(88, 26);
            this.cbCheckCopies.TabIndex = 9;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(1233, 222);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(91, 42);
            this.textBox1.TabIndex = 10;
            this.textBox1.Text = "Печатать этикеток:";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chkChangeDP
            // 
            this.chkChangeDP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkChangeDP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkChangeDP.Location = new System.Drawing.Point(1232, 302);
            this.chkChangeDP.Name = "chkChangeDP";
            this.chkChangeDP.Size = new System.Drawing.Size(93, 69);
            this.chkChangeDP.TabIndex = 11;
            this.chkChangeDP.Text = "Изменить длину и площадь у всех ролей";
            this.chkChangeDP.UseVisualStyleBackColor = true;
            this.chkChangeDP.CheckedChanged += new System.EventHandler(this.chkChangeDP_CheckedChanged);
            // 
            // btnNextPackage
            // 
            this.btnNextPackage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextPackage.AutoEllipsis = true;
            this.btnNextPackage.Image = global::Weigher.Properties.Resources.down64plus;
            this.btnNextPackage.Location = new System.Drawing.Point(1228, 557);
            this.btnNextPackage.Name = "btnNextPackage";
            this.btnNextPackage.Size = new System.Drawing.Size(92, 83);
            this.btnNextPackage.TabIndex = 5;
            this.btnNextPackage.TabStop = false;
            this.btnNextPackage.UseVisualStyleBackColor = true;
            this.btnNextPackage.Click += new System.EventHandler(this.btnNextPackage_Click);
            // 
            // btnNextProduct
            // 
            this.btnNextProduct.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextProduct.AutoEllipsis = true;
            this.btnNextProduct.Image = global::Weigher.Properties.Resources.down64;
            this.btnNextProduct.Location = new System.Drawing.Point(1228, 392);
            this.btnNextProduct.Name = "btnNextProduct";
            this.btnNextProduct.Size = new System.Drawing.Size(92, 83);
            this.btnNextProduct.TabIndex = 4;
            this.btnNextProduct.TabStop = false;
            this.btnNextProduct.UseVisualStyleBackColor = true;
            this.btnNextProduct.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.AutoEllipsis = true;
            this.button1.Image = global::Weigher.Properties.Resources.fileprint_9821;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(0, 481);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(159, 70);
            this.button1.TabIndex = 4;
            this.button1.TabStop = false;
            this.button1.Text = "Этикетки";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.tsbPrintCheck_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.AutoEllipsis = true;
            this.button2.Image = global::Weigher.Properties.Resources.fileprint_9821;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(673, 481);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(159, 70);
            this.button2.TabIndex = 4;
            this.button2.TabStop = false;
            this.button2.Text = "Палета";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.tsbPrint_Click);
            // 
            // cbPrintBrutto
            // 
            this.cbPrintBrutto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbPrintBrutto.AutoSize = true;
            this.cbPrintBrutto.Location = new System.Drawing.Point(857, 506);
            this.cbPrintBrutto.Name = "cbPrintBrutto";
            this.cbPrintBrutto.Size = new System.Drawing.Size(203, 22);
            this.cbPrintBrutto.TabIndex = 12;
            this.cbPrintBrutto.Text = "Печатать с весом брутто";
            this.cbPrintBrutto.UseVisualStyleBackColor = true;
            // 
            // btnVtulka
            // 
            this.btnVtulka.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVtulka.AutoEllipsis = true;
            this.btnVtulka.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVtulka.Location = new System.Drawing.Point(548, 731);
            this.btnVtulka.Name = "btnVtulka";
            this.btnVtulka.Size = new System.Drawing.Size(119, 54);
            this.btnVtulka.TabIndex = 4;
            this.btnVtulka.TabStop = false;
            this.btnVtulka.Text = "Взвесить втулку";
            this.btnVtulka.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnVtulka.UseVisualStyleBackColor = true;
            this.btnVtulka.Click += new System.EventHandler(this.btnVtulka_Click);
            // 
            // radioRolls1
            // 
            this.radioRolls1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioRolls1.AutoSize = true;
            this.radioRolls1.Checked = true;
            this.radioRolls1.Location = new System.Drawing.Point(4, 23);
            this.radioRolls1.Name = "radioRolls1";
            this.radioRolls1.Size = new System.Drawing.Size(30, 28);
            this.radioRolls1.TabIndex = 13;
            this.radioRolls1.TabStop = true;
            this.radioRolls1.Text = " 1";
            this.radioRolls1.UseVisualStyleBackColor = true;
            // 
            // radioRolls2
            // 
            this.radioRolls2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioRolls2.AutoSize = true;
            this.radioRolls2.Location = new System.Drawing.Point(40, 23);
            this.radioRolls2.Name = "radioRolls2";
            this.radioRolls2.Size = new System.Drawing.Size(30, 28);
            this.radioRolls2.TabIndex = 14;
            this.radioRolls2.Text = " 2";
            this.radioRolls2.UseVisualStyleBackColor = true;
            // 
            // radioRolls3
            // 
            this.radioRolls3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioRolls3.AutoSize = true;
            this.radioRolls3.Location = new System.Drawing.Point(76, 23);
            this.radioRolls3.Name = "radioRolls3";
            this.radioRolls3.Size = new System.Drawing.Size(30, 28);
            this.radioRolls3.TabIndex = 15;
            this.radioRolls3.Text = " 3";
            this.radioRolls3.UseVisualStyleBackColor = true;
            // 
            // radioRolls4
            // 
            this.radioRolls4.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioRolls4.AutoSize = true;
            this.radioRolls4.Location = new System.Drawing.Point(112, 23);
            this.radioRolls4.Name = "radioRolls4";
            this.radioRolls4.Size = new System.Drawing.Size(30, 28);
            this.radioRolls4.TabIndex = 16;
            this.radioRolls4.Text = " 4";
            this.radioRolls4.UseVisualStyleBackColor = true;
            // 
            // radioRolls5
            // 
            this.radioRolls5.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioRolls5.AutoSize = true;
            this.radioRolls5.Location = new System.Drawing.Point(148, 23);
            this.radioRolls5.Name = "radioRolls5";
            this.radioRolls5.Size = new System.Drawing.Size(30, 28);
            this.radioRolls5.TabIndex = 17;
            this.radioRolls5.Text = " 5";
            this.radioRolls5.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 18);
            this.label2.TabIndex = 19;
            this.label2.Text = "Количество, ролов в сумке:";
            // 
            // numRolsInBag
            // 
            this.numRolsInBag.Location = new System.Drawing.Point(184, 26);
            this.numRolsInBag.Name = "numRolsInBag";
            this.numRolsInBag.Size = new System.Drawing.Size(41, 24);
            this.numRolsInBag.TabIndex = 20;
            // 
            // numSkleek
            // 
            this.numSkleek.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.numSkleek.Location = new System.Drawing.Point(141, 22);
            this.numSkleek.Name = "numSkleek";
            this.numSkleek.Size = new System.Drawing.Size(41, 24);
            this.numSkleek.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 18);
            this.label1.TabIndex = 26;
            this.label1.Text = "Количество склеек:";
            // 
            // radioSkleek3
            // 
            this.radioSkleek3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radioSkleek3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioSkleek3.AutoSize = true;
            this.radioSkleek3.Location = new System.Drawing.Point(105, 22);
            this.radioSkleek3.Name = "radioSkleek3";
            this.radioSkleek3.Size = new System.Drawing.Size(30, 28);
            this.radioSkleek3.TabIndex = 24;
            this.radioSkleek3.Text = " 3";
            this.radioSkleek3.UseVisualStyleBackColor = true;
            // 
            // radioSkleek2
            // 
            this.radioSkleek2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radioSkleek2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioSkleek2.AutoSize = true;
            this.radioSkleek2.Location = new System.Drawing.Point(73, 22);
            this.radioSkleek2.Name = "radioSkleek2";
            this.radioSkleek2.Size = new System.Drawing.Size(26, 28);
            this.radioSkleek2.TabIndex = 23;
            this.radioSkleek2.Text = "2";
            this.radioSkleek2.UseVisualStyleBackColor = true;
            // 
            // radioSkleek1
            // 
            this.radioSkleek1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radioSkleek1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioSkleek1.AutoSize = true;
            this.radioSkleek1.Location = new System.Drawing.Point(41, 22);
            this.radioSkleek1.Name = "radioSkleek1";
            this.radioSkleek1.Size = new System.Drawing.Size(26, 28);
            this.radioSkleek1.TabIndex = 22;
            this.radioSkleek1.Text = "1";
            this.radioSkleek1.UseVisualStyleBackColor = true;
            // 
            // radioSkleek0
            // 
            this.radioSkleek0.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radioSkleek0.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioSkleek0.AutoSize = true;
            this.radioSkleek0.Checked = true;
            this.radioSkleek0.Location = new System.Drawing.Point(5, 22);
            this.radioSkleek0.Name = "radioSkleek0";
            this.radioSkleek0.Size = new System.Drawing.Size(30, 28);
            this.radioSkleek0.TabIndex = 21;
            this.radioSkleek0.TabStop = true;
            this.radioSkleek0.Text = " 0";
            this.radioSkleek0.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.radioSkleek0);
            this.panel1.Controls.Add(this.numSkleek);
            this.panel1.Controls.Add(this.radioSkleek1);
            this.panel1.Controls.Add(this.radioSkleek2);
            this.panel1.Controls.Add(this.radioSkleek3);
            this.panel1.Location = new System.Drawing.Point(222, 443);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(189, 50);
            this.panel1.TabIndex = 28;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.Controls.Add(this.radioRolls4);
            this.panel2.Controls.Add(this.radioRolls1);
            this.panel2.Controls.Add(this.radioRolls2);
            this.panel2.Controls.Add(this.numRolsInBag);
            this.panel2.Controls.Add(this.radioRolls3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.radioRolls5);
            this.panel2.Location = new System.Drawing.Point(222, 494);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(231, 57);
            this.panel2.TabIndex = 28;
            // 
            // fOrderDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1324, 811);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cbPrintBrutto);
            this.Controls.Add(this.chkChangeDP);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.cbCheckCopies);
            this.Controls.Add(this.labelWeith);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnRandom);
            this.Controls.Add(this.btnNextPackage);
            this.Controls.Add(this.btnVtulka);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnNextProduct);
            this.Controls.Add(this.dgRolls);
            this.Controls.Add(this.dgPackages);
            this.Controls.Add(this.dgOrderDetails);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1120, 746);
            this.Name = "fOrderDetails";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Заказ №";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.fOrderDetails_FormClosed);
            this.Load += new System.EventHandler(this.fOrderDetails_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgOrderDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailBindingSource)).EndInit();
            this.contextMenuOrderDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPackages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.packageBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRolls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rollBindingSource)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRolsInBag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSkleek)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripButton2;
        private System.Windows.Forms.DataGridView dgOrderDetails;
        private System.Windows.Forms.DataGridView dgPackages;
        private System.Windows.Forms.DataGridView dgRolls;
        private System.Windows.Forms.BindingSource orderDetailBindingSource;
        private System.Windows.Forms.BindingSource packageBindingSource;
        private System.Windows.Forms.BindingSource rollBindingSource;
        private System.Windows.Forms.Button btnNextProduct;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnRandom;
        private System.Windows.Forms.Button btnNextPackage;
        private System.Windows.Forms.ToolStripButton btnStudy;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox cbGroupRoll;
        private System.Windows.Forms.ToolStripButton tsbPrintGroupCheck;
        private System.Windows.Forms.Label labelWeith;
        private System.Windows.Forms.ComboBox cbCheckCopies;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.CheckBox chkChangeDP;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox cbPrintBrutto;
        private System.Windows.Forms.Button btnVtulka;
        private System.Windows.Forms.DataGridViewTextBoxColumn specificationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productCharacteristicDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rollWidthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Shelf;
        private System.Windows.Forms.DataGridViewTextBoxColumn VtulkaWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn FixLen;
        private System.Windows.Forms.DataGridViewTextBoxColumn FixArea;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.RadioButton radioRolls1;
        private System.Windows.Forms.RadioButton radioRolls2;
        private System.Windows.Forms.RadioButton radioRolls3;
        private System.Windows.Forms.RadioButton radioRolls4;
        private System.Windows.Forms.RadioButton radioRolls5;
        private System.Windows.Forms.ContextMenuStrip contextMenuOrderDetails;
        private System.Windows.Forms.ToolStripMenuItem cmAddBrakOrderDetails;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numRolsInBag;
        private System.Windows.Forms.NumericUpDown numSkleek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioSkleek3;
        private System.Windows.Forms.RadioButton radioSkleek2;
        private System.Windows.Forms.RadioButton radioSkleek1;
        private System.Windows.Forms.RadioButton radioSkleek0;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rollNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rollBarcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Skleek;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rollWeightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn packageNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn barcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TareWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mcarton;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mkrishki;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mstretch;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox cbInterface;
        private System.Windows.Forms.Panel panel2;
    }
}