﻿using System;
using System.Windows.Forms;

namespace Weigher
{
    public partial class fAskPwd: Form
    {
        public static bool AskPwd()
        {
            if (string.IsNullOrEmpty(WeigherSettings.Current.OtherSettings.SettingPwd))
                return true;
            fAskPwd f = new fAskPwd();
            return f.ShowDialog() == DialogResult.OK; 
        }

        public fAskPwd()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != WeigherSettings.Current.OtherSettings.SettingPwd)
            {
                MessageBox.Show("Не верный пароль!", "Ошибка...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult = DialogResult.Cancel;
            }
        }

    }
}
