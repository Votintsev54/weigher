﻿using System;
using System.Linq;
using System.Windows.Forms;
using WeigherData;
using DbCommon;

namespace Weigher
{
    public partial class fPackers: Form
    {
        public fPackers(CDatabaseWeigher db)
        {
            this.db = db;
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            fSettings f = new fSettings();
            f.ShowDialog();
        }

        CDatabaseWeigher db = new CDatabaseWeigher();
        SortList<Packer> plist;

        private void fOrders_Load(object sender, EventArgs e)
        {
                plist = db.Packer.GetAll();
                dataGridView1.DataSource = plist;
                tbNom.Text = "";
                tbName.Text = "";

                if (!fAskPwd.AskPwd()) Close();    
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string nom = tbNom.Text.Trim();
            if (nom == "")
            {
                MessageBox.Show("Номер - поле не должно быть пустым!", "Внимание...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string name= tbName.Text.Trim();
            if (name == "")
            {
                MessageBox.Show("ФИО - поле не должно быть пустым!", "Внимание...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Packer pex = plist.FirstOrDefault(p0 => p0.Nom == nom);
            if (pex != null)
            {
                MessageBox.Show("Упаковщик с таким номером уже существует!", "Ошибка...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Packer p = db.Packer.NewInstance();
            p.Nom = tbNom.Text;
            p.Name = tbName.Text;
            p.Save2();
            plist.Insert(0, p);

        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            string nom = tbNom.Text.Trim();
            if (nom == "")
            {
                MessageBox.Show("Номер - поле не должно быть пустым!", "Внимание...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string name = tbName.Text.Trim();
            if (name == "")
            {
                MessageBox.Show("ФИО - поле не должно быть пустым!", "Внимание...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Packer pex = plist.FirstOrDefault(p0 => p0.Nom == nom);
            Packer p = (Packer)dataGridView1.SelectedRows[0].DataBoundItem;
            if (pex != null)
            {
                if (pex != p)
                {
                    MessageBox.Show("Упаковщик с таким номером уже существует!", "Ошибка...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            p.Nom = tbNom.Text;
            p.Name = tbName.Text;
            p.Save2();
            this.Refresh();

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                Packer p = (Packer)dataGridView1.SelectedRows[0].DataBoundItem;
                tbNom.Text = p.Nom;
                 tbName.Text = p.Name;
            }
        }


    }
}
