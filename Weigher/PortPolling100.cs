﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using DbCommon.Helpers;
using MassaProtokol;
using WpfWeigher.Models;

namespace Weigher
{
    public class PortPolling100 { }
    public partial class fOrderDetails
    {

        public KeyConfig mKeys = new KeyConfig();
        ScaleForm scaleForm;

        /// <summary>
        /// Опрос ком порта
        /// </summary>
        private void PortPolling100(bool IsP100)
        {
            ScaleSettings scaleSettings = WeigherSettings.Current.ScaleSettings;
            ScaleData scaleData = new ScaleData() { Interrface = scaleSettings.ScaleInterface == ScaleInterface.Comport ? 1 : 2 };
            scaleData.IsP100 = IsP100;
            scaleData.Is57600 = scaleSettings.SerialSettings.BaudRate==57600;
            scaleData.IsScale = true;
            if (scaleSettings.Ip != null && !scaleSettings.Ip.Equals(IPAddress.None))
            {
                scaleData.Connection = $"{scaleSettings.Ip}:{scaleSettings.Port}";
            }
            else
            {
                scaleData.Connection = scaleSettings.SerialSettings.ComPort;
            }
            ScaleInfo scaleInfo = new ScaleInfo(mKeys);
            scaleInfo.LoadFromTCPSearch(scaleData);
            scaleForm = new ScaleForm(scaleInfo);
            scaleForm.OnWeigthChange += ScaleForm_OnWeigthChange;
            scaleForm.OnWeigthError += ScaleForm_OnWeigthError;
            scaleForm.ScaleForm_Load();

        }

        private void ScaleForm_OnWeigthError(object sender, EventArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                toolStripButton2.Enabled = false;
            }));
        }

        private void ScaleForm_OnWeigthChange(object sender, EventArgs e)
        {
            _wprev = (decimal)scaleForm.mWeight.GetDoubleWeight() * 100;
            var displayw = (_wprev / 100m).ToString(CultureInfo.InvariantCulture);
            BeginInvoke(new Action(() =>
            {
                toolStripButton2.Enabled = true;
                toolStripStatusLabel2.Text = @"Текущий вес: " + scaleForm.tWeight;
                labelWeith.Text = displayw;
                labelWeith.Refresh();
            }));
        }


    }
}
