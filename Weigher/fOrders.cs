﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WeigherData;
using System.IO;
using Excel;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using DbCommon;
using DbCommon.Helpers;
using WpfWeigher.Views;
using System.Collections.Generic;

namespace Weigher
{
    public partial class fOrders : Form
        , IFilter<Orders>
    {
        CDatabaseWeigher _db;
        string _errorMessage = null;
        bool _readOnly = false;
        public fOrders()
        {
            InitializeComponent();
            var ws = WeigherSettings.ReadFromFile();
            var sst = ws.SqlSettings;
            if (sst != null)
            {
                _db = sst.GetDatabase();
                if (_db != null)
                {
                    if (!TestDbVersion())
                    {
                        _db = null;
                    }
                }
            }
            _readOnly = ws.OtherSettings.ReadOnlyMode;
        }

        private bool TestDbVersion()
        {
            try
            {
                var vList = _db.DbVersion.GetAll();
                DbVersion vers = _db.DbVersion.GetAll().FirstOrDefault();
                if (vers == null)
                {
                    vers = _db.DbVersion.NewInstance();
                    vers.Name = CTableDbVersion.CurrentDbVersionConst;
                    vers.Save2();
                }
                if (string.Compare(vers.Name, CTableDbVersion.CurrentDbVersionConst) < 0)
                {
                    var msgres = MessageBox.Show("Обновилась структура БД или печ.формы. Выполнить обновление ?", "Внимание!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (msgres != DialogResult.OK)
                        return false;
                }

                // ReSharper disable once StringCompareIsCultureSpecific.1
                if (string.Compare(vers.Name, "vers. 20180313") < 0)
                {
                    if (_db.DatabaseType == DatabaseType.MsSql)
                    {
                        _db.ExecCmd("alter table OrderDetail alter column Density decimal(18,5)");
                        _db.ExecCmd("alter table Roll add VtulkaWeight decimal(18,3)");
                    }
                    if (_db.DatabaseType == DatabaseType.PostgreeSql)
                    {
                        _db.ExecCmd("alter table Roll add VtulkaWeight numeric");
                    }
                    vers.Name = "vers. 20180313";
                    vers.Save2();
                }
                // ReSharper disable once StringCompareIsCultureSpecific.1
                if (string.Compare(vers.Name, "vers. 20180314") < 0)
                {
                    if (_db.DatabaseType == DatabaseType.MsSql)
                    {
                        _db.ExecCmd("alter table Roll add BruttoWeight decimal(18,3)");
                    }
                    if (_db.DatabaseType == DatabaseType.PostgreeSql)
                    {
                        try
                        {
                            _db.ExecCmd("alter table Roll add BruttoWeight numeric");
                        }
                        catch
                        {
                            // ignored
                        }
                    }
                    _db.ExecCmd("update Roll set VtulkaWeight = (select VtulkaWeight from OrderDetail o where o.id=OrderDetailId) ");
                    _db.ExecCmd("update Roll set BruttoWeight = VtulkaWeight + RollWeight");
                    vers.Name = "vers. 20180314";
                    vers.Save2();
                }
                // ReSharper disable once StringCompareIsCultureSpecific.1
                if (string.Compare(vers.Name, "vers. 20180330") < 0)
                {
                    _db.ExecCmd("alter table Roll add RollCount int");
                    _db.ExecCmd("update Roll set RollCount = 1 ");
                    vers.Name = "vers. 20180330";
                    vers.Save2();
                }
                // ReSharper disable once StringCompareIsCultureSpecific.1
                if (string.Compare(vers.Name, "vers. 20180331") < 0)
                {
                    _db.ExecCmd("alter table  OrderDetail add ParentId int");
                    vers.Name = "vers. 20180331";
                    vers.Save2();
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionWithLangIdConst) < 0)
                {
                    try
                    {
                        _db.Lang.Create();
                    }
                    catch (Exception) { };
                    try
                    {
                        _db.ExecCmd("alter table  Report add LangId nvarchar(5)"); /// исправлено
					}
                    catch (Exception) { };
                    vers.Name = CTableDbVersion.DbVersionWithLangIdConst;
                    vers.Save2();
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionIncorrect) == 0)
                    vers.Name = CTableDbVersion.DbVersionWithLangIdConst;
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionWithSkleekConst) < 0)
                {
                    try
                    {
                        _db.ExecCmd("alter table Roll add Skleek int");
                        _db.ExecCmd("update Roll set Skleek = 0");
                        _db.ExecCmd("alter table Roll alter column Skleek int not null");
                        _db.ExecCmd("create default def_Skleek as 0");
                        _db.ExecCmd("exec sp_bindefault 'def_Skleek', 'Roll.Skleek'");

                        _db.ExecCmd("alter table Package add Mcarton decimal(18,3)");
                        _db.ExecCmd("alter table Package add Mkrishki decimal(18,3)");
                        _db.ExecCmd("alter table Package add Mstretch decimal(18,3)");
                        _db.ExecCmd("update Package set Mcarton = 0, Mkrishki=0,Mstretch=0");

                        _db.ExecCmd("alter table Package alter column Mcarton decimal(18, 3) not null");
                        _db.ExecCmd("alter table Package alter column Mkrishki decimal(18, 3) not null");
                        _db.ExecCmd("alter table Package alter column Mstretch decimal(18, 3) not null");

                        _db.ExecCmd("create default def_Massa as 0");
                        _db.ExecCmd("exec sp_bindefault 'def_Massa', 'Package.Mcarton'");
                        _db.ExecCmd("exec sp_bindefault 'def_Massa', 'Package.Mkrishki'");
                        _db.ExecCmd("exec sp_bindefault 'def_Massa', 'Package.Mstretch'");
                    }
                    catch { }

                    vers.Name = CTableDbVersion.DbVersionWithSkleekConst;
                    vers.Save2();
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionCustomersTable) < 0)
                {
                    _db.Customer.Create2();
                    _db.GroupCustomer.Create2();
                    _db.ExecCmd("insert into [dbo].[Customer](Id,Name) select NEWID(),Client from [dbo].[OrderDetail] group by Client");
                    _db.ExecCmd(@"update[dbo].[Customer] set GroupCustomerId = '5EABD40D-F33D-4440-B092-25A6F77836B0' where Name like N'%проксима%'");

                    _db.ExecCmd(@"alter table[dbo].[OrderDetail] add CustomerId uniqueidentifier");
                    _db.ExecCmd(@"update[dbo].[OrderDetail]  set CustomerId = (select Id from[dbo].[Customer] where Name = Client)");
                    _db.ExecCmd(@"alter table[dbo].[OrderDetail] alter column CustomerId uniqueidentifier NOT NULL");
                    _db.ExecCmd(@"alter table[dbo].[OrderDetail] drop column Client");

                    _db.ExecCmd(@"alter table[dbo].[Report] add GroupCustomerId uniqueidentifier");
                    _db.ExecCmd(@"update[dbo].[Report] set Name = N'Этикетки', GroupCustomerId = '5EABD40D-F33D-4440-B092-25A6F77836B0' where Name = N'Проксима:Этикетки'");
                    _db.ExecCmd(@"update[dbo].[Report] set Name = N'Упаковочный лист', GroupCustomerId = '5EABD40D-F33D-4440-B092-25A6F77836B0' where Name = N'Проксима:Упаковочный лист'");

                    vers.Name = CTableDbVersion.DbVersionCustomersTable;
                    vers.Save2();
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionInterfaceTable) < 0)
                {
                    _db.Orders.RevisionOnAddColumn();
                    _db.Customer.RevisionOnAddColumn();
                    _db.GroupCustomer.RevisionOnAddColumn();
                    _db.Report.RevisionOnAddColumn();
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionInterfaceTable2) < 0)
                {
                    _db.Interface.Create2();
                    _db.GroupCustomer.RevisionOnAddColumn();
                    _db.ExecCmd(@"update[dbo].[Report] set Name = N'Этикетки', GroupCustomerId = '5EABD40D-F33D-4440-B092-25A6F77836B0' where Name = N'Проксима:Этикетки'");
                    _db.ExecCmd(@"update[dbo].[Report] set Name = N'Упаковочный лист', GroupCustomerId = '5EABD40D-F33D-4440-B092-25A6F77836B0' where Name = N'Проксима:Упаковочный лист'");
                    _db.ExecCmd(@"update[dbo].[Report] set GroupCustomerId = '5EABD40D-F33D-4440-B092-25A6F77836B0',InterfaceId = 2 where Comment like '%Proksima'");
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionInterfaceTable5) < 0)
                {
                    _db.Interface.Truncate();
                    _db.ColumnsMap.Truncate();
                    _db.Interface.RestorePredefined();
                    _db.ColumnsMap.RestorePredefined();
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionClientBack) < 0)
                {
                    try
                    {
                        _db.ExecCmd(@"create function dbo.GetClientName(@UID uniqueidentifier)
returns nvarchar(500)
as
begin
	return (select Name from [dbo].[Customer] where id = @UID)
end;");
                        _db.ExecCmd(@"alter table [dbo].[OrderDetail] add Client as dbo.GetClientName(CustomerId)");
                    }
                    catch { }

                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionBaikalSi2) < 0)
                {
                    _db.Interface.Truncate();
                    _db.Interface.RestorePredefined();
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersionUpdateble) < 0)
                {
                    _db.OrderDetailLog.Create();
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersion2023) < 0)
                {
                    _db.ExecCmd(@"update dbo.ColumnsMap set ExcelName = N'Обозначение материала' where Name=N'PackThick'");
                    _db.ExecCmd(@"update dbo.OrderDetail set PackThick = N''");
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersion2023) < 0)
                {
                    _db.ExecCmd(@"delete r from dbo.Roll r where not exists(select * from dbo.OrderDetail where Id =r.OrderDetailId)");
                    _db.ExecCmd(@"if not exists(select * from sysobjects where name = 'FK_Roll_OrderDetail')
	ALTER TABLE dbo.Roll ADD CONSTRAINT FK_Roll_OrderDetail FOREIGN KEY (OrderDetailId)
	REFERENCES dbo.OrderDetail(ID) ON DELETE CASCADE ON UPDATE CASCADE");

                    _db.ExecCmd(@"delete r from dbo.OrderDetail r where not exists(select * from dbo.Orders where Id = r.OrderId)");
                    _db.ExecCmd(@"if not exists(select * from sysobjects where name = 'FK_OrderDetail_Orders')
    ALTER TABLE dbo.OrderDetail ADD CONSTRAINT FK_OrderDetail_Orders FOREIGN KEY(OrderId)
    REFERENCES dbo.Orders(ID) ON DELETE CASCADE ON UPDATE CASCADE");

                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersion2023_4) < 0)
                {
                    _db.Interface.Truncate();
                    _db.Interface.RestorePredefined();
                    _db.RemovedRoll.Create();
                    _db.ExecCmd(@"CREATE TRIGGER dbo.TrDeleteRoll
   ON  dbo.Roll AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	if (exists(select * from deleted where acc1c is not null))
		INSERT INTO dbo.RemovedRoll(Id,OrderDetailId,PackageId,RollNumber,FrameCount,RollWeight,RollBarcode,VtulkaWeight
			   ,BruttoWeight,RollCount,Skleek,acc1c,RemoveData)
		 SELECT Id,OrderDetailId,PackageId,RollNumber,FrameCount,RollWeight,RollBarcode,VtulkaWeight
			   ,BruttoWeight,RollCount,Skleek,acc1c,CURRENT_TIMESTAMP
			   from deleted where acc1c is not null
END	
");
                }
                if (string.Compare(vers.Name, CTableDbVersion.DbVersion2024_1) < 0)
                {
                    _db.ExecCmd(@"if not exists (SELECT * FROM sys.indexes WHERE name='nci_OrderDetailId')
	CREATE NONCLUSTERED INDEX [nci_OrderDetailId] ON [dbo].[Roll]([OrderDetailId] DESC)");
                }
                if (string.Compare(vers.Name, CTableDbVersion.CurrentDbVersionConst) < 0)
                {
                    UpdateReports();
                    vers.Name = CTableDbVersion.CurrentDbVersionConst;
                    vers.Save2();
                }

            }
            catch (Exception ex)
            {
                var sqlEx = ex as SqlException;
                var dbEx = ex as DbException;
                if ((sqlEx != null && sqlEx.Number == 208) || (sqlEx == null && dbEx != null && ex.Message.Contains("42P01")))
                {
                    _errorMessage = "Структура БД не соответствует требуемой, проверьте настройки соединения";
                    return false;
                }
                _errorMessage = ex.Message;
                return false;
            }
            return true;
        }

        private void UpdateReports()
        {
            var repList = _db.Report.GetAll();
            foreach (var emdRep in CTableReport.CreateEmbedReports())
            {
                string name = emdRep.Name;
                string comment = emdRep.Comment;
                var rep = repList.FirstOrDefault(rrr => rrr.Name == name && rrr.Comment == comment);
                if (rep == null)
                {
                    rep = _db.Report.NewInstance();
                    rep.Name = emdRep.Name;
                    rep.Comment = emdRep.Comment;
                    rep.GroupCustomerId = emdRep.GroupCustomerId;
                    rep.InterfaceId = emdRep.InterfaceId;
                    repList.Add(rep);
                }
                rep.Value = emdRep.Value;
            }
            repList.Save();
        }

        public CDatabaseWeigher Db
        {
            get { return _db; }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            fSettings f = new fSettings();
            f.ShowDialog();
        }


        SortList<Orders> dbOrders;
        SortList<Orders> fileOrders;
        delegate void DlgBackgroundLoad();

        private void fOrders_Load(object sender, EventArgs e)
        {
            if (_db == null)
            {
                var msg = (_errorMessage ?? @"Отсутствует соединение с сервером базы данных (либо требует настройки)!")
                    + "\r\nОткрыть настройки соединения?";
                if (MessageBox.Show(msg, @"Внимание...", MessageBoxButtons.YesNo, MessageBoxIcon.Error)
                                    == DialogResult.Yes)
                {
                    var sw = new SettingsWindow(WeigherSettings.Current);
                    sw.ShowDialog();
                }
                Close();
                return;
            }
            BeginInvoke((DlgBackgroundLoad)BackgroundLoad);
        }

        private void BackgroundLoad()
        {
            Cursor = Cursors.WaitCursor;
            DateTime dfrom = DateTime.Now.Date.AddMonths(-1);
            dateTimePicker1.Value = dfrom;
            try
            {
                filter_DateFrom = dfrom;
                if (Db != null)
                {
                    dbOrders = Db.Orders.GetObjectListByExpr(o => o.DateStart > dfrom, "DateStart desc");
                    ReadOrdersFromCatalog();
                    dataGridView1.DataSource = fileOrders;
                    Log.Info("ColorizeRows ...");
                    ColorizeRows();
                    Log.Info("SelectCurrentPacker ...");
                    SelectCurrentPacker();
                }
            }
            catch (DbException ex)
            {
                if (MessageBox.Show(@"Отсутствует соединение с сервером базы данных!
Открыть настройки соединения? или ошибка" + ex.Message,
                    @"Внимание...", MessageBoxButtons.YesNo, MessageBoxIcon.Error)
                    == DialogResult.Yes)
                {
                    var sw = new SettingsWindow(WeigherSettings.Current);
                    sw.ShowDialog();
                }
                Close();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "fOrders_Load");
                MessageBox.Show(@"Ошибка приложения, обратитесь к разработчику:" + ex.Message,
                    @"Внимание...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
            finally
            {
                dateTimePicker1.ValueChanged += DateTimeChanged;
                dateTimePicker2.ValueChanged += DateTimeChanged;
                Cursor = Cursors.Default;
            }
        }

        private void ReadOrdersFromCatalog()
        {
        start:
            string path = WeigherSettings.Current.OtherSettings.ExlPath;
            Log.Info("DirectoryInfo...");
            DirectoryInfo di = new DirectoryInfo(path);
            if (!di.Exists)
            {
                Log.Info("MessageBox.Show...");
                MessageBox.Show(@"Отсутствует каталог заказов: " + path + "\r\n"
                    + "Проверьте настройки!", @"Внимание...", MessageBoxButtons.OK, MessageBoxIcon.Error);

                fSettings fs = new fSettings();
                if (fs.ShowDialog() == DialogResult.OK)
                    goto start;
                Log.Info("MessageBox...cancel");
                return;
            }

            SearchOption so = WeigherSettings.Current.OtherSettings.ExlPathWsf
                ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

            int minNom = WeigherSettings.Current.OtherSettings.ExlMinNom;
            var allOrderNoms = Db.Orders.GetObjectListByExpr(o => o.Number >= minNom)
                .Select(o => o.Number).Distinct().ToHashSet();

            List<Orders> ret = new List<Orders>();
            Log.Info(" di.GetFiles ...");
            foreach (FileInfo fi in di.GetFiles("*.xls?", so))
            {
                Log.Info("fi.Name:" + fi.Name);
                string fn = fi.Name;
                string extens = Path.GetExtension(fn);
                if (extens != ".xlsx" && extens != ".xlsm")
                    continue;
                int len = fn.IndexOfAny(new[] { '_', ' ' });
                if (len <= 0) continue;
                if (!int.TryParse(fn.Substring(0, len), out var nom)) continue;
                if (nom < minNom) continue;
                if (allOrderNoms.Contains(nom)) continue;

                Orders ord = Db.Orders.NewInstance();
                ord.Number = nom;
                ord.Name = fn.Substring(len + 1, fn.Length - 5 - len - 1);
                ord.NameFts = fi.Name;
                ret.Add(ord);
            };
            Log.Info(" AddRange ...");
            fileOrders = Db.Orders.EmptySortList();
            fileOrders.AddRange(ret);
            fileOrders.Sort(new ListSortDesc[] { new ListSortDesc(typeof(Orders).GetProperty(nameof(Orders.Number)), System.ComponentModel.ListSortDirection.Descending) });
        }


        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Point point1 = dataGridView1.CurrentCellAddress;
            if (point1.X != e.ColumnIndex || point1.Y != e.RowIndex || e.Button != MouseButtons.Left) return;
            Orders ord = (Orders)dataGridView1.Rows[e.RowIndex].DataBoundItem;
            ShowOrderDetail(ord);
        }

        private void ShowOrderDetail(Orders ord)
        {
            SelectCurrentPacker();
            bool importOk = true;
            if (ord.ObjectState == BObject.ObjectStateType.Added)
            {
                var number = ord.Number;
                var testOrder = Db.Orders.GetObjectByExpr(o => o.Number == number);
                if (testOrder == null)
                    importOk = ImportOrderDetails(ord);
            }
            else
            {
                //var modOrder = Db.Orders.NewInstance();
                var modOrder = (Orders)ord.Clone();
                var updateOk = ImportOrderDetails(modOrder, false);
                if (updateOk)
                {
                    if (UpdateOrderIfModified(ord, modOrder))
                    {
                        MessageBox.Show("Описание заказа модифицировано!");
                    }
                }
            }
            if (importOk)
            {
                fOrderDetails detail = new fOrderDetails(Db, ord);
                detail.ShowDialog();
            }
        }


        private bool UpdateOrderIfModified(Orders ord, Orders modOrder)
        {
            var updateIs = false;
            var details = ord.OrderDetails.Where(dtl => dtl.ParentId == null).OrderBy(dtl => dtl.Id).ToList();
            var details2 = modOrder.OrderDetails.ToList();
            var count = details.Count;
            var count2 = details2.Count;
            if (count2 < count) count = count2;
            for (int i = 0; i < count; i++)
            {
                var d = details[i];
                var d2 = details2[i];
                if (d.Specification != d2.Specification
                        || d.Material != d2.Material
                        || d.CustomerId != d2.CustomerId
                        || d.ProductName != d2.ProductName
                        || d.ProductCharacteristic != d2.ProductCharacteristic
                        || d.Shelf != d2.Shelf
                        || d.FrameWeight != d2.FrameWeight
                        || d.PackThick != d2.PackThick
                        || d.RollWidth != d2.RollWidth
                        || d.Density != d2.Density
                        || d.Party != d2.Party)
                {
                    var log = Db.OrderDetailLog.NewInstance();
                    log.RestoreFrom(d);
                    log.ObjectState = BObject.ObjectStateType.Added;
                    log.Save2();
                    d.Specification = d2.Specification;
                    d.Material = d2.Material;
                    d.CustomerId = d2.CustomerId;
                    d.ProductName = d2.ProductName;
                    d.ProductCharacteristic = d2.ProductCharacteristic;
                    d.Shelf = d2.Shelf;
                    d.FrameWeight = d2.FrameWeight;
                    d.PackThick = d2.PackThick;
                    d.RollWidth = d2.RollWidth;
                    d.Density = d2.Density;
                    d.Party = d2.Party;
                    updateIs = true;
                }
            }
            if (count2 > count)
            {
                ord.OrderDetails.AddRange(details2.Skip(count));
                updateIs = true;
            }
            ord.Save2();
            return updateIs;
        }

        class DetailsMap
        {
            public int Specification;
            public int Material;
            public int Party;
            public int Client;
            public int ProductName;
            public int ProductCharacteristic;
            public int RollWidth;
            public int Density;
            public int VtulkaWeight;
            public int StubWeight;
            public int Shelf;
            public int PackWeight;
            public int PackThick;
            public int FrameWeight;
            public int Interface;
            public DetailsMap()
            {
                Specification = -1;
                Material = -1;
                Party = -1;
                Client = -1;
                ProductName = -1;
                ProductCharacteristic = -1;
                RollWidth = -1;
                Density = -1;
                VtulkaWeight = -1;
                StubWeight = -1;
                Shelf = -1;
                PackWeight = -1;
                PackThick = -1;
                FrameWeight = -1;
                Interface = -1;
            }
        }

        private bool ImportOrderDetails(Orders ord, bool saveOrder = true)
        {
            Cursor = Cursors.WaitCursor;
            string fileCopyName = null;
            try
            {

                string path = WeigherSettings.Current.OtherSettings.ExlPath;
                DirectoryInfo di = new DirectoryInfo(path);
                SearchOption so = WeigherSettings.Current.OtherSettings.ExlPathWsf
                    ? SearchOption.AllDirectories
                    : SearchOption.TopDirectoryOnly;
                FileInfo fi = di.GetFiles(ord.NameFts, so).FirstOrDefault() ?? di.GetFiles(ord.Number + "_*.xls?", so).FirstOrDefault();
                if (fi == null)
                {
                    Log.Info("Файл с таким номером заказа не найден");
                    return false;
                }
                fi.Refresh();
                var attr = fi.Attributes;
                var arh = (attr & FileAttributes.Archive) != 0;
                if (!saveOrder && !arh) return false;
                if (arh)
                {
                    try
                    {
                        fi.Attributes = attr & ~FileAttributes.Archive;
                    }
                    catch (Exception) { }
                }

                // сохраним новое имя файла и новое имя заказа
                if (ord.NameFts != fi.Name)
                {
                    ord.NameFts = fi.Name;
                    ord.Name = Path.GetFileNameWithoutExtension(fi.Name).Substring(ord.Number.ToString().Length + 1);
                }

                /// https://github.com/ExcelDataReader/ExcelDataReader
                FileStream stream;
                bool needCopy = false;
                do
                {
                    try
                    {
                        var filePath = fi.FullName;
                        if (needCopy)
                        {
                            var dest = Path.Combine(Path.GetTempPath(), Path.GetTempFileName());
                            File.Delete(dest);
                            File.Copy(fi.FullName, dest);
                            filePath = fileCopyName = dest;
                        }
                        stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                    }
                    catch (IOException iex)
                    {
                        if (needCopy)
                        {
                            Log.Error(iex, "Ошибка при копировании файла");
                            if (saveOrder)
                                MessageBox.Show(@"Файл занят! Закройте программу и повторите попытку."
                                        , "Ошибка чтения файла...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        };
                        needCopy = true;
                        stream = null; // файл видимо занят
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Ошибка при открытии файла");
                        if (saveOrder)
                            MessageBox.Show(@"Файл возможно испорчен или отсутствуют права на чтение! Закройте программу и повторите попытку."
                                    , "Ошибка чтения файла...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                } while (stream == null);

                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                try
                {

                    DataSet result = excelReader.AsDataSet();
                    DataTable sheet = result.Tables[WeigherSettings.Current.OtherSettings.ExlPageName];
                    if (sheet == null)
                    {
                        if (saveOrder)
                            MessageBox.Show("В файле нет листа с названием: " + WeigherSettings.Current.OtherSettings.ExlPageName);
                        return false;
                    }

                    if (sheet.Rows.Count < 2 || sheet.Columns.Count < 5)
                    {
                        if (saveOrder)
                            MessageBox.Show("В файле нет информации о заказах");
                        return false;
                    }

                    // ищем как смещена таблица в файле
                    int rowsOffset = 0;
                    int columnOffset = 0;
                    while (true)
                    {
                        for (columnOffset = 0; columnOffset < sheet.Columns.Count; columnOffset++)
                        {
                            string cname = sheet.Rows[rowsOffset][columnOffset].ToString();
                            if (!string.IsNullOrEmpty(cname)) goto rowsOffsetFound;
                        }
                        rowsOffset++;
                    }
                rowsOffsetFound:

                    // имена столбцов
                    // ТУ:Specification,Материал:Material,№ партии:Party,Заказчик:Client
                    //,Наименование продукции:ProductName,Характеристика продукции:ProductCharacteristic
                    //,Ширина роля:RollWidth,Грамматура:Density,Вес втулки:VtulkaWeight,Вес упаковки:PackWeight

                    var hcolumns = _db.ColumnsMap.GetAll().ToDictionary(m => m.ExcelName, m => m.Name);
                    var hcolumnsBack = _db.ColumnsMap.GetAll().GroupBy(m => m.Name).ToDictionary(gr => gr.Key, gr => gr.First().ExcelName);

                    // готовим карту столбцов
                    var dm = new DetailsMap();
                    var columns = sheet.Rows[rowsOffset].ItemArray.Length;
                    for (int c = columnOffset; c < columns; c++)
                    {
                        string cname = sheet.Rows[rowsOffset][c].ToString();
                        if (string.IsNullOrWhiteSpace(cname)) continue;
                        if (hcolumns.ContainsKey(cname))
                        {
                            try
                            {
                                dm.GetType().GetField((string)hcolumns[cname]).SetValue(dm, c);
                            }
                            catch
                            {
                            }
                        }
                    }
                    rowsOffset++;

                    // проверяем все ли столбцы нашли
                    string message = "";
                    foreach (var fld in dm.GetType().GetFields())
                    {
                        var name = fld.Name;
                        if (name == "FrameWeight") continue;
                        if (name == "Shelf") continue;
                        if (name == "VtulkaWeight") continue;
                        if (name == "PackWeight") continue;
                        if (name == "StubWeight") continue;
                        if (name == "PackThick") continue;
                        if (name == "Interface") continue;
                        if (((int)fld.GetValue(dm)) == -1)
                            message = message + (", " + hcolumnsBack[name]);
                    }

                    bool remindDensity = message.Contains("Density");

                    // Отдельное предупреждение про плотность
                    //if (remindDensity)
                    //{
                    //    MessageBox.Show("Нет столбца \"Грамматура\" !\r\nОбработка невозможна!", "Ошибка...",
                    //        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //    return false;
                    //}


                    if (message != "")
                    {
                        if (saveOrder)
                            MessageBox.Show("Не найдены столбцы с данными: " + message.Substring(1), "Ошибка...",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                    NumberFormatInfo provider = new NumberFormatInfo { NumberDecimalSeparator = "." };

                    decimal defDensity = 40;
                    OrderDetail prevOd = null;

                    for (int i = rowsOffset; sheet.Rows[i][2].ToString() != ""; i++)
                    {
                        var currentRow = sheet.Rows[i];
                        OrderDetail od = Db.OrderDetail.NewInstance();
                        od.Specification = currentRow[dm.Specification].ToString();
                        od.Material = currentRow[dm.Material].ToString();
                        od.Party = currentRow[dm.Party].ToString(); // # партии
                        od.Client = currentRow[dm.Client].ToString();
                        od.ProductName = currentRow[dm.ProductName].ToString();
                        od.ProductCharacteristic = currentRow[dm.ProductCharacteristic].ToString();
                        if (od.ProductCharacteristic == null) od.ProductCharacteristic = "_";

                        if (dm.VtulkaWeight >= 0)
                        {
                            decimal vtulkaWeight = 0;
                            if (decimal.TryParse(currentRow[dm.VtulkaWeight].ToString().Replace(',', '.'),
                                NumberStyles.Number, provider, out vtulkaWeight))
                                od.VtulkaWeight = vtulkaWeight;
                        }
                        if (dm.PackWeight >= 0)
                        {

                            decimal packWeight = 0;
                            if (decimal.TryParse(currentRow[dm.PackWeight].ToString().Replace(',', '.'),
                                NumberStyles.Number, provider, out packWeight))
                                od.PackWeight = packWeight;
                        }
                        if (dm.StubWeight >= 0)
                        {

                            decimal stubWeight = 0;
                            if (decimal.TryParse(currentRow[dm.StubWeight].ToString().Replace(',', '.'),
                                NumberStyles.Number, provider, out stubWeight))
                                od.StubWeight = stubWeight;
                        }
                        decimal rollWidth = 0;
                        if (decimal.TryParse(currentRow[dm.RollWidth].ToString().Replace(',', '.'),
                            NumberStyles.Number, provider, out rollWidth))
                            od.RollWidth = rollWidth;
                        if (dm.FrameWeight >= 0)
                        {
                            decimal frameWeight = 0;
                            if (decimal.TryParse(currentRow[dm.FrameWeight].ToString().Replace(',', '.'),
                                NumberStyles.Number, provider, out frameWeight))
                                od.FrameWeight = frameWeight;
                        }
                        if (dm.Interface >= 0)
                        {
                            string Interface = currentRow[dm.Interface].ToString();
                            if (Interface.Length > 0 && !ord.InterfaceId.HasValue)
                                ord.InterfaceId = Db.Interface.GetAll().FirstOrDefault(ter => ter.Name == Interface)?.Id;
                        }

                        if (dm.PackThick >= 0)
                        {
                            string packThick = currentRow[dm.PackThick].ToString();
                            od.PackThick = (string.IsNullOrEmpty(packThick)) ? (prevOd?.PackThick ?? "") : packThick;
                        }
                        else
                            od.PackThick = "";

                        decimal density = 0;
                        if (decimal.TryParse(currentRow[dm.Density].ToString().Replace(',', '.'), NumberStyles.Number, provider, out density))
                            od.Density = density;
                        if (od.Density == 0)
                        {
                            if (prevOd == null) remindDensity = true;
                            od.Density = prevOd?.Density ?? defDensity;
                        }

                        if (dm.Shelf >= 0)
                        {
                            decimal shelf = 0;
                            if (decimal.TryParse(currentRow[dm.Shelf].ToString().Replace(',', '.'),
                                NumberStyles.Number, provider, out shelf))
                                od.Shelf = (int)shelf;
                        }

                        if (od.RollWidth == 0)
                            od.RollWidth = 500;

                        ord.OrderDetails.Add(od);
                        prevOd = od;
                    }
                    if (saveOrder && remindDensity)
                        MessageBox.Show("Отсутствует грамматура материала. \r\nПроверьте значения!", "Внимание...",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    foreach (var detail in ord.OrderDetails)
                    {
                        detail.Barcode = _db.GetBarCode(WeigherSettings.Current.OtherSettings.BarPrefix, WeigherSettings.Current.OtherSettings.BarDigit);
                    }
                    ord.DateStart = DateTime.Now;
                    ord.PackerId = CurrentPacker.Id;
                    if (saveOrder)
                        ord.Save2();

                    var managerPageName = WeigherSettings.Current.OtherSettings.ManagerPageName;
                    try
                    {
                        var manSheet = result.Tables[managerPageName];
                        var row = manSheet.Rows[0];
                        int col = 0;
                        int labelFound = 0;
                        for (; col < 80; col++)
                        {
                            var value = row[col].ToString().Trim().ToLower();
                            if (labelFound == 0)
                            {
                                if (value == "внутренний номер заказчика")
                                    labelFound = 1;
                            }
                            else if (labelFound == 1)
                            {
                                if (!string.IsNullOrEmpty(value))
                                {
                                    ord.CustNumber = value;
                                    if (saveOrder)
                                        ord.Save2();
                                    return true;
                                }
                            }
                        }
                        if (saveOrder)
                            MessageBox.Show($"На листе <{managerPageName}> в первой строке не найден внутренний номер заказчика!", "Внимание...",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Не прочитан внутренний номер заказчика");
                        if (saveOrder)
                            MessageBox.Show("Не прочитан внутренний номер заказчика! ошибка:" + ex.Message, "Внимание...",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Непонятная ошибка_1_!!!!!!!!!!!!!!");
                    return false;
                }
                finally
                {
                    excelReader.Close();
                    stream.Close();
                    stream.Dispose();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Непонятная ошибка_2_!!!!!!!!!!!!!!");
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
                if (fileCopyName != null)
                {
                    File.Delete(fileCopyName);
                }
            }
        }

        private void ColorizeRows()
        {
            foreach (DataGridViewRow r in dataGridView1.Rows)
            {
                if (((Orders)r.DataBoundItem).ObjectState == BObject.ObjectStateType.Added)
                    foreach (DataGridViewCell c in r.Cells)
                        c.Style.ForeColor = Color.Blue;
            }
        }

        private void соединениеСБДToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fAskPwd.AskPwd())
            {
                var sw = new SettingsWindow(WeigherSettings.Current);
                sw.ShowDialog();
            }
        }


        private void panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //var s = _db.Package.GetAll().First().GetJson();
            //MessageBox.Show(s);
            if (MouseButtons == MouseButtons.Left)
            {
                frmReportList rl = new frmReportList(Db);
                rl.ShowDialog();
            }

        }

        private void упаковщикиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fPackers f = new fPackers(Db);
            f.ShowDialog();
        }

        static public Packer CurrentPacker = null;

        private void выбратьУпаковщикаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectCurrentPacker();
        }

        private void SelectCurrentPacker()
        {
            if (_readOnly) return;
            if (CurrentPacker != null)
                return;
            fSelectPackers f = new fSelectPackers(Db);
            f.ShowDialog();
            CurrentPacker = f.packer;
            toolStripStatusLabel1.Text = @"Упаковщик: " + (CurrentPacker == null ? "" : CurrentPacker.Name);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dataGridView1.SelectedRows.Count == 1)
                {
                    ShowOrderDetail((Orders)dataGridView1.SelectedRows[0].DataBoundItem);
                }

            }
        }


        #region Члены IFilter<Orders>

        bool filter_checkDateFrom = true;
        DateTime filter_DateFrom = DateTime.Now;
        bool filter_checkDateTo = false;
        DateTime filter_DateTo = DateTime.Now;

        string filter_number = "";
        string filter_name = "";
        int filter_study = 0;

        public bool Test(Orders obj)
        {
            //if (obj.ObjectState == BObject.ObjectStateType.Added) return true;
            return
                (filter_number == "" || obj.Number.ToString().StartsWith(filter_number))
                && (filter_name == "" || obj.Name.ToUpper().Contains(filter_name))
                && ((filter_study == 0)
                        || (filter_study == 1 && obj.DateFinish == null)
                        || (filter_study == 2 && obj.DateFinish != null));
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            SortList<Orders> list = filter_study == 0 ? fileOrders : dbOrders;

            filter_number = tbNumber.Text.ToUpper();
            filter_name = tbName.Text.ToUpper();
            list.funFilter = this;
            dataGridView1.DataSource = list;
            ColorizeRows();

        }

        private void tbNumber_TextChanged(object sender, EventArgs e)
        {
            button1_Click(sender, e);
        }

        private void DateTimeChanged(object sender, EventArgs e)
        {
            var reread = dateTimePicker1.Checked != filter_checkDateFrom;

            filter_checkDateFrom = dateTimePicker1.Checked;
            if (filter_checkDateFrom && dateTimePicker1.Value != filter_DateFrom)
                reread = true;
            filter_DateFrom = dateTimePicker1.Value;

            if (dateTimePicker2.Checked != filter_checkDateTo)
                reread = true;
            filter_checkDateTo = dateTimePicker2.Checked;
            if (filter_checkDateTo && dateTimePicker2.Value != filter_DateTo)
                reread = true;
            filter_DateTo = dateTimePicker2.Value;
            if (reread)
            {
                DateTime filterDateFrom2 = filter_DateFrom;
                DateTime filterDateTo2 = filter_DateTo;

                //Expression<Func<Orders, bool>> expr = o2 => o2.DateStart >= filter_DateFrom2 && o2.DateStart < filter_DateTo2;

                if (filter_checkDateFrom && filter_checkDateTo)
                    dbOrders = Db.Orders.GetObjectListByExpr(o2 => o2.DateStart >= filterDateFrom2 && o2.DateStart < filterDateTo2);
                else
                    if (filter_checkDateFrom)
                    dbOrders = Db.Orders.GetObjectListByExpr(o2 => o2.DateStart >= filterDateFrom2);
                else
                        if (filter_checkDateTo)
                    dbOrders = Db.Orders.GetObjectListByExpr(o2 => o2.DateStart < filterDateTo2);
                else
                    dbOrders = Db.Orders.GetAll();

            }
            button1_Click(sender, e);
        }

        private void radioStudy0_CheckedChanged(object sender, EventArgs e)
        {
            if (!((RadioButton)sender).Checked)
                return;
            if (radioStudy0.Checked) filter_study = 0;
            else
                if (radioStudy1.Checked) filter_study = 1;
            else filter_study = 2;

            button1_Click(sender, e);
        }

        private void соответствияСтолбцовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColumnsMapWindow cmw = new ColumnsMapWindow(_db);
            cmw.ShowDialog();
        }

        private void fOrders_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void ПечатныеОтчетыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!fAskPwd.AskPwd()) return;
            frmReportList rl = new frmReportList(Db);
            rl.ShowDialog();
        }

        private void справочникиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TermEditor cmw = new TermEditor(_db);
            cmw.ShowDialog();
        }

        private void связьСВесамиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fAskPwd.AskPwd())
            {
                ScaleConnection s = new ScaleConnection(WeigherSettings.Current);
                s.ShowDialog();
            }

        }

    }
}
