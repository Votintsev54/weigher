﻿using System;
using System.Threading;
using System.Windows.Forms;
using DbCommon.Helpers;

namespace Weigher
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {

            if (IsSingleInstance())
            {
                Log.Info("Запуск приложения!");
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new fOrders());
            }
        }

        private static Mutex _mutex;

        static bool IsSingleInstance()
        {
            const string mname = "WEIGHER_MUTEX";
            try
            {
                Mutex.OpenExisting(mname);
                return false;
            }
            catch
            {
                _mutex = new Mutex(true, mname);
                return true;
            }
        }
    }
}
