﻿using WeigherData;

namespace Weigher
{
    partial class frmReportCreate
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReportCreate));
            this.tbName = new System.Windows.Forms.TextBox();
            this.lbReportName = new System.Windows.Forms.Label();
            this.tbComment = new System.Windows.Forms.TextBox();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.lbComment = new System.Windows.Forms.Label();
            this.cmdSave = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.lbData = new System.Windows.Forms.Label();
            this.reportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportBindingSource, "Name", true));
            this.tbName.Location = new System.Drawing.Point(224, 9);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(256, 22);
            this.tbName.TabIndex = 1;
            // 
            // lbReportName
            // 
            this.lbReportName.Location = new System.Drawing.Point(6, 12);
            this.lbReportName.Name = "lbReportName";
            this.lbReportName.Size = new System.Drawing.Size(212, 16);
            this.lbReportName.TabIndex = 0;
            this.lbReportName.Tag = "1";
            this.lbReportName.Text = "Наименование";
            this.lbReportName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbComment
            // 
            this.tbComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbComment.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.reportBindingSource, "Comment", true));
            this.tbComment.Location = new System.Drawing.Point(224, 37);
            this.tbComment.MaxLength = 255;
            this.tbComment.Multiline = true;
            this.tbComment.Name = "tbComment";
            this.tbComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbComment.Size = new System.Drawing.Size(256, 45);
            this.tbComment.TabIndex = 3;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmdCancel.Image = global::Weigher.Properties.Resources.gtk_cancel;
            this.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdCancel.Location = new System.Drawing.Point(275, 332);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(145, 35);
            this.cmdCancel.TabIndex = 7;
            this.cmdCancel.Text = "Отменить";
            this.cmdCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // lbComment
            // 
            this.lbComment.Location = new System.Drawing.Point(9, 40);
            this.lbComment.Name = "lbComment";
            this.lbComment.Size = new System.Drawing.Size(209, 16);
            this.lbComment.TabIndex = 2;
            this.lbComment.Tag = "1";
            this.lbComment.Text = "Комментарий";
            this.lbComment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdSave.Image = global::Weigher.Properties.Resources._001_06;
            this.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdSave.Location = new System.Drawing.Point(65, 332);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(150, 35);
            this.cmdSave.TabIndex = 6;
            this.cmdSave.Text = "Сохранить";
            this.cmdSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // treeView1
            // 
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView1.CheckBoxes = true;
            this.treeView1.Location = new System.Drawing.Point(6, 88);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(474, 235);
            this.treeView1.TabIndex = 5;
            // 
            // lbData
            // 
            this.lbData.Location = new System.Drawing.Point(9, 69);
            this.lbData.Name = "lbData";
            this.lbData.Size = new System.Drawing.Size(209, 16);
            this.lbData.TabIndex = 4;
            this.lbData.Tag = "1";
            this.lbData.Text = "Данные";
            // 
            // reportBindingSource
            // 
            this.reportBindingSource.DataSource = typeof(Report);
            // 
            // frmReportCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(492, 373);
            this.Controls.Add(this.lbData);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.lbComment);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.lbReportName);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.tbComment);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "frmReportCreate";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Создание нового отчета";
            this.Load += new System.EventHandler(this.frmReportCreate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.reportBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbReportName;
        private System.Windows.Forms.TextBox tbComment;
        private System.Windows.Forms.Button cmdCancel;
        public System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lbComment;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.BindingSource reportBindingSource;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label lbData;
    }
}
