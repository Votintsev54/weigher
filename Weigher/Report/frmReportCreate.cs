﻿using System;
using System.Windows.Forms;
using System.Reflection;
using System.Xml;
using DbCommon;
using WeigherData;

namespace Weigher
{
    /// <summary>
    /// Форма для управления печатными отчетами системы.
    /// Наследует функциональность FormTranslate, для отображения на разных языках.
    /// </summary>
    public partial class frmReportCreate : Form
    {
        /// <summary>
        /// Печатный отчет.
        /// </summary>
        public Report _report = null;

        /// <summary>
        /// XML-документ.
        /// </summary>
        XmlDocument _doc = null;
        CDatabaseWeigher db;

        ///// <summary>
        ///// Конструктор формы по умолчанию.
        ///// </summary>
        //public frmReportCreate()
        //{
        //    InitializeComponent();
        //    _report = new Report();
        //    reportBindingSource.DataSource = _report;
        //}

        /// <summary>
        /// Конструктор формы с указанием печатного отчета.
        /// </summary>
        /// <param name="r">Печатный отчет.</param>
        public frmReportCreate(CDatabaseWeigher db, Report r)
        {
            InitializeComponent();
            this.db = db;
            _report = r == null ? db.Report.NewInstance() : r;
            reportBindingSource.DataSource = _report;
        }

        /// <summary>
        /// Обработка события Button.Click.
        /// Отменяют изменения и закрывает форму.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void cmdClose_Click(object sender, EventArgs e)
        {
            _report.Rollback();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Обработка события Button.Click.
        /// Сохраняет изменения и закрывает форму.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (_report.Name == "")
            {
                MessageBox.Show("Нет имени отчета !"
                    , "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Boolean bnew = _doc == null ;
            if (bnew)
            {
                _doc = new XmlDocument();
            }
            XmlElement elReport = bnew ? _doc.CreateElement("Report") : _doc.DocumentElement;
            if (bnew)
            {
                _doc.AppendChild(_doc.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\""));
                _doc.AppendChild(elReport);
                elReport.SetAttribute("ScriptLanguage", "CSharp");
            }
            DateTime now = DateTime.Now;
            elReport.SetAttribute("ReportInfo.Modified", now.ToShortDateString());
            if (bnew)
            {
                elReport.SetAttribute("ReportInfo.Created", now.ToShortDateString());
                elReport.SetAttribute("ReportInfo.CreatorVersion", "1.4.12.0");
            }
            XmlElement elDictionary = bnew ? _doc.CreateElement("Dictionary") : (XmlElement)elReport.SelectSingleNode("Dictionary");
            if (bnew)
            {
                elReport.AppendChild(elDictionary);
            }
            if (!bnew)
            {
                XmlNodeList sourcelist = elDictionary.SelectNodes("BusinessObjectDataSource");
                foreach (XmlNode node in sourcelist)
                {
                    elDictionary.RemoveChild(node);
                }
            }
            foreach (TreeNode tnClass in treeView1.Nodes)
            {
                if (tnClass.Checked)
                {
                    iTable it = (iTable)tnClass.Tag;
                    Type t = it.GetTableType();
                    XmlElement elBusinessObjectDataSource = _doc.CreateElement("BusinessObjectDataSource");
                    elDictionary.AppendChild(elBusinessObjectDataSource);
                    elBusinessObjectDataSource.SetAttribute("Name", t.Name);
                    elBusinessObjectDataSource.SetAttribute("ReferenceName", t.Name);
                    elBusinessObjectDataSource.SetAttribute("DataType", t.AssemblyQualifiedName);
                    elBusinessObjectDataSource.SetAttribute("Enabled", "true");
                    foreach (TreeNode tnProp in tnClass.Nodes)
                    {
                        if (tnProp.Checked)
                        {
                            PropertyInfo pi = t.GetProperty(tnProp.Text);
                            XmlElement elColumn = _doc.CreateElement("Column");
                            elBusinessObjectDataSource.AppendChild(elColumn);
                            elColumn.SetAttribute("Name", pi.Name);
                            elColumn.SetAttribute("DataType", pi.PropertyType.AssemblyQualifiedName);
                        }
                    }
                }
            }
            if (bnew)
            {
                XmlElement elParameter = _doc.CreateElement("Parameter");
                elDictionary.AppendChild(elParameter);
                elParameter.SetAttribute("Name", "ReportDate");
                elParameter.SetAttribute("DataType", "System.DateTime");
                elParameter.SetAttribute("Description", "Дата формирования отчета");


                XmlElement elReportPage = _doc.CreateElement("ReportPage");
                elReport.AppendChild(elReportPage);
                elReportPage.SetAttribute("Name", "Page1");
                XmlElement elReportTitleBand = _doc.CreateElement("ReportTitleBand");
                elReportPage.AppendChild(elReportTitleBand);
                elReportTitleBand.SetAttribute("Name", "ReportTitle1");
                elReportTitleBand.SetAttribute("Width", "718.2");
                elReportTitleBand.SetAttribute("Height", "217.35");

                XmlElement elTextObject = _doc.CreateElement("TextObject");
                elReportTitleBand.AppendChild(elTextObject);
                elTextObject.SetAttribute("Name", "Text1");
                elTextObject.SetAttribute("Width", "264.6");
                elTextObject.SetAttribute("Height", "18.9");
                elTextObject.SetAttribute("Text", "АТПМ");


                elTextObject = _doc.CreateElement("TextObject");
                elReportTitleBand.AppendChild(elTextObject);
                elTextObject.SetAttribute("Name", "Text2");
                elTextObject.SetAttribute("Left", "576.45");
                elTextObject.SetAttribute("Width", "141.75");
                elTextObject.SetAttribute("Height", "18.9");
                elTextObject.SetAttribute("Text", "Новосибирск, [Year([ReportDate])]г.");


                //<TextObject Name="Text6" Left="576.45" Width="141.75" Height="18.9" Text="Новосибирск, [Year([Date])]г."/>

                elTextObject = _doc.CreateElement("TextObject");
                elReportTitleBand.AppendChild(elTextObject);
                elTextObject.SetAttribute("Name", "Text3");
                elTextObject.SetAttribute("Left", "170.1");
                elTextObject.SetAttribute("Top", "37.8");
                elTextObject.SetAttribute("Width", "330.75");
                elTextObject.SetAttribute("Height", "37.8");
                elTextObject.SetAttribute("Text", "Автоматизированная обучающая система &quot;NARP&quot;");
                elTextObject.SetAttribute("HorzAlign", "Center");
                elTextObject.SetAttribute("Font", "Times New Roman, 12pt, style=Bold");


                //<TextObject Name="Text7" Left="170.1" Top="37.8" Width="330.75" Height="37.8" Text="Автоматизированная обучающая система &quot;NARP&quot;" 
                //HorzAlign="Center" Font="Times New Roman, 12pt, style=Bold"/>

                XmlElement elPageHeaderBand = _doc.CreateElement("PageHeaderBand");
                elReportPage.AppendChild(elPageHeaderBand);
                elPageHeaderBand.SetAttribute("Name", "PageHeader");
                elPageHeaderBand.SetAttribute("Width", "718.2");
                elPageHeaderBand.SetAttribute("Height", "18.9");
                XmlElement elDataBand = _doc.CreateElement("DataBand");
                elReportPage.AppendChild(elDataBand);
                elDataBand.SetAttribute("Name", "Data1");
                elDataBand.SetAttribute("Top", "244.25");
                elDataBand.SetAttribute("Width", "718.2");
                elDataBand.SetAttribute("Height", "18.9");
                XmlElement elReportSummaryBand = _doc.CreateElement("ReportSummaryBand");
                elReportPage.AppendChild(elReportSummaryBand);
                elReportSummaryBand.SetAttribute("Name", "ReportSummary1");
                elReportSummaryBand.SetAttribute("Top", "267.15");
                elReportSummaryBand.SetAttribute("Width", "718.2");
                elReportSummaryBand.SetAttribute("Height", "85.05");
            }

            FastReport.Report report = new FastReport.Report();
            //frmMessageBox.Show(doc.OuterXml);
            string sxml = _doc.OuterXml;
            sxml = sxml.Replace("&#xD;", "&#13;");
            sxml = sxml.Replace("&#xA;", "&#10;");
            report.LoadFromString(sxml);
            //report.Designer = new FastReport.Design.Designer();
            //report.Designer.AskSave = false;
            //report.Designer.MdiMode = true;
            //report.Designer.md
            //report.Designer.Show();
            report.Design(true);
            _report.Value = report.SaveToString().Substring(1);

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Настройки среды генератора отчетов FastReport.
        /// </summary>
        FastReport.EnvironmentSettings se = new FastReport.EnvironmentSettings();

        /// <summary>
        /// Обработчик события Form.Load.
        /// Не используется.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void frmReportCreate_Load(object sender, EventArgs e)
        {
            frmReportCreate_AfterTrunslate(sender, e);
        }

        /// <summary>
        /// Проверка структуры отчета.
        /// </summary>
        private void CheckClasses()
        {
            if (!string.IsNullOrEmpty(_report.Value))
            {
                _doc = new XmlDocument();
                _doc.LoadXml(_report.Value);
                XmlNodeList elBusinessObjectDataSourceList =
                    _doc.DocumentElement.SelectNodes("Dictionary/BusinessObjectDataSource");
                foreach (XmlNode node in elBusinessObjectDataSourceList)
                {
                    foreach (TreeNode tn in treeView1.Nodes)
                    {
                        if (tn.Text == node.Attributes["Name"].Value)
                        {
                            tn.Checked = true;
                            treeView1.Nodes.Remove(tn);
                            treeView1.Nodes.Insert(0, tn);
                            CheckProperties(tn, node);
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Проверка структуры элемента отчета.
        /// </summary>
        /// <param name="classTN">Узел объекта.</param>
        /// <param name="elBusinessObjectDataSource">Узел XML-документа.</param>
        private void CheckProperties(TreeNode classTN, XmlNode elBusinessObjectDataSource)
        {
            foreach (XmlNode node in elBusinessObjectDataSource.ChildNodes)
            {
                foreach (TreeNode tn in classTN.Nodes)
                {
                    if (node.Attributes["Name"] == null) continue;
                    if (tn.Text == node.Attributes["Name"].Value)
                    {
                        tn.Checked = true;
                        classTN.Nodes.Remove(tn);
                        classTN.Nodes.Insert(0, tn);
                        CheckProperties(tn, node);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Создание элемента отчета.
        /// </summary>
        /// <param name="t">Тип элемента.</param>
        /// <param name="parentTn">Родительский узел объекта.</param>
        private void CreatePropertis(iTable it, TreeNode parentTn)
        {
            var fdList = it.Columns.Values;
            PropertyInfo[] api = it.GetTableType().GetProperties();
            foreach (PropertyInfo pi in api)
            {
                TreeNode tn = new TreeNode(pi.Name);
                tn.Name = pi.Name;
                tn.Tag = pi;
                parentTn.Nodes.Add(tn);
                foreach (FieldDescription fd in fdList)
                {
                    if (pi.Name == fd.PropInfo.Name)
                    {
                        tn.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// Дерево для хранения промежуточных данных при перестроении других деревьев.
        /// </summary>
        private static TreeView cashTree = null;

        /// <summary>
        /// Обработчик события Form.AfterTrunslate.
        /// Перевод объектов формы, не выполняемый системой автоматически.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void frmReportCreate_AfterTrunslate(object sender, EventArgs e)
        {
            if (cashTree == null)
            {
                cashTree = treeView1;
                se = new FastReport.EnvironmentSettings();

                var tbls = db.Tables.Values ;

                foreach (iTable it in tbls)
                {
                    TreeNode tn = new TreeNode(it.TableName());
                    tn.Name = it.TableName();
                    tn.Tag = it;
                    treeView1.Nodes.Add(tn);
                    CreatePropertis(it, tn);
                }
            }
            else
            {
                treeView1.Parent.Controls.Add(cashTree);
                treeView1.Parent.Controls.Remove(treeView1);
                treeView1 = cashTree;
            }
            ClearCheck(treeView1.Nodes);
            CheckClasses();
        }

        /// <summary>
        /// Снятие всех чеков в указанной коллекции узлов дерева.
        /// </summary>
        /// <param name="treeNodeCollection">Коллекция узлов дерева.</param>
        private void ClearCheck(TreeNodeCollection treeNodeCollection)
        {
            foreach (TreeNode n in treeNodeCollection)
            {
                n.Checked = false;
                ClearCheck(n.Nodes);
            }
        }
    }
}
