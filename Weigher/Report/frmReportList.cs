﻿using System;
using System.Windows.Forms;
using DbCommon;
using WeigherData;

namespace Weigher
{
    /// <summary>
    /// Форма для создания, редактирования и удаления печатных отчетов в системе.
    /// Наследует функциональность FormTranslate, для отображения на разных языках.
    /// </summary>
    public partial class frmReportList : Form
    {
        CDatabaseWeigher db;
        /// <summary>
        /// Конструктор формы по умолчанию.
        /// </summary>
        public frmReportList()
        {
            InitializeComponent();
        }

        public frmReportList(CDatabaseWeigher db)
        {
            this.db = db;
            InitializeComponent();
        }

        /// <summary>
        /// Настройки среды генератора отчетов FastReport.
        /// </summary>
        FastReport.EnvironmentSettings fes = null;

        /// <summary>
        /// Обработчик события EnvironmentSettings.DesignerLoaded.
        /// Инициализация среды генератора отчетов FastReport.
        /// </summary>
        /// <param name="sender">Объект - источник события.</param>
        /// <param name="e">Аргументы события (не используются).</param>
        public void fes_DesignerLoaded(object sender, EventArgs e)
        {
            ((FastReport.Design.Designer)sender).AskSave = false;   
        }

        /// <summary>
        /// Обработчик события Form.Load.
        /// Инициализация элементов формы.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void frmReportList_Load(object sender, EventArgs e)
        {
            if (this.DesignMode) return;
            fes = new FastReport.EnvironmentSettings();
            fes.DesignerLoaded +=new EventHandler(fes_DesignerLoaded);
            reportBindingSource.DataSource = db.Report.GetAll();
            //AddNewGroup.Enabled = IsAddNew;
            //toolStripMenuItemAdd.Enabled = IsAddNew;
            //ToolStripButtonOpen.Enabled = IsEdit;
            //toolStripButtonCopy.Enabled = IsEdit;
            //DelToolStripButton.Enabled = IsRemove;
            //toolStripMenuItemEdit.Enabled = IsEdit;
            //toolStripMenuItemDel.Enabled = IsRemove;
        }

        /// <summary>
        /// Обработчик события DataGridView.CellDoubleClick.
        /// Редактирование отчета. Вызывает форму frmReportCreate.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e != null)
                CallEdit(reportBindingSource.Position); 
        }

        /// <summary>
        /// Редактирование отчета. Вызывает форму frmReportCreate.
        /// </summary>
        /// <param name="index">Индекс отчета в источнике данных.</param>
        private void CallEdit(int index)
        {
            if (index < 0) return;
            if (dataGridView1.SelectedRows.Count == 0) return;

            frmReportCreate rc = new frmReportCreate(db,(Report)dataGridView1.Rows[index].DataBoundItem);
            DialogResult dr = rc.ShowDialog(this);
            if (dr == DialogResult.OK)
            {
                rc._report.Save2();
                reportBindingSource.ResetBindings(false);
            }
      
        }

         /// <summary>
        /// Обработчик события ToolStripButton.Click.
        /// Создает новый отчет. Вызывает форму frmReportCreate.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void AddNewReport_Click(object sender, EventArgs e)
         {
             frmReportCreate rc = new frmReportCreate(db,null);
             DialogResult dr = rc.ShowDialog(this);
             if (dr == DialogResult.OK)
             {
                 rc._report.Save2();
                 reportBindingSource.Add(rc._report);
                 reportBindingSource.ResetBindings(false);
             }
         }

        /// <summary>
        /// Обработчик события ToolStripButton.Click.
        /// Редактирование отчета. Вызывает форму frmReportCreate.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void ToolStripButtonOpen_Click(object sender, EventArgs e)
         {
             CallEdit(reportBindingSource.Position);
         }

        /// <summary>
        /// Обработчик события ToolStripButton.Click.
        /// Удаление отчета. Запрашивается подтверждение.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void DelToolStripButton_Click(object sender, EventArgs e)
         {

            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show(this, "Необходимо выбрать отчет для удаления !", "Ошибка", MessageBoxButtons.OK);
                return;
            }

            SortList<Report> group_list = db.Report.EmptySortList();
            for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
            {
                group_list.Add((Report)dataGridView1.SelectedRows[i].DataBoundItem);
            }
            SortList<Report> del_list = db.Report.EmptySortList();

            for (int i = 0; i < group_list.Count; i++)
            {
                Report group = group_list[i];
                if (group != null)
                {
                    String message = "Для удаления выбран отчет:" + Environment.NewLine + "'" + group.Name + "'";
                    if (!string.IsNullOrEmpty(group.Comment))
                        message = message + " (" + group.Comment + ")";
                    message = message + Environment.NewLine + "Удалить?";
                    String caption = "AOC";

                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result = MessageBox.Show(this, message, caption, buttons, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);

                    if (result == DialogResult.Cancel)
                    {
                        break;
                    }
                    if (result == DialogResult.Yes)
                    {
                        group.MarkForDeletion();
                        Boolean success = true;
                        try
                        {
                            group.Save2();
                        }
                        catch(Exception ex)
                        {
                             MessageBox.Show("Ошибка: " + ex.Message, "AOC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            success = false;
                        }
                        if (success)
                            del_list.Add(group);
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        break;
                    }
                }
            }

            foreach (Report group in del_list)
            {
                ((SortList<Report>)reportBindingSource.DataSource).Remove(group);
            }
            reportBindingSource.ResetBindings(false);
         }

        /// <summary>
        /// Обработчик события Form.Activated.
        /// Обновление источника данных и установка фокуса на таблицу.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void GroupList_Activated(object sender, EventArgs e)
         {
             reportBindingSource.ResetBindings(false);
             this.dataGridView1.Focus();
         }

        /// <summary>
        /// Обработчик события DataGridView.KeyDown.
        /// Редактирование отчета по нажатию клавиши ENTER.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
             if (e.KeyCode == Keys.Enter)
             {
                 e.Handled = true;
                 CallEdit(reportBindingSource.Position);
             }
         }

        /// <summary>
        /// Обработчик события ToolStripMenuItem.Click.
        /// Создает новый отчет. Вызывает форму frmReportCreate.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void toolStripMenuItemAdd_Click(object sender, EventArgs e)
        {
            AddNewReport_Click(null, null);
        }

        /// <summary>
        /// Обработчик события ToolStripMenuItem.Click.
        /// Редактирование отчета. Вызывает форму frmReportCreate.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void toolStripMenuItemEdit_Click(object sender, EventArgs e)
        {
            CallEdit(reportBindingSource.Position);
        }

        /// <summary>
        /// Обработчик события ToolStripMenuItem.Click.
        /// Удаление отчета. Запрашивается подтверждение.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void toolStripMenuItemDel_Click(object sender, EventArgs e)
        {
            DelToolStripButton_Click(null, null);
        }

        /// <summary>
        /// Обработчик события DataGridView.CellMouseDown.
        /// Редактирование отчета по двойному клику мыши.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события.</param>
        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (dataGridView1.SelectedRows.Count < 2)
                {
                    int row = e.RowIndex;
                    if (row >= 0)
                        reportBindingSource.Position = row;
                }
            }
        }

        /// <summary>
        /// Обработчик события ToolStripButton.Click.
        /// Создание копии выбранного отчета.
        /// </summary>
        /// <param name="sender">Объект - источник события (не используется).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void toolStripButtonCopy_Click(object sender, EventArgs e)
        {
            if (reportBindingSource.Current != null)
            {
                Report r = (Report)((Report)reportBindingSource.Current).Clone();
                //int i = 0;
                string newname = r.Name + "(Копия)";
                //while (Report.GetReport(newname) != null)  newname = r.Name + "(Копия "+(++i).ToString()+")";

                r.IsPersistent = false;
                r.Name = newname;
                if (r.SaveTry())
                {
                    reportBindingSource.Insert(reportBindingSource.Position + 1, r);
                    reportBindingSource.ResetBindings(false);
                }
            }
        }

        /// <summary>
        /// Родительский элемент тулбара, для организации блокировки перетаскивания тулбара на другие формы.
        /// </summary>
        Control toolStrip_Parent = null;

        /// <summary>
        /// Родительский элемент, родительского элемента тулбара, для организации блокировки перетаскивания тулбара на другие формы.
        /// </summary>
        Control toolStrip_GrandParent = null;

        /// <summary>
        /// Обработчик события перетаскивания, блокирует перетаскивание тултипа в другое окно.
        /// </summary>
        /// <param name="sender">Объект - источник события, запоминаем родительский элемент.</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void toolStrip_BeginDrag(object sender, EventArgs e)
        {
            toolStrip_Parent = ((Control)sender).Parent;
            toolStrip_GrandParent = toolStrip_Parent.Parent;
            return;
        }

        /// <summary>
        /// Обработчик события окончания перетаскивания, блокирует перетаскивание тултипа в другое окно.
        /// </summary>
        /// <param name="sender">Объект - источник события (тулбар).</param>
        /// <param name="e">Аргументы события (не используются).</param>
        private void toolStrip_EndDrag(object sender, EventArgs e)
        {
            Control control_parent = ((Control)sender).Parent;
            Control control_grand_parent = control_parent.Parent;

            if (!control_grand_parent.Equals(toolStrip_GrandParent))
                ((Control)sender).Parent = toolStrip_Parent;

            return;
        }



   }
}
