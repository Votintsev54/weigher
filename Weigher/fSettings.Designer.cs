﻿namespace Weigher
{
    partial class fSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fSettings));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.btnFindDir = new System.Windows.Forms.Button();
            this.cbIncludeDir = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbSysPrinter = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbLabelPrinter = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioCheck2 = new System.Windows.Forms.RadioButton();
            this.radioCheck1 = new System.Windows.Forms.RadioButton();
            this.radioCheck0 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.tbMinNom = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbLabelPrinter2 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbRollCountButtons = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioQrJson = new System.Windows.Forms.RadioButton();
            this.numQRBlockSize = new System.Windows.Forms.NumericUpDown();
            this.cbQRLevel = new System.Windows.Forms.ComboBox();
            this.radioQrWithDelimeter = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbTerminator = new System.Windows.Forms.TextBox();
            this.radioQrNotPrint = new System.Windows.Forms.RadioButton();
            this.tbDelimeter = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbPackListPrintCount = new System.Windows.Forms.TextBox();
            this.tbPackCheckPrintCount = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbReadOnly = new System.Windows.Forms.CheckBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQRBlockSize)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Image = global::Weigher.Properties.Resources._001_06;
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOk.Location = new System.Drawing.Point(59, 682);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(123, 55);
            this.btnOk.TabIndex = 31;
            this.btnOk.Text = "Сохранить";
            this.btnOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = global::Weigher.Properties.Resources.DelToolStripButton_Image;
            this.btnCancel.Location = new System.Drawing.Point(355, 682);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(123, 55);
            this.btnCancel.TabIndex = 32;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(343, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Путь к каталогу, содержащему файлы заказов:";
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(10, 45);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(486, 24);
            this.tbPath.TabIndex = 1;
            // 
            // btnFindDir
            // 
            this.btnFindDir.Location = new System.Drawing.Point(503, 40);
            this.btnFindDir.Name = "btnFindDir";
            this.btnFindDir.Size = new System.Drawing.Size(38, 33);
            this.btnFindDir.TabIndex = 2;
            this.btnFindDir.Text = "...";
            this.btnFindDir.UseVisualStyleBackColor = true;
            this.btnFindDir.Click += new System.EventHandler(this.btnFindDir_Click);
            // 
            // cbIncludeDir
            // 
            this.cbIncludeDir.AutoSize = true;
            this.cbIncludeDir.Location = new System.Drawing.Point(26, 76);
            this.cbIncludeDir.Name = "cbIncludeDir";
            this.cbIncludeDir.Size = new System.Drawing.Size(182, 22);
            this.cbIncludeDir.TabIndex = 3;
            this.cbIncludeDir.Text = "Включая подкаталоги";
            this.cbIncludeDir.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Принтер А4";
            // 
            // cbSysPrinter
            // 
            this.cbSysPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSysPrinter.FormattingEnabled = true;
            this.cbSysPrinter.Location = new System.Drawing.Point(21, 44);
            this.cbSysPrinter.Name = "cbSysPrinter";
            this.cbSysPrinter.Size = new System.Drawing.Size(519, 26);
            this.cbSysPrinter.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(179, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Принтер штрих-этикеток";
            // 
            // cbLabelPrinter
            // 
            this.cbLabelPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLabelPrinter.FormattingEnabled = true;
            this.cbLabelPrinter.Location = new System.Drawing.Point(21, 104);
            this.cbLabelPrinter.Name = "cbLabelPrinter";
            this.cbLabelPrinter.Size = new System.Drawing.Size(519, 26);
            this.cbLabelPrinter.TabIndex = 9;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioCheck2);
            this.groupBox1.Controls.Add(this.radioCheck1);
            this.groupBox1.Controls.Add(this.radioCheck0);
            this.groupBox1.Location = new System.Drawing.Point(14, 405);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(234, 47);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Печатать этикеток";
            // 
            // radioCheck2
            // 
            this.radioCheck2.AutoSize = true;
            this.radioCheck2.Location = new System.Drawing.Point(134, 19);
            this.radioCheck2.Name = "radioCheck2";
            this.radioCheck2.Size = new System.Drawing.Size(34, 22);
            this.radioCheck2.TabIndex = 20;
            this.radioCheck2.TabStop = true;
            this.radioCheck2.Text = "2";
            this.radioCheck2.UseVisualStyleBackColor = true;
            // 
            // radioCheck1
            // 
            this.radioCheck1.AutoSize = true;
            this.radioCheck1.Location = new System.Drawing.Point(71, 19);
            this.radioCheck1.Name = "radioCheck1";
            this.radioCheck1.Size = new System.Drawing.Size(34, 22);
            this.radioCheck1.TabIndex = 19;
            this.radioCheck1.TabStop = true;
            this.radioCheck1.Text = "1";
            this.radioCheck1.UseVisualStyleBackColor = true;
            // 
            // radioCheck0
            // 
            this.radioCheck0.AutoSize = true;
            this.radioCheck0.Location = new System.Drawing.Point(15, 19);
            this.radioCheck0.Name = "radioCheck0";
            this.radioCheck0.Size = new System.Drawing.Size(34, 22);
            this.radioCheck0.TabIndex = 18;
            this.radioCheck0.TabStop = true;
            this.radioCheck0.Text = "0";
            this.radioCheck0.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(262, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "Минимальный номер файла заказа:";
            // 
            // tbMinNom
            // 
            this.tbMinNom.Location = new System.Drawing.Point(282, 109);
            this.tbMinNom.Name = "tbMinNom";
            this.tbMinNom.Size = new System.Drawing.Size(96, 24);
            this.tbMinNom.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbSysPrinter);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbLabelPrinter2);
            this.groupBox2.Controls.Add(this.cbLabelPrinter);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(9, 162);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(555, 208);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Принтеры";
            // 
            // cbLabelPrinter2
            // 
            this.cbLabelPrinter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLabelPrinter2.FormattingEnabled = true;
            this.cbLabelPrinter2.Location = new System.Drawing.Point(21, 164);
            this.cbLabelPrinter2.Name = "cbLabelPrinter2";
            this.cbLabelPrinter2.Size = new System.Drawing.Size(519, 26);
            this.cbLabelPrinter2.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(200, 18);
            this.label9.TabIndex = 10;
            this.label9.Text = "Принтер штрих-этикеток A5";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbPath);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnFindDir);
            this.groupBox3.Controls.Add(this.tbMinNom);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.cbIncludeDir);
            this.groupBox3.Location = new System.Drawing.Point(9, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(555, 146);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Настройка импорта заказов";
            // 
            // cbRollCountButtons
            // 
            this.cbRollCountButtons.AutoSize = true;
            this.cbRollCountButtons.Location = new System.Drawing.Point(14, 376);
            this.cbRollCountButtons.Name = "cbRollCountButtons";
            this.cbRollCountButtons.Size = new System.Drawing.Size(234, 22);
            this.cbRollCountButtons.TabIndex = 15;
            this.cbRollCountButtons.Text = "Кнопки с количеством ролов";
            this.cbRollCountButtons.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioQrJson);
            this.groupBox4.Controls.Add(this.numQRBlockSize);
            this.groupBox4.Controls.Add(this.cbQRLevel);
            this.groupBox4.Controls.Add(this.radioQrWithDelimeter);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.tbTerminator);
            this.groupBox4.Controls.Add(this.radioQrNotPrint);
            this.groupBox4.Controls.Add(this.tbDelimeter);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Location = new System.Drawing.Point(12, 458);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(555, 158);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "QR код";
            // 
            // radioQrJson
            // 
            this.radioQrJson.AutoSize = true;
            this.radioQrJson.Location = new System.Drawing.Point(375, 23);
            this.radioQrJson.Name = "radioQrJson";
            this.radioQrJson.Size = new System.Drawing.Size(54, 22);
            this.radioQrJson.TabIndex = 24;
            this.radioQrJson.TabStop = true;
            this.radioQrJson.Text = "json";
            this.radioQrJson.UseVisualStyleBackColor = true;
            // 
            // numQRBlockSize
            // 
            this.numQRBlockSize.Location = new System.Drawing.Point(134, 112);
            this.numQRBlockSize.Name = "numQRBlockSize";
            this.numQRBlockSize.Size = new System.Drawing.Size(135, 24);
            this.numQRBlockSize.TabIndex = 27;
            // 
            // cbQRLevel
            // 
            this.cbQRLevel.FormattingEnabled = true;
            this.cbQRLevel.Items.AddRange(new object[] {
            "L (7 %)",
            "M (15 %)",
            "Q (25 %)",
            "H (30 %)"});
            this.cbQRLevel.Location = new System.Drawing.Point(134, 80);
            this.cbQRLevel.Name = "cbQRLevel";
            this.cbQRLevel.Size = new System.Drawing.Size(136, 26);
            this.cbQRLevel.TabIndex = 25;
            // 
            // radioQrWithDelimeter
            // 
            this.radioQrWithDelimeter.AutoSize = true;
            this.radioQrWithDelimeter.Location = new System.Drawing.Point(168, 23);
            this.radioQrWithDelimeter.Name = "radioQrWithDelimeter";
            this.radioQrWithDelimeter.Size = new System.Drawing.Size(191, 22);
            this.radioQrWithDelimeter.TabIndex = 23;
            this.radioQrWithDelimeter.TabStop = true;
            this.radioQrWithDelimeter.Text = "строка с разделителем";
            this.radioQrWithDelimeter.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(291, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 18);
            this.label8.TabIndex = 8;
            this.label8.Text = "Конец строки";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(291, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Разделитель";
            // 
            // tbTerminator
            // 
            this.tbTerminator.Location = new System.Drawing.Point(458, 111);
            this.tbTerminator.Name = "tbTerminator";
            this.tbTerminator.Size = new System.Drawing.Size(80, 24);
            this.tbTerminator.TabIndex = 28;
            // 
            // radioQrNotPrint
            // 
            this.radioQrNotPrint.AutoSize = true;
            this.radioQrNotPrint.Location = new System.Drawing.Point(44, 23);
            this.radioQrNotPrint.Name = "radioQrNotPrint";
            this.radioQrNotPrint.Size = new System.Drawing.Size(108, 22);
            this.radioQrNotPrint.TabIndex = 22;
            this.radioQrNotPrint.TabStop = true;
            this.radioQrNotPrint.Text = "не печатать";
            this.radioQrNotPrint.UseVisualStyleBackColor = true;
            // 
            // tbDelimeter
            // 
            this.tbDelimeter.Location = new System.Drawing.Point(458, 74);
            this.tbDelimeter.Name = "tbDelimeter";
            this.tbDelimeter.Size = new System.Drawing.Size(80, 24);
            this.tbDelimeter.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 18);
            this.label6.TabIndex = 8;
            this.label6.Text = "Размер блока";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(202, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Уровень коррекции ошибок";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(288, 377);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(155, 18);
            this.label10.TabIndex = 6;
            this.label10.Text = "Упаковочных листов";
            // 
            // tbPackListPrintCount
            // 
            this.tbPackListPrintCount.Location = new System.Drawing.Point(470, 376);
            this.tbPackListPrintCount.Name = "tbPackListPrintCount";
            this.tbPackListPrintCount.Size = new System.Drawing.Size(80, 24);
            this.tbPackListPrintCount.TabIndex = 17;
            // 
            // tbPackCheckPrintCount
            // 
            this.tbPackCheckPrintCount.Location = new System.Drawing.Point(469, 416);
            this.tbPackCheckPrintCount.Name = "tbPackCheckPrintCount";
            this.tbPackCheckPrintCount.Size = new System.Drawing.Size(80, 24);
            this.tbPackCheckPrintCount.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(288, 416);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(149, 18);
            this.label11.TabIndex = 6;
            this.label11.Text = "Ярлык для поддона";
            // 
            // cbReadOnly
            // 
            this.cbReadOnly.AutoSize = true;
            this.cbReadOnly.Location = new System.Drawing.Point(14, 633);
            this.cbReadOnly.Name = "cbReadOnly";
            this.cbReadOnly.Size = new System.Drawing.Size(286, 22);
            this.cbReadOnly.TabIndex = 29;
            this.cbReadOnly.Text = "Запускать в режиме \"Только чтение\"";
            this.cbReadOnly.UseVisualStyleBackColor = true;
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(425, 631);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(125, 24);
            this.tbPassword.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(303, 634);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 18);
            this.label13.TabIndex = 8;
            this.label13.Text = "Пароль";
            // 
            // fSettings
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(569, 749);
            this.Controls.Add(this.cbReadOnly);
            this.Controls.Add(this.cbRollCountButtons);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbPackCheckPrintCount);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbPackListPrintCount);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fSettings";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры";
            this.Load += new System.EventHandler(this.fSettings_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQRBlockSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Button btnFindDir;
        private System.Windows.Forms.CheckBox cbIncludeDir;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbSysPrinter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbLabelPrinter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioCheck2;
        private System.Windows.Forms.RadioButton radioCheck1;
        private System.Windows.Forms.RadioButton radioCheck0;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbMinNom;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox cbRollCountButtons;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown numQRBlockSize;
        private System.Windows.Forms.ComboBox cbQRLevel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radioQrJson;
        private System.Windows.Forms.RadioButton radioQrWithDelimeter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioQrNotPrint;
        private System.Windows.Forms.TextBox tbDelimeter;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbTerminator;
        private System.Windows.Forms.ComboBox cbLabelPrinter2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbPackListPrintCount;
        private System.Windows.Forms.TextBox tbPackCheckPrintCount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox cbReadOnly;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label label13;
    }
}