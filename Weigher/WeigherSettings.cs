﻿using System;
using System.IO;
using System.Xml.Serialization;
using DbCommon;
using DbCommon.Helpers;
using WeigherDatabase;
using WpfWeigher;
using WpfWeigher.Common;
using WpfWeigher.Models;

namespace Weigher
{
    public class WeigherSettings : ViewModelBase, IParentSqlSettings, IParentScaleSettings
    {
        private static string _settingsFilePath;

        static WeigherSettings()
        {
            Current = ReadFromFile(true);
        }

        private SqlSettings _sqlSettings;

        public static WeigherSettings ReadFromFile(bool createFileIfNotExists = false)
        {
            _settingsFilePath = _settingsFilePath ?? SpecialFiles.SettingsFilePath;

            WeigherSettings settings = null;
            try
            {
                using (var reader = new StreamReader(_settingsFilePath))
                {
                    var x = new XmlSerializer(typeof(WeigherSettings));
                    settings = (WeigherSettings)x.Deserialize(reader);
                }
            }
            catch (FileNotFoundException)
            {
                var path = _settingsFilePath.Replace(SpecialFiles.SETTINGS_FILE_NAME, SpecialFiles.SETTINGS_Weigher_FILE_NAME);
                try
                {
                    using (var reader = new StreamReader(path))
                    {
                        var x = new XmlSerializer(typeof(WeigherSettings));
                        settings = (WeigherSettings)x.Deserialize(reader);
                        settings.Save(_settingsFilePath);
                    }
                }
                catch (Exception)
                {
                    Log.Info("нет файла настроек");
                    settings = new WeigherSettings { SqlSettings = new SqlSettings() };
                    settings.Save(_settingsFilePath);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Ошибка чтения файла настроек");
                settings = new WeigherSettings { SqlSettings = new SqlSettings() };
            }
            bool needSave = settings.PrintersSettings == null || settings.ScaleSettings == null ||
                settings.OtherSettings == null || settings.QrSettings == null;

            if (settings.PrintersSettings == null)
                settings.PrintersSettings = new SettingsPrinter();
            if (settings.OtherSettings == null)
            {
                settings.OtherSettings = new SettingsProp();
                settings.OtherSettings.ShowRollCountButtons = settings.ShowRollCountButtons;
            }
            if (settings.QrSettings == null)
                settings.QrSettings = new QrSettings();
            if (settings.ScaleSettings == null)
            {
                settings.ScaleSettings = new ScaleSettings();
                if (settings.ComPortSettings != null)
                {
                    settings.ScaleSettings.SerialSettings.ComPort = settings.ComPortSettings.Port;
                    settings.ComPortSettings = null;
                }
            }
            if (needSave)
                settings.Save(_settingsFilePath);
            return settings;
        }

        public void Save()
        {
            Save(_settingsFilePath);
        }

        public void Save(string filePath)
        {
            try
            {
                using (var writer = new StreamWriter(filePath))
                {
                    var s = new XmlSerializer(typeof(WeigherSettings));
                    s.Serialize(writer, this);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
            }
        }
        public SqlSettings SqlSettings
        {
            get => _sqlSettings;
            set => SetProperty(ref _sqlSettings, value);
        }

        public bool ShowRollCountButtons { get; set; }
        public SettingsPrinter PrintersSettings { get; set; }
        public SettingsPort ComPortSettings { get; set; }
        public SettingsProp OtherSettings { get; set; }
        public QrSettings QrSettings { get; set; }

        private static WeigherSettings current;
        public static WeigherSettings Current
        {
            get => current;
            set
            {
                current = value;
                QrSettings.qrSettingsInstance = value.QrSettings;
            }
        }
        public ScaleSettings ScaleSettings { get; set; }
    }
}