set pgPath=d:\Program Files\PostgreSQL\9.4\bin\
set bkpPath=.\

set bkpname=Weigher%Date:~6,4%%Date:~3,2%%Date:~0,2%.bkp

"%pgPath%pg_dump" -U postgres Weigher> "%bkpPath%%bkpname%"