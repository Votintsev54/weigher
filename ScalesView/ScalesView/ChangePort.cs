using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ScalesView
{
	public class ChangePort : Form
	{
		private IContainer components;

		private Label label1;

		private TextBox tPort;

		private int _mPort = 5001;

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			label1 = new System.Windows.Forms.Label();
			tPort = new System.Windows.Forms.TextBox();
			SuspendLayout();
			label1.AutoSize = true;
			label1.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			label1.Location = new System.Drawing.Point(5, 9);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(43, 16);
			label1.TabIndex = 0;
			label1.Text = "Порт:";
			tPort.Font = new System.Drawing.Font("Arial", 11.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			tPort.Location = new System.Drawing.Point(52, 5);
			tPort.Name = "tPort";
			tPort.Size = new System.Drawing.Size(58, 25);
			tPort.TabIndex = 20;
			tPort.TabStop = false;
			tPort.Text = "5001";
			tPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(117, 34);
			base.ControlBox = false;
			base.Controls.Add(tPort);
			base.Controls.Add(label1);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			base.Name = "ChangePort";
			base.TopMost = true;
			base.Load += new System.EventHandler(ChangePort_Load);
			base.Leave += new System.EventHandler(ChangePort_Leave);
			ResumeLayout(false);
			PerformLayout();
		}

		public ChangePort(int _pt)
		{
			InitializeComponent();
			_mPort = _pt;
		}

		private void ChangePort_Load(object sender, EventArgs e)
		{
			base.TopMost = true;
			tPort.Text = _mPort.ToString();
		}

		public int GetPort()
		{
			if (int.TryParse(tPort.Text, out int result))
			{
				_mPort = result;
				return result;
			}
			return -1;
		}

		public void SetPort(int _port)
		{
			tPort.Text = _port.ToString();
		}

		private void ChangePort_Leave(object sender, EventArgs e)
		{
			if (int.TryParse(tPort.Text, out int result))
			{
				_mPort = result;
			}
			else
			{
				tPort.Text = _mPort.ToString();
			}
		}
	}
}
