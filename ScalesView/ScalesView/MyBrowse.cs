using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace ScalesView
{
	public class MyBrowse : WebBrowser
	{
		public MyBrowse()
		{
			base.NewWindow += MyNewWindow;
		}

		private void MyNewWindow(object sender, CancelEventArgs e)
		{
			e.Cancel = true;
			try
			{
				string statusText = ((MyBrowse)sender).StatusText;
				Process.Start(statusText);
			}
			catch
			{
			}
		}
	}
}
