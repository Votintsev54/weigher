using System.Text;

namespace ScalesView
{
	internal static class ContrlKeys
	{
		public static int GetCTRLKey(string sKey)
		{
			switch (sKey)
			{
			case "Left CTRL":
				return 162;
			case "Right CTRL":
				return 163;
			case "CTRL":
				return 17;
			case "F1":
				return 112;
			case "F2":
				return 113;
			case "F3":
				return 114;
			case "F4":
				return 115;
			case "F5":
				return 116;
			case "F6":
				return 117;
			case "F7":
				return 118;
			case "F8":
				return 119;
			case "F9":
				return 120;
			case "F10":
				return 121;
			case "F11":
				return 122;
			case "F12":
				return 123;
			case "PrtScr":
				return 44;
			case "ScrlLock":
				return 145;
			default:
				return 0;
			}
		}

		public static string GetTextCTRLKey(int iKey)
		{
			switch (iKey)
			{
			case 162:
				return "Left CTRL";
			case 163:
				return "Right CTRL";
			case 17:
				return "CTRL";
			case 112:
				return "F1";
			case 113:
				return "F2";
			case 114:
				return "F3";
			case 115:
				return "F4";
			case 116:
				return "F5";
			case 117:
				return "F6";
			case 118:
				return "F7";
			case 119:
				return "F8";
			case 120:
				return "F9";
			case 121:
				return "F10";
			case 122:
				return "F11";
			case 123:
				return "F12";
			case 44:
				return "PrtScr";
			case 145:
				return "ScrlLock";
			default:
				return "<Нет>";
			}
		}

		public static int GetUserKey(string sKey)
		{
			if (sKey.Length > 1)
			{
				return 0;
			}
			try
			{
				byte[] bytes = Encoding.Unicode.GetBytes(sKey);
				return bytes[0];
			}
			catch
			{
				return 0;
			}
		}
	}
}
