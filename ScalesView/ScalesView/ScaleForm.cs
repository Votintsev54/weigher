using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace ScalesView
{
	public class ScaleForm : Form
	{
		public class CWeight
		{
			public int Weight;

			public int Division;

			public int Stable;

			public int Tare;

			public int TareDivision;

			public int Error;

			public bool IsNotError = true;

			public string GetWeight()
			{
				if (!IsNotError)
				{
					return "Ошибка!";
				}
				switch (Division)
				{
				case 0:
					if (Stable > 0)
					{
						return $"{(double)Weight / 10000.0:0.0000} кг".Replace(',', '.');
					}
					return $"{(double)Weight / 10000.0:0.0000}   ".Replace(',', '.');
				case 1:
					if (Stable > 0)
					{
						return $"{(double)Weight / 1000.0:0.000} кг".Replace(',', '.');
					}
					return $"{(double)Weight / 1000.0:0.000}    ".Replace(',', '.');
				case 2:
					if (Stable > 0)
					{
						return $"{(double)Weight / 100.0:0.00} кг".Replace(',', '.');
					}
					return $"{(double)Weight / 100.0:0.00}    ".Replace(',', '.');
				case 3:
					if (Stable > 0)
					{
						return $"{(double)Weight / 10.0:0.0} кг".Replace(',', '.');
					}
					return $"{(double)Weight / 10.0:0.0}    ".Replace(',', '.');
				case 4:
					if (Stable > 0)
					{
						return $"{Weight} кг".Replace(',', '.');
					}
					return $"{Weight}    ".Replace(',', '.');
				default:
					return "Ошибка!";
				}
			}

			public bool GetTare()
			{
				if (Tare > 0)
				{
					return true;
				}
				return false;
			}
		}

		private class Log
		{
			private string _addr;

			private bool _En;

			public Log(string addr, bool En)
			{
				_addr = addr;
				_En = En;
			}

			public void addlog(string stat)
			{
				addlog(_addr, stat, _En);
			}

			public void addlog(string addr, string stat, bool En)
			{
			}
		}

		public static int X_Size = 260;

		public static int Y_Size = 160;

		public ScaleInfo mScaleInfo;

		public Point Start_Location;

		private Thread ThreadReadScaleWeight;

		private CWeight mWeight = new CWeight();

		private int count;

		private IContainer components;

		private Label lNet;

		private Label lScaleName;

		private Panel pBackground;

		private Label lInterface;

		private Label lConnection;

		private System.Windows.Forms.Timer timer1000mS;

		private Label lScaleID;

		private Panel pWeight;

		private Label tWeight;

		public bool IsWork()
		{
			return mWeight.IsNotError;
		}

		private void ReadScaleWeight_1C(string sConnectString)
		{
			int num = 0;
			bool flag = false;
			Log log = new Log(sConnectString.Split(':')[0], En: true);
			if (!(sConnectString == ""))
			{
				ScalesMassaLocal scalesMassaLocal = new ScalesMassaLocal();
				try
				{
					while (true)
					{
						scalesMassaLocal.Connection = sConnectString;
						if (sConnectString.Contains("COM"))
						{
							log.addlog("Connect 1C = " + sConnectString + ":" + scalesMassaLocal.ComBoude.ToString());
						}
						else
						{
							log.addlog("Connect 1C = " + sConnectString);
						}
						int num2 = scalesMassaLocal.OpenConnection();
						if (num2 != 0)
						{
							log.addlog("Connect Error = " + num2.ToString() + "," + num.ToString());
							scalesMassaLocal.CloseConnection();
							if (num > 4)
							{
								lock (mWeight)
								{
									mWeight.IsNotError = false;
									Thread.Sleep(3000);
								}
							}
							else
							{
								num++;
								Thread.Sleep(400);
							}
						}
						else
						{
							log.addlog("Connect Ok");
							Thread.Sleep(200);
							num2 = scalesMassaLocal.ReadSerial();
							if (num2 != 0)
							{
								scalesMassaLocal.CloseConnection();
								log.addlog("Get Serial Error = " + num2.ToString() + "," + num.ToString());
								if (num > 5)
								{
									lock (mWeight)
									{
										mWeight.IsNotError = false;
									}
									Thread.Sleep(2000);
								}
								else
								{
									num++;
									Thread.Sleep(400);
								}
								scalesMassaLocal.CloseConnection();
								Thread.Sleep(400);
							}
							else
							{
								log.addlog("Get Serial Ok");
								lock (mWeight)
								{
									mWeight.IsNotError = true;
								}
								log.addlog("Set Parameters");
								lock (mScaleInfo)
								{
									mScaleInfo.SN = scalesMassaLocal.SerialNumber;
									mScaleInfo.SInterface = (scalesMassaLocal.Connection.Contains("COM") ? "RS232 (USB)" : "Ethernet");
								}
								log.addlog("Get Weight init");
								num = 0;
								flag = true;
								do
								{
									log.addlog("Get Weight ");
									Thread.Sleep(350);
									num2 = scalesMassaLocal.ReadWeight();
									switch (num2)
									{
									case 0:
										log.addlog("Get Weight Ok");
										lock (mWeight)
										{
											mWeight.Weight = scalesMassaLocal.Weight;
											mWeight.Division = scalesMassaLocal.Division;
											mWeight.Stable = scalesMassaLocal.Stable;
											mWeight.Error = 0;
										}
										break;
									case 21:
										log.addlog("Get Weight H");
										lock (mWeight)
										{
											mWeight.Error = 1;
											mWeight.Stable = 0;
										}
										break;
									default:
										log.addlog("Get Weight Error = " + num2.ToString());
										scalesMassaLocal.CloseConnection();
										Thread.Sleep(1000);
										flag = false;
										break;
									}
								}
								while (flag);
								Thread.Sleep(400);
							}
						}
					}
				}
				catch (ThreadAbortException)
				{
					log.addlog("EndProcess");
					scalesMassaLocal.CloseConnection();
					log.addlog("EndProcess Ok");
				}
			}
		}

		private void ReadScaleWeight_P100(string sConnectString)
		{
			int num = 0;
			bool flag = false;
			bool flag2 = false;
			string sN = mScaleInfo.SN;
			Log log = new Log(sConnectString.Split(':')[0], En: true);
			if (!(sConnectString == ""))
			{
				ScalesMassaLocal scalesMassaLocal = new ScalesMassaLocal();
				try
				{
					while (true)
					{
						flag = true;
						scalesMassaLocal.Connection = sConnectString;
						scalesMassaLocal.ComBoude = mScaleInfo.ComBoude;
						log.addlog("Connect P100 = " + scalesMassaLocal.ComBoude.ToString());
						if (flag2)
						{
							mScaleInfo.ComBoude = ((mScaleInfo.ComBoude == 4800) ? 57600 : 4800);
						}
						int num2 = scalesMassaLocal.OpenConnection();
						if (num2 != 0)
						{
							scalesMassaLocal.CloseConnection();
							log.addlog("Connect Error = " + num2.ToString() + "," + num.ToString());
							if (num > 5)
							{
								lock (mWeight)
								{
									mWeight.IsNotError = false;
									Thread.Sleep(2000);
								}
							}
							else
							{
								num++;
								Thread.Sleep(400);
							}
						}
						else
						{
							log.addlog("Connect Ok");
							Thread.Sleep(200);
							log.addlog("Get Serial ");
							num2 = scalesMassaLocal.ReadScaleInfo();
							if (num2 != 0)
							{
								log.addlog("Get Serial Error = " + num2.ToString() + "," + num.ToString());
								if (num > 5)
								{
									lock (mWeight)
									{
										mWeight.IsNotError = false;
									}
									Thread.Sleep(2000);
								}
								else
								{
									num++;
									Thread.Sleep(400);
								}
								scalesMassaLocal.CloseConnection();
								Thread.Sleep(400);
							}
							else
							{
								log.addlog("Get Serial Ok");
								Thread.Sleep(200);
								log.addlog("Get Name ");
								num2 = scalesMassaLocal.ReadScaleParameters();
								if (num2 != 0 || sN != scalesMassaLocal.SerialNumber)
								{
									if (num2 == 0)
									{
										log.addlog("Get Serial Error Change  " + sN + "," + scalesMassaLocal.SerialNumber);
										lock (mWeight)
										{
											mWeight.IsNotError = false;
										}
									}
									else
									{
										log.addlog("Get Name Error = " + num2.ToString() + "," + num.ToString());
									}
									scalesMassaLocal.CloseConnection();
									Thread.Sleep(400);
								}
								else
								{
									log.addlog("Get Name Ok");
									lock (mScaleInfo)
									{
										mScaleInfo.SN = scalesMassaLocal.SerialNumber;
										switch (scalesMassaLocal.ActiveInterface)
										{
										case 0:
											mScaleInfo.SInterface = "RS232";
											flag2 = true;
											break;
										case 1:
											mScaleInfo.SInterface = "USB";
											flag2 = true;
											break;
										case 2:
											mScaleInfo.SInterface = "Ethernet";
											break;
										case 3:
											mScaleInfo.SInterface = "Wi-Fi";
											break;
										default:
											mScaleInfo.SInterface = "___";
											break;
										}
										mScaleInfo.SName = scalesMassaLocal.Name;
									}
									log.addlog("Get Weight init");
									lock (mWeight)
									{
										mWeight.IsNotError = true;
									}
									do
									{
										log.addlog("Get Weight ");
										num = 0;
										Thread.Sleep(350);
										num2 = scalesMassaLocal.ReadMassa();
										switch (num2)
										{
										case 0:
											log.addlog("Get Weight Ok");
											lock (mWeight)
											{
												mWeight.Weight = scalesMassaLocal.Weight;
												mWeight.Division = scalesMassaLocal.Division;
												mWeight.Stable = scalesMassaLocal.Stable;
												mWeight.Tare = scalesMassaLocal.bTare;
												mWeight.Error = 0;
											}
											flag = true;
											break;
										case 108:
										case 109:
											log.addlog("Get Weight H");
											lock (mWeight)
											{
												mWeight.Error = 1;
												mWeight.Stable = 0;
											}
											flag = true;
											break;
										default:
											if (num2 > 200 && num2 < 300)
											{
												log.addlog("Get Weight Err" + (num2 - 100).ToString());
												lock (mWeight)
												{
													mWeight.Error = 1;
													mWeight.Stable = 0;
												}
											}
											else
											{
												log.addlog("Get Weight Error = " + num2.ToString() + "," + num.ToString());
												log.addlog("Close");
												scalesMassaLocal.CloseConnection();
												Thread.Sleep(1000);
												flag = false;
											}
											break;
										}
									}
									while (flag);
									Thread.Sleep(400);
								}
							}
						}
					}
				}
				catch (ThreadAbortException)
				{
					log.addlog("EndProcess");
					scalesMassaLocal.CloseConnection();
					log.addlog("EndProcess Ok");
				}
			}
		}

		public ScaleForm(ScaleInfo _si)
		{
			InitializeComponent();
			mScaleInfo = _si;
		}

		public string GetScaleWeight()
		{
			return tWeight.Text;
		}

		public int GetScaleLeds()
		{
			if (lNet.Visible)
			{
				return 2;
			}
			return 0;
		}

		public Color GetBackColor()
		{
			return BackColor;
		}

		private void ScaleForm_Load(object sender, EventArgs e)
		{
			base.Location = Start_Location;
			lScaleName.Text = mScaleInfo.SName;
			lScaleID.Text = $"ID: {mScaleInfo.SN}";
			lConnection.Text = mScaleInfo.Connection;
			lInterface.Text = "USB";
			while (TextRenderer.MeasureText("-15.000 Кг", tWeight.Font).Width > pWeight.Width)
			{
				float emSize = tWeight.Font.Size - 0.5f;
				tWeight.Font = new Font(tWeight.Font.Name, emSize, tWeight.Font.Style, tWeight.Font.Unit);
			}
			if (mScaleInfo.IsP100)
			{
				ThreadReadScaleWeight = new Thread((ThreadStart)delegate
				{
					ReadScaleWeight_P100(mScaleInfo.Connection);
				});
				ThreadReadScaleWeight.IsBackground = true;
				ThreadReadScaleWeight.Start();
			}
			else
			{
				ThreadReadScaleWeight = new Thread((ThreadStart)delegate
				{
					ReadScaleWeight_1C(mScaleInfo.Connection);
				});
				ThreadReadScaleWeight.IsBackground = true;
				ThreadReadScaleWeight.Start();
			}
			pBackground.BackColor = Color.FromArgb(255, 0, 102, 179);
		}

		private void timer1000mS_Tick(object sender, EventArgs e)
		{
			timer1000mS.Stop();
			if (mWeight != null)
			{
				lock (mWeight)
				{
					try
					{
						if (mWeight.Error == 0)
						{
							string weight = mWeight.GetWeight();
							if (tWeight.Text != weight)
							{
								tWeight.Text = weight;
							}
							lNet.Visible = mWeight.GetTare();
						}
						else
						{
							tWeight.Text = "Ошибка!";
						}
					}
					catch
					{
					}
					if (mWeight.IsNotError)
					{
						BackColor = Color.Azure;
						if (!base.Visible)
						{
							base.Visible = true;
						}
					}
					else
					{
						BackColor = Color.PapayaWhip;
						if (base.Visible)
						{
							base.Visible = false;
						}
					}
				}
				if (lScaleName.Text != mScaleInfo.SName)
				{
					lScaleName.Text = mScaleInfo.SName;
				}
				if (lInterface.Text != mScaleInfo.SInterface)
				{
					lInterface.Text = mScaleInfo.SInterface;
					lConnection.Text = mScaleInfo.Connection;
				}
			}
			timer1000mS.Start();
		}

		private void CreateScaleSingleView()
		{
			ScaleSingleView scaleSingleView = new ScaleSingleView(mScaleInfo.SName, mScaleInfo.SN, this, mScaleInfo.mKey);
			base.MdiParent.WindowState = FormWindowState.Minimized;
			base.MdiParent.Update();
			scaleSingleView.ShowDialog();
			base.MdiParent.WindowState = FormWindowState.Normal;
			scaleSingleView.Dispose();
		}

		public void SetEnableDriver(bool Enabled)
		{
			if (Enabled)
			{
				if (mScaleInfo.IsP100)
				{
					ThreadReadScaleWeight = new Thread((ThreadStart)delegate
					{
						ReadScaleWeight_P100(mScaleInfo.Connection);
					});
				}
				else
				{
					ThreadReadScaleWeight = new Thread((ThreadStart)delegate
					{
						ReadScaleWeight_1C(mScaleInfo.Connection);
					});
				}
				ThreadReadScaleWeight.IsBackground = true;
				ThreadReadScaleWeight.Start();
				timer1000mS.Start();
			}
			else
			{
				ThreadReadScaleWeight.Abort();
				timer1000mS.Stop();
			}
		}

		private void pBackground_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			CreateScaleSingleView();
		}

		private void lNet_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			CreateScaleSingleView();
		}

		private void pBackground_Paint(object sender, PaintEventArgs e)
		{
		}

		private void ScaleForm_FormClosing(object sender, FormClosingEventArgs e)
		{
		}

		private void tWeight_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			CreateScaleSingleView();
		}

		private void pWeight_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			CreateScaleSingleView();
		}

		private void lScaleID_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			CreateScaleSingleView();
		}

		private void lScaleName_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			CreateScaleSingleView();
		}

		private void lInterface_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			CreateScaleSingleView();
		}

		private void lConnection_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			CreateScaleSingleView();
		}

		private void ScaleForm_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			CreateScaleSingleView();
		}

		private void lInterface_Click(object sender, EventArgs e)
		{
		}

		private void ScaleForm_Shown(object sender, EventArgs e)
		{
			BringToFront();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			lNet = new System.Windows.Forms.Label();
			lScaleName = new System.Windows.Forms.Label();
			pBackground = new System.Windows.Forms.Panel();
			pWeight = new System.Windows.Forms.Panel();
			tWeight = new System.Windows.Forms.Label();
			lScaleID = new System.Windows.Forms.Label();
			lInterface = new System.Windows.Forms.Label();
			lConnection = new System.Windows.Forms.Label();
			timer1000mS = new System.Windows.Forms.Timer(components);
			pBackground.SuspendLayout();
			pWeight.SuspendLayout();
			SuspendLayout();
			lNet.AutoSize = true;
			lNet.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lNet.Location = new System.Drawing.Point(5, 65);
			lNet.Name = "lNet";
			lNet.Size = new System.Drawing.Size(42, 19);
			lNet.TabIndex = 17;
			lNet.Text = "NET";
			lNet.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(lNet_MouseDoubleClick);
			lScaleName.AutoSize = true;
			lScaleName.Font = new System.Drawing.Font("Verdana", 11.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lScaleName.ForeColor = System.Drawing.Color.White;
			lScaleName.Location = new System.Drawing.Point(3, 15);
			lScaleName.Name = "lScaleName";
			lScaleName.Size = new System.Drawing.Size(99, 18);
			lScaleName.TabIndex = 4;
			lScaleName.Text = "ScaleName";
			lScaleName.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(lScaleName_MouseDoubleClick);
			pBackground.BackColor = System.Drawing.Color.SlateBlue;
			pBackground.Controls.Add(pWeight);
			pBackground.Controls.Add(lScaleID);
			pBackground.Controls.Add(lNet);
			pBackground.Controls.Add(lScaleName);
			pBackground.Controls.Add(lInterface);
			pBackground.Controls.Add(lConnection);
			pBackground.ForeColor = System.Drawing.Color.White;
			pBackground.Location = new System.Drawing.Point(-5, -2);
			pBackground.Name = "pBackground";
			pBackground.Size = new System.Drawing.Size(245, 144);
			pBackground.TabIndex = 18;
			pBackground.Paint += new System.Windows.Forms.PaintEventHandler(pBackground_Paint);
			pBackground.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(pBackground_MouseDoubleClick);
			pWeight.BackColor = System.Drawing.Color.White;
			pWeight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			pWeight.Controls.Add(tWeight);
			pWeight.Location = new System.Drawing.Point(47, 37);
			pWeight.Name = "pWeight";
			pWeight.Size = new System.Drawing.Size(186, 74);
			pWeight.TabIndex = 20;
			pWeight.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(pWeight_MouseDoubleClick);
			tWeight.Font = new System.Drawing.Font("Arial Narrow", 36f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			tWeight.ForeColor = System.Drawing.Color.Black;
			tWeight.Location = new System.Drawing.Point(-1, 2);
			tWeight.Name = "tWeight";
			tWeight.Size = new System.Drawing.Size(185, 68);
			tWeight.TabIndex = 19;
			tWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			tWeight.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(tWeight_MouseDoubleClick);
			lScaleID.AutoSize = true;
			lScaleID.Font = new System.Drawing.Font("Verdana", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lScaleID.ForeColor = System.Drawing.Color.White;
			lScaleID.Location = new System.Drawing.Point(3, 0);
			lScaleID.Name = "lScaleID";
			lScaleID.Size = new System.Drawing.Size(29, 16);
			lScaleID.TabIndex = 18;
			lScaleID.Text = "ID:";
			lScaleID.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(lScaleID_MouseDoubleClick);
			lInterface.AutoSize = true;
			lInterface.Font = new System.Drawing.Font("Arial", 9f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lInterface.ForeColor = System.Drawing.Color.White;
			lInterface.Location = new System.Drawing.Point(45, 119);
			lInterface.Name = "lInterface";
			lInterface.Size = new System.Drawing.Size(28, 15);
			lInterface.TabIndex = 15;
			lInterface.Text = "???";
			lInterface.Click += new System.EventHandler(lInterface_Click);
			lInterface.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(lInterface_MouseDoubleClick);
			lConnection.AutoSize = true;
			lConnection.Font = new System.Drawing.Font("Arial", 9f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lConnection.ForeColor = System.Drawing.Color.White;
			lConnection.Location = new System.Drawing.Point(115, 119);
			lConnection.Name = "lConnection";
			lConnection.Size = new System.Drawing.Size(71, 15);
			lConnection.TabIndex = 12;
			lConnection.Text = "Connection";
			lConnection.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(lConnection_MouseDoubleClick);
			timer1000mS.Enabled = true;
			timer1000mS.Interval = 300;
			timer1000mS.Tick += new System.EventHandler(timer1000mS_Tick);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.White;
			base.ClientSize = new System.Drawing.Size(240, 142);
			base.ControlBox = false;
			base.Controls.Add(pBackground);
			ForeColor = System.Drawing.Color.Chocolate;
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			base.Name = "ScaleForm";
			base.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			base.FormClosing += new System.Windows.Forms.FormClosingEventHandler(ScaleForm_FormClosing);
			base.Load += new System.EventHandler(ScaleForm_Load);
			base.Shown += new System.EventHandler(ScaleForm_Shown);
			base.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(ScaleForm_MouseDoubleClick);
			pBackground.ResumeLayout(false);
			pBackground.PerformLayout();
			pWeight.ResumeLayout(false);
			ResumeLayout(false);
		}
	}
}
