using System;
using System.Collections.Generic;

namespace ScalesView
{
	[Serializable]
	public class ScalesList
	{
		private List<ScaleInfo> _list;

		public List<ScaleInfo> mScalesList
		{
			get
			{
				return _list;
			}
			set
			{
				_list = value;
			}
		}

		public ScalesList()
		{
			_list = new List<ScaleInfo>();
		}

		public void Add(ScaleInfo _si)
		{
			_list.Add(_si);
		}

		public void Delete(int _index)
		{
			_list.RemoveAt(_index);
		}

		public void Clear()
		{
			_list.Clear();
		}

		public ScaleInfo Get(int _index)
		{
			return _list[_index];
		}
	}
}
