using System.Windows.Forms;

namespace ScalesView
{
	internal class Messages
	{
		public static void MessageError(string _error)
		{
			MessageBox.Show(_error, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}

		public static void MessageWarning(string _error)
		{
			MessageBox.Show(_error, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}
	}
}
