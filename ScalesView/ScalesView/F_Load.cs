using ScalesView.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ScalesView
{
	public class F_Load : Form
	{
		private int FullTime;

		private string sName = "Загрузка...";

		private IContainer components;

		private Label lText;

		private Label lName;

		private ProgressBar pFinish;

		public F_Load(int _time)
		{
			InitializeComponent();
			FullTime = _time * 10;
			lName.ForeColor = (lText.ForeColor = Color.White);
			lName.BackColor = (lText.BackColor = Color.FromArgb(255, 0, 102, 179));
		}

		public F_Load(string _name)
		{
			InitializeComponent();
			sName = _name;
			lName.ForeColor = (lText.ForeColor = Color.White);
			lName.BackColor = (lText.BackColor = Color.FromArgb(255, 0, 102, 179));
		}

		public F_Load()
		{
			InitializeComponent();
			lName.ForeColor = (lText.ForeColor = Color.White);
			lName.BackColor = (lText.BackColor = Color.FromArgb(255, 0, 102, 179));
		}

		private void F_Load_Load(object sender, EventArgs e)
		{
			pFinish.Visible = ((FullTime != 0) ? true : false);
			pFinish.Maximum = 100;
			lText.Text = sName;
		}

		private void F_Load_FormClosing(object sender, FormClosingEventArgs e)
		{
		}

		private void label2_Click(object sender, EventArgs e)
		{
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			lText = new System.Windows.Forms.Label();
			lName = new System.Windows.Forms.Label();
			pFinish = new System.Windows.Forms.ProgressBar();
			SuspendLayout();
			lText.AutoSize = true;
			lText.Location = new System.Drawing.Point(201, 76);
			lText.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
			lText.Name = "lText";
			lText.Size = new System.Drawing.Size(91, 19);
			lText.TabIndex = 1;
			lText.Text = "Загрузка...";
			lName.AutoSize = true;
			lName.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lName.Location = new System.Drawing.Point(173, 15);
			lName.Name = "lName";
			lName.Size = new System.Drawing.Size(164, 24);
			lName.TabIndex = 2;
			lName.Text = "ScalesView 100";
			lName.Click += new System.EventHandler(label2_Click);
			pFinish.Location = new System.Drawing.Point(162, 111);
			pFinish.Name = "pFinish";
			pFinish.Size = new System.Drawing.Size(171, 23);
			pFinish.TabIndex = 3;
			base.AutoScaleDimensions = new System.Drawing.SizeF(10f, 19f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackgroundImage = ScalesView.Properties.Resources.LoadForm_1;
			base.ClientSize = new System.Drawing.Size(378, 149);
			base.ControlBox = false;
			base.Controls.Add(pFinish);
			base.Controls.Add(lName);
			base.Controls.Add(lText);
			Cursor = System.Windows.Forms.Cursors.WaitCursor;
			Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			base.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
			base.Name = "F_Load";
			base.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			base.FormClosing += new System.Windows.Forms.FormClosingEventHandler(F_Load_FormClosing);
			base.Load += new System.EventHandler(F_Load_Load);
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
