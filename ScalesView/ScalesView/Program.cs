using System;
using System.Threading;
using System.Windows.Forms;

namespace ScalesView
{
	internal static class Program
	{
		private const string SVMutexName = "ScalesView100";

		private const string DSMutexName = "ScalesSettings100";

		[STAThread]
		private static void Main()
		{
			bool flag = false;
			bool flag2 = false;
			using (Mutex mutex = new Mutex(initiallyOwned: false, "ScalesView100"))
			{
				flag = !mutex.WaitOne(0, exitContext: false);
				try
				{
					flag2 = ((Mutex.OpenExisting("ScalesSettings100") != null) ? true : false);
				}
				catch
				{
				}
				if (!flag && !flag2)
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(defaultValue: false);
					Application.Run(new MainScalesView());
				}
				else if (flag)
				{
					MessageBox.Show("Программа \"ScalesView 100\" уже запущена!", "Ошибка запуска программы", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					MessageBox.Show("Совместная работа программ \"ScalesView 100\" и\"Настройка весов 100\" невозможна.\r\nДля дальнейшей работы закройте программу \"Настройка весов 100\" и перезапустите \"ScalesView 100\".", "Ошибка запуска программы", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
			}
		}
	}
}
