using System;

namespace ScalesView
{
	[Serializable]
	public class KeyConfig
	{
		public int KG_Enable = 1;

		public int Top = 1;

		public int Enable = 1;

		public int CntrlKey = 162;

		public int Key = 74;

		public int Point;
	}
}
