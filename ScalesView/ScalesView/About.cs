using ScalesView.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace ScalesView
{
	public class About : Form
	{
		private IContainer components;

		private PictureBox pictureBox1;

		private Label label1;

		private Button bExit;

		private Label label2;

		private Label label3;

		private Label lVersion;

		public About()
		{
			InitializeComponent();
		}

		private void bExit_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void About_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				Close();
			}
		}

		private void About_Load(object sender, EventArgs e)
		{
			string str = Assembly.GetExecutingAssembly().GetName().Version.ToString();
			lVersion.Text = "Версия " + str;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			label1 = new System.Windows.Forms.Label();
			bExit = new System.Windows.Forms.Button();
			pictureBox1 = new System.Windows.Forms.PictureBox();
			label2 = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			lVersion = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
			SuspendLayout();
			label1.AutoSize = true;
			label1.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			label1.Location = new System.Drawing.Point(174, 12);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(164, 24);
			label1.TabIndex = 1;
			label1.Text = "ScalesView 100";
			bExit.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			bExit.Location = new System.Drawing.Point(319, 116);
			bExit.Name = "bExit";
			bExit.Size = new System.Drawing.Size(75, 34);
			bExit.TabIndex = 2;
			bExit.Text = "Выход";
			bExit.UseVisualStyleBackColor = true;
			bExit.Visible = false;
			bExit.Click += new System.EventHandler(bExit_Click);
			pictureBox1.Image = ScalesView.Properties.Resources.Massa_K;
			pictureBox1.Location = new System.Drawing.Point(12, 12);
			pictureBox1.Name = "pictureBox1";
			pictureBox1.Size = new System.Drawing.Size(130, 128);
			pictureBox1.TabIndex = 0;
			pictureBox1.TabStop = false;
			label2.AutoSize = true;
			label2.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			label2.Location = new System.Drawing.Point(174, 82);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(162, 24);
			label2.TabIndex = 3;
			label2.Text = "АО \"МАССА-К\"";
			label3.AutoSize = true;
			label3.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			label3.Location = new System.Drawing.Point(174, 116);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(77, 24);
			label3.TabIndex = 4;
			label3.Text = "2018 г.";
			lVersion.AutoSize = true;
			lVersion.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lVersion.Location = new System.Drawing.Point(174, 47);
			lVersion.Name = "lVersion";
			lVersion.Size = new System.Drawing.Size(87, 24);
			lVersion.TabIndex = 5;
			lVersion.Text = "Версия";
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(406, 157);
			base.Controls.Add(lVersion);
			base.Controls.Add(label3);
			base.Controls.Add(label2);
			base.Controls.Add(bExit);
			base.Controls.Add(label1);
			base.Controls.Add(pictureBox1);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			base.KeyPreview = true;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "About";
			Text = "О программе";
			base.Load += new System.EventHandler(About_Load);
			base.KeyDown += new System.Windows.Forms.KeyEventHandler(About_KeyDown);
			((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
