using ScalesView.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace ScalesView
{
	public class MainScalesView : Form
	{
		private const bool AutoSearch = false;

		private const bool AddNewScaleOnStart = true;

		private const int MinXSize = 680;

		private const int MinYSize = 260;

		public FormSizes mFormSizes;

		public KeyConfig mKeys;

		public ScalesList mScalesRegsList;

		public ChangePort mChangePort;

		public List<ScaleForm> mScaleForm = new List<ScaleForm>();

		private List<ScaleData> mScaleData = new List<ScaleData>();

		private Thread ThreadProcessSearch;

		private Point MaxPoint = default(Point);

		private F_Help mHelp;

		private IContainer components;

		private MenuStrip menuStrip1;

		private ToolStripMenuItem MenuSearchNewScales;

		private ToolStripMenuItem MenuAbout;

		private System.Windows.Forms.Timer timForScan;

		private ToolStripMenuItem MenuHelp;

		private ToolStripTextBox menuPort;

		private ToolStripMenuItem портToolStripMenuItem;

		public MainScalesView()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			foreach (Control control in base.Controls)
			{
				try
				{
					MdiClient mdiClient = (MdiClient)control;
					mdiClient.BackColor = BackColor;
				}
				catch (InvalidCastException)
				{
				}
			}
			string text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
			Text = "ScalesView 100 v" + text;
			if (ScalesSaver.RestoreForm(out mFormSizes) <= 0)
			{
				mFormSizes = new FormSizes();
				if (!ScalesSaver.StoreForm(mFormSizes))
				{
					Messages.MessageError("Ошибка сохранения файла параметров 1");
					Close();
				}
			}
			if (mFormSizes.Version != text)
			{
				mFormSizes.Version = text;
				mKeys = new KeyConfig();
				if (!ScalesSaver.StoreKeys(mKeys))
				{
					Messages.MessageError("Ошибка сохранения файла параметров 2");
					Close();
				}
			}
			base.Location = new Point(mFormSizes.LastLocation.X, mFormSizes.LastLocation.Y);
			base.Size = new Size(Math.Max(680, mFormSizes.LastSize.Width), Math.Max(260, mFormSizes.LastSize.Height));
			if (ScalesSaver.RestoreKeys(out mKeys) <= 0)
			{
				mKeys = new KeyConfig();
				if (!ScalesSaver.StoreKeys(mKeys))
				{
					Messages.MessageError("Ошибка сохранения файла параметров 2");
					Close();
				}
			}
			int num = ScalesSaver.RestoreScales(out mScalesRegsList);
			if (num <= 0)
			{
				if (num < 0)
				{
					Messages.MessageWarning("Ошибка чтения файла параметров весов!\r\nСписок зарегестрированных весов потерян!");
				}
				mScalesRegsList = new ScalesList();
				if (!ScalesSaver.StoreScales(mScalesRegsList))
				{
					Messages.MessageError("Ошибка сохранения файла параметров 3");
					Close();
				}
			}
			F_Load f_Load = new F_Load();
			f_Load.Location = new Point(base.Location.X + 50, base.Location.Y + 100);
			f_Load.Show();
			f_Load.Update();
			if (mFormSizes.SearchPort < 100)
			{
				mFormSizes.SearchPort = 5001;
			}
			f_Load.Close();
			Cursor = Cursors.Default;
			menuStrip1.BackColor = Color.FromArgb(255, 0, 102, 179);
			menuStrip1.ForeColor = Color.White;
			menuPort.Text = mFormSizes.SearchPort.ToString();
			timForScan.Start();
		}

		public void StoreGefaultKeys(KeyConfig _key)
		{
			if (!ScalesSaver.StoreKeys(_key))
			{
				Messages.MessageError("Ошибка сохранения файла параметров горячих клавиш.");
				Close();
			}
		}

		private void MainScalesView_FormClosing(object sender, FormClosingEventArgs e)
		{
			ScalesSaver.StoreForm(mFormSizes);
		}

		private void MainScalesView_ResizeEnd(object sender, EventArgs e)
		{
			Size size = base.Size;
			if (size.Width < 680 || size.Height < 260)
			{
				int width = Math.Max(680, size.Width);
				int height = Math.Max(260, size.Height);
				base.Size = new Size(width, height);
				size = base.Size;
			}
			ReplaceScalesWidows();
		}

		private void ReplaceScalesWidows()
		{
			int num = (base.Size.Width - 15) / ScaleForm.X_Size;
			if (num < 1)
			{
				num = 1;
			}
			for (int i = 0; i < mScaleForm.Count; i++)
			{
				mScaleForm[i].Location = new Point(i % num * ScaleForm.X_Size + 5, i / num * ScaleForm.Y_Size + 5);
			}
			MaxPoint = new Point(mScaleForm.Count % num * ScaleForm.X_Size + 5, mScaleForm.Count / num * ScaleForm.Y_Size + 5);
			if (base.WindowState == FormWindowState.Normal)
			{
				mFormSizes.LastSize = base.Size;
				mFormSizes.LastLocation = base.Location;
			}
			if (mChangePort != null)
			{
				int x = Math.Max(MaxPoint.X, base.ClientSize.Width - mChangePort.Size.Width - 30);
				int y = 0;
				mChangePort.Location = new Point(x, y);
			}
		}

		private void MainScalesView_SizeChanged(object sender, EventArgs e)
		{
			try
			{
				if (base.WindowState != FormWindowState.Maximized && mFormSizes.LastWinState == FormWindowState.Maximized)
				{
					Math.Max(680, mFormSizes.LastSize.Width);
					Math.Max(260, mFormSizes.LastSize.Height);
					base.Size = mFormSizes.LastSize;
					ReplaceScalesWidows();
				}
				else if (base.WindowState == FormWindowState.Maximized && mFormSizes.LastWinState != FormWindowState.Maximized)
				{
					ReplaceScalesWidows();
				}
				mFormSizes.LastWinState = base.WindowState;
			}
			catch
			{
			}
		}

		private void timForScan_Tick(object sender, EventArgs e)
		{
			timForScan.Stop();
			timForScan.Start();
		}

		private bool StartBackgroundSearh(int _port)
		{
			try
			{
				ThreadProcessSearch = new Thread((ThreadStart)delegate
				{
					StartScaleSearch(_port, 1);
				});
				ThreadProcessSearch.IsBackground = true;
				ThreadProcessSearch.Start();
				return true;
			}
			catch
			{
				return false;
			}
		}

		private void StartScaleSearch(int _port, int _mode)
		{
			ScaleSearchList scaleSearchList = new ScaleSearchList();
			lock (mScaleData)
			{
				if (_mode == 0)
				{
					mScaleData = scaleSearchList.GetScales(IsCom: true);
				}
				else
				{
					mScaleData = scaleSearchList.GetScales(_port);
				}
				scaleSearchList.StoreLog();
			}
		}

		private bool CreateScaleForm(ScaleInfo _si)
		{
			for (int i = 0; i < mScaleForm.Count; i++)
			{
				if (mScaleForm[i].mScaleInfo.SN == _si.SN)
				{
					return false;
				}
			}
			ScaleForm scaleForm = new ScaleForm(_si);
			int num = 5;
			int num2 = 5;
			try
			{
				if (mScaleForm.Count > 0)
				{
					Point location = mScaleForm[0].Location;
					num = location.X;
					num2 = location.Y;
				}
				scaleForm.MdiParent = this;
				int num3 = (base.Size.Width - 15) / ScaleForm.X_Size;
				if (num3 < 1)
				{
					num3 = 1;
				}
				scaleForm.Start_Location = new Point(mScaleForm.Count % num3 * ScaleForm.X_Size + num, mScaleForm.Count / num3 * ScaleForm.Y_Size + num2);
				scaleForm.Show();
				scaleForm.BringToFront();
				scaleForm.Activate();
				mScaleForm.Add(scaleForm);
				return true;
			}
			catch
			{
				MessageBox.Show("Ошибка создания окна");
			}
			return false;
		}

		private bool TryToChangeScaleConnect(ScaleInfo _si)
		{
			for (int i = 0; i < mScaleForm.Count; i++)
			{
				if (mScaleForm[i].mScaleInfo.SN == _si.SN && !mScaleForm[i].IsWork())
				{
					mScaleForm[i].SetEnableDriver(Enabled: false);
					Thread.Sleep(200);
					mScaleForm[i].mScaleInfo.Connection = _si.Connection;
					mScaleForm[i].mScaleInfo.ComBoude = _si.ComBoude;
					mScaleForm[i].SetEnableDriver(Enabled: true);
					return true;
				}
			}
			return false;
		}

		private bool MergeScalesLeft()
		{
			bool result = false;
			for (int num = mScaleForm.Count - 1; num >= 0; num--)
			{
				if (!mScaleForm[num].IsWork())
				{
					mScaleForm[num].SetEnableDriver(Enabled: false);
					Thread.Sleep(200);
					mScaleForm[num].Close();
					mScaleForm.RemoveAt(num);
					result = true;
				}
			}
			Thread.Sleep(200);
			return result;
		}

		private void MenuSearchNewScales_Click(object sender, EventArgs e)
		{
			int num;
			try
			{
				num = int.Parse(menuPort.Text);
			}
			catch
			{
				MessageBox.Show("Ошибка ввода порта!", "Изменение порта", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
				num = mFormSizes.SearchPort;
				menuPort.Text = num.ToString();
				menuStrip1.Update();
			}
			if (num == 0)
			{
				MessageBox.Show("Ошибка ввода порта, порт не должен быть равен <0>!", "Изменение порта", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
				num = mFormSizes.SearchPort;
				menuPort.Text = num.ToString();
				menuStrip1.Update();
			}
			if (mFormSizes.SearchPort != num)
			{
				string text = $"Изменить порт для поиска весов?\r\nТекущее значение: {mFormSizes.SearchPort}, Новое значение: {num}.";
				if (MessageBox.Show(text, "Изменение порта", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					mFormSizes.SearchPort = num;
				}
				else
				{
					menuPort.Text = mFormSizes.SearchPort.ToString();
				}
			}
			bool flag = MergeScalesLeft();
			F_Load f_Load = new F_Load("Идет поиск...");
			f_Load.Location = new Point(base.Location.X + 50, base.Location.Y + 100);
			f_Load.Show();
			f_Load.Update();
			StartScaleSearch(mFormSizes.SearchPort, 1);
			if (flag)
			{
				ReplaceScalesWidows();
			}
			AddScaleFromList();
			f_Load.Close();
			Cursor = Cursors.Default;
			timForScan.Start();
		}

		private void AddScaleFromList()
		{
			lock (mScaleData)
			{
				for (int i = 0; i < mScaleData.Count; i++)
				{
					ScaleInfo scaleInfo = new ScaleInfo(mKeys);
					if (scaleInfo.LoadFromTCPSearch(mScaleData[i]) && !CreateScaleForm(scaleInfo))
					{
						TryToChangeScaleConnect(scaleInfo);
					}
				}
			}
		}

		private void MenuAbout_Click(object sender, EventArgs e)
		{
			ScaleInfo scaleInfo = new ScaleInfo();
			scaleInfo.Connection = "192.168.1.152:5003";
			scaleInfo.IsP100 = false;
			CreateScaleForm(scaleInfo);
		}

		private void toolStripTextBox1_Click(object sender, EventArgs e)
		{
		}

		private void MenuHelp_Click(object sender, EventArgs e)
		{
			if (!File.Exists(Path.Combine(Directory.GetCurrentDirectory(), "SV_support.htm")))
			{
				MessageBox.Show("Файл справки отсутствует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			else if (mHelp != null)
			{
				if (mHelp.IsDisposed)
				{
					mHelp = new F_Help("SV_support.htm");
					mHelp.Show();
				}
				else
				{
					mHelp.Focus();
				}
			}
			else
			{
				mHelp = new F_Help("SV_support.htm");
				mHelp.Show();
			}
		}

		private void MainScalesView_Scroll(object sender, ScrollEventArgs e)
		{
			mHelp.Show();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScalesView.MainScalesView));
			menuStrip1 = new System.Windows.Forms.MenuStrip();
			портToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			menuPort = new System.Windows.Forms.ToolStripTextBox();
			timForScan = new System.Windows.Forms.Timer(components);
			MenuSearchNewScales = new System.Windows.Forms.ToolStripMenuItem();
			MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
			MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
			menuStrip1.SuspendLayout();
			SuspendLayout();
			menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[5]
			{
				MenuSearchNewScales,
				MenuAbout,
				MenuHelp,
				портToolStripMenuItem,
				menuPort
			});
			menuStrip1.Location = new System.Drawing.Point(0, 0);
			menuStrip1.Name = "menuStrip1";
			menuStrip1.ShowItemToolTips = true;
			menuStrip1.Size = new System.Drawing.Size(914, 58);
			menuStrip1.TabIndex = 1;
			menuStrip1.Text = "menuStrip1";
			портToolStripMenuItem.AutoSize = false;
			портToolStripMenuItem.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			портToolStripMenuItem.Name = "портToolStripMenuItem";
			портToolStripMenuItem.ShowShortcutKeys = false;
			портToolStripMenuItem.Size = new System.Drawing.Size(79, 54);
			портToolStripMenuItem.Text = "Порт:";
			menuPort.AutoToolTip = true;
			menuPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
			menuPort.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			menuPort.Font = new System.Drawing.Font("Verdana", 15.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			menuPort.Name = "menuPort";
			menuPort.Size = new System.Drawing.Size(70, 54);
			menuPort.Text = "65536";
			menuPort.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			menuPort.ToolTipText = "IP порт";
			timForScan.Interval = 60000;
			timForScan.Tick += new System.EventHandler(timForScan_Tick);
			MenuSearchNewScales.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			MenuSearchNewScales.Image = ScalesView.Properties.Resources.Search50_1;
			MenuSearchNewScales.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			MenuSearchNewScales.Name = "MenuSearchNewScales";
			MenuSearchNewScales.Size = new System.Drawing.Size(299, 54);
			MenuSearchNewScales.Text = " Поиск оборудования";
			MenuSearchNewScales.ToolTipText = " Поиск подключенного оборудования";
			MenuSearchNewScales.Click += new System.EventHandler(MenuSearchNewScales_Click);
			MenuAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			MenuAbout.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			MenuAbout.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			MenuAbout.Name = "MenuAbout";
			MenuAbout.Size = new System.Drawing.Size(159, 54);
			MenuAbout.Text = "О программе";
			MenuAbout.ToolTipText = "О программе";
			MenuAbout.Visible = false;
			MenuAbout.Click += new System.EventHandler(MenuAbout_Click);
			MenuHelp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			MenuHelp.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			MenuHelp.Image = ScalesView.Properties.Resources.Help50_1;
			MenuHelp.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			MenuHelp.Name = "MenuHelp";
			MenuHelp.Size = new System.Drawing.Size(161, 54);
			MenuHelp.Text = "Справка";
			MenuHelp.ToolTipText = "Справка";
			MenuHelp.Click += new System.EventHandler(MenuHelp_Click);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.White;
			base.ClientSize = new System.Drawing.Size(914, 380);
			base.Controls.Add(menuStrip1);
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.IsMdiContainer = true;
			base.MainMenuStrip = menuStrip1;
			base.Name = "MainScalesView";
			Text = "ScalesView 100";
			base.FormClosing += new System.Windows.Forms.FormClosingEventHandler(MainScalesView_FormClosing);
			base.Load += new System.EventHandler(Form1_Load);
			base.ResizeEnd += new System.EventHandler(MainScalesView_ResizeEnd);
			base.Scroll += new System.Windows.Forms.ScrollEventHandler(MainScalesView_Scroll);
			base.SizeChanged += new System.EventHandler(MainScalesView_SizeChanged);
			menuStrip1.ResumeLayout(false);
			menuStrip1.PerformLayout();
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
