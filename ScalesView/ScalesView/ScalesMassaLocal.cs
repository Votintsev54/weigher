using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ScalesView
{
	internal class ScalesMassaLocal
	{
		public class TCP_Send
		{
			public int ErrCode;

			public bool isReady;
		}

		public class UdpState
		{
			public IPEndPoint e;

			public UdpClient u;
		}

		public class ScalesUdpList
		{
			public string SN;

			public string IP;

			public int _Interface;
		}

		public const byte CMD_GET_INFO = 0;

		private const byte CMD_GET_NAME = 32;

		private const byte CMD_TCP_SET_SCALE_NAME = 37;

		private const byte CMD_TCP_SET_WIFI = 49;

		private const byte CMD_GET_MASSA = 35;

		private const byte CMD_TCP_SET_ETHERNET = 38;

		private const byte CMD_TCP_GET_ETHERNET = 41;

		private const byte CMD_TCP_GET_WIFI_SSIDS = 43;

		private const byte CMD_TCP_SET_TARE = 163;

		private const byte CMD_TCP_GET_TARE = 161;

		private const byte CMD_TCP_GET_WEIGHT = 160;

		private const byte CMD_TCP_GET_SERIAL = 144;

		private const byte CMD_TCP_GET_SCALE_INFO = 56;

		private const byte CMD_TCP_GET_SCALE_IDS = 35;

		private const byte CMD_TCP_SET_RS232 = 44;

		private const byte CMD_TCP_GET_RS232 = 47;

		private const byte CMD_USB_SET_SCALE_INFO = 50;

		private const byte CMD_TCP_SET_STATE = 53;

		private const byte CMD_TCP_GET_STATE = 54;

		private const byte CMD_ASK_INFO = 1;

		private const byte CMD_ACK_MASSA = 36;

		private const byte CMD_ACK_NAME = 33;

		private const byte CMD_TCP_ACK_SERIAL = 80;

		private const byte CMD_TCP_ACK_TARE = 17;

		private const byte CMD_TCP_ACK_SET_TARE = 18;

		private const byte CMD_TCP_NACK_TARE = 21;

		private const byte CMD_TCP_ACK_WEIGHT = 16;

		private const byte CMD_TCP_NACK_WEIGHT = 20;

		private const byte CMD_TCP_ACK_GET_ETHERNET = 42;

		private const byte CMD_TCP_ACK_GET_WIFI = 52;

		private const byte CMD_TCP_ACK_WIFI_SSIDS = 45;

		private const byte CMD_TCP_ACK_GET_STATE = 55;

		private const byte CMD_TCP_ACK_GET_RS232 = 48;

		private const byte CMD_TCP_ACK_SCALE_INFO = 34;

		private const byte CMD_TCP_ACK_SCALE_IDS = 36;

		private const byte CMD_TCP_ERROR = 40;

		private const byte CMD_TCP_ACK_SET = 39;

		private const byte CMD_TCP_NACK = 240;

		public string Connection = "";

		public int ComBoude = 57600;

		public int ActiveInterface = -1;

		public string SerialNumber = "";

		public string ProductionDate = "";

		public string ScaleType = "";

		public int CalibrationCode;

		public int Latitude;

		public string FullName = "";

		public string Name = "";

		public string Comment = "";

		public int Weight;

		public int Division;

		public int Stable;

		public int Tare;

		public int bTare;

		public int bZero;

		public int TareDivision;

		public int ScaleState;

		public int Leds;

		public int Keys;

		public string EthernetIP = "";

		public string EthernetMask = "";

		public int EthernetPort;

		public string EthernetGateway = "";

		public string EthernetMAC = "";

		public string WifiIP = "";

		public string WifiMask = "";

		public string WifiGateway = "";

		public int WifiPort;

		public string WifiSSID = "";

		public string WifiPassword = "";

		public string WiFiMAC = "";

		public int RS232Protokol;

		public string[] SSIDs;

		private int ErrorCode;

		private bool InDataReady = true;

		private byte[] IPReceiveBuffer = new byte[2048];

		private SerialPort mSerialPort;

		private TcpClient mTCP_Port;

		private NetworkStream stream;

		private UdpClient udp;

		public List<ScalesUdpList> mScalesUdpList = new List<ScalesUdpList>();

		public string SearchList = "";

		public int SearchInterface;

		public int SearchCount;

		private void ReceivedIPCallBack(IAsyncResult result)
		{
			TCP_Send tCP_Send = (TCP_Send)result.AsyncState;
			try
			{
				int length = stream.EndRead(result);
				int num = DecodeData(IPReceiveBuffer, length);
				if (num != 0)
				{
					switch (num)
					{
					default:
						return;
					case 108:
					case 109:
					case 201:
					case 202:
					case 203:
					case 204:
					case 205:
					case 206:
					case 207:
					case 208:
					case 209:
					case 210:
					case 211:
					case 212:
					case 213:
					case 214:
					case 215:
					case 216:
					case 217:
					case 218:
					case 219:
					case 220:
					case 221:
					case 222:
					case 223:
					case 224:
					case 225:
					case 226:
					case 227:
					case 228:
					case 229:
					case 230:
					case 231:
					case 232:
					case 233:
					case 234:
					case 235:
					case 236:
					case 237:
					case 238:
					case 239:
					case 240:
					case 241:
					case 242:
					case 243:
					case 244:
					case 245:
					case 246:
					case 247:
					case 248:
					case 249:
					case 250:
					case 251:
					case 252:
					case 253:
					case 254:
					case 255:
					case 256:
					case 257:
					case 258:
					case 259:
					case 260:
					case 261:
					case 262:
					case 263:
					case 264:
					case 265:
					case 266:
					case 267:
					case 268:
					case 269:
					case 270:
					case 271:
					case 272:
					case 273:
					case 274:
					case 275:
					case 276:
					case 277:
					case 278:
					case 279:
					case 280:
					case 281:
					case 282:
					case 283:
					case 284:
					case 285:
					case 286:
					case 287:
					case 288:
					case 289:
					case 290:
					case 291:
					case 292:
					case 293:
					case 294:
					case 295:
					case 296:
					case 297:
					case 298:
						break;
					}
				}
				tCP_Send.isReady = true;
				tCP_Send.ErrCode = num;
			}
			catch
			{
				tCP_Send.isReady = true;
				tCP_Send.ErrCode = 1;
			}
		}

		public int CloseConnection()
		{
			try
			{
				if (mSerialPort.IsOpen)
				{
					mSerialPort.Close();
					return 0;
				}
			}
			catch
			{
			}
			try
			{
				mTCP_Port.Close();
				return 0;
			}
			catch
			{
			}
			return 1;
		}

		public int OpenConnection()
		{
			try
			{
				if (mSerialPort.IsOpen)
				{
					return 2;
				}
			}
			catch
			{
			}
			try
			{
				if (mTCP_Port.Connected)
				{
					return 2;
				}
			}
			catch
			{
			}
			if (Connection.IndexOf("COM") == 0)
			{
				if (ComBoude != 4800)
				{
					mSerialPort = new SerialPort(Connection, 57600, Parity.None);
				}
				else
				{
					mSerialPort = new SerialPort(Connection, 4800, Parity.Even);
				}
				try
				{
					mSerialPort.Open();
				}
				catch
				{
					return 1;
				}
				return 0;
			}
			string[] array = Connection.Split(':');
			if (array.Length != 2)
			{
				return 1;
			}
			if (!IPAddress.TryParse(array[0], out IPAddress address))
			{
				return 1;
			}
			if (!int.TryParse(array[1], out int result))
			{
				return 1;
			}
			mTCP_Port = new TcpClient();
			mTCP_Port.ReceiveTimeout = 1000;
			mTCP_Port.SendTimeout = 1000;
			mTCP_Port.NoDelay = false;
			mTCP_Port.BeginConnect(address, result, null, null);
			for (int i = 0; i < 20; i++)
			{
				if (mTCP_Port.Connected)
				{
					stream = mTCP_Port.GetStream();
					return 0;
				}
				Thread.Sleep(50);
			}
			return 1;
		}

		private int SendDataLocal(byte[] mdata)
		{
			byte[] array = new byte[500];
			int num = 0;
			int num2 = 0;
			try
			{
				if (mSerialPort.IsOpen)
				{
					mSerialPort.WriteTimeout = 200;
					mSerialPort.Write(mdata, 0, mdata.Length);
					for (int i = 0; i < 10; i++)
					{
						if (mSerialPort.BytesToRead > 0)
						{
							num = mSerialPort.Read(array, num2, 500 - num2);
							num2 += num;
						}
						if (DecodeData(array, num2) == 0)
						{
							ErrorCode = 0;
							return 0;
						}
						Thread.Sleep(50);
					}
					if (mdata[5] == 0)
					{
						return 2;
					}
					return DecodeData(array, num2);
				}
			}
			catch
			{
			}
			try
			{
				if (mTCP_Port.Connected)
				{
					ErrorCode = 0;
					InDataReady = false;
					stream.Write(mdata, 0, mdata.Length);
					ErrorCode = 0;
					TCP_Send tCP_Send = new TCP_Send();
					mTCP_Port.ReceiveTimeout = 1250;
					mTCP_Port.SendTimeout = 1250;
					stream.BeginRead(IPReceiveBuffer, 0, 500, ReceivedIPCallBack, tCP_Send);
					for (int i = 0; i < 25; i++)
					{
						if (tCP_Send.isReady)
						{
							return tCP_Send.ErrCode;
						}
						Thread.Sleep(50);
					}
					return 2;
				}
			}
			catch
			{
			}
			return 1;
		}

		public void ReceiveCallback(IAsyncResult ar)
		{
			UdpClient u = ((UdpState)ar.AsyncState).u;
			IPEndPoint remoteEP = ((UdpState)ar.AsyncState).e;
			byte[] array;
			try
			{
				array = u.EndReceive(ar, ref remoteEP);
			}
			catch
			{
				return;
			}
			ErrorCode = DecodeData(array, array.Length);
			ScalesUdpList scalesUdpList = new ScalesUdpList();
			if (ErrorCode == 0)
			{
				scalesUdpList.SN = SerialNumber;
				scalesUdpList.IP = remoteEP.Address.ToString() + ":" + EthernetPort.ToString();
				SearchList = SearchList + scalesUdpList.IP + "\r\n";
				SearchCount++;
				scalesUdpList._Interface = ActiveInterface;
				mScalesUdpList.Add(scalesUdpList);
			}
			else
			{
				scalesUdpList.SN = "ERR " + ErrorCode.ToString();
				scalesUdpList.IP = remoteEP.Address.ToString();
				mScalesUdpList.Add(scalesUdpList);
			}
		}

		public int GetScalesList()
		{
			int num = 0;
			string hostName = Dns.GetHostName();
			IPHostEntry hostEntry = Dns.GetHostEntry(hostName);
			IPAddress[] addressList = hostEntry.AddressList;
			mScalesUdpList.Clear();
			SearchList = "";
			SearchCount = 0;
			for (int i = 0; i < addressList.Length; i++)
			{
				try
				{
					udp = new UdpClient();
					string[] array = addressList[i].ToString().Split('.', ',', ':', ';');
					IPAddress address = IPAddress.Parse($"{array[0]}.{array[1]}.{array[2]}.255");
					IPEndPoint iPEndPoint = new IPEndPoint(address, EthernetPort);
					CodeData(0, out byte[] DataOut);
					udp.Send(DataOut, DataOut.Length, iPEndPoint);
					UdpState udpState = new UdpState();
					udpState.e = iPEndPoint;
					udpState.u = udp;
					for (num = 0; num < 100; num++)
					{
						udp.BeginReceive(ReceiveCallback, udpState);
						Thread.Sleep(20);
					}
					udp.Close();
				}
				catch
				{
				}
			}
			return 0;
		}

		private int SendData(byte command, byte[] data)
		{
			byte[] DataOut;
			int num = CodeData(command, data, out DataOut);
			if (num <= 0)
			{
				return 100;
			}
			return SendDataLocal(DataOut);
		}

		private int SendData(byte command)
		{
			byte[] DataOut;
			int num = CodeData(command, out DataOut);
			if (num <= 0)
			{
				return 100;
			}
			return SendDataLocal(DataOut);
		}

		public int ReadScaleSSIDs()
		{
			return SendData(43);
		}

		public int ReadWeight()
		{
			return SendData(160);
		}

		public int ReadMassa()
		{
			return SendData(35);
		}

		public int ReadEthernetSettings()
		{
			return SendData(41);
		}

		public int ReadScaleState()
		{
			return SendData(54);
		}

		public int ReadRS232Settings()
		{
			return SendData(47);
		}

		public int ReadScaleInfo()
		{
			return SendData(0);
		}

		public int ReadSerial()
		{
			return SendData(144);
		}

		public int ReadScaleParameters()
		{
			return SendData(32);
		}

		public int ReadTare()
		{
			return SendData(161);
		}

		public int WriteRS232Settings()
		{
			if (RS232Protokol < 0 || RS232Protokol > 2)
			{
				return 12;
			}
			byte[] array = new byte[6]
			{
				(byte)RS232Protokol,
				0,
				0,
				0,
				0,
				0
			};
			for (int i = 1; i < 6; i++)
			{
				array[i] = 0;
			}
			return SendData(44, array);
		}

		public int WriteEthernetSettings()
		{
			int num = 0;
			if (!IPAddress.TryParse(EthernetIP, out IPAddress address))
			{
				return 12;
			}
			if (!IPAddress.TryParse(EthernetMask, out address))
			{
				return 12;
			}
			if (!IPAddress.TryParse(EthernetGateway, out address))
			{
				return 12;
			}
			if (EthernetPort == 0)
			{
				return 12;
			}
			byte[] array = new byte[14];
			string[] array2 = EthernetIP.Split('.');
			if (array2.Length != 4)
			{
				return 12;
			}
			byte result;
			for (int num2 = 3; num2 >= 0; num2--)
			{
				if (!byte.TryParse(array2[num2], out result))
				{
					return 12;
				}
				array[num++] = result;
			}
			array2 = EthernetMask.Split('.');
			if (array2.Length != 4)
			{
				return 12;
			}
			for (int num2 = 3; num2 >= 0; num2--)
			{
				if (!byte.TryParse(array2[num2], out result))
				{
					return 12;
				}
				array[num++] = result;
			}
			array2 = EthernetGateway.Split('.');
			if (array2.Length != 4)
			{
				return 12;
			}
			for (int num2 = 3; num2 >= 0; num2--)
			{
				if (!byte.TryParse(array2[num2], out result))
				{
					return 12;
				}
				array[num++] = result;
			}
			array[num++] = (byte)EthernetPort;
			array[num++] = (byte)(EthernetPort >> 8);
			return SendData(38, array);
		}

		public int WriteScaleName(string _name)
		{
			byte[] array = new byte[200];
			int num = 0;
			byte[] bytes = Encoding.GetEncoding("windows-1251").GetBytes(_name);
			for (int i = 0; i < bytes.Length; i++)
			{
				array[num++] = bytes[i];
			}
			array[num++] = 13;
			array[num++] = 10;
			byte[] array2 = new byte[num];
			for (int i = 0; i < num; i++)
			{
				array2[i] = array[i];
			}
			return SendData(37, array2);
		}

		public int WriteWifiSettings()
		{
			int num = 0;
			if (!IPAddress.TryParse(WifiIP, out IPAddress address))
			{
				return 12;
			}
			if (!IPAddress.TryParse(WifiMask, out address))
			{
				return 12;
			}
			if (!IPAddress.TryParse(WifiGateway, out address))
			{
				return 12;
			}
			if (WifiPort == 0)
			{
				return 12;
			}
			byte[] array = new byte[200];
			string[] array2 = WifiIP.Split('.');
			if (array2.Length != 4)
			{
				return 12;
			}
			byte result;
			for (int num2 = 3; num2 >= 0; num2--)
			{
				if (!byte.TryParse(array2[num2], out result))
				{
					return 12;
				}
				array[num++] = result;
			}
			array2 = WifiMask.Split('.');
			if (array2.Length != 4)
			{
				return 12;
			}
			for (int num2 = 3; num2 >= 0; num2--)
			{
				if (!byte.TryParse(array2[num2], out result))
				{
					return 12;
				}
				array[num++] = result;
			}
			array2 = WifiGateway.Split('.');
			if (array2.Length != 4)
			{
				return 12;
			}
			for (int num2 = 3; num2 >= 0; num2--)
			{
				if (!byte.TryParse(array2[num2], out result))
				{
					return 12;
				}
				array[num++] = result;
			}
			array[num++] = (byte)WifiPort;
			array[num++] = (byte)(WifiPort >> 8);
			byte[] bytes = Encoding.GetEncoding("windows-1251").GetBytes(WifiSSID);
			for (int num2 = 0; num2 < bytes.Length; num2++)
			{
				array[num++] = bytes[num2];
			}
			array[num++] = 13;
			array[num++] = 10;
			bytes = Encoding.GetEncoding("windows-1251").GetBytes(WifiPassword);
			for (int num2 = 0; num2 < bytes.Length; num2++)
			{
				array[num++] = bytes[num2];
			}
			array[num++] = 13;
			array[num++] = 10;
			byte[] array3 = new byte[num];
			for (int num2 = 0; num2 < num; num2++)
			{
				array3[num2] = array[num2];
			}
			return SendData(49, array3);
		}

		public int WriteScaleState()
		{
			byte[] array = new byte[4];
			array[0] = (byte)ScaleState;
			array[1] = (byte)Keys;
			array[2] = (array[3] = 0);
			return SendData(53, array);
		}

		public int WriteSystemInfo()
		{
			byte[] array = new byte[21];
			if (SerialNumber.Length != 10 && SerialNumber.Length != 0)
			{
				return 12;
			}
			if (ProductionDate.Length != 10 && ProductionDate.Length != 0)
			{
				return 12;
			}
			if (EthernetMAC.Length != 12 && EthernetMAC.Length != 0)
			{
				return 12;
			}
			if (WiFiMAC.Length != 12 && WiFiMAC.Length != 0)
			{
				return 12;
			}
			if (!ulong.TryParse(SerialNumber, NumberStyles.HexNumber, null, out ulong result))
			{
				return 12;
			}
			if (!ulong.TryParse(EthernetMAC, NumberStyles.HexNumber, null, out ulong result2))
			{
				return 12;
			}
			if (!ulong.TryParse(WiFiMAC, NumberStyles.HexNumber, null, out ulong result3))
			{
				return 12;
			}
			if (!DateTime.TryParse(ProductionDate, out DateTime result4))
			{
				return 12;
			}
			int num = 0;
			for (int i = 0; i < 5; i++)
			{
				array[num] = (byte)(result >> 32);
				result <<= 8;
				num++;
			}
			string s = $"{result4.Day:00}{result4.Month:00}{result4.Year - 2000:00}";
			if (!uint.TryParse(s, NumberStyles.HexNumber, null, out uint result5))
			{
				return 12;
			}
			for (int i = 0; i < 3; i++)
			{
				array[num] = (byte)(result5 >> 16);
				result5 <<= 8;
				num++;
			}
			for (int i = 0; i < 6; i++)
			{
				array[num] = (byte)(result2 >> 40);
				result2 <<= 8;
				num++;
			}
			for (int i = 0; i < 6; i++)
			{
				array[num] = (byte)(result3 >> 40);
				result3 <<= 8;
				num++;
			}
			array[num] = 165;
			return SendData(50, array);
		}

		private ushort CRC16(byte[] buf, int Start, int len)
		{
			ushort num = 0;
			for (ushort num2 = 0; num2 < len; num2 = (ushort)(num2 + 1))
			{
				ushort num3 = 0;
				ushort num4 = (ushort)(num >> 8 << 8);
				for (ushort num5 = 0; num5 < 8; num5 = (ushort)(num5 + 1))
				{
					num3 = ((((num4 ^ num3) & 0x8000) == 0) ? ((ushort)(num3 << 1)) : ((ushort)((num3 << 1) ^ 0x1021)));
					num4 = (ushort)(num4 << 1);
				}
				num = (ushort)(num3 ^ (num << 8) ^ (buf[num2 + Start] & 0xFF));
			}
			return num;
		}

		public int CodeData(byte Command, byte[] Data, out byte[] DataOut)
		{
			DataOut = new byte[Data.Length + 8];
			DataOut[0] = 248;
			DataOut[1] = 85;
			DataOut[2] = 206;
			DataOut[3] = (byte)(Data.Length + 1);
			DataOut[4] = 0;
			DataOut[5] = Command;
			for (int i = 0; i < Data.Length; i++)
			{
				DataOut[i + 6] = Data[i];
			}
			ushort num = CRC16(DataOut, 5, Data.Length + 1);
			DataOut[Data.Length + 7] = (byte)(num >> 8);
			DataOut[Data.Length + 6] = (byte)num;
			return Data.Length + 7;
		}

		public int CodeData(byte Command, out byte[] DataOut)
		{
			DataOut = new byte[8];
			DataOut[0] = 248;
			DataOut[1] = 85;
			DataOut[2] = 206;
			DataOut[3] = 1;
			DataOut[4] = 0;
			DataOut[5] = Command;
			ushort num = CRC16(DataOut, 5, 1);
			DataOut[7] = (byte)(num >> 8);
			DataOut[6] = (byte)num;
			return 8;
		}

		private int DecodeData(byte[] indata, int Length)
		{
			if (indata.Length < Length)
			{
				return 150;
			}
			if (Length < 8)
			{
				return 8;
			}
			if (indata[0] != 248 || indata[1] != 85 || indata[2] != 206)
			{
				return 9;
			}
			int num = indata[3];
			num += indata[4] << 8;
			if (Length != num + 7)
			{
				return 10;
			}
			byte b = indata[5];
			ushort num2 = CRC16(indata, 5, num);
			if (indata[num + 5] != (byte)num2 || indata[num + 6] != (byte)(num2 >> 8))
			{
				return 11;
			}
			switch (b)
			{
			case 20:
				ErrorCode = 21;
				break;
			case 240:
				ErrorCode = 32;
				break;
			case 40:
				ErrorCode = 100 + indata[6];
				break;
			case 39:
				ErrorCode = 0;
				break;
			case 48:
				RS232Protokol = indata[6];
				ErrorCode = 0;
				break;
			case 34:
			{
				SerialNumber = $"{indata[6]:X2}{indata[7]:X2}{indata[8]:X2}{indata[9]:X2}{indata[10]:X2}";
				ScaleType = $"{indata[11]:X2}{indata[12]:X2}{indata[13]:X2}";
				ProductionDate = $"{indata[14]:X2}.{indata[15]:X2}.20{indata[16]:X2}";
				CalibrationCode = indata[17] + (indata[18] << 8) + (indata[19] << 16) + (indata[20] << 24);
				Latitude = indata[21];
				ActiveInterface = indata[22];
				string[] array = Encoding.GetEncoding("windows-1251").GetString(indata, 23, num - 18).Split('\n');
				if (array.Length > 3)
				{
					FullName = array[0].Trim('\r');
					Name = array[1].Trim('\r');
				}
				ErrorCode = 0;
				break;
			}
			case 16:
			{
				uint num3 = (uint)(Weight = indata[6] + (indata[7] << 8) + (indata[8] << 16) + (indata[9] << 24));
				Division = indata[10];
				Stable = indata[11];
				ErrorCode = 0;
				break;
			}
			case 36:
			{
				uint num3 = (uint)(Weight = indata[6] + (indata[7] << 8) + (indata[8] << 16) + (indata[9] << 24));
				Division = indata[10];
				Stable = indata[11];
				bTare = indata[12];
				bZero = indata[13];
				ErrorCode = 0;
				break;
			}
			case 17:
			{
				uint num3 = (uint)(Tare = indata[6] + (indata[7] << 8) + (indata[8] << 16) + (indata[9] << 24));
				TareDivision = indata[10];
				ErrorCode = 0;
				break;
			}
			case 55:
				ScaleState = indata[6];
				Keys = indata[7];
				Leds = indata[8];
				ActiveInterface = indata[9];
				ErrorCode = 0;
				break;
			case 42:
				EthernetIP = string.Format("{3}.{2}.{1}.{0}", indata[6], indata[7], indata[8], indata[9]);
				EthernetMask = string.Format("{3}.{2}.{1}.{0}", indata[10], indata[11], indata[12], indata[13]);
				EthernetGateway = string.Format("{3}.{2}.{1}.{0}", indata[14], indata[15], indata[16], indata[17]);
				EthernetPort = indata[18] + (indata[19] << 8);
				if (num == 21)
				{
					EthernetMAC = $"{indata[25]:x2}:{indata[24]:x2}:{indata[23]:x2}:{indata[22]:x2}:{indata[21]:x2}:{indata[20]:x2}";
				}
				else
				{
					EthernetMAC = "Не найден";
				}
				ErrorCode = 0;
				break;
			case 33:
			{
				SerialNumber = ((indata[9] << 24) + (indata[8] << 16) + (indata[7] << 8) + indata[6]).ToString();
				string[] array = Encoding.GetEncoding("windows-1251").GetString(indata, 10, num - 4).Split('\n');
				Name = array[0].Trim('\r');
				ErrorCode = 0;
				break;
			}
			case 52:
			{
				WifiIP = string.Format("{3}.{2}.{1}.{0}", indata[6], indata[7], indata[8], indata[9]);
				WifiMask = string.Format("{3}.{2}.{1}.{0}", indata[10], indata[11], indata[12], indata[13]);
				WifiGateway = string.Format("{3}.{2}.{1}.{0}", indata[14], indata[15], indata[16], indata[17]);
				WifiPort = indata[18] + (indata[19] << 8);
				WiFiMAC = $"{indata[25]:x2}:{indata[24]:x2}:{indata[23]:x2}:{indata[22]:x2}:{indata[21]:x2}:{indata[20]:x2}";
				string[] array = Encoding.GetEncoding("windows-1251").GetString(indata, 26, num - 21).Split('\n');
				if (array.Length > 2)
				{
					WifiSSID = array[0].Trim('\r');
					WifiPassword = array[1].Trim('\r');
				}
				ErrorCode = 0;
				break;
			}
			case 80:
				SerialNumber = ((indata[9] << 24) + (indata[8] << 16) + (indata[7] << 8) + indata[6]).ToString();
				ActiveInterface = -1;
				break;
			case 1:
				ScaleType = $"{indata[8]:X2}{indata[9]:X2}{indata[10]:X2}";
				SerialNumber = ((indata[14] << 24) + (indata[13] << 16) + (indata[12] << 8) + indata[11]).ToString();
				ActiveInterface = indata[22];
				ErrorCode = 0;
				break;
			case 45:
			{
				string @string = Encoding.ASCII.GetString(indata, 6, num - 1);
				SSIDs = @string.Split('\n');
				for (int i = 0; i < SSIDs.Length; i++)
				{
					SSIDs[i].Replace('\r', '\0');
				}
				ErrorCode = 0;
				break;
			}
			default:
				ErrorCode = 71;
				break;
			}
			return ErrorCode;
		}
	}
}
