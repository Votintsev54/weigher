using System;
using System.Drawing;
using System.Windows.Forms;

namespace ScalesView
{
	[Serializable]
	public class FormSizes
	{
		public int Status;

		public Size LastSize;

		public FormWindowState LastWinState;

		public int SearchPort = 5001;

		public Point LastLocation;

		public string Version;
	}
}
