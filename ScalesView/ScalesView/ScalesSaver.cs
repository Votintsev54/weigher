using ScalesView.Properties;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ScalesView
{
	public class ScalesSaver
	{
		private static string GetFileKey()
		{
			string text = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Settings.Default.DirectoryInit;
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			return Path.Combine(text, Settings.Default.FileKey);
		}

		private static string GetFileForm()
		{
			string text = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Settings.Default.DirectoryInit;
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			return Path.Combine(text, Settings.Default.FileForm);
		}

		private static string GetFileScale()
		{
			string text = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Settings.Default.DirectoryInit;
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			return Path.Combine(text, Settings.Default.FileScales);
		}

		public static bool StoreKeys(KeyConfig _key)
		{
			FileStream fileStream;
			try
			{
				fileStream = new FileStream(GetFileKey(), FileMode.Create);
			}
			catch
			{
				return false;
			}
			try
			{
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(fileStream, _key);
				fileStream.Close();
			}
			catch
			{
				fileStream.Close();
				return false;
			}
			return true;
		}

		public static int RestoreKeys(out KeyConfig _key)
		{
			_key = null;
			if (!File.Exists(GetFileKey()))
			{
				return 0;
			}
			FileStream fileStream;
			try
			{
				fileStream = new FileStream(GetFileKey(), FileMode.Open);
			}
			catch
			{
				return -1;
			}
			try
			{
				IFormatter formatter = new BinaryFormatter();
				_key = (KeyConfig)formatter.Deserialize(fileStream);
				fileStream.Close();
			}
			catch
			{
				fileStream.Close();
				return -1;
			}
			return 1;
		}

		public static bool StoreForm(FormSizes _form)
		{
			string fileForm = GetFileForm();
			FileStream fileStream;
			try
			{
				fileStream = new FileStream(fileForm, FileMode.Create);
			}
			catch
			{
				return false;
			}
			try
			{
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(fileStream, _form);
				fileStream.Close();
			}
			catch
			{
				fileStream.Close();
				return false;
			}
			return true;
		}

		public static void DeleteFileForm()
		{
			if (File.Exists(GetFileForm()))
			{
				try
				{
					File.Delete(GetFileForm());
				}
				catch
				{
				}
			}
		}

		public static void DeleteFileKey()
		{
			if (File.Exists(GetFileKey()))
			{
				try
				{
					File.Delete(GetFileKey());
				}
				catch
				{
				}
			}
		}

		public static int RestoreForm(out FormSizes _form)
		{
			_form = null;
			if (!File.Exists(GetFileForm()))
			{
				return 0;
			}
			FileStream fileStream;
			try
			{
				fileStream = new FileStream(GetFileForm(), FileMode.Open);
			}
			catch
			{
				return -1;
			}
			try
			{
				IFormatter formatter = new BinaryFormatter();
				_form = (FormSizes)formatter.Deserialize(fileStream);
				fileStream.Close();
			}
			catch
			{
				return -1;
			}
			return 1;
		}

		public static bool StoreScales(ScalesList _scales)
		{
			FileStream fileStream;
			try
			{
				string fileScale = GetFileScale();
				fileStream = new FileStream(fileScale, FileMode.Create);
			}
			catch
			{
				return false;
			}
			try
			{
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(fileStream, _scales);
				fileStream.Close();
			}
			catch
			{
				fileStream.Close();
				return false;
			}
			return true;
		}

		public static int RestoreScales(out ScalesList _scales)
		{
			_scales = null;
			if (!File.Exists(GetFileScale()))
			{
				return 0;
			}
			FileStream fileStream;
			try
			{
				fileStream = new FileStream(GetFileScale(), FileMode.Open);
			}
			catch
			{
				return -1;
			}
			try
			{
				IFormatter formatter = new BinaryFormatter();
				_scales = (ScalesList)formatter.Deserialize(fileStream);
				fileStream.Close();
			}
			catch
			{
				fileStream.Close();
				return -1;
			}
			return 1;
		}
	}
}
