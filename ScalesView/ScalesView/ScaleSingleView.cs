using ScalesView.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ScalesView
{
	public class ScaleSingleView : Form
	{
		internal struct INPUT
		{
			public uint Type;

			public MOUSEKEYBDHARDWAREINPUT Data;
		}

		[StructLayout(LayoutKind.Explicit)]
		internal struct MOUSEKEYBDHARDWAREINPUT
		{
			[FieldOffset(0)]
			public HARDWAREINPUT Hardware;

			[FieldOffset(0)]
			public KEYBDINPUT Keyboard;

			[FieldOffset(0)]
			public MOUSEINPUT Mouse;
		}

		internal struct HARDWAREINPUT
		{
			public uint Msg;

			public ushort ParamL;

			public ushort ParamH;
		}

		internal struct KEYBDINPUT
		{
			public ushort Vk;

			public ushort Scan;

			public uint Flags;

			public uint Time;

			public IntPtr ExtraInfo;
		}

		internal struct MOUSEINPUT
		{
			public int X;

			public int Y;

			public uint MouseData;

			public uint Flags;

			public uint Time;

			public IntPtr ExtraInfo;
		}

		private IContainer components;

		private Button bKeySettings;

		private Label lHotKey;

		private Label lNet;

		private Label tScaleSerial;

		private Timer timerBasa;

		private Timer timerHotKey;

		private Panel pSWeight;

		private Label tSWeight;

		private Button bHelp;

		private Label lID;

		private string sName = "";

		private string sID = "";

		private ScaleForm msForm;

		private KeyConfig mKeys;

		private short Key1;

		private short Key2;

		private bool Keymode = true;

		private bool OutFlag;

		private int LastLeds = 100;

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScalesView.ScaleSingleView));
			lHotKey = new System.Windows.Forms.Label();
			lNet = new System.Windows.Forms.Label();
			tScaleSerial = new System.Windows.Forms.Label();
			timerBasa = new System.Windows.Forms.Timer(components);
			timerHotKey = new System.Windows.Forms.Timer(components);
			pSWeight = new System.Windows.Forms.Panel();
			tSWeight = new System.Windows.Forms.Label();
			lID = new System.Windows.Forms.Label();
			bKeySettings = new System.Windows.Forms.Button();
			bHelp = new System.Windows.Forms.Button();
			pSWeight.SuspendLayout();
			SuspendLayout();
			lHotKey.AutoSize = true;
			lHotKey.Font = new System.Drawing.Font("Verdana", 14.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			lHotKey.ForeColor = System.Drawing.Color.White;
			lHotKey.Location = new System.Drawing.Point(81, 189);
			lHotKey.Name = "lHotKey";
			lHotKey.Size = new System.Drawing.Size(247, 23);
			lHotKey.TabIndex = 31;
			lHotKey.Text = "Запись (Left CTRL + 'E')";
			lNet.AutoSize = true;
			lNet.Font = new System.Drawing.Font("Verdana", 21.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lNet.ForeColor = System.Drawing.Color.White;
			lNet.Location = new System.Drawing.Point(3, 109);
			lNet.Name = "lNet";
			lNet.Size = new System.Drawing.Size(80, 35);
			lNet.TabIndex = 29;
			lNet.Text = "NET";
			tScaleSerial.AutoSize = true;
			tScaleSerial.Font = new System.Drawing.Font("Verdana", 18f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			tScaleSerial.ForeColor = System.Drawing.Color.White;
			tScaleSerial.Location = new System.Drawing.Point(12, 27);
			tScaleSerial.Name = "tScaleSerial";
			tScaleSerial.Size = new System.Drawing.Size(93, 29);
			tScaleSerial.TabIndex = 28;
			tScaleSerial.Text = "Весы:";
			timerBasa.Tick += new System.EventHandler(timerBasa_Tick);
			timerHotKey.Tick += new System.EventHandler(timerHotKey_Tick);
			pSWeight.BackColor = System.Drawing.Color.White;
			pSWeight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			pSWeight.Controls.Add(tSWeight);
			pSWeight.Location = new System.Drawing.Point(83, 60);
			pSWeight.Name = "pSWeight";
			pSWeight.Size = new System.Drawing.Size(361, 114);
			pSWeight.TabIndex = 33;
			tSWeight.Font = new System.Drawing.Font("Arial Narrow", 75.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			tSWeight.Location = new System.Drawing.Point(0, 1);
			tSWeight.Name = "tSWeight";
			tSWeight.Size = new System.Drawing.Size(354, 109);
			tSWeight.TabIndex = 0;
			tSWeight.Text = "label1";
			tSWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			lID.AutoSize = true;
			lID.Font = new System.Drawing.Font("Verdana", 14.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lID.ForeColor = System.Drawing.Color.White;
			lID.Location = new System.Drawing.Point(13, 3);
			lID.Name = "lID";
			lID.Size = new System.Drawing.Size(51, 23);
			lID.TabIndex = 35;
			lID.Text = "ID: ";
			bKeySettings.Font = new System.Drawing.Font("Arial", 9f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			bKeySettings.Image = ScalesView.Properties.Resources.Settings50_1;
			bKeySettings.Location = new System.Drawing.Point(422, 180);
			bKeySettings.Name = "bKeySettings";
			bKeySettings.Size = new System.Drawing.Size(45, 45);
			bKeySettings.TabIndex = 32;
			bKeySettings.UseVisualStyleBackColor = true;
			bKeySettings.Click += new System.EventHandler(bKeySettings_Click);
			bHelp.Image = (System.Drawing.Image)resources.GetObject("bHelp.Image");
			bHelp.Location = new System.Drawing.Point(427, 12);
			bHelp.Name = "bHelp";
			bHelp.Size = new System.Drawing.Size(40, 40);
			bHelp.TabIndex = 34;
			bHelp.UseVisualStyleBackColor = true;
			bHelp.Visible = false;
			bHelp.Click += new System.EventHandler(bHelp_Click);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Blue;
			base.ClientSize = new System.Drawing.Size(490, 229);
			base.Controls.Add(lID);
			base.Controls.Add(bHelp);
			base.Controls.Add(pSWeight);
			base.Controls.Add(bKeySettings);
			base.Controls.Add(lHotKey);
			base.Controls.Add(lNet);
			base.Controls.Add(tScaleSerial);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "ScaleSingleView";
			Text = "ScalesView 100";
			base.FormClosing += new System.Windows.Forms.FormClosingEventHandler(ScaleSingleView_FormClosing);
			base.Load += new System.EventHandler(ScaleSingleView_Load);
			pSWeight.ResumeLayout(false);
			ResumeLayout(false);
			PerformLayout();
		}

		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern short GetAsyncKeyState(int vkey);

		[DllImport("USER32.dll")]
		private static extern short GetKeyState(int nVirtKey);

		[DllImport("user32.dll", SetLastError = true)]
		private static extern uint SendInput(uint numberOfInputs, INPUT[] inputs, int sizeOfInputStructure);

		public static void SendKeyDown(ushort keyCode)
		{
			INPUT iNPUT = default(INPUT);
			iNPUT.Type = 1u;
			INPUT iNPUT2 = iNPUT;
			iNPUT2.Data.Keyboard = default(KEYBDINPUT);
			iNPUT2.Data.Keyboard.Scan = keyCode;
			iNPUT2.Data.Keyboard.Flags = 4u;
			iNPUT2.Data.Keyboard.Time = 0u;
			iNPUT2.Data.Keyboard.ExtraInfo = IntPtr.Zero;
			INPUT[] inputs = new INPUT[1]
			{
				iNPUT2
			};
			if (SendInput(1u, inputs, Marshal.SizeOf(typeof(INPUT))) == 0)
			{
				throw new Exception();
			}
		}

		public static void SendKeyUp(ushort keyCode)
		{
			INPUT iNPUT = default(INPUT);
			iNPUT.Type = 1u;
			INPUT iNPUT2 = iNPUT;
			iNPUT2.Data.Keyboard = default(KEYBDINPUT);
			iNPUT2.Data.Keyboard.Scan = keyCode;
			iNPUT2.Data.Keyboard.Flags = 6u;
			iNPUT2.Data.Keyboard.Time = 0u;
			iNPUT2.Data.Keyboard.ExtraInfo = IntPtr.Zero;
			INPUT[] inputs = new INPUT[1]
			{
				iNPUT2
			};
			if (SendInput(1u, inputs, Marshal.SizeOf(typeof(INPUT))) == 0)
			{
				throw new Exception();
			}
		}

		public ScaleSingleView(string _Name, string _ID, ScaleForm _msForm, KeyConfig _Keys)
		{
			InitializeComponent();
			sName = _Name;
			sID = _ID;
			msForm = _msForm;
			mKeys = _Keys;
		}

		private void ScaleSingleView_Load(object sender, EventArgs e)
		{
			while (TextRenderer.MeasureText("-15.000 кг", tSWeight.Font).Width > pSWeight.Width)
			{
				float emSize = tSWeight.Font.Size - 0.5f;
				tSWeight.Font = new Font(tSWeight.Font.Name, emSize, tSWeight.Font.Style, tSWeight.Font.Unit);
			}
			SetHotKeys();
			ToolTip toolTip = new ToolTip();
			toolTip.SetToolTip(bKeySettings, "Настройка эмулятора клавиатуры");
			SetTextHelpHotKey();
			tScaleSerial.Text = sName;
			lID.Text = "ID: " + sID;
			timerBasa.Interval = 200;
			timerBasa.Start();
			timerHotKey.Interval = 100;
			timerHotKey.Start();
			BackColor = Color.FromArgb(255, 0, 102, 179);
		}

		private void bExit_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.OK;
			Close();
		}

		private void timerBasa_Tick(object sender, EventArgs e)
		{
			string scaleWeight = msForm.GetScaleWeight();
			tSWeight.Text = scaleWeight;
			int scaleLeds = msForm.GetScaleLeds();
			if (LastLeds != scaleLeds)
			{
				if ((scaleLeds & 2) != 0)
				{
					lNet.Visible = true;
				}
				else
				{
					lNet.Visible = false;
				}
				LastLeds = scaleLeds;
			}
		}

		private void SetHotKeys()
		{
			Key1 = (Key2 = 0);
			mKeys.Top = 1;
			mKeys.Enable = 1;
			if (mKeys.Top > 0)
			{
				base.TopMost = true;
			}
			else
			{
				base.TopMost = false;
			}
			if (mKeys.Enable != 0)
			{
				Key1 = (byte)mKeys.CntrlKey;
				Key2 = (byte)mKeys.Key;
				if (mKeys.KG_Enable == 1)
				{
					Keymode = true;
				}
				else
				{
					Keymode = false;
				}
			}
		}

		private void SendStringToApplacation(int isDel, int isDKey)
		{
			string[] array = tSWeight.Text.Split(' ');
			if (array.Length > 1 && array[1] == "г")
			{
				double num = double.Parse(array[0], CultureInfo.InvariantCulture) / 1000.0;
				array[0] = string.Format(CultureInfo.InvariantCulture, "{0:0.0000}", new object[1]
				{
					num
				});
			}
			if (mKeys.Point != 1)
			{
				array[0] = array[0].Replace('.', ',');
			}
			array[0] += "\r\n";
			byte[] bytes = Encoding.ASCII.GetBytes(array[0]);
			switch (isDel)
			{
			case 0:
			case 44:
			case 112:
			case 113:
			case 114:
			case 115:
			case 116:
			case 117:
			case 118:
			case 119:
			case 120:
			case 121:
			case 122:
			case 123:
			case 145:
				if (isDKey > 0)
				{
					SendKeyDown(8);
					SendKeyUp(8);
				}
				break;
			}
			if (array.Length > 1 && array[1] == "кг")
			{
				for (int i = 0; i < bytes.Length; i++)
				{
					SendKeyDown(bytes[i]);
					SendKeyUp(bytes[i]);
				}
			}
		}

		private void timerHotKey_Tick(object sender, EventArgs e)
		{
			bool flag = false;
			if (Key1 == 0 && Key2 == 0)
			{
				return;
			}
			if (OutFlag)
			{
				for (int i = 1; i < 255; i++)
				{
					if (GetAsyncKeyState(i) != 0)
					{
						flag = true;
					}
				}
				if (!flag)
				{
					try
					{
						SendStringToApplacation(Key1, Key2);
						OutFlag = false;
						return;
					}
					catch
					{
					}
					OutFlag = false;
				}
			}
			else if (Key1 != 0 && Key2 != 0)
			{
				if (GetAsyncKeyState(Key1) != 0 && GetAsyncKeyState(Key2) != 0)
				{
					OutFlag = true;
				}
			}
			else if (Key1 != 0)
			{
				if (GetAsyncKeyState(Key1) != 0)
				{
					OutFlag = true;
				}
			}
			else if (Key2 != 0 && GetAsyncKeyState(Key2) != 0)
			{
				OutFlag = true;
			}
		}

		private void ScaleSingleView_FormClosing(object sender, FormClosingEventArgs e)
		{
			timerHotKey.Stop();
		}

		private void bKeySettings_Click(object sender, EventArgs e)
		{
			bool topMost = base.TopMost;
			base.TopMost = false;
			SettingsHotKey settingsHotKey = new SettingsHotKey(mKeys);
			if (settingsHotKey.ShowDialog() == DialogResult.OK)
			{
				mKeys = settingsHotKey.sKey;
				SetHotKeys();
				SetTextHelpHotKey();
				((MainScalesView)msForm.MdiParent).StoreGefaultKeys(mKeys);
			}
			else
			{
				base.TopMost = topMost;
			}
		}

		private void SetTextHelpHotKey()
		{
			byte[] bytes = new byte[1]
			{
				(byte)mKeys.Key
			};
			char[] array = Encoding.ASCII.GetString(bytes).ToCharArray();
			string text = "";
			if (mKeys.Key > 0 && mKeys.CntrlKey > 0)
			{
				text = $"Запись - ('{ContrlKeys.GetTextCTRLKey(mKeys.CntrlKey)}' + '{array[0]}')";
			}
			else if (mKeys.Key > 0 && mKeys.CntrlKey == 0)
			{
				text = $"Запись - ('{array[0]}')";
			}
			else if (mKeys.Key == 0 && mKeys.CntrlKey > 0)
			{
				text = $"Запись - ('{ContrlKeys.GetTextCTRLKey(mKeys.CntrlKey)}')";
			}
			lHotKey.Text = text;
		}

		private void bHelp_Click(object sender, EventArgs e)
		{
			string text = "SV_weight.htm";
			if (!File.Exists(Path.Combine(Directory.GetCurrentDirectory(), text)))
			{
				text = "SV_support.htm";
			}
			if (!File.Exists(Path.Combine(Directory.GetCurrentDirectory(), text)))
			{
				MessageBox.Show("Файл справки отсутствует!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			F_Help f_Help = new F_Help(text);
			f_Help.TopMost = true;
			f_Help.Show();
			mKeys.Key = f_Help.i;
		}
	}
}
