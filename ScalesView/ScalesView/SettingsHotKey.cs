using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ScalesView
{
	public class SettingsHotKey : Form
	{
		public KeyConfig sKey;

		private bool isLoad;

		private IContainer components;

		private CheckBox cSPoint;

		private GroupBox groupBox1;

		private CheckBox cPoint;

		private CheckBox cKG_Enable;

		private Button bCancel;

		private Button bSave;

		private ComboBox cHotKey;

		private GroupBox gHotKeys;

		private Label lPlus;

		private ComboBox cCtrlKey;

		public SettingsHotKey(KeyConfig _key)
		{
			InitializeComponent();
			sKey = _key;
		}

		private void SettingsHotKey_Load(object sender, EventArgs e)
		{
			if (sKey.KG_Enable == 1)
			{
				cKG_Enable.Checked = true;
			}
			else
			{
				cKG_Enable.Checked = false;
			}
			byte[] bytes = new byte[1]
			{
				(byte)sKey.Key
			};
			char[] array = Encoding.ASCII.GetString(bytes).ToCharArray();
			cHotKey.Text = ((array[0] != 0) ? $"{array[0]}" : "<Нет>");
			cCtrlKey.Text = ContrlKeys.GetTextCTRLKey(sKey.CntrlKey);
			if (sKey.Point == 1)
			{
				cPoint.Checked = true;
				cSPoint.Checked = false;
			}
			else
			{
				cSPoint.Checked = true;
				cPoint.Checked = false;
			}
			isLoad = true;
			Color color3 = cPoint.ForeColor = (cSPoint.ForeColor = Color.FromArgb(255, 0, 102, 179));
			Color color5 = bSave.ForeColor = (bCancel.ForeColor = Color.White);
			Color color8 = bSave.BackColor = (bCancel.BackColor = Color.FromArgb(255, 0, 102, 179));
		}

		private void bCancel_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			Close();
		}

		private void bSave_Click(object sender, EventArgs e)
		{
			sKey.Enable = 1;
			sKey.CntrlKey = ContrlKeys.GetCTRLKey(cCtrlKey.Text);
			sKey.Key = 0;
			if (cHotKey.SelectedIndex == 0)
			{
				sKey.Key = 0;
			}
			else
			{
				byte[] bytes = Encoding.Unicode.GetBytes(cHotKey.Text);
				sKey.Key = bytes[0];
			}
			sKey.Top = 1;
			if (cKG_Enable.Checked)
			{
				sKey.KG_Enable = 1;
			}
			else
			{
				sKey.KG_Enable = 0;
			}
			if (cPoint.Checked)
			{
				sKey.Point = 1;
			}
			else
			{
				sKey.Point = 0;
			}
			base.DialogResult = DialogResult.OK;
			Close();
		}

		private void cPoint_Click(object sender, EventArgs e)
		{
			cSPoint.Checked = false;
			cPoint.Checked = true;
		}

		private void cSPoint_Click(object sender, EventArgs e)
		{
			cSPoint.Checked = true;
			cPoint.Checked = false;
		}

		private void cHotKey_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (isLoad && ((string)cCtrlKey.SelectedItem)[0] == 'F')
			{
				cHotKey.Text = "<Нет>";
			}
		}

		private void cSPoint_CheckedChanged(object sender, EventArgs e)
		{
			if (cSPoint.Checked)
			{
				cPoint.Checked = false;
			}
		}

		private void cPoint_CheckedChanged(object sender, EventArgs e)
		{
			if (cPoint.Checked)
			{
				cSPoint.Checked = false;
			}
		}

		private void cCtrlKey_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (isLoad)
			{
				string text = (string)cCtrlKey.SelectedItem;
				if (text[0] == 'F')
				{
					cHotKey.Text = "<Нет>";
					cHotKey.Enabled = false;
				}
				else
				{
					cHotKey.Enabled = true;
				}
			}
		}

		private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor, int _arc)
		{
			if (box != null)
			{
				Brush brush = new SolidBrush(textColor);
				Brush brush2 = new SolidBrush(borderColor);
				Pen pen = new Pen(brush2, 1f);
				SizeF sizeF = g.MeasureString(box.Text, box.Font);
				Rectangle rectangle = new Rectangle(box.ClientRectangle.X, box.ClientRectangle.Y + (int)(sizeF.Height / 2f), box.ClientRectangle.Width - 1, box.ClientRectangle.Height - (int)(sizeF.Height / 2f) - 1);
				g.Clear(BackColor);
				g.DrawString(box.Text, box.Font, brush, _arc * 2, 0f);
				g.DrawLine(pen, new PointF(rectangle.Location.X, rectangle.Location.Y + _arc), new PointF(rectangle.X, rectangle.Y + rectangle.Height - _arc));
				g.DrawArc(pen, rectangle.Location.X, rectangle.Location.Y, _arc * 2, _arc * 2, 180, 90);
				g.DrawLine(pen, new PointF(rectangle.X + rectangle.Width, rectangle.Y + _arc), new PointF(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height - _arc));
				g.DrawArc(pen, rectangle.Location.X + rectangle.Width - _arc * 2, rectangle.Y, _arc * 2, _arc * 2, 270, 90);
				g.DrawLine(pen, new PointF(rectangle.X + _arc, rectangle.Y + rectangle.Height), new PointF(rectangle.X + rectangle.Width - _arc, rectangle.Y + rectangle.Height));
				g.DrawArc(pen, rectangle.Location.X + rectangle.Width - _arc * 2, rectangle.Y + rectangle.Height - _arc * 2, _arc * 2, _arc * 2, 0, 90);
				g.DrawLine(pen, new PointF(rectangle.X + _arc, rectangle.Y), new PointF(rectangle.X + _arc * 2, rectangle.Y));
				g.DrawLine(pen, new PointF(rectangle.X + _arc * 2 + (int)sizeF.Width, rectangle.Y), new PointF(rectangle.X + rectangle.Width - _arc, rectangle.Y));
				g.DrawArc(pen, rectangle.Location.X, rectangle.Y + rectangle.Height - _arc * 2, _arc * 2, _arc * 2, 90, 90);
			}
		}

		private void gHotKeys_Paint(object sender, PaintEventArgs e)
		{
			GroupBox box = (GroupBox)sender;
			DrawGroupBox(box, e.Graphics, Color.Black, Color.FromArgb(255, 0, 102, 179), 20);
		}

		private void groupBox1_Paint(object sender, PaintEventArgs e)
		{
			GroupBox box = (GroupBox)sender;
			DrawGroupBox(box, e.Graphics, Color.Black, Color.FromArgb(255, 0, 102, 179), 20);
		}

		private void cCtrlKey_DrawItem(object sender, DrawItemEventArgs e)
		{
			cntrl_DrawItem(sender, e);
		}

		private void cHotKey_DrawItem(object sender, DrawItemEventArgs e)
		{
			cntrl_DrawItem(sender, e);
		}

		private void cntrl_DrawItem(object sender, DrawItemEventArgs e)
		{
			ComboBox comboBox = sender as ComboBox;
			if (comboBox != null)
			{
				e.DrawBackground();
				if (e.Index >= 0)
				{
					StringFormat stringFormat = new StringFormat();
					stringFormat.LineAlignment = StringAlignment.Near;
					stringFormat.Alignment = StringAlignment.Near;
					Brush brush = new SolidBrush(Color.FromArgb(255, 0, 102, 179));
					Rectangle rect = new Rectangle(e.Bounds.Location, new Size(e.Bounds.Width, e.Bounds.Height));
					e.Graphics.FillRectangle(brush, rect);
					e.Graphics.DrawString(comboBox.Items[e.Index].ToString(), comboBox.Font, Brushes.White, e.Bounds, stringFormat);
				}
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScalesView.SettingsHotKey));
			cSPoint = new System.Windows.Forms.CheckBox();
			groupBox1 = new System.Windows.Forms.GroupBox();
			cPoint = new System.Windows.Forms.CheckBox();
			cKG_Enable = new System.Windows.Forms.CheckBox();
			bCancel = new System.Windows.Forms.Button();
			bSave = new System.Windows.Forms.Button();
			cHotKey = new System.Windows.Forms.ComboBox();
			gHotKeys = new System.Windows.Forms.GroupBox();
			cCtrlKey = new System.Windows.Forms.ComboBox();
			lPlus = new System.Windows.Forms.Label();
			groupBox1.SuspendLayout();
			gHotKeys.SuspendLayout();
			SuspendLayout();
			cSPoint.AutoSize = true;
			cSPoint.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			cSPoint.Location = new System.Drawing.Point(124, 24);
			cSPoint.Name = "cSPoint";
			cSPoint.Size = new System.Drawing.Size(106, 22);
			cSPoint.TabIndex = 4;
			cSPoint.Text = "(,) Запятая";
			cSPoint.UseVisualStyleBackColor = true;
			cSPoint.CheckedChanged += new System.EventHandler(cSPoint_CheckedChanged);
			groupBox1.Controls.Add(cSPoint);
			groupBox1.Controls.Add(cPoint);
			groupBox1.Font = new System.Drawing.Font("Arial", 11.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			groupBox1.Location = new System.Drawing.Point(12, 91);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(251, 61);
			groupBox1.TabIndex = 11;
			groupBox1.TabStop = false;
			groupBox1.Text = "Разделитель";
			groupBox1.Paint += new System.Windows.Forms.PaintEventHandler(groupBox1_Paint);
			cPoint.AutoSize = true;
			cPoint.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			cPoint.Location = new System.Drawing.Point(25, 24);
			cPoint.Name = "cPoint";
			cPoint.Size = new System.Drawing.Size(86, 22);
			cPoint.TabIndex = 3;
			cPoint.Text = "(.) Точка";
			cPoint.UseVisualStyleBackColor = true;
			cPoint.CheckedChanged += new System.EventHandler(cPoint_CheckedChanged);
			cKG_Enable.AutoSize = true;
			cKG_Enable.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			cKG_Enable.Location = new System.Drawing.Point(24, 158);
			cKG_Enable.Name = "cKG_Enable";
			cKG_Enable.Size = new System.Drawing.Size(250, 22);
			cKG_Enable.TabIndex = 10;
			cKG_Enable.Text = "Выводить единицы измерения";
			cKG_Enable.UseVisualStyleBackColor = true;
			cKG_Enable.Visible = false;
			bCancel.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			bCancel.Location = new System.Drawing.Point(178, 170);
			bCancel.Name = "bCancel";
			bCancel.Size = new System.Drawing.Size(85, 30);
			bCancel.TabIndex = 7;
			bCancel.Text = "Отмена";
			bCancel.UseVisualStyleBackColor = true;
			bCancel.Click += new System.EventHandler(bCancel_Click);
			bSave.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			bSave.Location = new System.Drawing.Point(12, 170);
			bSave.Name = "bSave";
			bSave.Size = new System.Drawing.Size(96, 30);
			bSave.TabIndex = 9;
			bSave.Text = "Сохранить";
			bSave.UseVisualStyleBackColor = true;
			bSave.Click += new System.EventHandler(bSave_Click);
			cHotKey.BackColor = System.Drawing.SystemColors.MenuHighlight;
			cHotKey.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
			cHotKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			cHotKey.FormattingEnabled = true;
			cHotKey.Items.AddRange(new object[10]
			{
				"<Нет>",
				"B",
				"D",
				"E",
				"I",
				"J",
				"M",
				"Q",
				"R",
				"U"
			});
			cHotKey.Location = new System.Drawing.Point(166, 32);
			cHotKey.Name = "cHotKey";
			cHotKey.Size = new System.Drawing.Size(71, 26);
			cHotKey.TabIndex = 8;
			cHotKey.DrawItem += new System.Windows.Forms.DrawItemEventHandler(cHotKey_DrawItem);
			cHotKey.SelectedIndexChanged += new System.EventHandler(cHotKey_SelectedIndexChanged);
			gHotKeys.Controls.Add(cCtrlKey);
			gHotKeys.Controls.Add(cHotKey);
			gHotKeys.Controls.Add(lPlus);
			gHotKeys.Font = new System.Drawing.Font("Arial", 11.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			gHotKeys.Location = new System.Drawing.Point(12, 12);
			gHotKeys.Name = "gHotKeys";
			gHotKeys.Size = new System.Drawing.Size(251, 72);
			gHotKeys.TabIndex = 8;
			gHotKeys.TabStop = false;
			gHotKeys.Text = "Горячие клавиши";
			gHotKeys.Paint += new System.Windows.Forms.PaintEventHandler(gHotKeys_Paint);
			cCtrlKey.BackColor = System.Drawing.SystemColors.MenuHighlight;
			cCtrlKey.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
			cCtrlKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			cCtrlKey.FormattingEnabled = true;
			cCtrlKey.Items.AddRange(new object[5]
			{
				"Left CTRL",
				"Right CTRL",
				"CTRL",
				"PrtScr",
				"ScrlLock"
			});
			cCtrlKey.Location = new System.Drawing.Point(22, 32);
			cCtrlKey.Name = "cCtrlKey";
			cCtrlKey.Size = new System.Drawing.Size(101, 26);
			cCtrlKey.TabIndex = 9;
			cCtrlKey.DrawItem += new System.Windows.Forms.DrawItemEventHandler(cCtrlKey_DrawItem);
			cCtrlKey.SelectedIndexChanged += new System.EventHandler(cCtrlKey_SelectedIndexChanged);
			lPlus.AutoSize = true;
			lPlus.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			lPlus.Location = new System.Drawing.Point(135, 33);
			lPlus.Name = "lPlus";
			lPlus.Size = new System.Drawing.Size(22, 24);
			lPlus.TabIndex = 5;
			lPlus.Text = "+";
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(274, 210);
			base.ControlBox = false;
			base.Controls.Add(groupBox1);
			base.Controls.Add(cKG_Enable);
			base.Controls.Add(bCancel);
			base.Controls.Add(bSave);
			base.Controls.Add(gHotKeys);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "SettingsHotKey";
			Text = "Настройка";
			base.TopMost = true;
			base.Load += new System.EventHandler(SettingsHotKey_Load);
			groupBox1.ResumeLayout(false);
			groupBox1.PerformLayout();
			gHotKeys.ResumeLayout(false);
			gHotKeys.PerformLayout();
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
