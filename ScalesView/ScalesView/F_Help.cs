using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ScalesView
{
	public class F_Help : Form
	{
		private IContainer components;

		private string url;

		public int i;

		private MyBrowse webBrowser1;

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScalesView.F_Help));
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(748, 407);
			base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			base.Name = "F_Help";
			Text = "Справка ScalesView 100";
			base.Load += new System.EventHandler(F_Help_Load);
			ResumeLayout(false);
		}

		public F_Help(string _url)
		{
			InitializeComponent();
			url = _url;
		}

		private void F_Help_Load(object sender, EventArgs e)
		{
			webBrowser1 = new MyBrowse();
			webBrowser1.Parent = this;
			webBrowser1.Dock = DockStyle.Fill;
			webBrowser1.Show();
			string str = Path.Combine(Directory.GetCurrentDirectory(), url);
			webBrowser1.Navigate("file:///" + str);
		}
	}
}
