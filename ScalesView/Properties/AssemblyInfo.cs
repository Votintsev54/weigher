using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyProduct("Massa-K ScalesView")]
[assembly: CompilationRelaxations(8)]
[assembly: AssemblyTitle("Massa-K ScalesView")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Massa-K")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("7353fbaa-098d-436c-983d-cbbb323d2846")]
[assembly: AssemblyFileVersion("2.0.2.134")]
[assembly: AssemblyVersion("2.0.2.134")]
