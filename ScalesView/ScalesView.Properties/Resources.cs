using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace ScalesView.Properties
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
	[CompilerGenerated]
	[DebuggerNonUserCode]
	internal class Resources
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (object.ReferenceEquals(resourceMan, null))
				{
					ResourceManager resourceManager = resourceMan = new ResourceManager("ScalesView.Properties.Resources", typeof(Resources).Assembly);
				}
				return resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return resourceCulture;
			}
			set
			{
				resourceCulture = value;
			}
		}

		internal static Bitmap A50
		{
			get
			{
				object @object = ResourceManager.GetObject("A50", resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Help50_1
		{
			get
			{
				object @object = ResourceManager.GetObject("Help50_1", resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap LoadForm_1
		{
			get
			{
				object @object = ResourceManager.GetObject("LoadForm_1", resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Massa_K
		{
			get
			{
				object @object = ResourceManager.GetObject("Massa_K", resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Search50_1
		{
			get
			{
				object @object = ResourceManager.GetObject("Search50_1", resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Settings50_1
		{
			get
			{
				object @object = ResourceManager.GetObject("Settings50_1", resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal Resources()
		{
		}
	}
}
