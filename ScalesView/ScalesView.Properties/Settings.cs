using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace ScalesView.Properties
{
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
	[CompilerGenerated]
	internal sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());

		public static Settings Default => defaultInstance;

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("\\Massa-K\\ScalesView_V2\\")]
		public string DirectoryInit => (string)this["DirectoryInit"];

		[DefaultSettingValue("UserKey.sv")]
		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		public string FileKey => (string)this["FileKey"];

		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		[DefaultSettingValue("UserForm.sv")]
		public string FileForm => (string)this["FileForm"];

		[DefaultSettingValue("UserScales.sv")]
		[ApplicationScopedSetting]
		[DebuggerNonUserCode]
		public string FileScales => (string)this["FileScales"];
	}
}
