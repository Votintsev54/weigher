﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Installer.Model;
using Installer.ViewModel;

namespace Installer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            //WindowsIdentity identity = new WindowsIdentity(accessToken);
            //WindowsIdentity identity = WindowsIdentity.GetCurrent();
            //WindowsImpersonationContext context = identity.Impersonate();

            FirstGrid.DataContext = new InstallationParametersVm(new InstallationParameters());
        }
    }
}
