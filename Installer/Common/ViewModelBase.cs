﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Installer.Common
{
    public class ViewModelBase
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected bool SetProperty<T>(ref T field, T value, [CallerMemberName]string name = null)
        {
            if (!Equals(field, value))
            {
                field = value;
                // ReSharper disable once ExplicitCallerInfoArgument
                if (name != null) OnPropertyChanged(name);
                return true;
            }
            return false;
        }
 
    }
}