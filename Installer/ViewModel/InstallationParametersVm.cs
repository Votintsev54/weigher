﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Installer.Common;
using Installer.Model;
using IWshRuntimeLibrary;
using File = System.IO.File;

namespace Installer.ViewModel
{
    public class InstallationParametersVm : ViewModelBase
    {
        private string _company;
        private string _productShortName;
        //public List<FileDescription> Files;

        private string _installationPath;
        public InstallationParametersVm()
        {
        }
        public InstallationParametersVm(InstallationParameters model)
        {
            _company = model.Company;
            _productShortName = model.ProductShortName;
            _installationPath = model.InstallationPath;
            InitCommands();
            ReadyCommandAvailable = true;
        }

        public InstallationParameters GetModel()
        {
            return new InstallationParameters()
            {
                Company = Company,
                ProductShortName = ProductShortName,
                InstallationPath = InstallationPath
            };
        }

        public string InstallationPath
        {
            get { return _installationPath; }
            set { SetProperty(ref _installationPath, value); }
        }

        public string Company
        {
            get { return _company; }
            set { SetProperty(ref _company, value); }
        }

        public string ProductShortName
        {
            get { return _productShortName; }
            set { SetProperty(ref _productShortName, value); }
        }

        public ICommand ReadyCommand { get; private set; }

        private void InitCommands()
        {
            ReadyCommand = new RelayCommand(
                _ => ReadyCommandHandlerAsync(),
                _ => ReadyCommandAvailable);
        }

        public bool ReadyCommandAvailable { get; set; }

        private async void ReadyCommandHandlerAsync()
        {
            await Ready();
        }

        private Task<bool> Ready()
        {
            return Task.Run(() =>
            {

                StopApplication("Weigher");
                ReadyCommandAvailable = false;
                Assembly execAssembly = Assembly.GetExecutingAssembly();
                string execAssemblyName = execAssembly.FullName.Split(new char[] { ',' })[0];
                Directory.CreateDirectory(InstallationPath);
                string filePrefics = execAssemblyName + ".Files";
                foreach (var resFileName in execAssembly.GetManifestResourceNames())
                {
                    if (resFileName.StartsWith(filePrefics) && resFileName.EndsWith(".cmp"))
                    {
                        string fileName = resFileName.Substring(filePrefics.Length + 1, resFileName.Length - 4 - filePrefics.Length - 1);

                        string filePath = Path.Combine(InstallationPath, fileName);
                        using (var stream = execAssembly.GetManifestResourceStream(resFileName))
                        {
                            Decompress(stream, filePath);
                        }

                        if (fileName.EndsWith(".exe"))
                            CreateShortCut(filePath);
                    }
                }
                ReadyCommandAvailable = true;
                MessageBox.Show("Установка завершена");
                Environment.Exit(0);
                return true;
            });

        }

        // остановка переустанавливаемого процесса
        private void StopApplication(string procname)
        {
            System.Diagnostics.Process[] processes;
            processes = System.Diagnostics.Process.GetProcesses();
            bool killed = false;
            foreach (var proc in processes.Where(proc => proc.ProcessName == procname))
            {
                proc.Kill();
                killed = true;
            }
            while (killed)
            {
                Thread.Sleep(50);
                processes = System.Diagnostics.Process.GetProcesses();
                killed = processes.Any(proc => proc.ProcessName == procname);
            }
        }


        public bool CreateShortCut(WshShell shell, string filePath, Environment.SpecialFolder shortCutFolder)
        {
            string fileNameWe = Path.GetFileNameWithoutExtension(filePath);

            //путь к ярлыку
            string shortcutPath = Environment.GetFolderPath(shortCutFolder) + @"\" + fileNameWe + ".lnk";

            IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutPath);

            shortcut.Description = "Ярлык @@@...";
            //горячая клавиша
            //shortcut.Hotkey = "Ctrl+Shift+N";
            shortcut.TargetPath = filePath;
            try
            {
                shortcut.Save();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public void CreateShortCut(string filePath)
        {
            WshShell shell = new WshShell();
            if (!CreateShortCut(shell, filePath, Environment.SpecialFolder.CommonDesktopDirectory))
            {
                CreateShortCut(shell, filePath, Environment.SpecialFolder.DesktopDirectory);
            }

        }


        public static void Decompress(Stream fileToDecompress, string fileName)
        {

            using (FileStream decompressedFileStream = File.Create(fileName))
            {
                using (DeflateStream decompressionStream = new DeflateStream(fileToDecompress, CompressionMode.Decompress))
                {
                    decompressionStream.CopyTo(decompressedFileStream);
                }
            }
        }
    }
}