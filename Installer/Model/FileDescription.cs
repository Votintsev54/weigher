﻿namespace Installer.Model
{
    public class FileDescription
    {
        public string Name;
        public string Path = ".";
        public string ResourceName ;
    }
}