﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Documents;

namespace Installer.Model
{
    public class InstallationParameters
    {
        public string Company = "Ermatel";
        public string ProductShortName="Weigher";
        public List<FileDescription> Files;

        private string _installationPath;
        public InstallationParameters ()
        {
            _installationPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), Company, ProductShortName);
            _installationPath = Path.Combine(@"c:\", ProductShortName);
            Files = new List<FileDescription>();
        }

        public string InstallationPath
        {
            get { return _installationPath; }
            set { _installationPath = value; }
        }
    }
}