/****** Object:  StoredProcedure [dbo].[storno]    Script Date: 16.02.2022 11:50:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[storno]
	@Barcode nvarchar(50)
AS
BEGIN
BEGIN TRY
	DECLARE @TranFlag int;
	SET @TranFlag = @@TRANCOUNT;
	IF @TranFlag = 0 BEGIN TRAN; ELSE SAVE TRAN sv_Storno;
	DECLARE @RollCount int = 0
	SELECT @RollCount=COUNT(*) FROM dbo.Roll where rollbarcode=@Barcode

	IF @RollCount > 0 BEGIN
		IF @RollCount = 1 BEGIN
			DELETE dbo.Roll WHERE rollbarcode=@Barcode and acc1c is null
			INSERT INTO dbo.Roll (OrderDetailId,PackageId,RollNumber,FrameCount,RollWeight,RollBarcode,VtulkaWeight,BruttoWeight,RollCount,Skleek) 
			SELECT OrderDetailId,PackageId,RollNumber,FrameCount,-RollWeight,RollBarcode,VtulkaWeight,-BruttoWeight,RollCount,Skleek
				FROM dbo.Roll WHERE rollbarcode=@Barcode and acc1c is not null
		END ELSE
			SET @RollCount = 0
	END
	ELSE BEGIN
		DECLARE @PackageId int = 0
		SELECT @PackageId = id FROM dbo.Package where barcode=@Barcode

		IF @PackageId > 0 BEGIN
			DELETE dbo.Roll where PackageId=@PackageId and acc1c is null and BruttoWeight >0
			SET @RollCount = @@ROWCOUNT
			INSERT INTO dbo.Roll (OrderDetailId,PackageId,RollNumber,FrameCount,RollWeight,RollBarcode,VtulkaWeight,BruttoWeight,RollCount,Skleek) 
			SELECT OrderDetailId,PackageId,RollNumber,FrameCount,-RollWeight,RollBarcode,VtulkaWeight,-BruttoWeight,RollCount,Skleek
				FROM dbo.Roll r WHERE PackageId=@PackageId and acc1c is not null and BruttoWeight > 0
				AND NOT EXISTS(SELECT * FROM dbo.Roll  WHERE PackageId=@PackageId and BruttoWeight < 0 and RollNumber=r.RollNumber)
			SET @RollCount = @RollCount + @@ROWCOUNT
		END
	END

	IF @TranFlag = 0 COMMIT TRAN;
	RETURN @RollCount
END TRY
BEGIN CATCH
	IF @TranFlag = 0 ROLLBACK TRAN; ELSE  ROLLBACK TRAN sv_Storno;
	RETURN 0;
END CATCH
END;


GO


