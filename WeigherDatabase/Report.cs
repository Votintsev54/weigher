﻿using System.Globalization;
using System.Linq;
using System.Text;
using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QRCoder;
using System.Xml.Serialization;
using WeigherDatabase;
using System;
using System.Collections.Generic;
using DbCommon.Json;
using System.Text.RegularExpressions;

namespace WeigherData
{
    public partial class Report
    {
        public string InterfaceName => Interface?.Name;

    }
}