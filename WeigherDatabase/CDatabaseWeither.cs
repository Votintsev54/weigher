﻿using System;
using DbCommon;

namespace WeigherData
{
    public partial class CDatabaseWeigher
    {
        public string GetBarCode(string barPrefix,int barDigit)
        {
            int retval = 1;
            Sequences sq = this.Sequences.GetObjectByExpr(sq0 => sq0.Code == "BarCode");
            if (sq == null)
            {
                sq = this.Sequences.NewInstance();
                sq.Value = 1;
                sq.Code = "BarCode";
            }
            else
            {
                retval = sq.Value = (sq.Value + 1);
            }
            sq.Save2();
            return barPrefix + retval.ToString("D" + barDigit);
        }
        public override void CopyFrom(CDatabase sourceDb,Action<int> progressCallback)
        {
            var source = sourceDb as CDatabaseWeigher;
            Sequences.CopyFrom(source.Sequences);
            progressCallback?.Invoke(20);
            Roll.CopyFrom(source.Roll);
            progressCallback?.Invoke(40);
            Package.CopyFrom(source.Package);
            progressCallback?.Invoke(60);
            Orders.CopyFrom(source.Orders);
            progressCallback?.Invoke(80);
            OrderDetail.CopyFrom(source.OrderDetail);
            Packer.CopyFrom(source.Packer);
            progressCallback?.Invoke(100);
        }
    }

    
}