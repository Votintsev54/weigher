﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using DbCommon.Attributes;
using DbCommon.Helpers;

namespace WeigherData
{
    public partial class CTableReport
    {

        [StartValue]
        public static IEnumerable<Report> CreateEmbedReports()
        {
            var ret = new Report[]
            {
                new Report(){Name = "Этикетки",Comment = "Labels"},
                new Report(){Name = "Упаковочный лист",Comment = "PackingList"},
                new Report(){Name = "Ярлык к палетте",Comment = "PalletLabel"},
                new Report(){Name = "Спецификация",Comment = "Specification"},
                new Report(){Name = "Этикетки",Comment = "labelProksima",GroupCustomerId = new Guid("5EABD40D-F33D-4440-B092-25A6F77836B0") ,InterfaceId = 2},
                new Report(){Name = "Упаковочный лист",Comment = "PackingListProksima",GroupCustomerId = new Guid("5EABD40D-F33D-4440-B092-25A6F77836B0"),InterfaceId = 2},
                new Report(){Name = "Этикетки",Comment = "labelBaikalSi",InterfaceId = 3},
                new Report(){Name = "Упаковочный лист",Comment = "PackingListBaikalSi",InterfaceId = 3},
                new Report(){Name = "Этикетки",Comment = "labelNoName",InterfaceId = 4},
                new Report(){Name = "Упаковочный лист",Comment = "PackingListNoName",InterfaceId = 4},
                new Report(){Name = "Этикетки",Comment = "labelCustomerFree",InterfaceId = 5},
                //new Report(){Name = "Этикетки2",Comment = "LabelsN"}
            };
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var assembly = Assembly.GetAssembly(typeof(CTableReport));
            foreach (Report rep in ret)
            {
                string resName = "WeigherDatabase.ErmatelReports." + rep.Comment + ".frx";

                try
                {
                    using (var rs = assembly.GetManifestResourceStream(resName))
                    {
                        int len = (int)rs.Length;
                        byte[] buf = new byte[len];
                        rs.Read(buf, 0, len);
                        rep.Value = Encoding.UTF8.GetString(buf).Substring(1);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Ошибка при открытии файла с отчетом");
                }

            }
            return ret;
        }
    }

}