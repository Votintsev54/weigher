﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using DbCommon.Attributes;
using DbCommon.Helpers;

namespace WeigherData
{
    public partial class CTableInterface
    {

        [StartValue]
        public static IEnumerable<Interface> CreateCTableInterface()
        => new Interface[]
            {
                new Interface(){ Id=1,Name = "стандартный",PrinterA4="1",PrinterLabel="2",UseBags=true,UseSkleiki = false,UseGroups = true,UseBrutto = true},
                new Interface(){ Id=2,Name = "Проксима",PrinterA4="1",PrinterLabel="3",UseBags=true,UseSkleiki = true,UseGroups = true,UseBrutto = false},
                new Interface(){ Id=3,Name = "Байкалси",PrinterA4="1",PrinterLabel="2",UseBags=true,UseSkleiki = false,UseGroups = true,UseBrutto = false},
                new Interface(){ Id=4,Name = "Безымянный",PrinterA4="1",PrinterLabel="2",UseBags=true,UseSkleiki = false,UseGroups = true,UseBrutto = false},
                new Interface(){ Id=5,Name = "Без заказчика",PrinterA4="1",PrinterLabel="2",UseBags=true,UseSkleiki = false,UseGroups = true,UseBrutto = true},
            };           
    }

}