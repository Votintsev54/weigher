﻿using System.Collections.Generic;
using DbCommon.Attributes;

namespace WeigherData
{
    public partial class CTableDbVersion
    {
        public const string CurrentDbVersionConst = DbVersion2024_1;
        public const string DbVersion2024_1 = "vers. 20241005"; // 
        public const string DbVersion2023_4 = "vers. 20230105"; // 
        public const string DbVersion2023 = "vers. 20230101"; // 
        public const string DbVersionMebius6 = "vers. 20220911"; // восстанавливаем байкалси
        public const string DbVersionMebius5 = "vers. 20220803"; // устраняем печать 0 под мебиусом, шрифты + внешний ключ по OrderDetailId
        public const string DbVersionMebius4 = "vers. 20220727"; // номер поддона (автоувеличение поля)
        public const string DbVersionMebius3 = "vers. 20220707"; // шрифт у циферок
        public const string DbVersionMebius2 = "vers. 20220705"; // толщина линий
        public const string DbVersionMebius = "vers. 20220703"; // на значек мебиуса наносим вид материала
        public const string DbVersionPaletRenew = "vers. 20211202"; // на упаковочный лист вернули штрихкод упаковочного листа (2 отчета)
                                                                    // , размер болока QR кода по умолчанию сменился на 2
                                                                    // , переход на 4.8 framework
        public const string DbVersionPalleLabel5 = "vers. 20211005"; // вернули номер поддона
        public const string DbVersionPalleLabel4 = "vers. 20210920"; // определились со штрихкодом на листе
        public const string DbVersionPalleLabel3 = "vers. 20210915"; // пофиксил изменение аттрибутов лоченного файла, лист для грузчиков + штрихкод
        public const string DbVersionPalleLabel2 = "vers. 20210910"; // увеличил шрифт в листе для грузчиков, изменил ориентацию страницы, убрал с нее штихкод
        // возможность чтения из занятых файлов
        // пофиксил чтение каталога - падение на некоторых неправильных именах файлов (все имена не соответствующие (номер)_(Наименование).xlsm - пропускаются)
        public const string DbVersionPalleLabel= "vers. 20210402"; // добавлен лист с номером заказа для грузчиков, количество упаковочных листов по умолчанию
        public const string DbVersionPackingListNN = "vers. 20210318"; // добавлен упаковочный лист для "безымянного" интерфейса
        public const string DbVersionUpdateble = "vers. 20201221";
        public const string DbVersionBaikalSi2 = "vers. 20201202";
        public const string DbVersionBaikalSi = "vers. 20201122";
        public const string DbVersionClientBack = "vers. 20201120";
        public const string DbVersionInterfaceTable5 = "vers. 20201114";
        public const string DbVersionInterfaceTable4 = "vers. 20201113";
        public const string DbVersionInterfaceTable3 = "vers. 20201112";
        public const string DbVersionInterfaceTable2 = "vers. 20201111";
        public const string DbVersionInterfaceTable = "vers. 20201110";
        public const string DbVersionCustomersTable = "vers. 20201109";
        public const string DbVersionWithSkleekConst5 = "vers. 20201108";
        public const string DbVersionWithSkleekConst4 = "vers. 20201107";
        public const string DbVersionWithSkleekConst3 = "vers. 20201106";
        public const string DbVersionWithSkleekConst2 = "vers. 20201105";
        public const string DbVersionWithSkleekConst = "vers. 20200621";
        public const string DbVersionWithLangIdConst = "vers. 20200222";
        public const string DbVersionIncorrect= "vers. 20201001";

        [StartValue]
        public static IEnumerable<DbVersion> CurrentDbVersion()
            =>new DbVersion[]{new DbVersion(){Name = CurrentDbVersionConst } };        
    }
}