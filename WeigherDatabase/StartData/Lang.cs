﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using DbCommon.Attributes;
using DbCommon.Helpers;

namespace WeigherData
{
    public partial class CTableLang
    {

        [StartValue]
        public static IEnumerable<Lang> CreateCTableLang()
        => new Lang[]
            {
                new Lang(){ Id="ru",Name = "Русский"},
                new Lang(){ Id="kz",Name = "Казахский"},
                new Lang(){ Id="uk",Name = "Украинский"},
            };           
    }

}