﻿using System.Collections.Generic;
using DbCommon.Attributes;

namespace WeigherData
{
    public partial class CTableGroupCustomer
    {
        [StartValue]
        public static IEnumerable<GroupCustomer> embedColumnsMap()
            => new GroupCustomer[] { new GroupCustomer() { Id = new System.Guid("5EABD40D-F33D-4440-B092-25A6F77836B0"), Name = "АО \"Компания Проксима\"", InterfaceId = 2 } };
    }
}