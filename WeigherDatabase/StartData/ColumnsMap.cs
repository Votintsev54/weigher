﻿using System.Collections.Generic;
using DbCommon.Attributes;

namespace WeigherData
{
    public partial class CTableColumnsMap
    {
        [StartValue]
        public static IEnumerable<ColumnsMap> embedColumnsMap()
        => new ColumnsMap[]
            {
                new ColumnsMap(){ExcelName ="ТУ",Name="Specification"},
                new ColumnsMap(){ExcelName ="Материал",Name="Material"},
                new ColumnsMap(){ExcelName ="№ партии",Name="Party"},
                new ColumnsMap(){ExcelName ="Заказчик",Name="Client"},
                new ColumnsMap(){ExcelName ="Наименование продукции",Name="ProductName"},
                new ColumnsMap(){ExcelName ="Характеристика продукции",Name="ProductCharacteristic"},
                new ColumnsMap(){ExcelName ="Ширина роля",Name="RollWidth"},
                new ColumnsMap(){ExcelName ="Грамматура",Name="Density"},
                new ColumnsMap(){ExcelName ="Вес втулки",Name="VtulkaWeight"},
                new ColumnsMap(){ExcelName ="Вес заглушки",Name="StubWeight"},
                new ColumnsMap(){ExcelName ="Вес упаковки",Name="PackWeight"},
                new ColumnsMap(){ExcelName ="Толщина упаковки(знак)",Name="PackThick"},
                new ColumnsMap(){ExcelName ="Вес кадра",Name="FrameWeight"},
                new ColumnsMap(){ExcelName ="Срок хранения",Name="Shelf"},
                new ColumnsMap(){ExcelName ="Тип интерфейса бирки",Name="Interface"},
                new ColumnsMap(){ExcelName ="Вес одной этикетки",Name="FrameWeight"},

            };
    }
}