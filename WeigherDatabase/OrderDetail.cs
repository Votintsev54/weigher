﻿using System;
using System.Linq;
using System.Xml.Serialization;

namespace WeigherData
{
    public partial class OrderDetail
    {
        public string Client
        {
            get => Customer.Name;
            set
            {
                string name = value;
                string inn = ExtractInn(ref name);
                CDatabaseWeigher db = (CDatabaseWeigher)_table.parentDataBase;
                var cust = db.Customer.GetObjectByExpr(cs => cs.Name == name);
                if (cust == null)
                {
                    cust = db.Customer.NewInstance();
                    if (inn != null)
                    {
                        var groupCust = db.GroupCustomer.GetObjectByExpr(cs => cs.Inn == inn);
                        if (groupCust == null)
                        {
                            groupCust = db.GroupCustomer.NewInstance();
                            groupCust.Name = name;
                            groupCust.Inn = inn;
                            groupCust.Save2();
                        }
                        cust.Inn = inn;
                        cust.GroupCustomer = groupCust;
                    }
                    cust.Name = name;
                    cust.Save2();
                }
                Customer = cust;
            }
        }

        /// <summary>
        /// Извлекаем ИНН, если он в конце имени либо вначали и отделен запятой
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private string ExtractInn(ref string name)
        {
            var zptIndex = name.LastIndexOf(",");
            if (zptIndex < 0) return null;
            var innCandidate = name.Substring(zptIndex + 1).Trim();
            if (innCandidate.Length >= 10 && innCandidate.Length <= 12 && innCandidate.ToCharArray().All(c => c >= '0' && c <= '9'))
            {
                name = name.Substring(0, zptIndex).Trim();
                return innCandidate;
            }
            zptIndex = name.IndexOf(",");
            innCandidate = name.Substring(0, zptIndex).Trim();
            if (innCandidate.Length >= 10 && innCandidate.Length <= 12 && innCandidate.ToCharArray().All(c => c >= '0' && c <= '9'))
            {
                name = name.Substring(zptIndex + 1).Trim();
                return innCandidate;
            }
            return null;
        }


        [XmlIgnore]
        public string PackThickNom
        {
            get
            {
                try
                {
                    var arr = _packThick.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.None);
                    if (arr.Length >= 2)
                        return arr[1];
                }
                catch { }
                return "";
            }
        }

        [XmlIgnore]
        public string PackThickMatherial
        {
            get
            {
                try
                {
                    return _packThick.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.None)[0];
                }
                catch { }
                return "";

            }
        }

    }
}