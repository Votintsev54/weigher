﻿using System.Globalization;
using System.Linq;
using System.Text;
using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QRCoder;
using System.Xml.Serialization;
using WeigherDatabase;
using System;
using System.Collections.Generic;
using DbCommon.Json;
using System.Text.RegularExpressions;

namespace WeigherData
{
    public partial class Package
    {

        private string qrCodeString;

        //        Описание QR строки упаковочного листа:
        //Разделитель: [;] Окончание строки[.]
        //Порядок полей:
        //*) Поставщик: Формат: Строка; Пример: Ermatel
        //*) Заказчик: Формат: Строка; Пример: ООО "Фабрика мороженного Славица"
        //*) Номер заказа: Формат: Строка; Пример: 191744
        //*) Дата упаковки: Формат: Число.Месяц.Год; Пример: 13.06.2019
        //*) Наименование: Формат: Строка; Пример: Умка с арахисом
        //*) Материал: Формат: Строка; Пример: BOPPмат20+BOPPжем35

        //*) Итого Количество: Число 10,0; Пример - 27
        //*) Итого Длина: Число 15,2; Пример - 35613,59
        //8) Итого Масса нетто: Число 15,2; Пример - 335,48
        //9) Итого Масса брутто: Число 15,2; Пример - 342,50

        //Состав палетты:
        //0) Номер строки: Число 10,0; Пример - 1 
        //1) Длина: Число 15,2; Пример - 1216,56
        //2) Масса нетто: Число 15,2; Пример - 11,46
        //3) Масса брутто: Число 15,2; Пример - 11,72

        //0) Номер строки: Число 10,0; Пример - 2 
        //1) Длина: Число 15,2; Пример - 1212,31
        //2) Масса нетто: Число 15,2; Пример - 11,42
        //3) Масса брутто: Число 15,2; Пример - 11,58
        //и так весь состав
        //Итоговая строка:
        //Ermatel
        //[;]ООО "Фабрика мороженного Славица"[;]191744[;]13.06.2019[;] Умка с арахисом[;]BOPPмат20+BOPPжем35[;]27[;]35613,59[;]335,48[;]342,50[;]1[;]1216,56[;]11,46[;]11,72[;]..... 27[;]1343,95[;]12,66[;]12,92[.]

        [XmlIgnore]
        public string QrCodeString
        {
            get
            {
                if (qrCodeString == null)
                {

                }
                return qrCodeString;
            }

            set
            {
                qrCodeString = value;
            }
        }


        public StringBuilder GetQrCodeString(StringBuilder sb = null)
        {
            string delim = QrSettings.qrSettingsInstance?.Delimeter;
            if (sb == null) sb = new StringBuilder();
            int packageId = Id;
            var orderDetails = Order.OrderDetails.Where(detail => detail.Rolls.Any(roll => roll.PackageId == packageId)).ToList();

            var rolls =
                (from detail in orderDetails from roll in detail.Rolls where roll.PackageId == packageId select roll)
                    .ToList();
            sb.Append("Ermatel").Append(delim);
            sb.Append(orderDetails.First().Customer).Append(delim);
            sb.Append(Order.Number).Append(delim);
            sb.Append(Date.ToString("d", CultureInfo.CreateSpecificCulture("ru-RU"))).Append(delim);

            foreach (var orderDetail in orderDetails)
            {
                sb.Append(orderDetail.ProductName).Append(delim);
                sb.Append(orderDetail.Material).Append(delim);

                int orderDetailId = orderDetail.Id;
                var inDetailRolls = rolls.Where(r => r.OrderDetailId == orderDetailId);

                sb.Append(inDetailRolls.Count()).Append(delim);
                sb.Append(inDetailRolls.Sum(r => r.RollLength).ToString("F")).Append(delim);
                sb.Append(inDetailRolls.Sum(r => r.RollWeight).ToString("F")).Append(delim);
                sb.Append(inDetailRolls.Sum(r => r.BruttoWeight).ToString("F")).Append(delim);

                foreach (var roll in inDetailRolls)
                {
                    roll.GetQrCodeString(sb, delim);
                }
            }
            sb.Append(this.Mcarton).Append(delim);
            sb.Append(this.Mkrishki).Append(delim);
            sb.Append(this.Mstretch).Append(delim);
            return sb;
        }
        class CustomConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                return (objectType == typeof(int) || objectType == typeof(decimal));
            }
            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                if (value is int iVal)
                {
                    writer.WriteValue(iVal);
                    return;
                }
                if (value is decimal dVal)
                {
                    string retVal = dVal.ToString("F2", nfi).TrimEnd(new char[] { '0' }).TrimEnd(new char[] { '.' });

                    writer.WriteValue(retVal);
                    return;
                }
            }
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }
            public override bool CanRead => false;
            public override bool CanWrite => true;


        }

        static NumberFormatInfo nfi = new NumberFormatInfo { NumberDecimalSeparator = "." };

        static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            Formatting = Newtonsoft.Json.Formatting.None,
            Converters = new List<JsonConverter> { new CustomConverter() },
            FloatFormatHandling = FloatFormatHandling.String
        };
        public string GetJson(StringBuilder sb = null)
        {

            int packageId = Id;
            var orderDetails = Order.OrderDetails.Where(detail => detail.Rolls.Any(roll => roll.PackageId == packageId)).ToList();

            var rolls =
                (from detail in orderDetails from roll in detail.Rolls where roll.PackageId == packageId select roll)
                    .ToList();

            var customer = orderDetails.First().Customer;

            var jsonOrder = new
            {
                Producer = "Ermatel",
                Inn = customer.Inn,
                Client = customer.Name,
                OrderNumber = Order.Number,
                CustNumber = Order.CustNumber,
                Date = Date.ToString("d", CultureInfo.CreateSpecificCulture("ru-RU")),
                Details = orderDetails.Select(od => new
                {
                    od.ProductName,
                    od.Material,
                    Vtulka = od.VtulkaWeight,
                    Dens = od.Density,
                    //    Rols = rolls.Where(r => r.OrderDetailId == od.Id)
                    //.Select(r => new decimal[] { 
                    //    r.RollNumber
                    //, Math.Round(r.RollLength, 2, MidpointRounding.ToEven)
                    //, Math.Round(r.RollWeight, 2, MidpointRounding.ToEven)
                    //, Math.Round(r.BruttoWeight, 2, MidpointRounding.ToEven)
                    //, r.Skleek })
                    RolN = packNumbers(rolls.Where(r => r.OrderDetailId == od.Id).Select(r => r.RollNumber).ToArray()),
                    //RolW = rolls.Where(r => r.OrderDetailId == od.Id).Select(r => Math.Round(r.RollWeight, 2, MidpointRounding.ToEven)).ToArray(),
                    RolW = rolls.Where(r => r.OrderDetailId == od.Id).Select(r => (int)(100 * Math.Round(r.RollWeight, 2, MidpointRounding.ToEven))).ToArray(),
                    RolS = packSkleek(rolls.Where(r => r.OrderDetailId == od.Id).Select(r => r.Skleek).ToArray()),
                }).ToArray(),
                Carton = this.Mcarton,
                Krishka = this.Mkrishki,
                Stretch = this.Mstretch
            };

            string json = JsonConvert.SerializeObject(jsonOrder);

            StringBuilder sbJson = new StringBuilder();
            Regex regex = new Regex(@"[.]\d0[],]", RegexOptions.IgnoreCase);
            int iStart = 0;
            foreach (Match mc in regex.Matches(json))
            {
                sbJson.Append(json.Substring(iStart, mc.Index + 2 - iStart));
                iStart = mc.Index + 3;
            }
            sbJson.Append(json.Substring(iStart));

            json = sbJson.Replace(".0,", ",").Replace(".0]", "]").Replace(".0}", "}").ToString();

            return json;
        }

        private string packSkleek(int[] vs)
        {
            StringBuilder ret = new StringBuilder();
            int state = 0;
            int prevnum = -1;
            int count = 0;
            foreach (var num in vs)
            {
                switch (state)
                {
                    case 0:
                        state = 1;
                        break;
                    case 1:
                        if (prevnum != num)
                        {
                            if (count == 1)
                                ret.Append(prevnum).Append(",");
                            else
                                ret.Append($"{prevnum}({count}),");
                            count = 0;
                        }
                        break;
                }
                count++;
                prevnum = num;
            }
            if (state > 0)
            {
                if (count == 1)
                    ret.Append(prevnum);
                else
                    ret.Append($"{prevnum}({count})");
            }
            return ret.ToString();
        }

        private string packNumbers(int[] vs)
        {
            StringBuilder ret = new StringBuilder();
            int state = 0;
            int prevnum = -1;
            foreach (var num in vs)
            {

                switch (state)
                {
                    case 0:
                        ret.Append(num);
                        state = 1;
                        break;
                    case 1:
                    case 2:
                        if (prevnum + 1 == num)
                            state = 2;
                        else
                        {
                            ret.Append("-").Append(prevnum).Append(",").Append(num);
                            state = 1;
                        }
                        break;
                }
                prevnum = num;
            }
            if (state == 2)
                ret.Append("-").Append(prevnum);
            return ret.ToString();
        }

        public static Image emptyBitmap = new Bitmap(1, 1);

        private Image GetBitmapImage(int blockSize)
        {
            if (QrSettings.qrSettingsInstance.QrState == QrSettings.QrStates.NotPrint)
                return emptyBitmap;
            QRCodeGenerator.ECCLevel eccLevel = QrSettings.qrSettingsInstance.ECCLevel;

            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                var qrstring = QrSettings.qrSettingsInstance.QrState == QrSettings.QrStates.PrintWithDelimeter
                    ? GetQrCodeString().ToString()
                    : GetJson().ToString();
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(qrstring, eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {
                        return qrCode.GetGraphic(blockSize, Color.Black, Color.White, null);
                    }
                }
            }
        }

        [XmlIgnore]
        public Image QrImage => GetBitmapImage(QrSettings.qrSettingsInstance.BlockSize);

        [XmlIgnore]
        public Image QrImage2 => GetBitmapImage(QrSettings.qrSettingsInstance.BlockSize + 1);

    }
}