﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using DbCommon.Attributes;
using DbCommon.Helpers;

namespace WeigherData
{
    public partial class Roll
    {

        //        Описание QR строки упаковочного листа:
        //Разделитель: [;] Окончание строки[.]
        //Порядок полей:
        //0) Поставщик: Формат: Строка; Пример: Ermatel
        //1) Заказчик: Формат: Строка; Пример: ООО "Фабрика мороженного Славица"
        //2) Номер заказа: Формат: Строка; Пример: 191744
        //3) Дата упаковки: Формат: Число.Месяц.Год; Пример: 13.06.2019
        //4) Наименование: Формат: Строка; Пример: Умка с арахисом
        //5) Материал: Формат: Строка; Пример: BOPPмат20+BOPPжем35
        //6) Итого Количество: Число 10,0; Пример - 27
        //7) Итого Длина: Число 15,2; Пример - 35613,59
        //8) Итого Масса нетто: Число 15,2; Пример - 335,48
        //9) Итого Масса брутто: Число 15,2; Пример - 342,50
        //Состав палетты:
        //0) Номер строки: Число 10,0; Пример - 1 
        //1) Длина: Число 15,2; Пример - 1216,56
        //2) Масса нетто: Число 15,2; Пример - 11,46
        //3) Масса брутто: Число 15,2; Пример - 11,72

        //0) Номер строки: Число 10,0; Пример - 2 
        //1) Длина: Число 15,2; Пример - 1212,31
        //2) Масса нетто: Число 15,2; Пример - 11,42
        //3) Масса брутто: Число 15,2; Пример - 11,58
        //и так весь состав
        //Итоговая строка:
        //Ermatel
        //[;]ООО "Фабрика мороженного Славица"[;]191744[;]13.06.2019[;] Умка с арахисом[;]BOPPмат20+BOPPжем35[;]27[;]35613,59[;]335,48[;]342,50[;]1[;]1216,56[;]11,46[;]11,72[;]..... 27[;]1343,95[;]12,66[;]12,92[.]

        /// <summary>
        /// Длина рола
        /// </summary>
        public decimal RollLength
        {
            get
            {
                decimal len = -1;
                if (OrdersDetail.FixLen.HasValue)
                    len = OrdersDetail.FixLen.Value;
                else if (OrdersDetail.RollWidth.HasValue)
                    len = RollWeight * 1000000 / OrdersDetail.RollWidth.Value / OrdersDetail.Density;
                return len;
            }
        }

        public string ZakazNumber => OrdersDetail.Orders.CustNumber;

        public string Etiketok => OrdersDetail.FrameWeight == 0 ? "-" : ((int)(RollWeight * 1000 / OrdersDetail.FrameWeight)).ToString();
        public int EtiketokInt => OrdersDetail.FrameWeight == 0 ? 0 : ((int)(RollWeight * 1000 / OrdersDetail.FrameWeight));

        public StringBuilder GetQrCodeString(StringBuilder sb = null, string delim = null)
        {
            if (sb == null) sb = new StringBuilder();
            sb.Append(RollNumber.ToString("F0")).Append(delim);
            sb.Append(RollLength.ToString("F")).Append(delim);
            sb.Append(RollWeight.ToString("F")).Append(delim);
            sb.Append(BruttoWeight.ToString("F")).Append(delim);
            sb.Append(Skleek.ToString("F")).Append(delim);
            return sb;
        }

    }
}