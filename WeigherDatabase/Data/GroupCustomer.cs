using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableGroupCustomer: CTable< GroupCustomer, CDatabaseWeigher,Guid>
  {
  	CTableGroupCustomer()
	{}
	        public CTableGroupCustomer(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(GroupCustomer), "Id", "Guid", "uniqueidentifier", 0, 0, FieldDescription.fieldProp.PrimaryKey );
			_fd["Name"] = new FieldDescription(typeof(GroupCustomer), "Name", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["Inn"] = new FieldDescription(typeof(GroupCustomer), "Inn", "string", "nvarchar", 12, 0, FieldDescription.fieldProp.Nullable );
			_fd["InterfaceId"] = new FieldDescription(typeof(GroupCustomer), "InterfaceId", "int?", "int", 0, 0, FieldDescription.fieldProp.Nullable  | FieldDescription.fieldProp.ForeignKey );
			_fd["LangId"] = new FieldDescription(typeof(GroupCustomer), "LangId", "string", "nvarchar", 5, 0, FieldDescription.fieldProp.Nullable );
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// Группа заказчиков(группа юрлиц)
	/// </summary>
	public partial class GroupCustomer : BObject,IPrimaryKey<Guid> 
    {
        public GroupCustomer() { }
        public GroupCustomer(CTableGroupCustomer t)
            : base(t)
        { }

		private Guid _id = Guid.NewGuid();
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public Guid Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _inn = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Inn
        {
            get { return _inn; }
            set {
                    if (Compare(_inn, value) != 0)
                    {
                        SetDirtyFlag();
                        _inn = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int? _interfaceId = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int? InterfaceId
        {
            get { return _interfaceId; }
            set {
                    if (Compare(_interfaceId, value) != 0)
                    {
                        SetDirtyFlag();
                        _interfaceId = value;
                       _interfaceIdObject = null ;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		Interface _interfaceIdObject ;
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public Interface Interface
        {
		
            get 
			{ 
				if (_interfaceIdObject == null && _interfaceId != null)
					_interfaceIdObject = (Interface)((CDatabaseWeigher)_table.parentDataBase).Interface.GetById(_interfaceId);
				return _interfaceIdObject ; 
			}
            set 
			{ 
				InterfaceId = value?.Id ?? null ;
				_interfaceIdObject = value ;
			}
        }

		private string _langId = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string LangId
        {
            get { return _langId; }
            set {
                    if (Compare(_langId, value) != 0)
                    {
                        SetDirtyFlag();
                        _langId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
