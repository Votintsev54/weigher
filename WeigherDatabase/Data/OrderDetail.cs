using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableOrderDetail: CTable< OrderDetail, CDatabaseWeigher,int>
  {
  	CTableOrderDetail()
	{}
	        public CTableOrderDetail(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(OrderDetail), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["OrderId"] = new FieldDescription(typeof(OrderDetail), "OrderId", "int", "int", 0, 0, FieldDescription.fieldProp.ForeignKey );
			_fd["ParentId"] = new FieldDescription(typeof(OrderDetail), "ParentId", "int?", "int", 0, 0, FieldDescription.fieldProp.Nullable );
			_fd["Specification"] = new FieldDescription(typeof(OrderDetail), "Specification", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["Material"] = new FieldDescription(typeof(OrderDetail), "Material", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["CustomerId"] = new FieldDescription(typeof(OrderDetail), "CustomerId", "Guid", "uniqueidentifier", 0, 0, FieldDescription.fieldProp.ForeignKey );
			_fd["ProductName"] = new FieldDescription(typeof(OrderDetail), "ProductName", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["ProductCharacteristic"] = new FieldDescription(typeof(OrderDetail), "ProductCharacteristic", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["VtulkaWeight"] = new FieldDescription(typeof(OrderDetail), "VtulkaWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["Shelf"] = new FieldDescription(typeof(OrderDetail), "Shelf", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["PackWeight"] = new FieldDescription(typeof(OrderDetail), "PackWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["StubWeight"] = new FieldDescription(typeof(OrderDetail), "StubWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["FrameWeight"] = new FieldDescription(typeof(OrderDetail), "FrameWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["PackThick"] = new FieldDescription(typeof(OrderDetail), "PackThick", "string", "nvarchar", 20, 0, FieldDescription.fieldProp.Empty);
			_fd["RollWidth"] = new FieldDescription(typeof(OrderDetail), "RollWidth", "decimal?", "decimal", 18, 2, FieldDescription.fieldProp.Nullable );
			_fd["Density"] = new FieldDescription(typeof(OrderDetail), "Density", "decimal", "decimal", 18, 5, FieldDescription.fieldProp.Empty);
			_fd["Party"] = new FieldDescription(typeof(OrderDetail), "Party", "string", "nvarchar", 50, 0, FieldDescription.fieldProp.Nullable );
			_fd["FixLen"] = new FieldDescription(typeof(OrderDetail), "FixLen", "decimal?", "decimal", 18, 2, FieldDescription.fieldProp.Nullable );
			_fd["FixArea"] = new FieldDescription(typeof(OrderDetail), "FixArea", "decimal?", "decimal", 18, 2, FieldDescription.fieldProp.Nullable );
			_fd["Barcode"] = new FieldDescription(typeof(OrderDetail), "Barcode", "string", "nvarchar", 50, 0, FieldDescription.fieldProp.Empty);
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// 
	/// </summary>
	public partial class OrderDetail : BObject,IPrimaryKey<int> 
    {
        public OrderDetail() { }
        public OrderDetail(CTableOrderDetail t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _orderId = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int OrderId
        {
            get { return _orderId; }
            set {
                    if (Compare(_orderId, value) != 0)
                    {
                        SetDirtyFlag();
                        _orderId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public Orders Orders
        {
		
            get { return ((CDatabaseWeigher)_table.parentDataBase).Orders.GetById(_orderId); }
            set { this.OrderId = value == null ? 0 : value.Id ; }
        }

		private int? _parentId = null;
		
		/// <summary>
		/// ссылка на родительский детайл(для брака)
		/// </summary>
		[XmlAttribute]
        public int? ParentId
        {
            get { return _parentId; }
            set {
                    if (Compare(_parentId, value) != 0)
                    {
                        SetDirtyFlag();
                        _parentId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _specification = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Specification
        {
            get { return _specification; }
            set {
                    if (Compare(_specification, value) != 0)
                    {
                        SetDirtyFlag();
                        _specification = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _material = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Material
        {
            get { return _material; }
            set {
                    if (Compare(_material, value) != 0)
                    {
                        SetDirtyFlag();
                        _material = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private Guid _customerId = Guid.Empty;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public Guid CustomerId
        {
            get { return _customerId; }
            set {
                    if (Compare(_customerId, value) != 0)
                    {
                        SetDirtyFlag();
                        _customerId = value;
                       _customerIdObject = null ;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		Customer _customerIdObject ;
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public Customer Customer
        {
		
            get 
			{ 
				if (_customerIdObject == null && _customerId != null)
					_customerIdObject = (Customer)((CDatabaseWeigher)_table.parentDataBase).Customer.GetById(_customerId);
				return _customerIdObject ; 
			}
            set 
			{ 
				CustomerId = value?.Id ?? Guid.Empty ;
				_customerIdObject = value ;
			}
        }

		private string _productName = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string ProductName
        {
            get { return _productName; }
            set {
                    if (Compare(_productName, value) != 0)
                    {
                        SetDirtyFlag();
                        _productName = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _productCharacteristic = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string ProductCharacteristic
        {
            get { return _productCharacteristic; }
            set {
                    if (Compare(_productCharacteristic, value) != 0)
                    {
                        SetDirtyFlag();
                        _productCharacteristic = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _vtulkaWeight = 0m;
		
		/// <summary>
		/// Вес втулки
		/// </summary>
		[XmlAttribute]
        public decimal VtulkaWeight
        {
            get { return _vtulkaWeight; }
            set {
                    if (Compare(_vtulkaWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _vtulkaWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _shelf = 6;
		
		/// <summary>
		/// Срок хранения, мес
		/// </summary>
		[XmlAttribute]
        public int Shelf
        {
            get { return _shelf; }
            set {
                    if (Compare(_shelf, value) != 0)
                    {
                        SetDirtyFlag();
                        _shelf = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _packWeight = 0m;
		
		/// <summary>
		/// Вес упаковки
		/// </summary>
		[XmlAttribute]
        public decimal PackWeight
        {
            get { return _packWeight; }
            set {
                    if (Compare(_packWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _packWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _stubWeight = 0m;
		
		/// <summary>
		/// Вес заглушки
		/// </summary>
		[XmlAttribute]
        public decimal StubWeight
        {
            get { return _stubWeight; }
            set {
                    if (Compare(_stubWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _stubWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _frameWeight = 0m;
		
		/// <summary>
		/// Вес кадра
		/// </summary>
		[XmlAttribute]
        public decimal FrameWeight
        {
            get { return _frameWeight; }
            set {
                    if (Compare(_frameWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _frameWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _packThick = "5 PP";
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string PackThick
        {
            get { return _packThick; }
            set {
                    if (Compare(_packThick, value) != 0)
                    {
                        SetDirtyFlag();
                        _packThick = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal? _rollWidth = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public decimal? RollWidth
        {
            get { return _rollWidth; }
            set {
                    if (Compare(_rollWidth, value) != 0)
                    {
                        SetDirtyFlag();
                        _rollWidth = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _density = 0;
		
		/// <summary>
		/// Плотность упаковки
		/// </summary>
		[XmlAttribute]
        public decimal Density
        {
            get { return _density; }
            set {
                    if (Compare(_density, value) != 0)
                    {
                        SetDirtyFlag();
                        _density = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _party = null;
		
		/// <summary>
		/// № партии
		/// </summary>
		[XmlAttribute]
        public string Party
        {
            get { return _party; }
            set {
                    if (Compare(_party, value) != 0)
                    {
                        SetDirtyFlag();
                        _party = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal? _fixLen = null;
		
		/// <summary>
		/// Фиксированная длина
		/// </summary>
		[XmlAttribute]
        public decimal? FixLen
        {
            get { return _fixLen; }
            set {
                    if (Compare(_fixLen, value) != 0)
                    {
                        SetDirtyFlag();
                        _fixLen = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal? _fixArea = null;
		
		/// <summary>
		/// Фиксированная площадь
		/// </summary>
		[XmlAttribute]
        public decimal? FixArea
        {
            get { return _fixArea; }
            set {
                    if (Compare(_fixArea, value) != 0)
                    {
                        SetDirtyFlag();
                        _fixArea = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _barcode = "";
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Barcode
        {
            get { return _barcode; }
            set {
                    if (Compare(_barcode, value) != 0)
                    {
                        SetDirtyFlag();
                        _barcode = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
	
        private SortList< Roll > _rolls = null;
        
        [XmlArray]
        public SortList< Roll > Rolls
        {
            get
            {
                if (_rolls != null) return _rolls;
				if (_table != null)
				{
					var thisid = this.Id ;
					_rolls = ((CDatabaseWeigher)_table.parentDataBase). Roll .GetObjectListByExpr(obj => obj.OrderDetailId == thisid);
				}
				else
					_rolls = new SortList< Roll >(_table);
				return _rolls;
            }
        }


        public override object DeepClone(int deep = 99)
        {
            var t = (OrderDetail)Clone();
			if (deep > 0)
             t._rolls = Rolls.CloneAll(deep-1);             
          return t;
        }


		[XmlIgnore]
        public override BObject.ObjectStateType ObjectState
        {
            get
            {
                if (_objectState != BObject.ObjectStateType.Unchanged)
                    return _objectState;
				BObject.ObjectStateType list_state = BObject.ObjectStateType.Unchanged;	
              
				if (_rolls != null)
				{  
					list_state = this.Rolls.ObjectListState();
					if (list_state != BObject.ObjectStateType.Unchanged)
						return list_state;
				}
					
				return list_state;
            }
		}

			
        public override void Save2()
			
        {
            ObjectStateType state = this.ObjectState;
            if (state == ObjectStateType.Unchanged)
            {
                return;
            }
			
                
            if (state == ObjectStateType.Deleted)
            {
                // удаление каскадное !
                base.Save2();
                return;
            } 
			
			
            base.Save2();
			
	
            foreach (Roll obj in this.Rolls)
                obj.OrderDetailId = this.Id;
            this.Rolls.Save();
			
			

            return;
        }
			

        public override void Rollback()
        {
            base.Rollback();
			
	
            this.Rolls.Rollback();
        }
	
    }

}
	
