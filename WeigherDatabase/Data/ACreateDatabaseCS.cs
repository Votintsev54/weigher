﻿

using System.Data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


// C:\MyProjects\weigher\WeigherDatabase\Data\weigher.xml
namespace WeigherData
{

    public partial class CDatabaseWeigher : CDatabase 
	{
	    public CDatabaseWeigher(): base(DatabaseType.MsSql)
        {
            Init();
        }

        public CDatabaseWeigher(DatabaseType databaseType)
            : base(databaseType)
        {
            Init();
        }

        public CDatabaseWeigher(DatabaseType databaseType,string connectionString)
            : base(databaseType)
        {
            _sConnectionString = connectionString;
            Init();
			ConnectionMode = DbCommon.Enums.ConnectionModes.DatabaseConnection;
        }

        public string CDatabaseName = "Weigher";
      	public CTableOrders Orders;
      	public CTableInterface Interface;
      	public CTableOrderDetail OrderDetail;
      	public CTableOrderDetailLog OrderDetailLog;
      	public CTablePackage Package;
      	public CTableRoll Roll;
      	public CTableRemovedRoll RemovedRoll;
      	public CTableSequences Sequences;
      	public CTablePacker Packer;
      	public CTableCustomer Customer;
      	public CTableGroupCustomer GroupCustomer;
      	public CTableReport Report;
      	public CTableReportWords ReportWords;
      	public CTableLang Lang;
      	public CTableColumnsMap ColumnsMap;
      	public CTableDbVersion DbVersion;
        public void Init()
        {
  			Orders = new CTableOrders(this);
  			Interface = new CTableInterface(this);
  			OrderDetail = new CTableOrderDetail(this);
  			OrderDetailLog = new CTableOrderDetailLog(this);
  			Package = new CTablePackage(this);
  			Roll = new CTableRoll(this);
  			RemovedRoll = new CTableRemovedRoll(this);
  			Sequences = new CTableSequences(this);
  			Packer = new CTablePacker(this);
  			Customer = new CTableCustomer(this);
  			GroupCustomer = new CTableGroupCustomer(this);
  			Report = new CTableReport(this);
  			ReportWords = new CTableReportWords(this);
  			Lang = new CTableLang(this);
  			ColumnsMap = new CTableColumnsMap(this);
  			DbVersion = new CTableDbVersion(this);
	
		  _tables.Add("Orders",Orders);	
		  _tables.Add("Interface",Interface);	
		  _tables.Add("OrderDetail",OrderDetail);	
		  _tables.Add("OrderDetailLog",OrderDetailLog);	
		  _tables.Add("Package",Package);	
		  _tables.Add("Roll",Roll);	
		  _tables.Add("RemovedRoll",RemovedRoll);	
		  _tables.Add("Sequences",Sequences);	
		  _tables.Add("Packer",Packer);	
		  _tables.Add("Customer",Customer);	
		  _tables.Add("GroupCustomer",GroupCustomer);	
		  _tables.Add("Report",Report);	
		  _tables.Add("ReportWords",ReportWords);	
		  _tables.Add("Lang",Lang);	
		  _tables.Add("ColumnsMap",ColumnsMap);	
		  _tables.Add("DbVersion",DbVersion);	
		}
        
        

	}
}

