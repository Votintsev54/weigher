using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableDbVersion: CTable< DbVersion, CDatabaseWeigher,int>
  {
  	CTableDbVersion()
	{}
	        public CTableDbVersion(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(DbVersion), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["Name"] = new FieldDescription(typeof(DbVersion), "Name", "string", "nvarchar", 128, 0, FieldDescription.fieldProp.Empty);
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// Версия базы данных
	/// </summary>
	public partial class DbVersion : BObject,IPrimaryKey<int> 
    {
        public DbVersion() { }
        public DbVersion(CTableDbVersion t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = "";
		
		/// <summary>
		/// Наименование
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
