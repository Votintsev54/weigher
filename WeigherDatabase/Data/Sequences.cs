using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableSequences: CTable< Sequences, CDatabaseWeigher,int>
  {
  	CTableSequences()
	{}
	        public CTableSequences(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(Sequences), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["Code"] = new FieldDescription(typeof(Sequences), "Code", "string", "nvarchar", 50, 0, FieldDescription.fieldProp.Empty);
			_fd["Value"] = new FieldDescription(typeof(Sequences), "Value", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.NowCash;
        }
				
	}
	

	/// <summary>
	/// 
	/// </summary>
	public partial class Sequences : BObject,IPrimaryKey<int> 
    {
        public Sequences() { }
        public Sequences(CTableSequences t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _code = "";
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Code
        {
            get { return _code; }
            set {
                    if (Compare(_code, value) != 0)
                    {
                        SetDirtyFlag();
                        _code = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _value = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int Value
        {
            get { return _value; }
            set {
                    if (Compare(_value, value) != 0)
                    {
                        SetDirtyFlag();
                        _value = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
