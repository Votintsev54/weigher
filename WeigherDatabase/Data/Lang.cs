using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableLang: CTable< Lang, CDatabaseWeigher,int>
  {
  	CTableLang()
	{}
	        public CTableLang(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(Lang), "Id", "string", "nvarchar", 5, 0, FieldDescription.fieldProp.PrimaryKey );
			_fd["Name"] = new FieldDescription(typeof(Lang), "Name", "string", "nvarchar", 128, 0, FieldDescription.fieldProp.Empty);
			_fd["Comment"] = new FieldDescription(typeof(Lang), "Comment", "string", "nvarchar", 256, 0, FieldDescription.fieldProp.Nullable );
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// Печатные отчеты
	/// </summary>
	[XmlType("языки")]
	public partial class Lang : BObject 
    {
        public Lang() { }
        public Lang(CTableLang t)
            : base(t)
        { }

		private string _id = "0";
		
		/// <summary>
		/// Первичный ключ
		/// </summary>
		[XmlAttribute]
        public string Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = "";
		
		/// <summary>
		/// Наименование
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _comment = null;
		
		/// <summary>
		/// Комментарий
		/// </summary>
		[XmlAttribute]
        public string Comment
        {
            get { return _comment; }
            set {
                    if (Compare(_comment, value) != 0)
                    {
                        SetDirtyFlag();
                        _comment = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
