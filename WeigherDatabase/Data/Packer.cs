using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTablePacker: CTable< Packer, CDatabaseWeigher,int>
  {
  	CTablePacker()
	{}
	        public CTablePacker(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(Packer), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["Nom"] = new FieldDescription(typeof(Packer), "Nom", "string", "nvarchar", 20, 0, FieldDescription.fieldProp.Empty);
			_fd["Name"] = new FieldDescription(typeof(Packer), "Name", "string", "nvarchar", 128, 0, FieldDescription.fieldProp.Empty);
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// Упаковщики
	/// </summary>
	public partial class Packer : BObject,IPrimaryKey<int> 
    {
        public Packer() { }
        public Packer(CTablePacker t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _nom = "";
		
		/// <summary>
		/// Номер упаковщика
		/// </summary>
		[XmlAttribute]
        public string Nom
        {
            get { return _nom; }
            set {
                    if (Compare(_nom, value) != 0)
                    {
                        SetDirtyFlag();
                        _nom = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = "";
		
		/// <summary>
		/// Наименование
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
