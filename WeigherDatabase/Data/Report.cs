using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableReport: CTable< Report, CDatabaseWeigher,int>
  {
  	CTableReport()
	{}
	        public CTableReport(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(Report), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["Name"] = new FieldDescription(typeof(Report), "Name", "string", "nvarchar", 128, 0, FieldDescription.fieldProp.Empty);
			_fd["LangId"] = new FieldDescription(typeof(Report), "LangId", "string", "nvarchar", 5, 0, FieldDescription.fieldProp.Nullable );
			_fd["Comment"] = new FieldDescription(typeof(Report), "Comment", "string", "nvarchar", 256, 0, FieldDescription.fieldProp.Nullable );
			_fd["Value"] = new FieldDescription(typeof(Report), "Value", "string", "ntext", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["GroupCustomerId"] = new FieldDescription(typeof(Report), "GroupCustomerId", "Guid?", "uniqueidentifier", 0, 0, FieldDescription.fieldProp.Nullable  | FieldDescription.fieldProp.ForeignKey );
			_fd["InterfaceId"] = new FieldDescription(typeof(Report), "InterfaceId", "int?", "int", 0, 0, FieldDescription.fieldProp.Nullable  | FieldDescription.fieldProp.ForeignKey );
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// Печатные отчеты
	/// </summary>
	[XmlType("отчеты")]
	public partial class Report : BObject,IPrimaryKey<int> 
    {
        public Report() { }
        public Report(CTableReport t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = "";
		
		/// <summary>
		/// Наименование
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _langId = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string LangId
        {
            get { return _langId; }
            set {
                    if (Compare(_langId, value) != 0)
                    {
                        SetDirtyFlag();
                        _langId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _comment = null;
		
		/// <summary>
		/// Комментарий
		/// </summary>
		[XmlAttribute]
        public string Comment
        {
            get { return _comment; }
            set {
                    if (Compare(_comment, value) != 0)
                    {
                        SetDirtyFlag();
                        _comment = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _value = "";
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Value
        {
            get { return _value; }
            set {
                    if (Compare(_value, value) != 0)
                    {
                        SetDirtyFlag();
                        _value = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private Guid? _groupCustomerId = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public Guid? GroupCustomerId
        {
            get { return _groupCustomerId; }
            set {
                    if (Compare(_groupCustomerId, value) != 0)
                    {
                        SetDirtyFlag();
                        _groupCustomerId = value;
                       _groupCustomerIdObject = null ;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		GroupCustomer _groupCustomerIdObject ;
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public GroupCustomer GroupCustomer
        {
		
            get 
			{ 
				if (_groupCustomerIdObject == null && _groupCustomerId != null)
					_groupCustomerIdObject = (GroupCustomer)((CDatabaseWeigher)_table.parentDataBase).GroupCustomer.GetById(_groupCustomerId);
				return _groupCustomerIdObject ; 
			}
            set 
			{ 
				GroupCustomerId = value?.Id ?? null ;
				_groupCustomerIdObject = value ;
			}
        }

		private int? _interfaceId = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int? InterfaceId
        {
            get { return _interfaceId; }
            set {
                    if (Compare(_interfaceId, value) != 0)
                    {
                        SetDirtyFlag();
                        _interfaceId = value;
                       _interfaceIdObject = null ;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		Interface _interfaceIdObject ;
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public Interface Interface
        {
		
            get 
			{ 
				if (_interfaceIdObject == null && _interfaceId != null)
					_interfaceIdObject = (Interface)((CDatabaseWeigher)_table.parentDataBase).Interface.GetById(_interfaceId);
				return _interfaceIdObject ; 
			}
            set 
			{ 
				InterfaceId = value?.Id ?? null ;
				_interfaceIdObject = value ;
			}
        }
    }

}
	
