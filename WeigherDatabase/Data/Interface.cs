using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableInterface: CTable< Interface, CDatabaseWeigher,int>
  {
  	CTableInterface()
	{}
	        public CTableInterface(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(Interface), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["Name"] = new FieldDescription(typeof(Interface), "Name", "string", "nvarchar", 100, 0, FieldDescription.fieldProp.Empty);
			_fd["PrinterA4"] = new FieldDescription(typeof(Interface), "PrinterA4", "string", "nvarchar", 50, 0, FieldDescription.fieldProp.Nullable );
			_fd["PrinterLabel"] = new FieldDescription(typeof(Interface), "PrinterLabel", "string", "nvarchar", 50, 0, FieldDescription.fieldProp.Nullable );
			_fd["UseBags"] = new FieldDescription(typeof(Interface), "UseBags", "bool", "bit", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["UseSkleiki"] = new FieldDescription(typeof(Interface), "UseSkleiki", "bool", "bit", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["UseGroups"] = new FieldDescription(typeof(Interface), "UseGroups", "bool", "bit", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["UseBrutto"] = new FieldDescription(typeof(Interface), "UseBrutto", "bool", "bit", 0, 0, FieldDescription.fieldProp.Empty);
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// 
	/// </summary>
	public partial class Interface : BObject,IPrimaryKey<int> 
    {
        public Interface() { }
        public Interface(CTableInterface t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = "";
		
		/// <summary>
		/// Наименование
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _printerA4 = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string PrinterA4
        {
            get { return _printerA4; }
            set {
                    if (Compare(_printerA4, value) != 0)
                    {
                        SetDirtyFlag();
                        _printerA4 = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _printerLabel = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string PrinterLabel
        {
            get { return _printerLabel; }
            set {
                    if (Compare(_printerLabel, value) != 0)
                    {
                        SetDirtyFlag();
                        _printerLabel = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private bool _useBags = false;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public bool UseBags
        {
            get { return _useBags; }
            set {
                    if (Compare(_useBags, value) != 0)
                    {
                        SetDirtyFlag();
                        _useBags = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private bool _useSkleiki = false;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public bool UseSkleiki
        {
            get { return _useSkleiki; }
            set {
                    if (Compare(_useSkleiki, value) != 0)
                    {
                        SetDirtyFlag();
                        _useSkleiki = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private bool _useGroups = false;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public bool UseGroups
        {
            get { return _useGroups; }
            set {
                    if (Compare(_useGroups, value) != 0)
                    {
                        SetDirtyFlag();
                        _useGroups = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private bool _useBrutto = false;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public bool UseBrutto
        {
            get { return _useBrutto; }
            set {
                    if (Compare(_useBrutto, value) != 0)
                    {
                        SetDirtyFlag();
                        _useBrutto = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
