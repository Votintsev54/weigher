using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableReportWords: CTable< ReportWords, CDatabaseWeigher,int>
  {
  	CTableReportWords()
	{}
	        public CTableReportWords(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(ReportWords), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["Name"] = new FieldDescription(typeof(ReportWords), "Name", "string", "nvarchar", 128, 0, FieldDescription.fieldProp.Empty);
			_fd["NameTrans"] = new FieldDescription(typeof(ReportWords), "NameTrans", "string", "nvarchar", 128, 0, FieldDescription.fieldProp.Empty);
			_fd["LangId"] = new FieldDescription(typeof(ReportWords), "LangId", "string", "nvarchar", 5, 0, FieldDescription.fieldProp.Empty);
			_fd["Comment"] = new FieldDescription(typeof(ReportWords), "Comment", "string", "nvarchar", 256, 0, FieldDescription.fieldProp.Nullable );
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// Печатные отчеты
	/// </summary>
	[XmlType("слова и фразы отчетов")]
	public partial class ReportWords : BObject,IPrimaryKey<int> 
    {
        public ReportWords() { }
        public ReportWords(CTableReportWords t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = "";
		
		/// <summary>
		/// Наименование
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _nameTrans = "";
		
		/// <summary>
		/// Наименование
		/// </summary>
		[XmlAttribute]
        public string NameTrans
        {
            get { return _nameTrans; }
            set {
                    if (Compare(_nameTrans, value) != 0)
                    {
                        SetDirtyFlag();
                        _nameTrans = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _langId = "";
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string LangId
        {
            get { return _langId; }
            set {
                    if (Compare(_langId, value) != 0)
                    {
                        SetDirtyFlag();
                        _langId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _comment = null;
		
		/// <summary>
		/// Комментарий
		/// </summary>
		[XmlAttribute]
        public string Comment
        {
            get { return _comment; }
            set {
                    if (Compare(_comment, value) != 0)
                    {
                        SetDirtyFlag();
                        _comment = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
