using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableCustomer: CTable< Customer, CDatabaseWeigher,Guid>
  {
  	CTableCustomer()
	{}
	        public CTableCustomer(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(Customer), "Id", "Guid", "uniqueidentifier", 0, 0, FieldDescription.fieldProp.PrimaryKey );
			_fd["Name"] = new FieldDescription(typeof(Customer), "Name", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["Inn"] = new FieldDescription(typeof(Customer), "Inn", "string", "nvarchar", 12, 0, FieldDescription.fieldProp.Nullable );
			_fd["GroupCustomerId"] = new FieldDescription(typeof(Customer), "GroupCustomerId", "Guid?", "uniqueidentifier", 0, 0, FieldDescription.fieldProp.Nullable  | FieldDescription.fieldProp.ForeignKey );
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// Заказчики
	/// </summary>
	public partial class Customer : BObject,IPrimaryKey<Guid> 
    {
        public Customer() { }
        public Customer(CTableCustomer t)
            : base(t)
        { }

		private Guid _id = Guid.NewGuid();
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public Guid Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _inn = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Inn
        {
            get { return _inn; }
            set {
                    if (Compare(_inn, value) != 0)
                    {
                        SetDirtyFlag();
                        _inn = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private Guid? _groupCustomerId = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public Guid? GroupCustomerId
        {
            get { return _groupCustomerId; }
            set {
                    if (Compare(_groupCustomerId, value) != 0)
                    {
                        SetDirtyFlag();
                        _groupCustomerId = value;
                       _groupCustomerIdObject = null ;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		GroupCustomer _groupCustomerIdObject ;
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public GroupCustomer GroupCustomer
        {
		
            get 
			{ 
				if (_groupCustomerIdObject == null && _groupCustomerId != null)
					_groupCustomerIdObject = (GroupCustomer)((CDatabaseWeigher)_table.parentDataBase).GroupCustomer.GetById(_groupCustomerId);
				return _groupCustomerIdObject ; 
			}
            set 
			{ 
				GroupCustomerId = value?.Id ?? null ;
				_groupCustomerIdObject = value ;
			}
        }
    }

}
	
