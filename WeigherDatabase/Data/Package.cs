using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTablePackage: CTable< Package, CDatabaseWeigher,int>
  {
  	CTablePackage()
	{}
	        public CTablePackage(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(Package), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["OrderId"] = new FieldDescription(typeof(Package), "OrderId", "int", "int", 0, 0, FieldDescription.fieldProp.ForeignKey );
			_fd["PackageNumber"] = new FieldDescription(typeof(Package), "PackageNumber", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["StubCount"] = new FieldDescription(typeof(Package), "StubCount", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["TareWeight"] = new FieldDescription(typeof(Package), "TareWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["Mcarton"] = new FieldDescription(typeof(Package), "Mcarton", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["Mkrishki"] = new FieldDescription(typeof(Package), "Mkrishki", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["Mstretch"] = new FieldDescription(typeof(Package), "Mstretch", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["BruttoWeight"] = new FieldDescription(typeof(Package), "BruttoWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["Barcode"] = new FieldDescription(typeof(Package), "Barcode", "string", "nvarchar", 50, 0, FieldDescription.fieldProp.Empty);
			_fd["Date"] = new FieldDescription(typeof(Package), "Date", "DateTime", "smalldatetime", 0, 0, FieldDescription.fieldProp.Empty);
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// 
	/// </summary>
	[XmlType("Палетта(поддон)")]
	public partial class Package : BObject,IPrimaryKey<int> 
    {
        public Package() { }
        public Package(CTablePackage t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _orderId = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int OrderId
        {
            get { return _orderId; }
            set {
                    if (Compare(_orderId, value) != 0)
                    {
                        SetDirtyFlag();
                        _orderId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public Orders Order
        {
		
            get { return ((CDatabaseWeigher)_table.parentDataBase).Orders.GetById(_orderId); }
            set { this.OrderId = value == null ? 0 : value.Id ; }
        }

		private int _packageNumber = 0;
		
		/// <summary>
		/// Номер палеты(упаковочного листа)
		/// </summary>
		[XmlAttribute]
        public int PackageNumber
        {
            get { return _packageNumber; }
            set {
                    if (Compare(_packageNumber, value) != 0)
                    {
                        SetDirtyFlag();
                        _packageNumber = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _stubCount = 0;
		
		/// <summary>
		/// количество заглушек
		/// </summary>
		[XmlAttribute]
        public int StubCount
        {
            get { return _stubCount; }
            set {
                    if (Compare(_stubCount, value) != 0)
                    {
                        SetDirtyFlag();
                        _stubCount = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _tareWeight = 0;
		
		/// <summary>
		/// Масса поддона
		/// </summary>
		[XmlAttribute]
        public decimal TareWeight
        {
            get { return _tareWeight; }
            set {
                    if (Compare(_tareWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _tareWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _mcarton = 0;
		
		/// <summary>
		/// Масса картона
		/// </summary>
		[XmlAttribute]
        public decimal Mcarton
        {
            get { return _mcarton; }
            set {
                    if (Compare(_mcarton, value) != 0)
                    {
                        SetDirtyFlag();
                        _mcarton = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _mkrishki = 0;
		
		/// <summary>
		/// Масса крышки
		/// </summary>
		[XmlAttribute]
        public decimal Mkrishki
        {
            get { return _mkrishki; }
            set {
                    if (Compare(_mkrishki, value) != 0)
                    {
                        SetDirtyFlag();
                        _mkrishki = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _mstretch = 0;
		
		/// <summary>
		/// Масса стрейча
		/// </summary>
		[XmlAttribute]
        public decimal Mstretch
        {
            get { return _mstretch; }
            set {
                    if (Compare(_mstretch, value) != 0)
                    {
                        SetDirtyFlag();
                        _mstretch = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _bruttoWeight = 0;
		
		/// <summary>
		/// Общий вес
		/// </summary>
		[XmlAttribute]
        public decimal BruttoWeight
        {
            get { return _bruttoWeight; }
            set {
                    if (Compare(_bruttoWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _bruttoWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _barcode = "";
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Barcode
        {
            get { return _barcode; }
            set {
                    if (Compare(_barcode, value) != 0)
                    {
                        SetDirtyFlag();
                        _barcode = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private DateTime _date = DateTime.Now;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public DateTime Date
        {
            get { return _date; }
            set {
                    if (Compare(_date, value) != 0)
                    {
                        SetDirtyFlag();
                        _date = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
