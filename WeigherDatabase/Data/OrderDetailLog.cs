using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableOrderDetailLog: CTable< OrderDetailLog, CDatabaseWeigher,int>
  {
  	CTableOrderDetailLog()
	{}
	        public CTableOrderDetailLog(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["DateUpd"] = new FieldDescription(typeof(OrderDetailLog), "DateUpd", "DateTime", "smalldatetime", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["Id"] = new FieldDescription(typeof(OrderDetailLog), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["OrderId"] = new FieldDescription(typeof(OrderDetailLog), "OrderId", "int", "int", 0, 0, FieldDescription.fieldProp.ForeignKey );
			_fd["Specification"] = new FieldDescription(typeof(OrderDetailLog), "Specification", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["Material"] = new FieldDescription(typeof(OrderDetailLog), "Material", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["ProductName"] = new FieldDescription(typeof(OrderDetailLog), "ProductName", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["ProductCharacteristic"] = new FieldDescription(typeof(OrderDetailLog), "ProductCharacteristic", "string", "nvarchar", 500, 0, FieldDescription.fieldProp.Nullable );
			_fd["Shelf"] = new FieldDescription(typeof(OrderDetailLog), "Shelf", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["FrameWeight"] = new FieldDescription(typeof(OrderDetailLog), "FrameWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["PackThick"] = new FieldDescription(typeof(OrderDetailLog), "PackThick", "string", "nvarchar", 20, 0, FieldDescription.fieldProp.Empty);
			_fd["RollWidth"] = new FieldDescription(typeof(OrderDetailLog), "RollWidth", "decimal?", "decimal", 18, 2, FieldDescription.fieldProp.Nullable );
			_fd["Density"] = new FieldDescription(typeof(OrderDetailLog), "Density", "decimal", "decimal", 18, 5, FieldDescription.fieldProp.Empty);
			_fd["Party"] = new FieldDescription(typeof(OrderDetailLog), "Party", "string", "nvarchar", 50, 0, FieldDescription.fieldProp.Nullable );
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// 
	/// </summary>
	public partial class OrderDetailLog : BObject,IPrimaryKey<int> 
    {
        public OrderDetailLog() { }
        public OrderDetailLog(CTableOrderDetailLog t)
            : base(t)
        { }

		private DateTime _dateUpd = DateTime.Now;
		
		/// <summary>
		/// Дата изменения
		/// </summary>
		[XmlAttribute]
        public DateTime DateUpd
        {
            get { return _dateUpd; }
            set {
                    if (Compare(_dateUpd, value) != 0)
                    {
                        SetDirtyFlag();
                        _dateUpd = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _orderId = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int OrderId
        {
            get { return _orderId; }
            set {
                    if (Compare(_orderId, value) != 0)
                    {
                        SetDirtyFlag();
                        _orderId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public Orders Orders
        {
		
            get { return ((CDatabaseWeigher)_table.parentDataBase).Orders.GetById(_orderId); }
            set { this.OrderId = value == null ? 0 : value.Id ; }
        }

		private string _specification = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Specification
        {
            get { return _specification; }
            set {
                    if (Compare(_specification, value) != 0)
                    {
                        SetDirtyFlag();
                        _specification = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _material = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string Material
        {
            get { return _material; }
            set {
                    if (Compare(_material, value) != 0)
                    {
                        SetDirtyFlag();
                        _material = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _productName = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string ProductName
        {
            get { return _productName; }
            set {
                    if (Compare(_productName, value) != 0)
                    {
                        SetDirtyFlag();
                        _productName = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _productCharacteristic = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string ProductCharacteristic
        {
            get { return _productCharacteristic; }
            set {
                    if (Compare(_productCharacteristic, value) != 0)
                    {
                        SetDirtyFlag();
                        _productCharacteristic = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _shelf = 6;
		
		/// <summary>
		/// Срок хранения, мес
		/// </summary>
		[XmlAttribute]
        public int Shelf
        {
            get { return _shelf; }
            set {
                    if (Compare(_shelf, value) != 0)
                    {
                        SetDirtyFlag();
                        _shelf = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _frameWeight = 0m;
		
		/// <summary>
		/// Вес кадра
		/// </summary>
		[XmlAttribute]
        public decimal FrameWeight
        {
            get { return _frameWeight; }
            set {
                    if (Compare(_frameWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _frameWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _packThick = "5 PP";
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string PackThick
        {
            get { return _packThick; }
            set {
                    if (Compare(_packThick, value) != 0)
                    {
                        SetDirtyFlag();
                        _packThick = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal? _rollWidth = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public decimal? RollWidth
        {
            get { return _rollWidth; }
            set {
                    if (Compare(_rollWidth, value) != 0)
                    {
                        SetDirtyFlag();
                        _rollWidth = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _density = 0;
		
		/// <summary>
		/// Плотность упаковки
		/// </summary>
		[XmlAttribute]
        public decimal Density
        {
            get { return _density; }
            set {
                    if (Compare(_density, value) != 0)
                    {
                        SetDirtyFlag();
                        _density = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _party = null;
		
		/// <summary>
		/// № партии
		/// </summary>
		[XmlAttribute]
        public string Party
        {
            get { return _party; }
            set {
                    if (Compare(_party, value) != 0)
                    {
                        SetDirtyFlag();
                        _party = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
