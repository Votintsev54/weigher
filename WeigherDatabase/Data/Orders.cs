using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableOrders: CTable< Orders, CDatabaseWeigher,int>
  {
  	CTableOrders()
	{}
	        public CTableOrders(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(Orders), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["Name"] = new FieldDescription(typeof(Orders), "Name", "string", "nvarchar", 1000, 0, FieldDescription.fieldProp.Empty);
			_fd["Number"] = new FieldDescription(typeof(Orders), "Number", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["DateStart"] = new FieldDescription(typeof(Orders), "DateStart", "DateTime", "smalldatetime", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["DateFinish"] = new FieldDescription(typeof(Orders), "DateFinish", "DateTime?", "smalldatetime", 0, 0, FieldDescription.fieldProp.Nullable );
			_fd["PackerId"] = new FieldDescription(typeof(Orders), "PackerId", "int", "int", 0, 0, FieldDescription.fieldProp.ForeignKey );
			_fd["InterfaceId"] = new FieldDescription(typeof(Orders), "InterfaceId", "int?", "int", 0, 0, FieldDescription.fieldProp.Nullable  | FieldDescription.fieldProp.ForeignKey );
			_fd["NameFts"] = new FieldDescription(typeof(Orders), "NameFts", "string", "nvarchar", 1000, 0, FieldDescription.fieldProp.Empty);
			_fd["CustNumber"] = new FieldDescription(typeof(Orders), "CustNumber", "string", "nvarchar", 50, 0, FieldDescription.fieldProp.Nullable );
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// 
	/// </summary>
	public partial class Orders : BObject,IPrimaryKey<int> 
    {
        public Orders() { }
        public Orders(CTableOrders t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = "";
		
		/// <summary>
		/// Наименование
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _number = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int Number
        {
            get { return _number; }
            set {
                    if (Compare(_number, value) != 0)
                    {
                        SetDirtyFlag();
                        _number = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private DateTime _dateStart = DateTime.Now;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public DateTime DateStart
        {
            get { return _dateStart; }
            set {
                    if (Compare(_dateStart, value) != 0)
                    {
                        SetDirtyFlag();
                        _dateStart = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private DateTime? _dateFinish = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public DateTime? DateFinish
        {
            get { return _dateFinish; }
            set {
                    if (Compare(_dateFinish, value) != 0)
                    {
                        SetDirtyFlag();
                        _dateFinish = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _packerId = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int PackerId
        {
            get { return _packerId; }
            set {
                    if (Compare(_packerId, value) != 0)
                    {
                        SetDirtyFlag();
                        _packerId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public Packer Packer
        {
		
            get { return ((CDatabaseWeigher)_table.parentDataBase).Packer.GetById(_packerId); }
            set { this.PackerId = value == null ? 0 : value.Id ; }
        }

		private int? _interfaceId = null;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int? InterfaceId
        {
            get { return _interfaceId; }
            set {
                    if (Compare(_interfaceId, value) != 0)
                    {
                        SetDirtyFlag();
                        _interfaceId = value;
                       _interfaceIdObject = null ;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		Interface _interfaceIdObject ;
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public Interface Interface
        {
		
            get 
			{ 
				if (_interfaceIdObject == null && _interfaceId != null)
					_interfaceIdObject = (Interface)((CDatabaseWeigher)_table.parentDataBase).Interface.GetById(_interfaceId);
				return _interfaceIdObject ; 
			}
            set 
			{ 
				InterfaceId = value?.Id ?? null ;
				_interfaceIdObject = value ;
			}
        }

		private string _nameFts = "";
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string NameFts
        {
            get { return _nameFts; }
            set {
                    if (Compare(_nameFts, value) != 0)
                    {
                        SetDirtyFlag();
                        _nameFts = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _custNumber = null;
		
		/// <summary>
		/// Внутренний номер заказчика
		/// </summary>
		[XmlAttribute]
        public string CustNumber
        {
            get { return _custNumber; }
            set {
                    if (Compare(_custNumber, value) != 0)
                    {
                        SetDirtyFlag();
                        _custNumber = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
	
        private SortList< OrderDetail > _orderdetails = null;
        
        [XmlArray]
        public SortList< OrderDetail > OrderDetails
        {
            get
            {
                if (_orderdetails != null) return _orderdetails;
				if (_table != null)
				{
					var thisid = this.Id ;
					_orderdetails = ((CDatabaseWeigher)_table.parentDataBase). OrderDetail .GetObjectListByExpr(obj => obj.OrderId == thisid);
				}
				else
					_orderdetails = new SortList< OrderDetail >(_table);
				return _orderdetails;
            }
        }

	
        private SortList< Package > _packages = null;
        
        [XmlArray]
        public SortList< Package > Packages
        {
            get
            {
                if (_packages != null) return _packages;
				if (_table != null)
				{
					var thisid = this.Id ;
					_packages = ((CDatabaseWeigher)_table.parentDataBase). Package .GetObjectListByExpr(obj => obj.OrderId == thisid);
				}
				else
					_packages = new SortList< Package >(_table);
				return _packages;
            }
        }


        public override object DeepClone(int deep = 99)
        {
            var t = (Orders)Clone();
			if (deep > 0)
             t._orderdetails = OrderDetails.CloneAll(deep-1);             
             t._packages = Packages.CloneAll(deep-1);             
          return t;
        }


		[XmlIgnore]
        public override BObject.ObjectStateType ObjectState
        {
            get
            {
                if (_objectState != BObject.ObjectStateType.Unchanged)
                    return _objectState;
				BObject.ObjectStateType list_state = BObject.ObjectStateType.Unchanged;	
              
				if (_orderdetails != null)
				{  
					list_state = this.OrderDetails.ObjectListState();
					if (list_state != BObject.ObjectStateType.Unchanged)
						return list_state;
				}
              
				if (_packages != null)
				{  
					list_state = this.Packages.ObjectListState();
					if (list_state != BObject.ObjectStateType.Unchanged)
						return list_state;
				}
					
				return list_state;
            }
		}

			
        public override void Save2()
			
        {
            ObjectStateType state = this.ObjectState;
            if (state == ObjectStateType.Unchanged)
            {
                return;
            }
			
                
            if (state == ObjectStateType.Deleted)
            {
                // удаление каскадное !
                base.Save2();
                return;
            } 
			
			
            base.Save2();
			
	
            foreach (OrderDetail obj in this.OrderDetails)
                obj.OrderId = this.Id;
            this.OrderDetails.Save();
	
            foreach (Package obj in this.Packages)
                obj.OrderId = this.Id;
            this.Packages.Save();
			
			

            return;
        }
			

        public override void Rollback()
        {
            base.Rollback();
			
	
            this.OrderDetails.Rollback();
	
            this.Packages.Rollback();
        }
	
    }

}
	
