using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableRoll: CTable< Roll, CDatabaseWeigher,int>
  {
  	CTableRoll()
	{}
	        public CTableRoll(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(Roll), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["OrderDetailId"] = new FieldDescription(typeof(Roll), "OrderDetailId", "int", "int", 0, 0, FieldDescription.fieldProp.ForeignKey );
			_fd["PackageId"] = new FieldDescription(typeof(Roll), "PackageId", "int", "int", 0, 0, FieldDescription.fieldProp.ForeignKey );
			_fd["RollNumber"] = new FieldDescription(typeof(Roll), "RollNumber", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["FrameCount"] = new FieldDescription(typeof(Roll), "FrameCount", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["RollWeight"] = new FieldDescription(typeof(Roll), "RollWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["RollBarcode"] = new FieldDescription(typeof(Roll), "RollBarcode", "string", "nvarchar", 50, 0, FieldDescription.fieldProp.Empty);
			_fd["VtulkaWeight"] = new FieldDescription(typeof(Roll), "VtulkaWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["BruttoWeight"] = new FieldDescription(typeof(Roll), "BruttoWeight", "decimal", "decimal", 18, 3, FieldDescription.fieldProp.Empty);
			_fd["RollCount"] = new FieldDescription(typeof(Roll), "RollCount", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["Skleek"] = new FieldDescription(typeof(Roll), "Skleek", "int", "int", 0, 0, FieldDescription.fieldProp.Empty);
			_fd["acc1c"] = new FieldDescription(typeof(Roll), "acc1c", "DateTime?", "smalldatetime", 0, 0, FieldDescription.fieldProp.Nullable );
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// 
	/// </summary>
	public partial class Roll : BObject,IPrimaryKey<int> 
    {
        public Roll() { }
        public Roll(CTableRoll t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _orderDetailId = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int OrderDetailId
        {
            get { return _orderDetailId; }
            set {
                    if (Compare(_orderDetailId, value) != 0)
                    {
                        SetDirtyFlag();
                        _orderDetailId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public OrderDetail OrdersDetail
        {
		
            get { return ((CDatabaseWeigher)_table.parentDataBase).OrderDetail.GetById(_orderDetailId); }
            set { this.OrderDetailId = value == null ? 0 : value.Id ; }
        }

		private int _packageId = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int PackageId
        {
            get { return _packageId; }
            set {
                    if (Compare(_packageId, value) != 0)
                    {
                        SetDirtyFlag();
                        _packageId = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
		/// <summary>
		/// ссылка на внешний объект(только для случая когда 1 первичный ключ)
		/// </summary>
		[XmlIgnore]
        public Package Package
        {
		
            get { return ((CDatabaseWeigher)_table.parentDataBase).Package.GetById(_packageId); }
            set { this.PackageId = value == null ? 0 : value.Id ; }
        }

		private int _rollNumber = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public int RollNumber
        {
            get { return _rollNumber; }
            set {
                    if (Compare(_rollNumber, value) != 0)
                    {
                        SetDirtyFlag();
                        _rollNumber = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _frameCount = 0;
		
		/// <summary>
		/// Количество кадров
		/// </summary>
		[XmlAttribute]
        public int FrameCount
        {
            get { return _frameCount; }
            set {
                    if (Compare(_frameCount, value) != 0)
                    {
                        SetDirtyFlag();
                        _frameCount = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _rollWeight = 0;
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public decimal RollWeight
        {
            get { return _rollWeight; }
            set {
                    if (Compare(_rollWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _rollWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _rollBarcode = "";
		
		/// <summary>
		/// 
		/// </summary>
		[XmlAttribute]
        public string RollBarcode
        {
            get { return _rollBarcode; }
            set {
                    if (Compare(_rollBarcode, value) != 0)
                    {
                        SetDirtyFlag();
                        _rollBarcode = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _vtulkaWeight = 0m;
		
		/// <summary>
		/// Вес втулки(втулок)
		/// </summary>
		[XmlAttribute]
        public decimal VtulkaWeight
        {
            get { return _vtulkaWeight; }
            set {
                    if (Compare(_vtulkaWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _vtulkaWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private decimal _bruttoWeight = 0m;
		
		/// <summary>
		/// Вес брутто
		/// </summary>
		[XmlAttribute]
        public decimal BruttoWeight
        {
            get { return _bruttoWeight; }
            set {
                    if (Compare(_bruttoWeight, value) != 0)
                    {
                        SetDirtyFlag();
                        _bruttoWeight = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _rollCount = 0;
		
		/// <summary>
		/// Количество ролов в сумке
		/// </summary>
		[XmlAttribute]
        public int RollCount
        {
            get { return _rollCount; }
            set {
                    if (Compare(_rollCount, value) != 0)
                    {
                        SetDirtyFlag();
                        _rollCount = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private int _skleek = 0;
		
		/// <summary>
		/// Количество склеек
		/// </summary>
		[XmlAttribute]
        public int Skleek
        {
            get { return _skleek; }
            set {
                    if (Compare(_skleek, value) != 0)
                    {
                        SetDirtyFlag();
                        _skleek = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private DateTime? _acc1c = null;
		
		/// <summary>
		/// Поле для 1с, индикатор что данные считаны
		/// </summary>
		[XmlAttribute]
        public DateTime? acc1c
        {
            get { return _acc1c; }
            set {
                    if (Compare(_acc1c, value) != 0)
                    {
                        SetDirtyFlag();
                        _acc1c = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
