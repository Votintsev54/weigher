using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DbCommon;
using System.Drawing;
using System.Xml.Serialization;


namespace WeigherData{
  public partial class CTableColumnsMap: CTable< ColumnsMap, CDatabaseWeigher,int>
  {
  	CTableColumnsMap()
	{}
	        public CTableColumnsMap(CDatabaseWeigher db)
            : base(db)
        { 
			IsChangesLog = false;
			_fd["Id"] = new FieldDescription(typeof(ColumnsMap), "Id", "int", "int", 0, 0, FieldDescription.fieldProp.Identity | FieldDescription.fieldProp.PrimaryKey );
			_fd["Name"] = new FieldDescription(typeof(ColumnsMap), "Name", "string", "nvarchar", 128, 0, FieldDescription.fieldProp.Empty);
			_fd["ExcelName"] = new FieldDescription(typeof(ColumnsMap), "ExcelName", "string", "nvarchar", 256, 0, FieldDescription.fieldProp.Empty);
        }		


				
		public override CasheType GetCashType()
        {
            return CasheType.MemCash;
        }
				
	}
	

	/// <summary>
	/// Карта колонок
	/// </summary>
	public partial class ColumnsMap : BObject,IPrimaryKey<int> 
    {
        public ColumnsMap() { }
        public ColumnsMap(CTableColumnsMap t)
            : base(t)
        { }

		private int _id = 0;
		
		/// <summary>
		/// Первичный ключ, автонумератор
		/// </summary>
		[XmlAttribute]
        public int Id
        {
            get { return _id; }
            set {
                    if (Compare(_id, value) != 0)
                    {
                        SetDirtyFlag();
                        _id = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _name = "";
		
		/// <summary>
		/// Наименование
		/// </summary>
		[XmlAttribute]
        public string Name
        {
            get { return _name; }
            set {
                    if (Compare(_name, value) != 0)
                    {
                        SetDirtyFlag();
                        _name = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }

		private string _excelName = "";
		
		/// <summary>
		/// наименование в ексель
		/// </summary>
		[XmlAttribute]
        public string ExcelName
        {
            get { return _excelName; }
            set {
                    if (Compare(_excelName, value) != 0)
                    {
                        SetDirtyFlag();
                        _excelName = value;
                        OnPropertyDataChanged(null);
				        OnPropertyChanged();
                    }
                }
          }
    }

}
	
