﻿using System;
using System.IO;
using System.Xml.Serialization;
using DbCommon;
using DbCommon.Helpers;
using QRCoder;

namespace WeigherDatabase
{
    public class QrSettings : ViewModelBase
    {
        public static QrSettings qrSettingsInstance;
        public enum QrStates
        {
            NotPrint,
            PrintWithDelimeter,
            PrintJson
        }

        private int _blockSize = 2;
        private string _delimeter = "[;]";
        private string _terminator = "[.]";
        private QRCodeGenerator.ECCLevel _eccLevel = QRCodeGenerator.ECCLevel.L;
        private QrStates _qrState = QrStates.PrintJson;

        public int BlockSize { get => _blockSize; set => SetProperty(ref _blockSize, value); }
        public string Delimeter { get => _delimeter; set => SetProperty(ref _delimeter, value); }
        public string Terminator { get => _terminator; set => SetProperty(ref _terminator, value); }
        public QRCodeGenerator.ECCLevel ECCLevel { get => _eccLevel; set => SetProperty(ref _eccLevel, value); }
        public QrStates QrState { get => _qrState; set => SetProperty(ref _qrState, value); }

    }
}