﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace WpfWeigher.Common
{
    public class BusyIndicator
    {
        public static readonly DependencyProperty AttachedProperty =
           DependencyProperty.RegisterAttached("Attached", typeof(bool), typeof(BusyIndicator), new FrameworkPropertyMetadata(false, AttachedChanged));

        public static bool GetAttached(DependencyObject element)
        {
            return (bool)element.GetValue(AttachedProperty);
        }

        public static void SetAttached(DependencyObject o, bool value)
        {
            o.SetValue(AttachedProperty, value);
        }

        private static ConcurrentDictionary<UIElement, BusyAdorner> pairs = new ConcurrentDictionary<UIElement, BusyAdorner>();
        private static void AttachedChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            bool value = (bool)e.NewValue;
            if (dependencyObject is FrameworkElement frameworkElement)
            {
                if (value)
                {
                    if (!frameworkElement.IsLoaded)
                        frameworkElement.Loaded += FrameworkElement_Loaded;
                    else
                        AddBusyAdorner(frameworkElement);
                }
                else
                    RemoveAllAdorners(frameworkElement);
            }

        }

        private static void FrameworkElement_Loaded(object sender, RoutedEventArgs e)
        {
            var frameworkElement = (FrameworkElement)sender;
            frameworkElement.Loaded -= FrameworkElement_Loaded;
            AddBusyAdorner(frameworkElement);
        }

        public static void AddBusyAdorner(UIElement adornedElement)
        {
            if (pairs.TryGetValue(adornedElement, out var adornerPrev))
                return;
            var adorner = new BusyAdorner(adornedElement);
            var adornerLayer = AdornerLayer.GetAdornerLayer(adornedElement);
            adornerLayer?.Add(adorner);
            pairs[adornedElement] = adorner;
        }

        public static void RemoveAllAdorners(UIElement adornedElement)
        {
            if (pairs.TryRemove(adornedElement, out var adorner))
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(adornedElement);
                if (adornerLayer != null)
                {
                    adornerLayer.Remove(adorner);
                    adorner.Dispose();
                }
            }

            //var adornerLayer = AdornerLayer.GetAdornerLayer(adornedElement);
            //if (adornerLayer != null)
            //{
            //    var adornerList = adornerLayer.GetAdorners(adornerLayer);
            //    if (adornerList != null)
            //    {
            //        foreach (var adorner in adornerList)
            //        {
            //            adornerLayer.Remove(adorner);
            //        }
            //    }
            //}
        }
    }
}
