﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;

namespace WpfWeigher.Common
{
    public class BusyAdorner : Adorner, IDisposable
    {
        private DispatcherTimer timer = new DispatcherTimer();
        public BusyAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
            timer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private double rotAngle = 0;
        private void Timer_Tick(object sender, EventArgs e)
        {
            rotAngle += 45;
            if (rotAngle >= 360)
                rotAngle -= 360;
            InvalidateVisual();
        }


        public bool Disposed = false;

        protected override void OnRender(DrawingContext dc)
        {
            if (Disposed) return;
            var adornedControl = this.AdornedElement as FrameworkElement;
            if (adornedControl == null) return;

            Rect rect = new Rect(0, 0, adornedControl.ActualWidth, adornedControl.ActualHeight);
            dc.DrawRectangle(new SolidColorBrush(Colors.LightSkyBlue) { Opacity = 0.6 }, null, rect);

            Pen p = new Pen(new SolidColorBrush(Colors.Gray), 1);
            double cntrX = rect.Width / 2;
            double cntrY = rect.Height / 2;

            var tr = new RotateTransform(rotAngle, cntrX, cntrY);

            dc.PushTransform(tr);


            Rect r = new Rect(0, 0, 10, 20);
            r.Offset(cntrX + 30, cntrY - 10);
            int count = 8;
            for (int i = 0; i < count; i++)
            {
                dc.PushTransform(new RotateTransform(i * (360 / count), cntrX, cntrY));
                byte a = (byte)(i * i * 5 + 10);
                SolidColorBrush solidColorBrush = new SolidColorBrush(Color.FromArgb(a, 0, 0, 255));
                dc.DrawRoundedRectangle(solidColorBrush, null, r, 5, 5);
                dc.Pop();
            }

            dc.Pop();
        }

        public void Dispose()
        {
            timer.Stop();
            Disposed = true;
        }
    }
}
