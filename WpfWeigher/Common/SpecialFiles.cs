﻿using System;
using System.IO;

namespace WpfWeigher.Common
{
    public static class SpecialFiles
    {
        // ReSharper disable InconsistentNaming
        public const string SETTINGS_FILE_NAME = "ProximaWeigherSettings.xml";
        public const string SETTINGS_Weigher_FILE_NAME = "WeigherSettings.xml";
        // ReSharper restore InconsistentNaming

        static SpecialFiles()
        {
            if (!Directory.Exists(SettingsFileDirectory))
            {
                Directory.CreateDirectory(SettingsFileDirectory);
            }
        }

        public static readonly string SettingsFileDirectory
            = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), @"Ermatel");
        public static readonly string SettingsFilePath = Path.Combine(SettingsFileDirectory, SETTINGS_FILE_NAME);
    }
}