﻿using WpfWeigher.Models;

namespace WpfWeigher
{
    public interface IParentSqlSettings
    {
        SqlSettings SqlSettings { get; set; }
        void Save();
        void Save(string filePath);
    }
    public interface IParentScaleSettings
    {
        ScaleSettings ScaleSettings { get; set; }
        void Save();
        void Save(string filePath);
    }
}