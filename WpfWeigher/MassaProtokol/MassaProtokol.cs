﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassaProtokol
{
    public class MassaCommand
    {
        protected MassaCommand() { }

        protected byte[] _bytes;
        public MassaCommand(MassaCommandsEnum massaCommandsEnum)
        {
            _bytes = new byte[] { 0xF8, 0x55, 0xCE, 0x01, 0x00, (byte)massaCommandsEnum, 0x00, 0x00 };
            SetCrc();
        }

        public int Length => BitConverter.ToUInt16(_bytes, 3);
        public MassaCommandsEnum Command => (MassaCommandsEnum)_bytes[5];


        public MassaCommand(byte[] bytes)
        {
            _bytes = bytes;
            TestCrc();
        }

        private void TestCrc()
        {
            if (_bytes.Length > 5)
            {
                int len = BitConverter.ToUInt16(_bytes,3);
                bool valid = len + 7 == _bytes.Length;
                if (valid)
                {
                    valid = _bytes[0] == 0xF8 && _bytes[1] == 0x55 && _bytes[2] == 0xCE;
                    ushort calcCrc = GetCrc();
                    var crcIndex = _bytes.Length - 2;
                    short crc = BitConverter.ToInt16(_bytes, crcIndex);
                    CrcOk = calcCrc == crc;
                    Valid = valid;
                }
            }
        }
        public bool Valid { get; set; } = false;
        public bool CrcOk { get; set; } = false;

        private ushort Crc16_(byte[] buf, int Start, int len)
        {
            //ushort testcrc2 = CRC16(new byte[] { 0x24, 0x14, 0, 0, 0, 2, 1, 0, 0, 0, 0, 0, 0 }, 0, 13);////8FE3
            ushort crc = 0;
            for (var i = 0; i < len; i++)
            {
                ushort a = 0;
                ushort num4 = (ushort)(crc & 0xFF00);
                for (var k = 0; k < 8; k++)
                {
                    a = (((num4 ^ a) & 0x8000) == 0) ? ((ushort)(a << 1)) : ((ushort)((a << 1) ^ 0x1021));
                    num4 = (ushort)(num4 << 1);
                }
                crc = (ushort)(a ^ (crc << 8) ^ buf[i + Start]);
            }
            return crc;
        }
        private ushort Crc16(IEnumerable< byte> buf)
        {
            ushort crc = 0;
            foreach (var data in buf)
            {
                ushort a = 0;
                ushort num4 = (ushort)(crc & 0xFF00);
                for (var k = 0; k < 8; k++)
                {
                    a = (((num4 ^ a) & 0x8000) == 0) ? ((ushort)(a << 1)) : ((ushort)((a << 1) ^ 0x1021));
                    num4 = (ushort)(num4 << 1);
                }
                crc = (ushort)(a ^ (crc << 8) ^ data);
            }
            return crc;
        }

        protected void SetCrc()
        {
            ushort crc = GetCrc();
            var crcBytes = BitConverter.GetBytes(crc);
            var crcIndex = _bytes.Length - 2;
            _bytes[crcIndex] = crcBytes[0];
            _bytes[crcIndex + 1] = crcBytes[1];
        }

        private ushort GetCrc()
        {
            return Crc16(_bytes.Skip(5).Take(Length));
        }

        public byte[] GetBytes() => _bytes;

    }
}
