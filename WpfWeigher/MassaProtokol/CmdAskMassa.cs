﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassaProtokol
{
    public class CmdAskMassa : MassaCommand
    {

        public CmdAskMassa(byte[] bytes)
            : base(bytes)
        {
            Valid &= Command == MassaCommandsEnum.CMD_ACK_MASSA && (Length == 9 || Length == 13);
            ExtractParams();
        }

        private void ExtractParams()
        {
            Weight = BitConverter.ToInt32(_bytes, 6);
            if (Length == 13)
                Tare = BitConverter.ToInt32(_bytes, 14);
        }

        public int Weight;
        public byte Division => _bytes[10];
        public byte Stable => _bytes[11];
        public byte Net => _bytes[12];
        public byte Zero => _bytes[13];
        public int Tare;
    }
}
