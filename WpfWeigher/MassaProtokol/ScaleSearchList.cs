﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;


namespace MassaProtokol
{
    public class ScaleSearchList
    {
        private class TCPp
        {
            public IPAddress ip;
            public string ComPort = "";
            public int port;
            public TcpClient tcp;
            public byte[] buff;
            public int LastCommand = -1;
            public int LastAnsver = -1;
            public ScaleData mScaleData;

            public TCPp(IPAddress ip1, int _port, TcpClient ip2)
            {
                ip = ip1;
                tcp = ip2;
                port = _port;
                buff = new byte[50];
                LastCommand = -1;
                mScaleData = new ScaleData();
                ComPort = "";
            }

            public TCPp(string _port)
            {
                ComPort = _port;
                buff = new byte[50];
                LastCommand = -1;
                mScaleData = new ScaleData();
            }
        }

        public class UdpState
        {
            public IPEndPoint e;

            public UdpClient u;
        }

        private class IPp
        {
            public IPAddress ip = IPAddress.None;

            public IPAddress cur_ip = IPAddress.None;

            public IPp(IPAddress ip1, IPAddress ip2, int _id)
            {
                ip = ip1;
                cur_ip = ip2;
            }
        }

        public List<string> log_str = new List<string>();

        public List<string> logIP_str = new List<string>();

        private List<TCPp> ip_datalist = new List<TCPp>();

        private int IPPort = 5001;

        private List<ScaleData> mListScale = new List<ScaleData>();

        private bool _ComSearchStatus = true;

        private byte[] commandA0 = new byte[8] { 248, 85, 206, 1, 0, 160, 160, 0 };
        private byte[] command00 = new byte[8] { 248, 85, 206, 1, 0, 0, 0, 0 };
        private byte[] command90 = new byte[8] { 248, 85, 206, 1, 0, 144, 144, 0 };

        private int len;

        private UdpClient udp;

        public void StoreLog()
        {
        }

        private bool DecodeIP(TCPp _scale)
        {
            _scale.LastAnsver = _scale.buff[5];
            switch (_scale.buff[5])
            {
                case 1:
                    _scale.mScaleData.IDN = $"{_scale.buff[8]:X2}{_scale.buff[9]:X2}{_scale.buff[10]:X2}";
                    _scale.mScaleData.SN = BitConverter.ToInt32(_scale.buff, 11).ToString();
                    _scale.mScaleData.IsP100 = ((_scale.buff[15] != 0) ? true : false);
                    _scale.mScaleData.Interrface = _scale.buff[22];
                    return true;
                case 80:
                    _scale.mScaleData.SN = BitConverter.ToInt32(_scale.buff, 6).ToString();
                    _scale.mScaleData.Interrface = 2;
                    return true;
                case 16:
                case 20:
                    _scale.mScaleData.IsScale = true;
                    _scale.mScaleData.Connection = $"{_scale.ip}:{_scale.port}";
                    return true;
                default:
                    return false;
            }
        }

        private bool DecodeCOM(TCPp _scale)
        {
            int num = 0;
            _scale.LastAnsver = _scale.buff[5];
            switch (_scale.buff[5])
            {
                case 1:
                    log_str.Add($"{_scale.ComPort},Receive 01");
                    _scale.mScaleData.IDN = $"{_scale.buff[8]:X2}{_scale.buff[9]:X2}{_scale.buff[10]:X2}";
                    num = _scale.buff[11] + _scale.buff[12] * 256 + _scale.buff[13] * 65536 + _scale.buff[14] * 16777216;
                    _scale.mScaleData.SN = num.ToString();
                    _scale.mScaleData.IsP100 = ((_scale.buff[15] != 0) ? true : false);
                    _scale.mScaleData.Interrface = _scale.buff[22];
                    return true;
                case 80:
                    log_str.Add($"{_scale.ComPort},Receive 50");
                    num = _scale.buff[6] + _scale.buff[7] * 256 + _scale.buff[8] * 65536 + _scale.buff[9] * 16777216;
                    _scale.mScaleData.SN = num.ToString();
                    _scale.mScaleData.Interrface = 0;
                    return true;
                case 16:
                case 20:
                    log_str.Add($"{_scale.ComPort},Receive {_scale.buff[5]}");
                    _scale.mScaleData.IsScale = true;
                    _scale.mScaleData.Connection = _scale.ComPort;
                    return true;
                default:
                    log_str.Add($"Err {_scale.ComPort},Receive {_scale.buff[5]}");
                    return false;
            }
        }

        private void pingSender_Complete(object sender, PingCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                ((AutoResetEvent)e.UserState).Set();
                return;
            }
            if (e.Error != null)
            {
                ((AutoResetEvent)e.UserState).Set();
                return;
            }
            PingReply reply = e.Reply;
            IPAddress ip = ((IPp)e.UserState).ip;
            IPAddress cur_ip = ((IPp)e.UserState).cur_ip;
            if (reply.Status == IPStatus.Success && cur_ip != ip)
            {
                TcpClient tcpClient = new TcpClient(AddressFamily.InterNetwork);
                tcpClient.ReceiveTimeout = 1000;
                TCPp state = new TCPp(ip, IPPort, tcpClient);
                tcpClient.BeginConnect(ip, IPPort, ConnectCallback, state);
                logIP_str.Add($"Send Connect: {ip},{cur_ip},{reply.Address},{reply.Status}");
            }
        }

        public void UdpReceiveCallback(IAsyncResult ar)
        {
            UdpClient u = ((UdpState)ar.AsyncState).u;
            IPEndPoint remoteEP = ((UdpState)ar.AsyncState).e;
            try
            {
                u.EndReceive(ar, ref remoteEP);
            }
            catch
            {
                return;
            }
            TcpClient tcpClient = new TcpClient(AddressFamily.InterNetwork);
            logIP_str.Add("Find UDP IP " + remoteEP.Address.ToString() + " Send Connect");
            tcpClient.ReceiveTimeout = 1000;
            TCPp state = new TCPp(remoteEP.Address, IPPort, tcpClient);
            tcpClient.BeginConnect(remoteEP.Address, IPPort, ConnectCallback, state);
        }

        private void Receive_Callback(IAsyncResult ar)
        {
            log_str.Add("R");
            try
            {
                TCPp tCPp = (TCPp)ar.AsyncState;
                if (tCPp != null)
                {
                    log_str.Add(string.Format("{0},{1}", tCPp.ip.ToString(), "Обработка приема"));
                    if (tCPp.tcp.Connected)
                    {
                        int num = tCPp.tcp.GetStream().EndRead(ar);
                        log_str.Add($"{tCPp.ip.ToString()},Прочитано {num}");
                        if (num > 0)
                        {
                            log_str.Add(string.Format("{0},{1}", tCPp.ip.ToString(), "Begin Decode"));
                            if (DecodeIP(tCPp))
                            {
                                log_str.Add(string.Format("{0},{1},Command=0x{2:x},Ansver=0x{3:x}", tCPp.ip.ToString(), "Decode ok", tCPp.LastCommand, tCPp.LastAnsver));
                            }
                            switch (tCPp.LastCommand)
                            {
                                case 0:
                                    log_str.Add(string.Format("{0},{1}", tCPp.ip.ToString(), "Dicsonnect"));
                                    break;
                                case 144:
                                    tCPp.LastCommand = 0;
                                    log_str.Add(string.Format("{0},{1}", tCPp.ip.ToString(), "BeginSend 0x00"));
                                    tCPp.tcp.GetStream().BeginWrite(command00, 0, command00.Length, Send_Callback, tCPp);
                                    break;
                                case 160:
                                    tCPp.LastCommand = 144;
                                    log_str.Add(string.Format("{0},{1}", tCPp.ip.ToString(), "BeginSend 0x90"));
                                    tCPp.tcp.GetStream().BeginWrite(command90, 0, command90.Length, Send_Callback, tCPp);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        log_str.Add(string.Format("{0},{1}", tCPp.ip.ToString(), "Dicsonnect no read error"));
                    }
                }
                else
                {
                    log_str.Add("Error = 3");
                }
            }
            catch (Exception)
            {
                log_str.Add("Error = 2");
            }
        }

        private void Send_Callback(IAsyncResult ar)
        {
            try
            {
                TCPp tCPp = (TCPp)ar.AsyncState;
                if (tCPp != null)
                {
                    tCPp.tcp.GetStream().EndWrite(ar);
                    int num;
                    switch (tCPp.LastCommand)
                    {
                        case 160:
                            num = 14;
                            break;
                        case 144:
                            num = 12;
                            break;
                        default:
                            num = 34;
                            break;
                    }
                    log_str.Add(string.Format("{0},{1},Command=0x{2:x},ReadLen={3}", tCPp.ip.ToString(), "BeginReceave", tCPp.LastCommand, num));
                    tCPp.tcp.GetStream().ReadTimeout = 500;
                    tCPp.tcp.GetStream().BeginRead(tCPp.buff, 0, num, Receive_Callback, tCPp);
                }
            }
            catch (Exception)
            {
                log_str.Add("Error = 4");
            }
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                TCPp tCPp = (TCPp)ar.AsyncState;
                if (tCPp != null)
                {
                    if (tCPp.tcp.Connected)
                    {
                        tCPp.tcp.NoDelay = false;
                        tCPp.LastCommand = 160;
                        tCPp.tcp.GetStream().ReadTimeout = 100;
                        lock (ip_datalist)
                        {
                            ip_datalist.Add(tCPp);
                        }
                        log_str.Add(string.Format("{0},{1}", tCPp.ip.ToString(), "Connect, BeginSend 0xa0"));
                        tCPp.tcp.GetStream().BeginWrite(commandA0, 0, commandA0.Length, Send_Callback, tCPp);
                        logIP_str.Add("Connect  OK: " + tCPp.ip.ToString());
                    }
                    else
                    {
                        tCPp.tcp.Close();
                    }
                }
            }
            catch (Exception)
            {
                log_str.Add("Error = 5");
            }
        }

        public List<ScaleData> GetScales(int port, bool IsIP, bool IsCOM)
        {
            List<ScaleData> list = new List<ScaleData>();
            if (!IsCOM && !IsIP)
            {
                return list;
            }
            ip_datalist.Clear();
            log_str.Clear();
            IPPort = port;
            GetListIP(IsIP, IsCOM);
            for (int i = 0; i < ip_datalist.Count; i++)
            {
                list.Add(ip_datalist[i].mScaleData);
            }
            return list;
        }

        public List<ScaleData> GetScales()
        {
            return GetScales(5001, IsIP: true, IsCOM: true);
        }

        public List<ScaleData> GetScales(int port)
        {
            return GetScales(port, IsIP: true, IsCOM: true);
        }

        public List<ScaleData> GetScales(bool IsCom)
        {
            return GetScales(5001, !IsCom, IsCom);
        }

        private void GetListIP(bool IsIP, bool IsCOM)
        {
            Thread ThreadProcessCOM = null;
            Thread ThreadProcessIP = IsIP ? new Thread(StartListIP) { IsBackground = true } : null;
            Thread ThreadProcessUDP = IsIP ? new Thread(StartListUDP) { IsBackground = true } : null;
            _ComSearchStatus = true;
            if (IsCOM)
            {
                _ComSearchStatus = false;
                ThreadProcessCOM = new Thread(StartListCOM) { IsBackground = true };
            }
            ThreadProcessIP?.Start();
            ThreadProcessUDP?.Start();
            ThreadProcessCOM?.Start();
            Thread.Sleep(3000);
            int num = 0;
            while (!_ComSearchStatus && num++ < 15)
            {
                Thread.Sleep(1000);
            }
            if (IsIP)
            {
                foreach (var ip_data in ip_datalist)
                {
                    if (ip_data.tcp != null)
                    {
                        log_str.Add(string.Format("{0},{1}", ip_data.tcp.Client.LocalEndPoint.ToString(), "Dicsonnect"));
                        ip_data.tcp.Close();
                    }
                }
            }
        }

        private bool TestCom(string comport)
        {
            return TestCom(comport, 57600, Parity.None);
        }

        private bool TestCom(string comport, int boude, Parity _par)
        {
            SerialPort serialPort = new SerialPort(comport, boude, _par);
            try
            {
                serialPort.Open();
            }
            catch
            {
                return false;
            }
            if (serialPort.IsOpen)
            {
                TCPp tCPp = new TCPp(comport);
                serialPort.WriteTimeout = 300;
                serialPort.ReadTimeout = 300;
                int num = 0;
                int num2 = 0;
                do
                {
                    byte[] array;
                    switch (num)
                    {
                        case 0:
                            array = commandA0;
                            num2 = 14;
                            tCPp.LastCommand = 160;
                            log_str.Add($"{comport}:{boude},Send A0");
                            break;
                        case 1:
                            array = command90;
                            tCPp.LastCommand = 144;
                            num2 = 12;
                            log_str.Add($"{comport}:{boude}, Send 90");
                            break;
                        case 2:
                            array = command00;
                            tCPp.LastCommand = 0;
                            num2 = 34;
                            lock (ip_datalist)
                            {
                                tCPp.mScaleData.Is57600 = ((boude == 57600) ? true : false);
                                ip_datalist.Add(tCPp);
                                log_str.Add($"{comport}:{boude},Send 00");
                            }
                            break;
                        default:
                            array = command00;
                            tCPp.LastCommand = -1;
                            num2 = 34;
                            break;
                    }
                    Thread.Sleep(100);
                    try
                    {
                        serialPort.Write(array, 0, array.Length);
                    }
                    catch
                    {
                        serialPort.Close();
                        return false;
                    }
                    Thread.Sleep(400);
                    try
                    {
                        len = serialPort.Read(tCPp.buff, 0, num2);
                    }
                    catch (TimeoutException)
                    {
                        serialPort.Close();
                        return false;
                    }
                    if (!DecodeCOM(tCPp))
                    {
                        serialPort.Close();
                        return false;
                    }
                    tCPp.buff[5] = 68;
                }
                while (num++ < 2);
                serialPort.Close();
                return true;
            }
            return false;
        }

        private void StartListCOM()
        {
            string[] portNames = SerialPort.GetPortNames();
            for (int i = 0; i < portNames.Length; i++)
            {
                log_str.Add("Start " + portNames[i]);
                if (!TestCom(portNames[i]))
                {
                    TestCom(portNames[i], 4800, Parity.Even);
                }
            }
            _ComSearchStatus = true;
        }

        private void StartListIP()
        {
            string hostName = Dns.GetHostName();
            IPHostEntry hostEntry = Dns.GetHostEntry(hostName);
            IPAddress[] addressList = hostEntry.AddressList;
            List<IPAddress> list = new List<IPAddress>();
            for (int i = 0; i < addressList.Length; i++)
            {
                if (addressList[i].AddressFamily == AddressFamily.InterNetwork && addressList[i].ToString() != "127.0.0.1")
                {
                    list.Add(addressList[i]);
                }
            }
            for (int j = 0; j < list.Count; j++)
            {
                byte[] addressBytes = list[j].GetAddressBytes();
                byte[] array = new byte[4]
                {
                    addressBytes[0],
                    addressBytes[1],
                    addressBytes[2],
                    addressBytes[3]
                };
                for (int k = 1; k < 254; k++)
                {
                    try
                    {
                        IPAddress iPAddress = IPAddress.Parse($"{array[0]}.{array[1]}.{array[2]}.{k}");
                        Ping ping = new Ping();
                        ping.PingCompleted += pingSender_Complete;
                        byte[] bytes = Encoding.ASCII.GetBytes(".............................");
                        PingOptions options = new PingOptions(200, dontFragment: true);
                        IPp userToken = new IPp(iPAddress, list[j], j);
                        ping.SendAsync(iPAddress, 3000, bytes, options, userToken);
                        logIP_str.Add("Ping: " + iPAddress.ToString());
                    }
                    catch
                    {
                    }
                }
            }
        }

        public void StartListUDP()
        {
            int num = 0;
            string hostName = Dns.GetHostName();
            IPHostEntry hostEntry = Dns.GetHostEntry(hostName);
            IPAddress[] addressList = hostEntry.AddressList;
            for (int i = 0; i < addressList.Length; i++)
            {
                if (!addressList[i].IsIPv6LinkLocal)
                {
                    try
                    {
                        udp = new UdpClient();
                        ScalesMassaLocal scalesMassaLocal = new ScalesMassaLocal();
                        addressList[i].ToString().Split('.', ',', ':', ';');
                        IPAddress broadcast = IPAddress.Broadcast;
                        IPEndPoint iPEndPoint = new IPEndPoint(broadcast, IPPort);
                        scalesMassaLocal.CodeData(0, out byte[] DataOut);
                        udp.Send(DataOut, DataOut.Length, iPEndPoint);
                        UdpState udpState = new UdpState();
                        udpState.e = iPEndPoint;
                        udpState.u = udp;
                        logIP_str.Add("START UDP!!!!!");
                        for (num = 0; num < 100; num++)
                        {
                            udp.BeginReceive(UdpReceiveCallback, udpState);
                            Thread.Sleep(20);
                        }
                        udp.Close();
                    }
                    catch
                    {
                    }
                }
            }
        }
    }
}
