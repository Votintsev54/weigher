﻿namespace MassaProtokol
{
	public class ScaleData
	{
		public string Connection { get; set; } = "";

		public string IDN { get; set; } = "";

		public string SN { get; set; } = "";

		public int Interrface { get; set; } = -1;

		public bool IsP100 { get; set; }

		public bool IsScale { get; set; }

		public bool Is57600 { get; set; } = true;
	}
}