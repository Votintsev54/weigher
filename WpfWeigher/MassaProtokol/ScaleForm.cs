﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MassaProtokol
{
    public class ScaleForm
    {
		public event EventHandler OnWeigthChange;
		public event EventHandler OnWeigthError;
		public ScaleInfo mScaleInfo;
        private System.Timers.Timer timer1000mS;
        public CWeight mWeight = new CWeight();
        public ScaleForm(ScaleInfo si)
        {
			mScaleInfo = si;
			InitializeComponent();
        }

        private void InitializeComponent()
        {
            timer1000mS = new System.Timers.Timer(300);
            timer1000mS.Elapsed += timer1000mS_Tick;
            timer1000mS.AutoReset = true;
            timer1000mS.Enabled = true;
        }

        public string lScaleIDText = "";
        private Thread ThreadReadScaleWeight;
        public void ScaleForm_Load()
        {
            lScaleNameText = mScaleInfo.SName;
            lScaleIDText = $"ID: {mScaleInfo.SN}";
            lConnectionText = mScaleInfo.Connection;
            lInterfaceText = "USB";
            if (mScaleInfo.IsP100)
            {
                ThreadReadScaleWeight = new Thread((ThreadStart)delegate
                {
                    ReadScaleWeight_P100(mScaleInfo.Connection);
                });
                ThreadReadScaleWeight.IsBackground = true;
                ThreadReadScaleWeight.Start();
            }
            else
            {
                ThreadReadScaleWeight = new Thread((ThreadStart)delegate
                {
                    ReadScaleWeight_1C(mScaleInfo.Connection);
                });
                ThreadReadScaleWeight.IsBackground = true;
                ThreadReadScaleWeight.Start();
            }
        }

        public string tWeight = "";
        bool lNetVisible = false;
        string lScaleNameText = "";
        string lInterfaceText = "";
        string lConnectionText = "";
        private void timer1000mS_Tick(Object source, System.Timers.ElapsedEventArgs e)
        {
            if (mWeight != null)
            {
                lock (mWeight)
                {
                    try
                    {
                        if (mWeight.Error == 0)
                        {
                            string weight = mWeight.GetWeight();
							if (tWeight != weight)
							{
								tWeight = weight;
								OnWeigthChange?.Invoke(this, null);
							}
                            lNetVisible = mWeight.GetTare();
                        }
                        else
                            tWeight = "Ошибка!";
                    }
                    catch
                    {
                    }
                    if (mWeight.IsNotError)
                    {
                        // все хорошо
                    }
                    else
                    {
						// не все хорошо
						OnWeigthError?.Invoke(this, null);
					}
                }
                if (lScaleNameText != mScaleInfo.SName)
                    lScaleNameText = mScaleInfo.SName;
                if (lInterfaceText != mScaleInfo.SInterface)
                {
                    lInterfaceText = mScaleInfo.SInterface;
                    lConnectionText = mScaleInfo.Connection;
                }
            }
        }

		private void ReadScaleWeight_P100(string sConnectString)
		{
			int num = 0;
			bool flag = false;
			bool flag2 = false;
			string sN = mScaleInfo.SN;
			if (!(sConnectString == ""))
			{
				ScalesMassaLocal scalesMassaLocal = new ScalesMassaLocal();
				try
				{
					while (true)
					{
						flag = true;
						scalesMassaLocal.Connection = sConnectString;
						scalesMassaLocal.ComBoude = mScaleInfo.ComBoude;
						//log.addlog("Connect P100 = " + scalesMassaLocal.ComBoude.ToString());
						if (flag2)
						{
							mScaleInfo.ComBoude = ((mScaleInfo.ComBoude == 4800) ? 57600 : 4800);
						}
						int num2 = scalesMassaLocal.OpenConnection();
						if (num2 != 0)
						{
							scalesMassaLocal.CloseConnection();
							//log.addlog("Connect Error = " + num2.ToString() + "," + num.ToString());
							if (num > 5)
							{
								lock (mWeight)
								{
									mWeight.IsNotError = false;
									Thread.Sleep(2000);
								}
							}
							else
							{
								num++;
								Thread.Sleep(400);
							}
						}
						else
						{
							//log.addlog("Connect Ok");
							Thread.Sleep(200);
							//log.addlog("Get Serial ");
							num2 = scalesMassaLocal.ReadScaleInfo();
							if (num2 != 0)
							{
								//log.addlog("Get Serial Error = " + num2.ToString() + "," + num.ToString());
								if (num > 5)
								{
									lock (mWeight)
									{
										mWeight.IsNotError = false;
									}
									Thread.Sleep(2000);
								}
								else
								{
									num++;
									Thread.Sleep(400);
								}
								scalesMassaLocal.CloseConnection();
								Thread.Sleep(400);
							}
							else
							{
								//log.addlog("Get Serial Ok");
								Thread.Sleep(200);
								//log.addlog("Get Name ");
								num2 = scalesMassaLocal.ReadScaleParameters();
								if (num2 != 0 || sN != scalesMassaLocal.SerialNumber)
								{
									if (num2 == 0)
									{
										//log.addlog("Get Serial Error Change  " + sN + "," + scalesMassaLocal.SerialNumber);
										lock (mWeight)
										{
											mWeight.IsNotError = false;
										}
										sN = scalesMassaLocal.SerialNumber;
									}
									else
									{
										//log.addlog("Get Name Error = " + num2.ToString() + "," + num.ToString());
									}
									scalesMassaLocal.CloseConnection();
									Thread.Sleep(400);
								}
								else
								{
									//log.addlog("Get Name Ok");
									lock (mScaleInfo)
									{
										mScaleInfo.SN = scalesMassaLocal.SerialNumber;
										switch (scalesMassaLocal.ActiveInterface)
										{
											case 0:
												mScaleInfo.SInterface = "RS232";
												flag2 = true;
												break;
											case 1:
												mScaleInfo.SInterface = "USB";
												flag2 = true;
												break;
											case 2:
												mScaleInfo.SInterface = "Ethernet";
												break;
											case 3:
												mScaleInfo.SInterface = "Wi-Fi";
												break;
											default:
												mScaleInfo.SInterface = "___";
												break;
										}
										mScaleInfo.SName = scalesMassaLocal.Name;
									}
									//log.addlog("Get Weight init");
									lock (mWeight)
									{
										mWeight.IsNotError = true;
									}
									do
									{
										//log.addlog("Get Weight ");
										num = 0;
										Thread.Sleep(350);
										num2 = scalesMassaLocal.ReadMassa();
										switch (num2)
										{
											case 0:
												//log.addlog("Get Weight Ok");
												lock (mWeight)
												{
													mWeight.Weight = scalesMassaLocal.Weight;
													mWeight.Division = scalesMassaLocal.Division;
													mWeight.Stable = scalesMassaLocal.Stable;
													mWeight.Tare = scalesMassaLocal.bTare;
													mWeight.Error = 0;
												}
												flag = true;
												break;
											case 108:
											case 109:
												//log.addlog("Get Weight H");
												lock (mWeight)
												{
													mWeight.Error = 1;
													mWeight.Stable = 0;
												}
												flag = true;
												break;
											default:
												if (num2 > 200 && num2 < 300)
												{
													//log.addlog("Get Weight Err" + (num2 - 100).ToString());
													lock (mWeight)
													{
														mWeight.Error = 1;
														mWeight.Stable = 0;
													}
												}
												else
												{
													//log.addlog("Get Weight Error = " + num2.ToString() + "," + num.ToString());
													//log.addlog("Close");
													scalesMassaLocal.CloseConnection();
													Thread.Sleep(1000);
													flag = false;
												}
												break;
										}
									}
									while (flag);
									Thread.Sleep(400);
								}
							}
						}
					}
				}
				catch (ThreadAbortException)
				{
					//log.addlog("EndProcess");
					scalesMassaLocal.CloseConnection();
					//log.addlog("EndProcess Ok");
				}
			}
		}

		private void ReadScaleWeight_1C(string sConnectString)
		{
			int num = 0;
			bool flag = false;
			//Log log = new Log(sConnectString.Split(':')[0], En: true);
			if (!(sConnectString == ""))
			{
				ScalesMassaLocal scalesMassaLocal = new ScalesMassaLocal();
				try
				{
					while (true)
					{
						scalesMassaLocal.Connection = sConnectString;
						if (sConnectString.Contains("COM"))
						{
							//log.addlog("Connect 1C = " + sConnectString + ":" + scalesMassaLocal.ComBoude.ToString());
						}
						else
						{
							//log.addlog("Connect 1C = " + sConnectString);
						}
						int num2 = scalesMassaLocal.OpenConnection();
						if (num2 != 0)
						{
							//log.addlog("Connect Error = " + num2.ToString() + "," + num.ToString());
							scalesMassaLocal.CloseConnection();
							if (num > 4)
							{
								lock (mWeight)
								{
									mWeight.IsNotError = false;
									Thread.Sleep(3000);
								}
							}
							else
							{
								num++;
								Thread.Sleep(400);
							}
						}
						else
						{
							//log.addlog("Connect Ok");
							Thread.Sleep(200);
							num2 = scalesMassaLocal.ReadSerial();
							if (num2 != 0)
							{
								scalesMassaLocal.CloseConnection();
								//log.addlog("Get Serial Error = " + num2.ToString() + "," + num.ToString());
								if (num > 5)
								{
									lock (mWeight)
									{
										mWeight.IsNotError = false;
									}
									Thread.Sleep(2000);
								}
								else
								{
									num++;
									Thread.Sleep(400);
								}
								scalesMassaLocal.CloseConnection();
								Thread.Sleep(400);
							}
							else
							{
								//log.addlog("Get Serial Ok");
								lock (mWeight)
								{
									mWeight.IsNotError = true;
								}
								//log.addlog("Set Parameters");
								lock (mScaleInfo)
								{
									mScaleInfo.SN = scalesMassaLocal.SerialNumber;
									mScaleInfo.SInterface = (scalesMassaLocal.Connection.Contains("COM") ? "RS232 (USB)" : "Ethernet");
								}
								//log.addlog("Get Weight init");
								num = 0;
								flag = true;
								do
								{
									//log.addlog("Get Weight ");
									Thread.Sleep(350);
									num2 = scalesMassaLocal.ReadWeight();
									switch (num2)
									{
										case 0:
											//log.addlog("Get Weight Ok");
											lock (mWeight)
											{
												mWeight.Weight = scalesMassaLocal.Weight;
												mWeight.Division = scalesMassaLocal.Division;
												mWeight.Stable = scalesMassaLocal.Stable;
												mWeight.Error = 0;
											}
											break;
										case 21:
											//log.addlog("Get Weight H");
											lock (mWeight)
											{
												mWeight.Error = 1;
												mWeight.Stable = 0;
											}
											break;
										default:
											//log.addlog("Get Weight Error = " + num2.ToString());
											scalesMassaLocal.CloseConnection();
											Thread.Sleep(1000);
											flag = false;
											break;
									}
								}
								while (flag);
								Thread.Sleep(400);
							}
						}
					}
				}
				catch (ThreadAbortException)
				{
					//log.addlog("EndProcess");
					scalesMassaLocal.CloseConnection();
					//log.addlog("EndProcess Ok");
				}
			}
		}

		public void Close()
		{
			timer1000mS.Stop();
			timer1000mS.Close();
			ThreadReadScaleWeight.Abort();
			ThreadReadScaleWeight.Join();
		}
	}
}
