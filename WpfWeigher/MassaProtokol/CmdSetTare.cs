﻿using System;

namespace MassaProtokol
{
    public class CmdSetTare: MassaCommand
    {

        public CmdSetTare()
//            : base(MassaCommandsEnum.CMD_ACK_SET_TARE)
        {
            _bytes = new byte[] { 0xF8, 0x55, 0xCE, 0x00, 0x05, (byte)MassaCommandsEnum.CMD_ACK_SET_TARE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            SetCrc();
        }
        public CmdSetTare(int tare)
        {
            _bytes = new byte[] { 0xF8, 0x55, 0xCE, 0x00, 0x05, (byte)MassaCommandsEnum.CMD_ACK_SET_TARE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            Array.Copy(BitConverter.GetBytes(tare), 0, _bytes, 6, 4);
            SetCrc();
        }

        public int Tare;
    }
}
