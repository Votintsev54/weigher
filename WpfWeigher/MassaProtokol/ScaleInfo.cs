﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassaProtokol
{
	public class ScaleInfo
	{
		public string Connection = "";

		public int ComBoude = 57600;

		public string SN = "";

		public string SName = "";

		public string SType = "";

		public string RS232_Port = "";

		public string USB_Port = "";

		public bool Ethernet_IsUse;

		public string Ethernet_IP = "";

		public int Ethernet_Port;

		public string Ethernet_Mask = "";

		public string Ethernet_Gate = "";

		public string Ethernet_MAC = "";

		public bool WiFi_IsUse;

		public string WiFi_IP = "";

		public int WiFi_Port;

		public string WiFi_Mask = "";

		public string WiFi_Gate = "";

		public string WiFi_MAC = "";

		public string WiFi_SSID = "";

		public string WiFi_PWD = "";

		public int ActiveInterface = -1;

		public string SInterface = "";

		public bool IsUse;

		public bool IsRegistred;

		public bool IsP100 = true;

		public KeyConfig mKey;

		public ScaleInfo(KeyConfig _key)
		{
			mKey = _key;
		}

		public ScaleInfo()
		{
		}

		public bool LoadFromTCPSearch(ScaleData _ssch)
		{
			if (!_ssch.IsScale)
			{
				return false;
			}
			if (_ssch.Connection == "" && _ssch.SN == "")
			{
				return false;
			}
			Connection = _ssch.Connection;
			if (Connection.Contains("COM"))
			{
				ComBoude = (_ssch.Is57600 ? 57600 : 4800);
			}
			SN = _ssch.SN;
			ActiveInterface = _ssch.Interrface;
			SType = _ssch.IDN;
			IsP100 = _ssch.IsP100;
			if (mKey == null)
			{
				mKey = new KeyConfig();
			}
			return true;
		}
	}
}
