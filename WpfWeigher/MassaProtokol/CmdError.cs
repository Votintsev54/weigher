﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassaProtokol
{
    public enum CmdErrorEnum
    {
        NoSupport = 0x07,   // Команда не поддерживается 
        Overload = 0x08,    // Нагрузка на весовом устройстве превышает НПВ 
        Error9 = 0x09,      // Весовое устройство не в режиме взвешивания 
        ErrorA = 0x0A,      // Ошибка входных данных 0x0B Ошибка сохранения данных 
        NoWiFi = 0x10,      // Интерфейс WiFi не поддерживается 
        NoEthernet = 0x11,  // Интерфейс Ethernet не поддерживается 
        NoZero = 0x15,      // Установка >0< невозможна  
        NoConnectModule = 0x17, // Нет связи с модулем взвешивающим 
        Error18 = 0x18,         // Установлена нагрузка на платформу при включении весового устройства 
        ModuleError = 0x19,     // Весовое устройство неисправно 
        UnknownError =   0xF0,  // Неизвестная ошибка
    }
    public class CmdError : MassaCommand
    {

        public CmdError(byte[] bytes)
            : base(bytes)
        {
            Valid &= Command == MassaCommandsEnum.CMD_ERROR && Length == 2;
            ExtractParams();
        }

        private void ExtractParams()
        {
        }

        /// <summary>
        /// Код ошибки:  0x17 – Нет связи с модулем взвешивающим 
        /// </summary>
        public int ErrorCode => _bytes[6];
        /// <summary>

    }
}
