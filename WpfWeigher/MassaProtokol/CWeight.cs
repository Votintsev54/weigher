﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassaProtokol
{
    public class CWeight
    {
        public int Weight;

        public int Division;

        public int Stable;

        public int Tare;

        public int TareDivision;

        public int Error;

        public bool IsNotError = true;

        public double GetDoubleWeight()
        {
            if (!IsNotError)
                return 0;
            switch (Division)
            {
                case 0:
                    return (double)Weight / 10000;
                case 1:
                    return (double)Weight / 1000;
                case 2:
                    return (double)Weight / 100;
                case 3:
                    return (double)Weight / 10;
                case 4:                    
                    return Weight;
                default:
                    return 0;
            }
        }

        public string GetWeight()
        {
            if (!IsNotError)
            {
                return "Ошибка!";
            }
            switch (Division)
            {
                case 0:
                    if (Stable > 0)
                    {
                        return $"{(double)Weight / 10000.0:0.0000} кг".Replace(',', '.');
                    }
                    return $"{(double)Weight / 10000.0:0.0000}   ".Replace(',', '.');
                case 1:
                    if (Stable > 0)
                    {
                        return $"{(double)Weight / 1000.0:0.000} кг".Replace(',', '.');
                    }
                    return $"{(double)Weight / 1000.0:0.000}    ".Replace(',', '.');
                case 2:
                    if (Stable > 0)
                    {
                        return $"{(double)Weight / 100.0:0.00} кг".Replace(',', '.');
                    }
                    return $"{(double)Weight / 100.0:0.00}    ".Replace(',', '.');
                case 3:
                    if (Stable > 0)
                    {
                        return $"{(double)Weight / 10.0:0.0} кг".Replace(',', '.');
                    }
                    return $"{(double)Weight / 10.0:0.0}    ".Replace(',', '.');
                case 4:
                    if (Stable > 0)
                    {
                        return $"{Weight} кг".Replace(',', '.');
                    }
                    return $"{Weight}    ".Replace(',', '.');
                default:
                    return "Ошибка!";
            }
        }

        public bool GetTare()
        {
            if (Tare > 0)
            {
                return true;
            }
            return false;
        }
    }
}
