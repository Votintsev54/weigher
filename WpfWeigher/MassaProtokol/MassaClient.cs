﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using DbCommon.Helpers;
using MassaProtokol;
using System.IO.Ports;
using System.Threading;

namespace Weigher.MassaProtokol
{
    public class MassaClient
    {
        public MassaClient()
        {
        }

        private Stream _st;

        public MassaClient(Stream st)
        {
            try
            {
                _st = st;
                st.ReadTimeout = 1000; // 1 sek
            }
            catch (Exception ex)
            {
                WriteLogError(ex, "");
                return;
            }
        }

        public MassaClient(Stream st, SerialPort mPort) : this(st)
        {
            this.mPort = mPort;
        }

        public async Task<CmdAskScalePar> ScaleParRequest()
        {
            var cmd = new MassaCommand(MassaCommandsEnum.CMD_GET_SCALE_PAR);
            var outbuf = cmd.GetBytes();
            Log.Info("-----" + string.Join(",", outbuf.Select(b => b.ToString("X2"))));
            await _st.WriteAsync(outbuf, 0, outbuf.Length);
            await _st.FlushAsync();
            Log.Info("await _st.FlushAsync();");
            MassaCommand cmdAsk = await ReadMassaCommand();
            Log.Info("await ReadMassaCommand();");
            if (cmdAsk.Command == MassaCommandsEnum.CMD_ACK_SCALE_PAR)
            {
                var cmdAskScalePar = new CmdAskScalePar(cmdAsk.GetBytes());
                return cmdAskScalePar; 
            }
            return null;

        }

        public async Task<int> WeightRequest()
        {
            //var cmd = new MassaCommand(MassaCommandsEnum.CMD_GET_MASSA);
            var cmd = new MassaCommand(MassaCommandsEnum.CMD_GET_UNDOC_A0);
            var outbuf = cmd.GetBytes();
            Log.Info("-----" + string.Join(",", outbuf.Select(b => b.ToString("X2"))));
            await _st.WriteAsync(outbuf, 0, outbuf.Length);
            await _st.FlushAsync();
            Log.Info("await _st.FlushAsync();");
            MassaCommand cmdAsk = await ReadMassaCommand();
            Log.Info("await ReadMassaCommand();");
            if (cmdAsk.Command == MassaCommandsEnum.CMD_ACK_MASSA)
            {
                var cmdAskMassa = new CmdAskMassa(cmdAsk.GetBytes());
                return cmdAskMassa.Weight;
            }
            return -1;
        }

        private const int MAX_CMD_LEN = 0x100;
        private const int MIN_CMD_LEN = 8;
        static readonly byte[] Header = { 0xF8, 0x55, 0xCE };
        private byte[] _buffer = new byte[2048];
        private int _bytesinbuf = 0;
        private int _cmdStart = -1;
        private SerialPort mPort;

        static int SearchHeader(byte[] buf, int length)
        {
            var limit = length - Header.Length;
            for (var i = 0; i <= limit; i++)
            {
                var k = 0;
                while (k < Header.Length && Header[k] == buf[i + k]) k++;
                if (k == Header.Length) return i;
            }
            return -1;
        }

        private async Task<MassaCommand> ReadMassaCommand()
        {
            while (true)
            {
                if (_cmdStart < 0 && _bytesinbuf > MAX_CMD_LEN)
                    _bytesinbuf = 0;
                Log.Info("await _st.ReadAsync(); started");
                var count = await _st.ReadAsync(_buffer, _bytesinbuf, _buffer.Length - _bytesinbuf);
                Log.Info("await _st.ReadAsync();--" + count);
                if (count == 0)
                    return null;
                _bytesinbuf += count;
                // ищем заголовок
                if (_cmdStart < 0)
                    _cmdStart = SearchHeader(_buffer, _bytesinbuf);
                if (_cmdStart >= 0 && _bytesinbuf >= _cmdStart + MIN_CMD_LEN)
                {
                    int len = BitConverter.ToInt16(_buffer, _cmdStart + 3);
                    if (_bytesinbuf >= _cmdStart + MIN_CMD_LEN - 1 + len)
                    {
                        var cmdbuf = new byte[len + 3 + 2 + 2]; // заголовок + поле длины +crc + длина команды
                        Array.Copy(_buffer, _cmdStart, cmdbuf, 0, cmdbuf.Length);
                        _bytesinbuf = 0;
                        _cmdStart = -1;
                        return new MassaCommand(cmdbuf);
                    }
                }
            }
        }

        public void StartWaitAskScalePar(IAsyncResult ar)
        {

        }


        public static bool WriteEvent(string sEntry)
        {
            string sAppName = "GPSService3";
            string sLog = "Application";
            EventLogEntryType eEventType = EventLogEntryType.Information;

            EventLog oEventLog = new EventLog();
            try
            {
                if (!EventLog.SourceExists(sAppName))
                {
                    EventLog.CreateEventSource(sAppName, sLog);
                }
                oEventLog.Source = sAppName;
                oEventLog.WriteEntry(sEntry, eEventType);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void WriteLogTrace(string message, int threadId)
        {
            // Log.Trace(message, thread_id);
        }

        private static void WriteLogTrace(string message)
        {
            Log.Trace(message);
        }

        private static void WriteLogError(Exception ex, string message)
        {
            Log.Error(ex, message);
        }

        internal object WeightRequest2()
        {
            var cmd = new MassaCommand(MassaCommandsEnum.CMD_GET_MASSA);
            var outbuf = cmd.GetBytes();
            Log.Info("-----" + string.Join(",", outbuf.Select(b => b.ToString("X2"))));
            mPort.Write(outbuf, 0, outbuf.Length);
            mPort.BaseStream.Flush();
            Log.Info("mPort.BaseStream.Flush();");
            Thread.Sleep(1000);
            var count = mPort.Read(_buffer, 0, _buffer.Length);
            Log.Info("-----" + string.Join(",", _buffer.Take(count).Select(b => b.ToString("X2"))));
            return -1;
        }
    }
}
