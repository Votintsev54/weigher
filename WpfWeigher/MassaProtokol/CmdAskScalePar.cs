﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassaProtokol
{
    public class CmdAskScalePar : MassaCommand
    {

        public CmdAskScalePar(byte[] bytes)
            : base(bytes)
        {
            Valid &= Command == MassaCommandsEnum.CMD_ACK_SCALE_PAR;
            ExtractParams();
        }

        private void ExtractParams()
        {
            //List<string> properties = new List<string>();
            //int start = 6;
            string allProp = BitConverter.ToString(_bytes, 6, Length);
            var properties = allProp.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            if (properties.Length >= 8)
            {
                Pmax = properties[0];
                Pmin = properties[1];
                Pe = properties[2];
                Pt = properties[3];
                Fix = properties[4];
                Calcode = properties[5];
                PoVer = properties[6];
                PoSumm = properties[7];
            }
        }

        /// <summary>
        /// Максимальная нагрузка, Max , в соответствии с п 7.1.1 ГОСТ OIML R76-1-2011, строка текста, оканчивающаяся разделителем 0x0D 0x0A,  например «Max 6/15 кг» 
        /// </summary>
        public string Pmax;
        /// <summary>
        /// Минимальная нагрузка, Min, в соответствии с п 7.1.1 ГОСТ OIML R76-1-2011, строка текста оканчивающаяся разделителем 0x0D 0x0A, например «Min 0,04 кг» char
        /// </summary>
        public string Pmin;
        /// <summary>
        /// Поверочный интервал весов, e, в соответствии с п 7.1.1 ГОСТ OIML R76-1-2011, строка текста оканчивающаяся разделителем 0x0D 0x0A, включающая в себя 
        /// поверочный интервал для всех диапазонов и единицу измерения, например «e = 2/5 г» 
        /// </summary>
        public string Pe;
        /// <summary>
        /// Максимальная масса тары, T, в соответствии с п 7.1.1 ГОСТ OIML R76-1-2011, строка текста, 
        /// оканчивающаяся разделителем 0x0D 0x0A, включающая в себя единицу измерения, например «T = - 6 кг» 
        /// </summary>
        public string Pt;
        /// <summary>
        /// Параметр фиксации веса, строка текста, оканчивающаяся разделителем 0x0D 0x0A, «Fix = 0» -- нет фиксации веса, «Fix = 1» -- есть фиксация веса (медицинский режим) 
        /// </summary>
        public string Fix;
        /// <summary>
        /// Код юстировки, строка текста, оканчивающаяся разделителем 0x0D 0x0A, например, «Code = 012345»  
        /// </summary>
        public string Calcode;
        /// <summary>
        /// Версия ПО датчика взвешивания, строка текста, оканчивающаяся разделителем 0x0D 0x0A 
        /// </summary>
        public string PoVer;
        /// <summary>
        /// Контрольная сумма ПО датчика взвешивания, строка текста, оканчивающаяся разделителем 0x0D 0x0A 
        /// </summary>
        public string PoSumm;

    }
}
