﻿using System;

namespace WpfWeigher.Models
{
    public class PostgreeSqlConnectionSettings : SqlConnectionSettings
    {
        public PostgreeSqlConnectionSettings()
        {
            ServerLocal = true;
            ServerName = "localhost";
            BdName = "Weigher";
            Login = "postgres";
            Password = "q1234567";
            ServerPort = 5432;
        }

        public override SqlConnectionSettings Clone()
        {
            var ret = new PostgreeSqlConnectionSettings();
            ret.ServerLocal = ServerLocal;
            ret.ServerName = ServerName;
            ret.BdName = BdName;
            ret.Login = Login;
            ret.Password = Password;
            ret.ConnectTimeout = ConnectTimeout;
            return ret;
        }

        bool _serverLocal;
        public override bool ServerLocal
        {
            get { return _serverLocal; }
            set
            {
                if (_serverLocal != value)
                {
                    _serverLocal = value;
                    if (value)
                        ServerName = "localhost";
                    OnPropertyChanged();
                }
            }
        }

        public int ServerPort { get; set; }

        public override bool Eq(object obj)
        {
            var cs = obj as PostgreeSqlConnectionSettings;
            if (cs == null) return false;
            if (_serverLocal != cs._serverLocal) return false;
            if (ServerName != cs.ServerName) return false;
            if (ServerPort != cs.ServerPort) return false;
            if (BdName != cs.BdName) return false;
            if (Login != cs.Login) return false;
            if (Password != cs.Password) return false;
            return true;
        }

        public override string GetConnectionString()
        {
            var connstring = string.Format("Server={0};Port={1};User Id={2};Password={3};Database={4};", ServerName, ServerPort, Login, Password, BdName);
            return connstring;
        }

        public override string GetMasterConnectionString(string login, string password)
        {
            if (string.IsNullOrEmpty(login)) login = "postgres";
            if (string.IsNullOrEmpty(password)) password = "postgres";
            var connstring = string.Format("Server={0};Port={1};User Id={2};Password={3};Database={4};", ServerName, ServerPort, login, password, "postgres");
            return connstring;
        }
    }
}