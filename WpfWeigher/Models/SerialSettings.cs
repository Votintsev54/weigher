﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace WpfWeigher.Models
{
   public class SerialSettings //class SettingsPort
    {
        public SerialSettings()
        {
        }

        public string ComPort { get; set; } = "COM0";

        public int BaudRate { get; set; } = 4800;

        public StopBits StopBits { get; set; } = StopBits.One;

        public Parity Parity { get; set; } = Parity.Even;

    }

}
