﻿using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;

namespace WpfWeigher.Models
{
    public class MsSqlConnectionSettings : SqlConnectionSettings
    {

        public MsSqlConnectionSettings() : base()
        {
            ServerLocal = true;
            ServerName = "(local)";
            BdName = "Weigher";
            Login = "sa";
            Password = "";
            ConnectTimeout = 60;
        }
        public override SqlConnectionSettings Clone()
        {
            var ret = new MsSqlConnectionSettings();
            ret.ServerLocal = ServerLocal;
            ret.Trusted = Trusted;
            ret.ServerName = ServerName;
            ret.BdName = BdName;
            ret.Login = Login;
            ret.Password = Password;
            ret.ConnectTimeout= ConnectTimeout;
            return ret;
        }

        bool _trusted = true;
        public bool Trusted
        {
            get { return _trusted; }
            set
            {
                if (_trusted != value)
                {
                    _trusted = value;
                    if (value)
                    {
                        Login = "";
                        Password = "";
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Login))
                            Login = "sa";
                    }
                    OnPropertyChanged();
                }
            }
        }

        bool _serverLocal = true;
        public override bool ServerLocal
        {
            get { return _serverLocal; }
            set
            {
                if (_serverLocal != value)
                {
                    _serverLocal = value;
                    if (value)
                        ServerName = "(local)";
                    OnPropertyChanged();
                }
            }
        }


        public override bool Eq(object obj)
        {
            var cs = obj as MsSqlConnectionSettings;
            if (cs == null) return false;
            if (_trusted != cs._trusted) return false;
            if (_serverLocal != cs._serverLocal) return false;
            if (!_serverLocal)
            {
                if (ServerName != cs.ServerName) return false;
            }
            if (BdName != cs.BdName) return false;
            if (!_trusted)
            {
                if (Login != cs.Login) return false;
                if (Password != cs.Password) return false;
            }
            return true;
        }

        public override string GetConnectionString()
        {
            // ReSharper disable once CollectionNeverQueried.Local
            var builder = new SqlConnectionStringBuilder
            {
                DataSource = ServerName,
                ConnectTimeout = ConnectTimeout,
                InitialCatalog = BdName
            };
            if (Trusted)
                builder.IntegratedSecurity = Trusted;
            else
            {
                builder.Password = Password;
                builder.UserID = Login;
            }
            return builder.ConnectionString;
        }

        public override string GetMasterConnectionString(string login, string password)
        {
            // ReSharper disable once CollectionNeverQueried.Local
            var builder = new SqlConnectionStringBuilder
            {
                DataSource = ServerName,
                ConnectTimeout = ConnectTimeout,
                InitialCatalog = "master"
            };
            if (string.IsNullOrEmpty(login))
                builder.IntegratedSecurity = Trusted;
            else
            {
                builder.Password = password;
                builder.UserID = login;
            }
            return builder.ConnectionString;
        }

    }
}