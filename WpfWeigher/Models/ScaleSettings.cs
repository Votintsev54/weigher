﻿using System;
using System.IO;
using System.Xml.Serialization;
using WpfWeigher.Common;
using DbCommon;
using DbCommon.Helpers;
using WeigherData;
using System.Net;

namespace WpfWeigher.Models
{
    public class ScaleSettings
    {
        public ScaleSettings() { }
        public ScaleProtokols Protokol { get; set; } = ScaleProtokols.Protokol2;
        public ScaleInterface ScaleInterface { get; set; } = ScaleInterface.Comport;
        public SerialSettings SerialSettings = new SerialSettings();
        public int Port = 5001;
        [XmlElement(ElementName = "Ip")]
        public string IPAddressAsString
        {
            get { return Ip?.ToString(); }
            set
            {
                IPAddress a;
                if (IPAddress.TryParse(value, out a))
                    Ip = a;
                else
                    Ip = IPAddress.None;
            }
        }
        [XmlIgnore]
        public IPAddress Ip { get; set; } = IPAddress.None;
    }
    public enum ScaleProtokols
    {
        Protokol2,
        Stndr,
        Protokol100,
        Protokol1c
    }
    public enum ScaleInterface
    {
        Comport,
        Ethernet
    }
}