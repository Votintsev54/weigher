﻿using DbCommon;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using WpfWeigher.Common;

namespace WpfWeigher.Models
{
    [XmlInclude(typeof(PostgreeSqlConnectionSettings))]
    [XmlInclude(typeof(MsSqlConnectionSettings))]
    public abstract class SqlConnectionSettings : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual bool ServerLocal { get; set; }
        public virtual string ServerName { get; set; }
        public virtual string BdName { get; set; }
        public virtual string Login { get; set; }
        public virtual string Password { get; set; }
        public virtual int ConnectTimeout { get; set; }

        public abstract bool Eq(object obj);

        public abstract string GetConnectionString();
        public abstract string GetMasterConnectionString(string login, string password);
        public abstract SqlConnectionSettings Clone();
    }
}