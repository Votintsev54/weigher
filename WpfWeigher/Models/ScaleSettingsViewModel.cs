﻿using System;
using DbCommon;
using System.Net;
using System.IO.Ports;
using System.Windows.Data;
using System.ComponentModel;
using System.Threading.Tasks;
using MassaProtokol;
using System.Collections.Generic;
using System.Windows.Threading;
using PropertyChanged;

namespace WpfWeigher.Models
{
    public class ScaleSettingsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ScaleSettingsViewModel()
        { }
        public ScaleSettingsViewModel(ScaleSettings scaleSettings)
        {
            Protokol = scaleSettings.Protokol;
            ScaleInterface = scaleSettings.ScaleInterface;
            Ip = scaleSettings.Ip;
            Port = scaleSettings.Port;
            ComPort = scaleSettings.SerialSettings?.ComPort ?? "";
            BaudRate = scaleSettings.SerialSettings?.BaudRate ?? 4800;
            StopBits = scaleSettings.SerialSettings?.StopBits ?? StopBits.One;
            Parity = scaleSettings.SerialSettings?.Parity ?? Parity.Even;
        }

        public ScaleSettings Models
        {
            get
            {
                ScaleSettings model = new ScaleSettings()
                {
                    Protokol = Protokol,
                    ScaleInterface = ScaleInterface,
                    Ip = Ip,
                    Port = Port,
                };
                if (ScaleInterface == ScaleInterface.Comport)
                {
                    model.SerialSettings = new SerialSettings();
                    model.SerialSettings.ComPort = ComPort;
                    model.SerialSettings.BaudRate = BaudRate;
                    model.SerialSettings.StopBits = StopBits;
                    model.SerialSettings.Parity = Parity;
                }
                return model;
            }
        }

        public bool IsBusy { get; set; }
        internal void SelectSale(ScaleData sd)
        {
            Protokol = sd.IsP100 ? ScaleProtokols.Protokol100 : ScaleProtokols.Protokol1c;
            ScaleInterface = sd.Connection.Contains("COM") ? ScaleInterface.Comport : ScaleInterface.Ethernet;
            if (ScaleInterface == ScaleInterface.Ethernet)
            {
                Port = 5001;
                Ip = IPAddress.None;
                if (SplitAddress(sd.Connection, out string ipstr, out string portstr))
                {
                    if (IPAddress.TryParse(ipstr, out var ipAddress) && int.TryParse(portstr, out var ipPort))
                    {
                        Ip = ipAddress;
                        Port = ipPort;
                    }
                }
            }
            else
            {
                ComPort = sd.Connection;
                BaudRate = sd.Is57600 ? 57600 : 4800;
            }
        }

        private bool SplitAddress(string connection, out string ipstr, out string portstr)
        {
            int dtindex = connection.LastIndexOf(':');
            if (dtindex < 0)
            {
                ipstr = string.Empty;
                portstr = string.Empty;
                return false;
            }
            ipstr = connection.Substring(0, dtindex);
            portstr = connection.Substring(dtindex + 1);
            return true;
        }
        
        public ScaleProtokols Protokol { get; set; }
        public void OnProtokolChanged()
        {
            switch (Protokol)
            {
                case ScaleProtokols.Protokol2:
                    //return "4800;1 стартовый;8 бит данных;1 бит контроля по четности;1 стоповый бит.";
                    BaudRate = 4800;
                    StopBits = StopBits.One;
                    Parity = Parity.Even;
                    break;
                case ScaleProtokols.Stndr:
                    //return "19200;1 стартовый;8 бит данных;бит 0;1 стоповый бит.";
                    BaudRate = 19200;
                    StopBits = StopBits.One;
                    Parity = Parity.None;
                    break;
                case ScaleProtokols.Protokol100:
                    BaudRate = 57600;
                    StopBits = StopBits.One;
                    Parity = Parity.Even;
                    break;
                case ScaleProtokols.Protokol1c:
                    BaudRate = 57600;
                    StopBits = StopBits.One;
                    Parity = Parity.Even;
                    break;
            }

        }

        [DependsOn("Protokol")]
        public string Description
        {
            get
            {
                switch (Protokol)
                {
                    case ScaleProtokols.Protokol2:
                        return "4800;1 стартовый;8 бит данных;1 бит контроля по четности;1 стоповый бит.";
                    case ScaleProtokols.Stndr:
                        return "19200;1 стартовый;8 бит данных;бит 0;1 стоповый бит.";
                    case ScaleProtokols.Protokol100:
                        return  "";
                    case ScaleProtokols.Protokol1c:
                        return  "";
                }
                return "";
            }
        }
        public bool InterfaceEnable => (Protokol == ScaleProtokols.Protokol100 || Protokol == ScaleProtokols.Protokol1c);
        public ScaleInterface ScaleInterface { get; set; }
        public bool IpEnable => (Protokol == ScaleProtokols.Protokol100 || Protokol == ScaleProtokols.Protokol1c)
            && ScaleInterface == ScaleInterface.Ethernet;
        public IPAddress Ip { get; set; } = IPAddress.None;
        public int Port { get; set; } = 5001;

        //SerialSettings SerialSettings = new SerialSettings("COM2", ScaleProtokols.Protokol2);
        public bool ComPortEnable => (Protokol == ScaleProtokols.Protokol2 || Protokol == ScaleProtokols.Stndr)
            || ScaleInterface == ScaleInterface.Comport;
        public string ComPort { get; set; } = "";
        public int BaudRate { get; set; } = 4800;

        public static int[] BaudRates => new Int32[] { 4800, 9600, 14400, 19200, 38400, 56000, 57600, 115200, 128000, 256000 };
        public StopBits StopBits { get; set; } = StopBits.One;
        public static StopBits[] StopBitsItems => new StopBits[] { StopBits.One, StopBits.OnePointFive, StopBits.Two };
        public Parity Parity { get; set; } = Parity.Even;
        public static Parity[] ParityItems => new Parity[] { Parity.None, Parity.Odd, Parity.Even, Parity.Mark, Parity.Space };
        public List<ScaleData> ScaleDataList { get; set; }

        public Task btnFind(System.Windows.Threading.Dispatcher dispatcher)
        {
            IsBusy = true;
            Task.Run(() =>
            {
                var searchList = new ScaleSearchList();
                var scaleDataList = searchList.GetScales();
                dispatcher.Invoke(() =>
                 {
                     ScaleDataList = scaleDataList;
                     IsBusy = false;
                 });
            });
            return Task.FromResult(0);
        }


    }
    public class RbcConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.Equals(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.Equals(true) ? parameter : Binding.DoNothing;
        }
    }

    public class TestConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.Equals(true) ? parameter : Binding.DoNothing;
        }
    }
}