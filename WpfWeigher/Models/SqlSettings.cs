﻿using System;
using System.IO;
using System.Xml.Serialization;
using WpfWeigher.Common;
using DbCommon;
using DbCommon.Helpers;
using WeigherData;

namespace WpfWeigher.Models
{
    public class SqlSettings : ViewModelBase
    {
        private DatabaseType _databaseType = DatabaseType.PostgreeSql;
        private SqlConnectionSettings _sqlConnectionSettings = new PostgreeSqlConnectionSettings();

        //private string _deviceIp = "10.0.6.10";
        // public string DeviceIp
        //{
        //    get { return _deviceIp; }
        //    set { SetProperty(ref _deviceIp, value); }
        //}

        //private int _devicePort = 502;
        //public int DevicePort
        //{
        //    get { return _devicePort; }
        //    set { SetProperty(ref _devicePort, value); }
        //}

        public SqlSettings Clone()
        {
            SqlSettings ret = new SqlSettings();
            ret._databaseType = _databaseType;
            ret._sqlConnectionSettings = _sqlConnectionSettings.Clone();
            return ret;
        }

        public bool Eq(object obj)
        {
            var cp = obj as SqlSettings;
            return cp != null
                   && (_databaseType == cp._databaseType
                       //&& (_deviceIp == cp._deviceIp
                         //  && (_devicePort == cp._devicePort
                               && _sqlConnectionSettings.Eq(cp._sqlConnectionSettings //))
                               ));
        }


        public DbCommon.DatabaseType DatabaseType
        {
            get { return _databaseType; }
            set
            {
                if (SetProperty(ref _databaseType, value))
                {
                    switch (value)
                    {
                        case DatabaseType.MsSql:
                            SqlConnectionSettings = new MsSqlConnectionSettings();
                            break;
                        case DatabaseType.PostgreeSql:
                            SqlConnectionSettings = new PostgreeSqlConnectionSettings();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException("value", value, null);
                    }
                }
            }
        }

        public SqlConnectionSettings SqlConnectionSettings
        {
            get { return _sqlConnectionSettings; }
            set { SetProperty(ref _sqlConnectionSettings, value); }
        }

        public CDatabaseWeigher GetDatabase()
        {
            CDatabaseWeigher ret = new CDatabaseWeigher(DatabaseType, SqlConnectionSettings.GetConnectionString());
            try
            {
                ret.ExecCmd("select 1");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "GetDatabase test");
                return null;
            }
            return ret;
        }

        public void Restore(SqlSettings prevCpSettings)
        {
            _databaseType = prevCpSettings._databaseType;
            _sqlConnectionSettings = prevCpSettings._sqlConnectionSettings.Clone();
        }

        private SqlSettings _sqlSettingsSource;
        public SqlSettings SqlSettingsSource
        {
            get => _sqlSettingsSource;
            set => SetProperty(ref _sqlSettingsSource, value);
        }

    }
}