﻿using System;
using System.IO;
using System.Windows.Controls;

namespace WpfWeigher.ValidationRules
{
    public class PortRule : ValidationRule
    {
        public static PortRule Instance = new PortRule();

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (!(value is string))
                return new ValidationResult(false, "некорректное значение");
            var port = 0;
            if (int.TryParse((string)value, out port))
            {

                if (port <= 0)
                    return new ValidationResult(false, "должен быль > 0");
                if (port != 502 && port < 1024)
                    return new ValidationResult(false, "должен быль > 1023 или 502");
                if (port > 65535)
                    return new ValidationResult(false, "должен быль < 65536");
            }
            else
            {
                return new ValidationResult(false, "должен быль целым числом");
            }
            return new ValidationResult(true, null);
        }
    }
}