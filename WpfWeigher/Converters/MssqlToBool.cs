﻿using System.Windows.Data;
using DbCommon;

namespace WpfWeigher.Converters
{
    [ValueConversion(typeof(DatabaseType), typeof(bool))]
    public class MssqlToBool : IValueConverter
    {

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return (DatabaseType) value == DatabaseType.MsSql;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return (bool) value ? DatabaseType.MsSql : DatabaseType.PostgreeSql;
        }
    }

    [ValueConversion(typeof(DatabaseType), typeof(bool))]
    public class PostgreeToBool : IValueConverter
    {

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return (DatabaseType)value == DatabaseType.PostgreeSql;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return (bool)value ? DatabaseType.PostgreeSql : DatabaseType.MsSql;
        }
    }
}