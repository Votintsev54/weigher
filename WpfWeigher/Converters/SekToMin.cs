﻿using System.Windows.Data;

namespace WpfWeigher.Converters
{
    [ValueConversion(typeof(int), typeof(string))]
    public class SekToMin : IValueConverter
    {

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            int ival = (int)value;
            var min = ival / 60;
            var sek = ival % 60;
            return string.Format("{0:D2}:{1:D2}",min,sek);
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return 0;
        }

        public static SekToMin Instance = new SekToMin();
    }
}