﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Data;

namespace WpfWeigher.Converters
{
    [ValueConversion(typeof(bool), typeof(bool))]
    public class BoolInv: IValueConverter
    {

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return !(bool) value;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return !(bool)value;
        }

        public static BoolInv Instance = new BoolInv();
    }
}