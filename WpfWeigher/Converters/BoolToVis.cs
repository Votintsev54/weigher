﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Data;

namespace WpfWeigher.Converters
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVis : IValueConverter
    {

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return (bool) value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return Visibility.Visible == (Visibility) value;
        }
    }

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisInv : IValueConverter
    {

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return (bool)value ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // ReSharper disable once PossibleNullReferenceException
            return Visibility.Visible != (Visibility)value;
        }
    }
}