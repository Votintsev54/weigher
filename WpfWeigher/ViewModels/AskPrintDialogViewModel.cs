﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;


namespace WpfWeigher.ViewModels
{
    public class AskPrintDialogViewModel : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public AskPrintDialogViewModel() { }
        public int CheckCopies { get; set; } = 1;
        public bool CheckNoAsk { get; set; } = true;
        public bool CheckFirstAsk { get; set; } = true;
        public bool CheckPreview { get; set; } = false;

        public bool AskForPrint() => !CheckNoAsk || CheckFirstAsk;
        public object Clone() => MemberwiseClone();
    }
}
