﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfWeigher.Views
{
    /// <summary>
    /// Логика взаимодействия для AskPrintDialog.xaml
    /// </summary>
    public partial class AskPrintDialog : Window
    {
        public AskPrintDialog(ViewModels.AskPrintDialogViewModel askPrintDialogViewModel)
        {
            InitializeComponent();
            cbCopies.ItemsSource = new[] { 1, 2, 3, 4 };
            DataContext = askPrintDialogViewModel.Clone();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
