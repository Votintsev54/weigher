﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeigherData;

namespace WpfWeigher.Views
{
    /// <summary>
    /// Логика взаимодействия для ColumnsMapWindow.xaml
    /// </summary>
    public partial class ColumnsMapWindow : Window
    {
        private CDatabaseWeigher _db;
        public ColumnsMapWindow(CDatabaseWeigher db)
        {
            _db = db;
            InitializeComponent();
            DataContext =_db.ColumnsMap.GetAll();
        }

        private void ColumnsMapWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
