﻿using MassaProtokol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfWeigher.Models;

namespace WpfWeigher.Views
{
    /// <summary>
    /// Логика взаимодействия для ScaleConnection.xaml
    /// </summary>
    public partial class ScaleConnection : Window
    {
        private IParentScaleSettings _parentScaleSettings;

        public ScaleSettingsViewModel _vm;
        public ScaleConnection()
        {
            InitializeComponent();
        }

        public ScaleConnection(IParentScaleSettings parentScaleSettings)
        {
            InitializeComponent();
            _parentScaleSettings = parentScaleSettings;
            _vm = new ScaleSettingsViewModel(_parentScaleSettings.ScaleSettings);
            DataContext = _vm;
        }

        private void ThisWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            _parentScaleSettings.ScaleSettings = _vm.Models;
            _parentScaleSettings.Save();
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnFind_Click(object sender, RoutedEventArgs e)
        {
            _vm.btnFind(this.Dispatcher);
        }

        private void SelectScale_Click(object sender, RoutedEventArgs e)
        {
            ScaleData sd = (sender as FrameworkElement)?.DataContext as ScaleData;
            if (sd != null)
                _vm.SelectSale(sd);
        }
    }
}
