﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WpfWeigher.Models;
using DbCommon;
using DbCommon.Helpers;
using WeigherData;
using System.Runtime.CompilerServices;
using System.Windows.Media;
// ReSharper disable PossibleNullReferenceException

namespace WpfWeigher.Views
{
    /// <summary>
    /// Логика взаимодействия для SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window, INotifyPropertyChanged
    {
        private int _mode = 0;
        public SettingsWindow(IParentSqlSettings parentSettings, int mode = 0)
        {
            InitializeComponent();
            _parentSettings = parentSettings;
            _mode = mode;
            if (mode > 0)
            {
                Background = Brushes.LightGray;
                Title = "Источник копирования";
                WindowStartupLocation = WindowStartupLocation.Manual;
                CreateDb.Visibility = Visibility.Collapsed;
                CopyDb.Visibility = Visibility.Collapsed;
                CopyStart.Visibility = Visibility.Visible;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T field, T value, [CallerMemberName]string name = null)
        {
            if (Equals(field, value))
                return false;
            if (value is IComparable comparable && comparable.CompareTo(field) == 0)
                return false;
            field = value;
            OnPropertyChanged(name);
            return true;
        }


        private SqlSettings _model;

        private bool _modified;
        public bool Modified
        {
            get => _modified;
            set => SetProperty(ref _modified, value);
        }

        private IParentSqlSettings _parentSettings;

        private void SettingsWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            ReloadData();
        }

        private void ReloadData()
        {
            if (_model != null)
            {
                _model.PropertyChanged -= Vm_PropertyChanged;
                _model.SqlConnectionSettings.PropertyChanged -= Vm_PropertyChanged;
                _model.PropertyChanged -= Vm_PropertyChanged;
            }
            if (_mode == 0)
                _model = _parentSettings.SqlSettings?.Clone() ?? new SqlSettings();
            else
                _model = _parentSettings.SqlSettings.SqlSettingsSource?.Clone() ?? new SqlSettings();
            this.DataContext = _model;
            _model.PropertyChanged += Vm_PropertyChanged;
            _model.SqlConnectionSettings.PropertyChanged += Vm_PropertyChanged;
            Modified = false;
        }

        void Vm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Modified = _parentSettings.SqlSettings == null || !_parentSettings.SqlSettings.Eq(_model);
            if (e.PropertyName == nameof(SqlSettings.SqlConnectionSettings))
                _model.SqlConnectionSettings.PropertyChanged += Vm_PropertyChanged;
        }

        private void Clear_OnClick(object sender, RoutedEventArgs e)
        {
            ReloadData();
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == 0)
            {
                if (_parentSettings.SqlSettings == null)
                    _parentSettings.SqlSettings = _model;
                else
                    _parentSettings.SqlSettings.Restore(_model);
            }
            else
            {
                if (_parentSettings.SqlSettings.SqlSettingsSource == null)
                    _parentSettings.SqlSettings.SqlSettingsSource = _model;
                else
                    _parentSettings.SqlSettings.SqlSettingsSource.Restore(_model);
            }
            _parentSettings.Save();
            ReloadData();
        }

        private void CreteDb_OnClick(object sender, RoutedEventArgs e)
        {
            Task.Run(() => CreateDatabase());
        }

        private void CreateDatabase()
        {
            AskAdminPwdDialog dialog = null;
            bool? showDialog = new bool?();
            var vm = _model.Clone();
            Dispatcher.Invoke(() =>
            {
                switch (vm.DatabaseType)
                {
                    case DatabaseType.MsSql:
                        dialog = new AskAdminPwdDialog { DataStruct = { Login = "sa", Password = "" } };
                        break;
                    case DatabaseType.PostgreeSql:
                        dialog = new AskAdminPwdDialog { DataStruct = { Login = "postgres", Password = "q1234567" } };
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                dialog.Owner = this;
                showDialog = dialog.ShowDialog();
            });
            if (showDialog == null || !showDialog.Value) return;

            Dispatcher.Invoke(() => { this.Cursor = Cursors.Wait; });

            CDatabaseWeigher db = new CDatabaseWeigher(vm.DatabaseType
                , vm.SqlConnectionSettings.GetMasterConnectionString(dialog.DataStruct.Login, dialog.DataStruct.Password));
        LabelCreateDatabase:
            try
            {
                db.CreateDatabase(vm.SqlConnectionSettings.BdName);
            }
            catch (Exception ex)
            {
                var exception = ex as SqlException;
                bool dbExists = exception != null ? exception.Number == 1801 : ex.Message.Contains("42P04");

                if (dbExists)
                {
                    MessageBoxResult res = MessageBoxResult.None;
                    Dispatcher.Invoke(() =>
                    {
                        res = MessageBox.Show(
                            "База данных с таким именем уже существует! пересоздать?", "внимание вопрос",
                            MessageBoxButton.YesNo);
                    });
                    if (res == MessageBoxResult.No)
                        return;
                    if (res == MessageBoxResult.Yes)
                    {

                        db.DeleteDatabase(vm.SqlConnectionSettings.BdName);
                        goto LabelCreateDatabase;
                    }
                }
                Log.Error(ex, "CreateDatabase");
                ShowError(ex);
                Dispatcher.Invoke(() => { this.Cursor = Cursors.Arrow; });
                return;
            }
            try
            {
                db = new CDatabaseWeigher(vm.DatabaseType, vm.SqlConnectionSettings.GetConnectionString());
                db.CreateShema();
                db.ClearAllPools();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "CreateDatabase CreateShema");
                ShowError(ex);
            }
            Dispatcher.Invoke(() => { this.Cursor = Cursors.Arrow; });
        }

        private void ShowError(Exception ex)
        {
            Dispatcher.Invoke(() =>
            {
                MessageBox.Show(ex.Message, "Ошибка при создании БД", MessageBoxButton.OK,
                    MessageBoxImage.Asterisk);
            });
        }

        private void PasswordBox_OnPostgreePasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox pb = (PasswordBox)sender;
            ((PostgreeSqlConnectionSettings)_model.SqlConnectionSettings).Password = pb.Password;
        }

        private void PasswordBox_OnMsSqlPasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox pb = (PasswordBox)sender;
            ((MsSqlConnectionSettings)_model.SqlConnectionSettings).Password = pb.Password;
        }

        private void MsSqlPasswordBoxt_OnLoaded(object sender, RoutedEventArgs e)
        {
            PasswordBox pb = (PasswordBox)sender;
            MsSqlConnectionSettings ps = _model.SqlConnectionSettings as MsSqlConnectionSettings;
            if (ps != null)
                pb.Password = ps.Password;
        }

        private void PostgreePasswordBox_OnLoaded(object sender, RoutedEventArgs e)
        {
            PasswordBox pb = (PasswordBox)sender;
            PostgreeSqlConnectionSettings ps = _model.SqlConnectionSettings as PostgreeSqlConnectionSettings;
            if (ps != null)
                pb.Password = ps.Password;
        }

        private void CopyDb_OnClick(object sender, RoutedEventArgs e)
        {
            var sw = new SettingsWindow(_parentSettings, 1) { Left = Left - Width, Top = Top };
            sw.ShowDialog();
        }
        private void CopyStart_OnClick(object sender, RoutedEventArgs e)
        {
            Save_OnClick(null, null);
            var sourceDb = _parentSettings.SqlSettings.SqlSettingsSource.GetDatabase();
            var destinationDb = _parentSettings.SqlSettings.GetDatabase();

            if (sourceDb == null)
            {
                MessageBox.Show("Нет соединения с базой - источником", "ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (sourceDb == null)
            {
                MessageBox.Show("Нет соединения с базой - получателем", "ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ButtonPanel.Visibility = Visibility.Collapsed;
            Progress.Visibility = Visibility.Visible;


            Task.Run(() => destinationDb.CopyFrom(sourceDb
                , (percent) => { Dispatcher.Invoke(() => Progress.Value = percent); }))
                .ContinueWith((_) =>
            {
                Dispatcher.Invoke(() =>
                {
                    Close();
                    MessageBox.Show("Копирование закончено", "Завершение", MessageBoxButton.OK, MessageBoxImage.Information);
                });
            });
        }


    }
}
