﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeigherData;

namespace WpfWeigher.Views
{
    /// <summary>
    /// Логика взаимодействия для TermEditor.xaml
    /// </summary>
    public partial class TermEditor : Window
    {
        private CDatabaseWeigher db;

        public TermEditor()
        {
            InitializeComponent();
        }

        public TermEditor(CDatabaseWeigher db) : this()
        {
            this.db = db;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
