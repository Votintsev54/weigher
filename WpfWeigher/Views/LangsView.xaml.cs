﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfWeigher.Views
{
    /// <summary>
    /// Логика взаимодействия для LangsView.xaml
    /// </summary>
    public partial class LangsView : UserControl
    {
        private LangVm langVm;

        public LangsView()
        {
            InitializeComponent();
        }

        public LangsView(LangVm langVm)
        {
            this.langVm = langVm;
        }
    }
}
